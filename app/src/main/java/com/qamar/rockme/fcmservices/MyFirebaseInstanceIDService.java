package com.qamar.rockme.fcmservices;

/**
 * Created by Development on 6/8/2017.
 */

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.qamar.rockme.R;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.helper.SharedPreference;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService implements ServiceResponse {
    private ModelParsedResponse modelParsedResponse = null;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String registrationId = FirebaseInstanceId.getInstance().getToken();
        Log.i("FcmInstanceIDListener", "Found Registration Id:" + registrationId);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
//        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
//        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
//        registrationComplete.putExtra("token", refreshedToken);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e("Send Token", "sendRegistrationToServer: " + token);
        FrontEngine.getInstance().deviceToken = token;
        if (SharedPreference.getInstance(this).getBooleanValue(SharedPreference.getInstance(this).check_login) == true) {
            requestHttpCall(1, getPostParameters());
        }
    }

    //get parameters start
    private String getPostParameters() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("FirebaseToken", "");
            jsonObject.put("DeviceType", "android");
            jsonObject.put("Token", "" + FrontEngine.getInstance().deviceToken);
            jsonObject.put("Udid", Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

//    private void storeRegIdInPref(String token) {
//        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putString("regId", token);
//        editor.commit();
//    }

    @Override
    public void onResult(int type, HttpResponse o) {
//        SharedPreference.getInstance(this).
//                saveValueInSharedPreference(SharedPreference.getInstance(this).device_token, FrontEngine.getInstance().deviceToken);
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                if (modelParsedResponse != null) {

                    if (modelParsedResponse.getMessages() != null) {
                        //objGlobalHelperNormal.callDialog(LoginActivity.this, getString(R.string.alert), modelParsedResponse.getMessages());
                    } else {
                        //customDialogSomethingWent();
                    }

                } else {
                    //customDialogSomethingWent();
                }


            } catch (JsonParseException exp) {
                //customDialogSomethingWent();
            }


        } else {
            modelParsedResponse = null;
            //customDialogSomethingWent();
        }


    }

    @Override
    public void requestHttpCall(final int type, final String... params) {
        CheckInternet.getInstance().internetCheckTask(this, new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                    HashMap map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.content_type),
                            getString(R.string.app_json), getString(R.string.authorization),
                            SharedPreference.getInstance(MyFirebaseInstanceIDService.this).
                                    getStringValue(MyFirebaseInstanceIDService.this.getString(R.string.sp_accesstoken))});

                    FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                            map,
                            getString(R.string.empty),
                            jsonObject,
                            new CallBackRetrofit(type, MyFirebaseInstanceIDService.this
                            ));
                }
            }
        });
    }
}
