package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.Like;

import java.util.ArrayList;

public class LikesAdapter extends RecyclerView.Adapter<LikesAdapter.MyViewHolder> {

    private Context context;
    private boolean isLikes;
    private ArrayList<Artwork> artworks;
    private ArrayList<Like> likes;

    public LikesAdapter(Context context,ArrayList<Artwork> artworks, boolean isLikes) {
        this.context = context;
        this.artworks = artworks;
        this.isLikes = isLikes;
    }

    public LikesAdapter(Context context, ArrayList<Like> likes) {
        this.context = context;
        this.likes = likes;
        isLikes = true;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_likes,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
     if(isLikes)
     {
         holder.tvLikes.setVisibility(View.VISIBLE);
         {
             Like like = likes.get(position);
             holder.tvArtWork.setText(like.getArtworkName());
             holder.tvLikes.setText(like.getLikes());
             Glide.with(context).load(like.getArtworkPicture()).into(holder.ivPic);
         }

     }

     else {
        Artwork artwork = artworks.get(position);
         holder.tvLikes.setVisibility(View.GONE);
         holder.tvArtWork.setText(artwork.getArtworkName());

         Glide.with(context).load(artwork.getArtworkImage()).into(holder.ivPic);


     }

    }

    @Override
    public int getItemCount() {
        if(isLikes) return likes.size();
        else
            return artworks.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvLikes,tvArtWork;
        ImageView ivPic;
        CardView card_view;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvLikes = itemView.findViewById(R.id.tvLikes);
            tvArtWork = itemView.findViewById(R.id.tvArtWork);
            ivPic = itemView.findViewById(R.id.ivPic);
            card_view = itemView.findViewById(R.id.card_view);
        }
    }
}
