package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.models.Wallet;

import java.util.ArrayList;

public class MyWalletAdapter extends RecyclerView.Adapter<MyWalletAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Wallet> wallets;


    public MyWalletAdapter(Context context, ArrayList<Wallet> wallets) {
        this.context = context;
        this.wallets = wallets;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_wallet,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
     Wallet wallet = wallets.get(position);
     holder.tvTitle.setText(wallet.getHistory());
     holder.tvTimeAgo.setText(wallet.getActionDate());
     holder.tvMoney.setText(wallet.getAmount()+" Coin");
    }

    @Override
    public int getItemCount() {
        return wallets.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitle,tvTimeAgo,tvMoney;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvFeedText);
            tvTimeAgo = itemView.findViewById(R.id.tvTimeAgo);
            tvMoney = itemView.findViewById(R.id.tvMoney);
        }
    }
}
