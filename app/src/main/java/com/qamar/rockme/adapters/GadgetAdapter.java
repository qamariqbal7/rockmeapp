package com.qamar.rockme.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.MyGadgetDetailFragment;
import com.qamar.rockme.fragments.MyGadgetsFragment;
import com.qamar.rockme.models.MyGadgets;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class GadgetAdapter extends RecyclerView.Adapter<GadgetAdapter.ViewHolder>
        implements View.OnClickListener, View.OnTouchListener {

    private ArrayList<MyGadgets> arrayListGedget = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private MyGadgets mMyGadgets;
    private MyGadgetsFragment mMyGadgetsFragment;
    private String labelGadgets;
    private DatabaseReference mDatabase;

    // data is passed into the constructor
    public GadgetAdapter(Context context, ArrayList<MyGadgets> arrayListGedget,
                         MyGadgetsFragment mMyGadgetsFragment, String labelGadgets) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListGedget = arrayListGedget;
        this.context = context;
        this.mMyGadgetsFragment = mMyGadgetsFragment;
        this.labelGadgets = labelGadgets;
    }

    public GadgetAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;

    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.gadget_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mMyGadgets = arrayListGedget.get(position);

        if (labelGadgets.equals("Saved Gadgets")) {
            holder.tvGadgetName.setText(mMyGadgets.getUserGadgetName());
        } else if (labelGadgets.equals("Shared Gadgets")) {
            holder.tvGadgetName.setText(mMyGadgets.getGadgetName());
        }
        holder.tvGadgetMacAddress.setText(mMyGadgets.getMacAddress());

        holder.llGadget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mMyGadgetsFragment.isPreview) {
                    if (labelGadgets.equals("Saved Gadgets")) {
                        Fragment fragment = new MyGadgetDetailFragment();
                        Bundle args = new Bundle();
                        args.putSerializable(context.getResources().getString(R.string.gadget_object), arrayListGedget.get(position));
                        args.putBoolean(context.getResources().getString(R.string.gadget_check_add), false);
                        args.putSerializable(context.getString(R.string.refrence), mMyGadgetsFragment);
                        fragment.setArguments(args);

                        ((HomeActivity) context).replaceFragment2(fragment, (HomeActivity) context);
                    }
                } else {
                    mDatabase = FirebaseDatabase.getInstance().getReference();
                    mDatabase.child(arrayListGedget.get(position).getMacAddress()).child("Command").
                            setValue(mMyGadgetsFragment.gadgetCommond);
                }
            }
        });

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return arrayListGedget.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvGadgetName, tvGadgetMacAddress;
        private LinearLayout llGadget;

        public ViewHolder(View itemView) {
            super(itemView);

            tvGadgetName = itemView.findViewById(R.id.tvGadgetName);
            tvGadgetMacAddress = itemView.findViewById(R.id.tvGadgetMacAddress);

            llGadget = itemView.findViewById(R.id.llGadget);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public MyGadgets getItem(int id) {
        return this.arrayListGedget.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
