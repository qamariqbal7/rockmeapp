package com.qamar.rockme.adapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.ProfileOtherFragment;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class AddContactsAdapter extends RecyclerView.Adapter<AddContactsAdapter.ViewHolder>
        implements View.OnClickListener, View.OnTouchListener, ServiceResponse {

    private ArrayList<UserData> arrayListContacts = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private UserData mUserData;

    private Dialog dialogContact;
    private ImageView ivProfilePic;
    private ProgressBar pbProfilePic;
    private TextView tvAddInvite, tvViewProfile, tvFriends, tvProfileName;
    private int currentPosition;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;

    // data is passed into the constructor
    public AddContactsAdapter(Context context, ArrayList<UserData> arrayListContacts, KProgressHUD mKProgressHUD) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListContacts = arrayListContacts;
        this.context = context;
        this.mKProgressHUD = mKProgressHUD;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.add_contacts_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mUserData = arrayListContacts.get(position);
        if(mUserData.getProfilePicture() != null) {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivContactsPic
                    ,mUserData.getProfilePicture(),
                    holder.pbContacts, R.drawable.profile);
        } else {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivContactsPic
                    , "",
                    holder.pbContacts, R.drawable.profile);
        }

        holder.tvContactsName.setText(mUserData.getUserName());

        holder.llContactsbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPosition = position;
                dialogContact = ((HomeActivity) context).objGlobalHelperNormal.
                        showDialog(R.layout.dialog_contact, context,
                                true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationFade);
                initAndSetListeners();
                setData();
            }
        });

    }

    private void initAndSetListeners() {
        ivProfilePic = dialogContact.findViewById(R.id.ivProfilePic);
        pbProfilePic = dialogContact.findViewById(R.id.pbProfilePic);

        tvAddInvite = dialogContact.findViewById(R.id.tvAddInvite);
        tvViewProfile = dialogContact.findViewById(R.id.tvViewProfile);
        tvFriends = dialogContact.findViewById(R.id.tvFriends);
        tvProfileName = dialogContact.findViewById(R.id.tvProfileName);

        tvAddInvite.setOnTouchListener(this);
        tvAddInvite.setOnClickListener(this);
        tvViewProfile.setOnTouchListener(this);
        tvViewProfile.setOnClickListener(this);
    }

    private void setData () {
        if(arrayListContacts.get(currentPosition).getProfilePicture() != null) {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, ivProfilePic
                    ,arrayListContacts.get(currentPosition).getProfilePicture(),
                    pbProfilePic, R.drawable.profile);
        } else {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, ivProfilePic
                    , "",
                    pbProfilePic, R.drawable.profile);
        }
        tvAddInvite.setText(context.getString(R.string.ADD_CONTACT));
        tvViewProfile.setText(context.getString(R.string.VIEW_PROFILE));
        tvFriends.setText(arrayListContacts.get(currentPosition).getEmailAddress());
        tvProfileName.setText(arrayListContacts.get(currentPosition).getUserName());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvAddInvite:
                dialogContact.dismiss();
                if (arrayListContacts.get(currentPosition).getHasAdded() == 0) {
                    mKProgressHUD = KProgressHUD.create(context)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(context.getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(1, "");
                } else {
                    ((HomeActivity) context).objGlobalHelperNormal.callDialog((HomeActivity) context,
                            context.getString(R.string.alert), context.getString(R.string.alreadyAddProfile));
                }
                break;

            case R.id.tvViewProfile:
                dialogContact.dismiss();

                Fragment fragment = new ProfileOtherFragment();
                Bundle args = new Bundle();
                args.putInt(context.getResources().getString(R.string.user_id), arrayListContacts.get(currentPosition).getUserId());
                fragment.setArguments(args);

                ((HomeActivity) context).replaceFragment(fragment, (HomeActivity) context);
                break;
        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListContacts.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivContactsPic;
        private ProgressBar pbContacts;
        private TextView tvContactsName;
        private LinearLayout llContactsbg;

        public ViewHolder(View itemView) {
            super(itemView);

            ivContactsPic = itemView.findViewById(R.id.ivContactsPic);
            pbContacts = itemView.findViewById(R.id.pbContacts);
            tvContactsName = itemView.findViewById(R.id.tvContactsName);
            llContactsbg = itemView.findViewById(R.id.llContactsbg);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public UserData getItem(int id) {
        return this.arrayListContacts.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                arrayListContacts.get(currentPosition).setHasAdded(1);
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) context).objGlobalHelperNormal.callDialog((HomeActivity) context, context.getString(R.string.alert),
                            modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) context).objGlobalHelperNormal.callDialog((HomeActivity) context,
                            context.getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) context).objGlobalHelperNormal.callDialog((HomeActivity) context, context.getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(context, context.getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, String... params) {

        CheckInternet.getInstance().internetCheckTask(context, new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{context.getString(R.string.authorization),
                            ((HomeActivity) context).prefs.getStringValue(context.getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                context.getString(R.string.AddFollowerUrl) + "?follow=" + arrayListContacts.get(currentPosition).getUserId(),
                                null,
                                new CallBackRetrofit(type,
                                        AddContactsAdapter.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
//                    helper.spinnerStop(spinnerDialog);
                    ((HomeActivity) context).objGlobalHelperNormal.
                            callDialog((HomeActivity) context, context.getString(R.string.alert), context.getString(R.string.please_connect));
                }
            }
        });
    }
}
