package com.qamar.rockme.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.WebViewFragment;
import com.qamar.rockme.models.CreateArt;

import java.util.ArrayList;

public class AdapterArtCreatorChild extends RecyclerView.Adapter<AdapterArtCreatorChild.MyViewHolder> {

    private Context context;
    private ArrayList<CreateArt> createArts;

    public AdapterArtCreatorChild(Context context, ArrayList<CreateArt> createArts) {
        this.context = context;
        this.createArts = createArts;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_arecreator_child,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
       CreateArt createArt = createArts.get(position);
       holder.tvArtWork.setText(createArt.getGadgetName());
        Glide.with(context).load(createArt.getGadgetPicture())
                .centerCrop()
                .override(300,300)
                .into(holder.ivPic);

        holder.ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateArt createArt1 = createArts.get(position);
                Fragment fragment = new WebViewFragment();
                Bundle args = new Bundle();
                args.putString("gadgetId", String.valueOf(createArt1.getGadgetID()));
                args.putString("gadgetString",String.valueOf(createArt1.getGadgetString()));
                fragment.setArguments(args);
                ((HomeActivity) context).replaceFragment2(fragment, null);
            }
        });

    }

    @Override
    public int getItemCount() {
        return createArts.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tvArtWork;
        private ImageView ivPic;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvArtWork = itemView.findViewById(R.id.tvArtWork);
            ivPic = itemView.findViewById(R.id.ivPic);
        }
    }
}
