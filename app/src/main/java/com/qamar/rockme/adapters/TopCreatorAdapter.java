package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.models.TopCreator;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TopCreatorAdapter extends RecyclerView.Adapter<TopCreatorAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<TopCreator> topCreators;

    public TopCreatorAdapter(Context context, ArrayList<TopCreator> topCreators) {
        this.context = context;
        this.topCreators = topCreators;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_top_creator,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TopCreator creator = topCreators.get(position);
        holder.tvName.setText(creator.getFullName());
        holder.tvUserName.setText(creator.getUserName());
        holder.tvArtWork.setText(creator.getArtworkCount());
        holder.tvRank.setText(creator.getRanking());
        if(!creator.getProfilePicture().equals(""))
        Glide.with(context).load(creator.getProfilePicture()).into(holder.ivCreatorPic);
    }

    @Override
    public int getItemCount() {
        return topCreators.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tvName,tvArtWork,tvUserName,tvRank;
        private CircleImageView ivCreatorPic;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvFeedTittleOld);
            tvArtWork = itemView.findViewById(R.id.tvArtWork);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvRank = itemView.findViewById(R.id.tvRank);
            ivCreatorPic = itemView.findViewById(R.id.ivCreatorPic);
        }
    }
}
