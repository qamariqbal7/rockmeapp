package com.qamar.rockme.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.RockMeDetailFragment;
import com.qamar.rockme.models.RockMeFeeds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class RockMeFeedsAdapter2 extends RecyclerView.Adapter<RockMeFeedsAdapter2.MyViewHolder>  {

    private Context context;
    private ArrayList<RockMeFeeds> rockmeFeed;
    private static String today;

    public RockMeFeedsAdapter2(Context context, ArrayList<RockMeFeeds> rockmeFeedsArraylist) {
        this.context = context;
        this.rockmeFeed = rockmeFeedsArraylist;
        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_rock_me,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final RockMeFeeds rockMeFeed=rockmeFeed.get(position);
    // final FeaturedArtworks featuredArtwork = featuredArtworks.get(position);
      //  Artwork artwork =featuredArtwork.getArtwork();
     holder.tvName.setText(rockMeFeed.getUser().getUserName());
   //  holder.tvTitle.setText(artwork.getCategoryName());
        Glide.with(context).load(rockMeFeed.getNewsFeedImage())
                .centerCrop()
                .override(600,600)
                .into(holder.ivPic);
        Glide.with(context).load(rockMeFeed.getUser().getProfilePicture()).centerCrop()
                .into(holder.ivProfilePic);
        holder.ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new RockMeDetailFragment();
                Bundle args = new Bundle();
               // args.putSerializable(context.getResources().getString(R.string.artwork_object),rockMeFeed.getNewsFeedId());
                args.putString("FeedId", rockMeFeed.getNewsFeedId()+"");
                args.putString("FeedLikes", rockMeFeed.getLikesCount()+"");
                args.putString("FeedImage", rockMeFeed.getNewsFeedImage()+"");
                args.putString("FeedTittle", rockMeFeed.getNewsFeedTitle()+"");
                args.putString("FeedCount", rockMeFeed.getCommentsCount()+"");
                args.putString("Name", rockMeFeed.getUser().getUserName()+"");
                args.putString("FeedText", rockMeFeed.getNewsFeedText()+"");
                args.putString("IsLiked", rockMeFeed.getIsLiked()+"");
                fragment.setArguments(args);

                ((HomeActivity) context).replaceFragment2(fragment, (AppCompatActivity)context);
            }
        });
       // SimpleDateFormat spf;
       // spf= new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        long time = 0;
        try {
            time = sdf.parse(rockMeFeed.getInsertDate()).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();

        CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
        holder.tvTimeAgo.setText(ago);
        holder.cbLike.setEnabled(false);
     if(rockMeFeed.getIsLiked().equals("true"))
     {
         holder.cbLike.setChecked(true);

     }
     else {
         holder.cbLike.setChecked(false);
     }

     holder.cbLike.setText(String.valueOf(rockMeFeed.getLikesCount()));
     holder.cbCheat.setText(rockMeFeed.getCommentsCount());
     holder.tvFeedTittle.setText(rockMeFeed.getNewsFeedTitle());
     holder.tvFeedText.setText(rockMeFeed.getNewsFeedText());
    // holder.tvViews.setText(String.valueOf(artwork.getLikes())+" View");
//     String timeAgo = getTimeStamp(featuredArtwork.getEditDate().replace("T"," "));
    //holder.tvTimeAgo.setText(rockMeFeed.getInsertDate());
//     try {
//         if(!artwork.getCategoryName().equals(""))
//             holder.tvCat.setText(artwork.getCategoryName());
//     }
//     catch (NullPointerException n){}
     rockMeFeed.getUser().getUserId();
     holder.cbLike.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Fragment fragment = new RockMeDetailFragment();
             Bundle args = new Bundle();
             // args.putSerializable(context.getResources().getString(R.string.artwork_object),rockMeFeed.getNewsFeedId());
             args.putString("FeedId", rockMeFeed.getNewsFeedId()+"");
             args.putString("FeedLikes", rockMeFeed.getLikesCount()+"");
             args.putString("FeedImage", rockMeFeed.getNewsFeedImage()+"");
             args.putString("FeedTittle", rockMeFeed.getNewsFeedTitle()+"");
             args.putString("FeedCount", rockMeFeed.getCommentsCount()+"");
             args.putString("Name", rockMeFeed.getUser().getUserName()+"");
             args.putString("FeedText", rockMeFeed.getNewsFeedText()+"");
             args.putString("IsLiked", rockMeFeed.getIsLiked()+"");
             fragment.setArguments(args);

             ((HomeActivity) context).replaceFragment2(fragment, (AppCompatActivity)context);

         }
     });
     holder.cbShare.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             Intent intent = new Intent(Intent.ACTION_SEND);
             intent.setType("text/plain");
             intent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
             intent.putExtra(Intent.EXTRA_TEXT, "This is my text");
             context.startActivity(Intent.createChooser(intent, "choose one"));
         }
     });
    }


    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    @Override
    public int getItemCount() {
        return rockmeFeed.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder{
        private CheckBox cbShare,cbLike,cbCheat;
        private TextView tvFeedTittle,tvFeedText,tvName,tvTimeAgo,tvTitle,tvViews,tvCat;
        private ImageView ivPic,ivProfilePic;
        public MyViewHolder(View itemView) {
            super(itemView);
            ivProfilePic=itemView.findViewById(R.id.profile_image);
            cbShare = itemView.findViewById(R.id.cbShare);
            cbLike = itemView.findViewById(R.id.cbLike);
            cbCheat = itemView.findViewById(R.id.cbCheat);
            tvName=itemView.findViewById(R.id.tvName);
            tvFeedTittle = itemView.findViewById(R.id.tvFeedTittleOld);
            tvTimeAgo = itemView.findViewById(R.id.tvTimeAgo);
            tvFeedText = itemView.findViewById(R.id.tvFeedText);
            tvViews = itemView.findViewById(R.id.tvFeedTittleOld);
            tvCat = itemView.findViewById(R.id.tvCat);
            ivPic = itemView.findViewById(R.id.ivPic);

        }
    }
}
