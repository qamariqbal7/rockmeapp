package com.qamar.rockme.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.models.Followers;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterFollowing extends RecyclerView.Adapter<AdapterFollowing.MyViewHolder>  {

    private Activity context;
    private ArrayList<Followers> followersArrayList;
    private ItemClickListener mClickListener;

    private int userId;
    public AdapterFollowing(Activity context, ArrayList<Followers> followers) {
        this.context = context;
        this.followersArrayList = followers;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_following,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
     final Followers followers = followersArrayList.get(position);
     holder.tvName.setText(followers.getFollowerName());
     Glide.with(context).load(followers.getProfilePicture())
             .placeholder(R.drawable.profile)
             .into(holder.profile_image);

    }

    @Override
    public int getItemCount() {
        return followersArrayList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName,tvDisableFollower;
        CircleImageView profile_image;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvFeedTittleOld);
            tvDisableFollower = itemView.findViewById(R.id.tvDisableFollower);
            profile_image = itemView.findViewById(R.id.profile_image);
            tvDisableFollower.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
