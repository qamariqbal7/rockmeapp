package com.qamar.rockme.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.SkuDetails;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.PackagesFragment;
import com.qamar.rockme.models.Packages;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.ViewHolder>
        implements View.OnTouchListener, View.OnClickListener {

    private ArrayList<Packages> arrayListPackages = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private PackagesFragment mPackagesFragment;
    private Packages mPackages;
    private Dialog dialog;
    public static int currentPosition;
    List<SkuDetails> skuDetailsList;
    BillingClient billingClient;

    // data is passed into the constructor
    public PackagesAdapter(Context context, ArrayList<Packages> arrayListPackages, PackagesFragment mPackagesFragment,BillingClient billingClientprms,List<SkuDetails> skuDetailsListprams) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListPackages = arrayListPackages;
        this.context = context;
        skuDetailsList=skuDetailsListprams;
        billingClient=billingClientprms;
        this.mPackagesFragment = mPackagesFragment;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.package_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mPackages = arrayListPackages.get(position);
        /*Log.e("data",
                mPackages.getBundleImage()+"\n"+mPackages.getBundleName()
                        +"\n"+mPackages.getBundleAmount()+"\n"+mPackages.getBundleCoints()
        +"\n"+mPackages.getBundleId()+"\n"+mPackages.getStatus());*/
        holder.tvBundleName.setText(mPackages.getBundleName());
        holder.tvBundleCoins.setText(mPackages.getBundleCoints() + " Coins");
        holder.tvBundleAmount.setText("$" + mPackages.getBundleAmount()+ " USD");

        holder.llBundle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPosition = position;
                dialog = ((HomeActivity) context).objGlobalHelperNormal.showDialog(R.layout.dialog_alert, context,
                        true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
                initializeControlsAndSetData();
            }
        });
    }

    public void initializeControlsAndSetData() {
        ((TextView) dialog.findViewById(R.id.tvHeading)).setText(context.getString(R.string.alert));
        ((TextView) dialog.findViewById(R.id.tvDesc)).setText(context.getString(R.string.buy_coins_msg));
        ((TextView) dialog.findViewById(R.id.btnOne)).setText(context.getString(R.string.yes));
        ((TextView) dialog.findViewById(R.id.btnTwo)).setText(context.getString(R.string.no));
        dialog.findViewById(R.id.btnOne).setOnClickListener(this);
        dialog.findViewById(R.id.btnOne).setOnTouchListener(this);
        dialog.findViewById(R.id.btnTwo).setOnClickListener(this);
        dialog.findViewById(R.id.btnTwo).setOnTouchListener(this);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListPackages.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvBundleName, tvBundleCoins,tvBundleAmount;
        private RelativeLayout llBundle;

        public ViewHolder(View itemView) {
            super(itemView);

            tvBundleName = itemView.findViewById(R.id.tvBundleName);
            tvBundleCoins = itemView.findViewById(R.id.tvBundleCoins);
            tvBundleAmount = itemView.findViewById(R.id.tvBundleAmount);

            llBundle = itemView.findViewById(R.id.llBundle);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Packages getItem(int id) {
        return this.arrayListPackages.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnOne:
                dialog.dismiss();

                BillingFlowParams billingFlowParams=BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetailsList.get(0)).build();
                billingClient.launchBillingFlow((Activity) context,billingFlowParams);
              //  billingFlowParams.
               //  mPackagesFragment.buyCoins(arrayListPackages.get(currentPosition));
                break;

            case R.id.btnTwo:
                dialog.dismiss();
                break;
        }
    }
}
