package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.fragments.WorkPlaceFragment;
import com.qamar.rockme.models.WorkPlace;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class WorkPlaceAdapter extends RecyclerView.Adapter<WorkPlaceAdapter.ViewHolder> {

    private ArrayList<WorkPlace> arrayListWorkPlace = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private WorkPlace mWorkPlace;
    private WorkPlaceFragment mWorkPlaceFragment;
    private boolean isFree;

    // data is passed into the constructor
    public WorkPlaceAdapter(Context context, ArrayList<WorkPlace> arrayListWorkPlace, WorkPlaceFragment mWorkPlaceFragment,boolean isFree) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListWorkPlace = arrayListWorkPlace;
        this.context = context;
        this.mWorkPlaceFragment = mWorkPlaceFragment;
        this.isFree = isFree;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.workplace_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mWorkPlace = arrayListWorkPlace.get(position);
        //holder.ivPic.setImageResource(mWorkPlace.getImg());
        holder.tvName.setText(mWorkPlace.getArtworkName());
        holder.tvMoney.setText(mWorkPlace.getArtworkAmount());

        if(isFree)
        {
            holder.tvName.setVisibility(View.VISIBLE);
        }
        else
            {
                holder.tvName.setVisibility(View.GONE);
            }

//        if(mWorkPlace.getArtworkViews()==null||mWorkPlace.getArtworkViews().equals(""))
//        {
//            //holder.tvViews.setText(mWorkPlace.getArtworkName());
//        }
//        else {
//           // holder.tvViews.setText(mWorkPlace.getArtworkViews()+" View");
//        }

        if(!mWorkPlace.getArtworkImage().equals(""))
            Glide.with(context).load(mWorkPlace.getArtworkImage())
                    .centerCrop()
                    .override(250,250)
                    //.placeholder(R.drawable.black_5)
                    .into(holder.ivPic);
        holder.ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWorkPlaceFragment.setImageAndObject(arrayListWorkPlace.get(position));
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListWorkPlace.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivPic;
        private TextView tvMoney,tvName,tvViews;

        public ViewHolder(View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            tvName = itemView.findViewById(R.id.tvFeedTittle);
            tvMoney = itemView.findViewById(R.id.tvMoney);
            //tvViews = itemView.findViewById(R.id.tvFeedTittleOld);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public WorkPlace getItem(int id) {
        return this.arrayListWorkPlace.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
