package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.fragments.WorkPlaceFragment;
import com.qamar.rockme.models.Popular;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<Popular> populars;
    private WorkPlaceFragment workPlaceFragment;

    public SliderAdapter(Context context, ArrayList<Popular> populars,WorkPlaceFragment workPlaceFragment) {
        this.context = context;
        this.populars = populars;
        this.workPlaceFragment = workPlaceFragment;

    }

    @Override
    public int getCount() {
        return populars.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adapter_popular_item_slider, null);

        TextView tvName = (TextView) view.findViewById(R.id.tvFeedTittleOld);
        TextView tvMoney = (TextView) view.findViewById(R.id.tvMoney);
        TextView tvViews = (TextView) view.findViewById(R.id.tvFeedTittleOld);
        ImageView ivWorkplace = view.findViewById(R.id.ivWorkplace);
        CircleImageView ivPopularProfile = view.findViewById(R.id.ivPopularProfile);

        Popular popular = populars.get(position);

        tvName.setText(popular.getArtworkName());
        tvMoney.setText(popular.getArtworkAmount());
        tvViews.setText(popular.getArtworkViews()+" Views");

        if(!popular.getArtworkPicture().equals(""))
            Glide.with(context).load(popular.getArtworkPicture()).into(ivWorkplace);
        if(!popular.getProfilePicture().equals(""))
            Glide.with(context).load(popular.getProfilePicture()).into(ivPopularProfile);

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        ivWorkplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                workPlaceFragment.clickViewPager(position);
            }
        });

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }



}
