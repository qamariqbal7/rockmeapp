package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.fragments.MyGadgetsFragment;
import com.qamar.rockme.models.MainGadgets;
import com.qamar.rockme.models.MyGadgets;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class MainGadgetAdapter extends RecyclerView.Adapter<MainGadgetAdapter.ViewHolder>
        implements View.OnClickListener, View.OnTouchListener {

    private ArrayList<MainGadgets> arrayListMainGedget = new ArrayList<>();
    private ArrayList<MyGadgets> arrayListGedget = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private MainGadgets mMainGadgets;
    private MyGadgetsFragment mMyGadgetsFragment;
    private int currentPostion;

    // data is passed into the constructor
    public MainGadgetAdapter(Context context, ArrayList<MainGadgets> arrayListMainGedget, MyGadgetsFragment mMyGadgetsFragment) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListMainGedget = arrayListMainGedget;
        this.context = context;
        this.mMyGadgetsFragment = mMyGadgetsFragment;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.main_gadget_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mMainGadgets = arrayListMainGedget.get(position);
        holder.tvMainGadget.setText(mMainGadgets.getLabelGadgets());
       /* holder.tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileViewAllFragment();
                Bundle args = new Bundle();
                args.putSerializable(context.getString(R.string.artwork_arraylist), arrayListMainArtwork.get(position).getArrayListArtwork());
                args.putString(context.getString(R.string.label_artwork), arrayListMainArtwork.get(position).getLabelArtwork());
                fragment.setArguments(args);

                ((HomeActivity) context).replaceFragment(fragment, (HomeActivity) context);
            }
        });*/
        arrayListGedget = arrayListMainGedget.get(position).getArrayListGadgets();
        currentPostion = position;
        holder.adapterSetForGadgets();

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return arrayListMainGedget.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvMainGadget;
        private RecyclerView rvGadgetList;
        private GadgetAdapter mGadgetAdapter;

        public ViewHolder(View itemView) {
            super(itemView);

            tvMainGadget = itemView.findViewById(R.id.tvMainGadget);
            rvGadgetList = itemView.findViewById(R.id.rvGadgetList);

            setUpGadgetRecyclerView();

            itemView.setOnClickListener(this);
        }

        private void setUpGadgetRecyclerView() {
            rvGadgetList.setLayoutManager(new LinearLayoutManager(context));
        }

        private void adapterSetForGadgets() {
            mGadgetAdapter = new GadgetAdapter(context, arrayListGedget, mMyGadgetsFragment,
                    arrayListMainGedget.get(currentPostion).getLabelGadgets());
            rvGadgetList.setAdapter(mGadgetAdapter);
            mGadgetAdapter.notifyDataSetChanged();
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public MainGadgets getItem(int id) {
        return this.arrayListMainGedget.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
