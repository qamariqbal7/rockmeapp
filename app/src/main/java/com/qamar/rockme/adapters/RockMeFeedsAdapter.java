package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.models.RockMeFeeds;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class RockMeFeedsAdapter extends RecyclerView.Adapter<RockMeFeedsAdapter.ViewHolder> {

    private ArrayList<RockMeFeeds> arrayListRockMeFeeds = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private RockMeFeeds mRockMeFeeds;

    // data is passed into the constructor
    public RockMeFeedsAdapter(Context context, ArrayList<RockMeFeeds> arrayListRockMeFeeds) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListRockMeFeeds = arrayListRockMeFeeds;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.rockme_feeds_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mRockMeFeeds = arrayListRockMeFeeds.get(position);
        if(mRockMeFeeds.getUser().getProfilePicture() != null) {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivProfilePic
                    ,mRockMeFeeds.getUser().getProfilePicture(),
                    holder.pbProfilePic, R.drawable.profile);
        } else {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivProfilePic
                    , "",
                    holder.pbProfilePic, R.drawable.profile);
        }

        holder.tvProfileName.setText(mRockMeFeeds.getUser().getUserName());
        holder.tvProfileUserName.setText(mRockMeFeeds.getUser().getEmailAddress());
        holder.tvTimeAgo.setReferenceTime(mRockMeFeeds.getTimeAgo());

        if (mRockMeFeeds.getUser().getIsLiked() == 0) {
            holder.ivHeart.setImageResource(R.drawable.heart_outline);
        } else {
            holder.ivHeart.setImageResource(R.drawable.heart_fill);
        }

        holder.tvHeartCount.setText(""+mRockMeFeeds.getUser().getUserLikes());

        if(mRockMeFeeds.getArtworkImage() != null) {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, holder.ivArtWorkPic
                    ,mRockMeFeeds.getArtworkImage(),
                    holder.pbArtWorkPic, R.drawable.profile);
        } else {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, holder.ivArtWorkPic
                    , "",
                    holder.pbArtWorkPic, R.drawable.fadeGrey3Color);
        }

        holder.tvDesc.setText(mRockMeFeeds.getNewsFeedText());

//        holder.llFeedTwo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((HomeActivity) context).replaceFragment(new StoreArtworkViewFragment(), (HomeActivity) context);
//            }
//        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListRockMeFeeds.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivProfilePic, ivHeart, ivArtWorkPic;
        private ProgressBar pbProfilePic, pbArtWorkPic;

        private TextView tvProfileName, tvProfileUserName, tvHeartCount, tvDesc;
        private RelativeTimeTextView tvTimeAgo;
        private LinearLayout llFeedTwo;

        public ViewHolder(View itemView) {
            super(itemView);

            ivProfilePic = itemView.findViewById(R.id.ivProfilePic);
            pbProfilePic = itemView.findViewById(R.id.pbProfilePic);
            ivArtWorkPic = itemView.findViewById(R.id.ivArtWorkPic);
            pbArtWorkPic = itemView.findViewById(R.id.pbArtWorkPic);

            tvProfileName = itemView.findViewById(R.id.tvProfileName);
            tvProfileUserName = itemView.findViewById(R.id.tvProfileUserName);
            tvHeartCount = itemView.findViewById(R.id.tvHeartCount);
            tvDesc = itemView.findViewById(R.id.tvDesc);

            ivHeart = itemView.findViewById(R.id.ivHeart);
            tvTimeAgo = itemView.findViewById(R.id.tvTimeAgo);
            llFeedTwo = itemView.findViewById(R.id.llFeedTwo);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public RockMeFeeds getItem(int id) {
        return this.arrayListRockMeFeeds.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
