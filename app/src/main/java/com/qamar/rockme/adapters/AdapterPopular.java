package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.models.Popular;

import java.util.ArrayList;

public class AdapterPopular extends RecyclerView.Adapter<AdapterPopular.MyViewHolder> {

    private Context context;
    private ArrayList<Popular> populars;

    public AdapterPopular(Context context, ArrayList<Popular> populars) {
        this.context = context;
        this.populars = populars;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_popular,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
      Popular popular = populars.get(position);
      holder.tvArtWork.setText(popular.getArtworkName());
      holder.tvViews.setText(popular.getArtworkViews()+" Views");
      holder.tvMoney.setText(popular.getArtworkAmount());

      Glide.with(context).load(popular.getArtworkPicture())
              .centerCrop()
              .override(280,280)
              .into(holder.ivPic);
    }

    @Override
    public int getItemCount() {
        return populars.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView ivPic;
        private TextView tvMoney,tvViews,tvArtWork;
        public MyViewHolder(View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.ivPic);
            tvMoney = itemView.findViewById(R.id.tvMoney);
            tvViews = itemView.findViewById(R.id.tvFeedTittleOld);
            tvArtWork = itemView.findViewById(R.id.tvArtWork);
        }
    }
}
