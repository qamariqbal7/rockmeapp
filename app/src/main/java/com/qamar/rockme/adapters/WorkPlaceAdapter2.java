package com.qamar.rockme.adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.PopularFragment;
import com.qamar.rockme.fragments.StoreArtworkViewFragment;
import com.qamar.rockme.fragments.WorkPlaceFragment;
import com.qamar.rockme.helper.RecyclerItemClickListener;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.WorkPlace;
import com.qamar.rockme.models.WorkPlace2;

import java.util.ArrayList;

public class WorkPlaceAdapter2 extends RecyclerView.Adapter<WorkPlaceAdapter2.MyViewHolder> {

    private AppCompatActivity context;
    private ArrayList<WorkPlace2> workPlace2s;
    private WorkPlaceFragment mWorkPlaceFragment;

    boolean isFree;

    public WorkPlaceAdapter2(AppCompatActivity context, ArrayList<WorkPlace2> workPlace2s, WorkPlaceFragment mWorkPlaceFragment) {
        this.context = context;
        this.workPlace2s = workPlace2s;
        this.mWorkPlaceFragment = mWorkPlaceFragment;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.workplace_item_parent,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
    final WorkPlace2 workPlace2 =  workPlace2s.get(position);


    holder.rvWorkPlace.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
    holder.rvWorkPlace.setItemAnimator(new DefaultItemAnimator());

    if(workPlace2.getTitle().toLowerCase().equals("free"))
    {
        isFree = true;
        holder.tvTitle.setText("FREE ARTWORKS");
    }
    else if(workPlace2.getTitle().toLowerCase().equals("latest"))
    {
        holder.tvTitle.setText("MARKETPLACE");
    }
    else {
        holder.tvTitle.setText(workPlace2.getTitle());
        holder.tvTitle.setPadding(0,80,0,0);
        holder.tvViewAll.setPadding(0,80,0,0);
        isFree = false;
    }

    WorkPlaceAdapter workPlaceAdapter = new WorkPlaceAdapter(context,workPlace2.getWorkPlaces(),mWorkPlaceFragment,isFree);
    holder.rvWorkPlace.setAdapter(workPlaceAdapter);

    holder.tvViewAll.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            bundle.putString("catId","0");
            bundle.putString("titleKey",workPlace2.getTitle());
            PopularFragment popularFragment = new PopularFragment();
            popularFragment.setArguments(bundle);
            FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame2, popularFragment);
           // transaction.addToBackStack(null);
            transaction.commit();
        }
    });

    holder.rvWorkPlace.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
           WorkPlace workPlace =  workPlace2.getWorkPlaces().get(position);
            Artwork artwork = new Artwork();
            artwork.setArtworkId(workPlace.getArtworkId());
            artwork.setArtworkName(workPlace.getArtworkName());
            artwork.setArtworkImage(workPlace.getArtworkImage());
            artwork.setArtworkAmount(Integer.parseInt(workPlace.getArtworkAmount()));
            Fragment fragment = new StoreArtworkViewFragment();
            Bundle args = new Bundle();
            args.putSerializable(context.getResources().getString(R.string.artwork_object), artwork);
            args.putString("ArtworkID", String.valueOf(workPlace.getArtworkId()));
            fragment.setArguments(args);

            ((HomeActivity) context).replaceFragment2(fragment, context);
        }
    }));

    }

    @Override
    public int getItemCount() {
        return workPlace2s.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitle,tvViewAll;
        private RecyclerView rvWorkPlace;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvFeedText);
            tvViewAll = itemView.findViewById(R.id.tvViewAll);
            rvWorkPlace = itemView.findViewById(R.id.rvWorkPlace);
        }
    }
}
