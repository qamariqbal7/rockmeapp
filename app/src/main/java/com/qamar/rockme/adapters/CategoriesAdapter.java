package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.models.Category;

import java.util.ArrayList;


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Category> categories;


    public CategoriesAdapter(Context context, ArrayList<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_categories,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
       Category category = categories.get(position);
       // holder.textView.setText(arrayList.iterator().next().toString());
       holder.textView.setText(category.getCategoryName());

       holder.image.setChecked(category.isChecked());

       switch (category.getCategoryName())
       {
           case "Bags":
               holder.image.setButtonDrawable(R.drawable.bag_button_bg);
               break;
           case "Hoodies":
               holder.image.setButtonDrawable(R.drawable.hoodie_button_bg);
               break;
           case "Popular":
               holder.image.setButtonDrawable(R.drawable.start_button_bg);
               break;
           case "Shoes":
               holder.image.setButtonDrawable(R.drawable.shoe_button_bg);
               break;
       }

    }



    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public CheckBox image;
        public MyViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text);
            image = itemView.findViewById(R.id.image);
        }
    }
}
