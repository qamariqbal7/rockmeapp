package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.models.CreateArtMain;

import java.util.ArrayList;

public class AdapterArtCreator extends RecyclerView.Adapter<AdapterArtCreator.MyViewHolder> {

    private Context context;
    private ArrayList<CreateArtMain> createArtMains;

    public AdapterArtCreator(Context context, ArrayList<CreateArtMain> createArtMains) {
        this.context = context;
        this.createArtMains = createArtMains;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_arecreator,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        CreateArtMain createArtMain = createArtMains.get(position);
        if(position!=0)
        {
            holder.tvTitle.setPadding(0,100,0,0);
        }
    holder.tvTitle.setText(createArtMain.getCat());
    AdapterArtCreatorChild artCreatorChild = new AdapterArtCreatorChild(context,createArtMain.getCreateArts());
    holder.rvArtWork.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
    holder.rvArtWork.setAdapter(artCreatorChild);
    }

    @Override
    public int getItemCount() {
        return createArtMains.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private RecyclerView rvArtWork;
        private TextView tvTitle;
        public MyViewHolder(View itemView) {
            super(itemView);
            rvArtWork = itemView.findViewById(R.id.rvArtWork);
            tvTitle = itemView.findViewById(R.id.tvFeedText);
        }
    }
}
