package com.qamar.rockme.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.StoreArtworkViewFragment;
import com.qamar.rockme.models.TopFeeds;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class TopFeedsAdapter extends RecyclerView.Adapter<TopFeedsAdapter.ViewHolder> {

    private ArrayList<TopFeeds> arrayListTopFeeds = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private TopFeeds mTopFeeds;

    // data is passed into the constructor
    public TopFeedsAdapter(Context context, ArrayList<TopFeeds> arrayListTopFeeds) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListTopFeeds = arrayListTopFeeds;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.top_feeds_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mTopFeeds = arrayListTopFeeds.get(position);
        if(mTopFeeds.getUser().getProfilePicture() != null) {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivProfilePic
                    ,mTopFeeds.getUser().getProfilePicture(),
                    holder.pbProfilePic, R.drawable.profile);
        } else {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivProfilePic
                    , "",
                    holder.pbProfilePic, R.drawable.profile);
        }

        holder.tvProfileName.setText(mTopFeeds.getUser().getUserName());

        if(mTopFeeds.getArtwork().getArtworkImage() != null) {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, holder.ivArtWorkPic
                    ,mTopFeeds.getArtwork().getArtworkImage(),
                    holder.pbArtWorkPic, R.drawable.profile);
        } else {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, holder.ivArtWorkPic
                    , "",
                    holder.pbArtWorkPic, R.drawable.fadeGrey3Color);
        }

        holder.tvArtWork.setText(mTopFeeds.getArtwork().getArtworkName());

        if (mTopFeeds.getArtwork().getIsLiked() == 0) {
            holder.ivHeart.setImageResource(R.drawable.heart_outline);
        } else {
            holder.ivHeart.setImageResource(R.drawable.heart_fill);
        }

        holder.tvHeartCount.setText(""+mTopFeeds.getArtwork().getLikes());

        holder.tvArtWorkAgo.setReferenceTime(mTopFeeds.getArtwork().getTimeAgo());

        holder.llFeedOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new StoreArtworkViewFragment();
                Bundle args = new Bundle();
                args.putSerializable(context.getResources().getString(R.string.artwork_object), arrayListTopFeeds.get(position).getArtwork());
                fragment.setArguments(args);

                ((HomeActivity) context).replaceFragment(fragment, (HomeActivity) context);
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListTopFeeds.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivProfilePic, ivArtWorkPic, ivHeart;
        private ProgressBar pbProfilePic, pbArtWorkPic;

        private TextView tvProfileName, tvArtWork, tvHeartCount;
        private RelativeTimeTextView tvArtWorkAgo;
        private LinearLayout llFeedOne;

        public ViewHolder(View itemView) {
            super(itemView);

            ivProfilePic = itemView.findViewById(R.id.ivProfilePic);
            pbProfilePic = itemView.findViewById(R.id.pbProfilePic);

            ivArtWorkPic = itemView.findViewById(R.id.ivArtWorkPic);
            pbArtWorkPic = itemView.findViewById(R.id.pbArtWorkPic);

            tvProfileName = itemView.findViewById(R.id.tvProfileName);
            tvArtWork = itemView.findViewById(R.id.tvArtWork);
            tvHeartCount = itemView.findViewById(R.id.tvHeartCount);

            ivHeart = itemView.findViewById(R.id.ivHeart);
            tvArtWorkAgo = itemView.findViewById(R.id.tvArtWorkAgo);
            llFeedOne = itemView.findViewById(R.id.llFeedOne);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public TopFeeds getItem(int id) {
        return this.arrayListTopFeeds.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
