package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.models.Notifications;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private ArrayList<Notifications> arrayListNotification = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private Notifications mNotifications;

    // data is passed into the constructor
    public NotificationAdapter(Context context, ArrayList<Notifications> arrayListNotification) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListNotification = arrayListNotification;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.notification_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mNotifications = arrayListNotification.get(position);
        holder.tvNotificationDesc.setText(mNotifications.getNotification());
        holder.tvNotificationAgo.setReferenceTime(mNotifications.getTimeAgo());

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListNotification.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvNotificationDesc;
        private RelativeTimeTextView tvNotificationAgo;

        public ViewHolder(View itemView) {
            super(itemView);

            tvNotificationDesc = itemView.findViewById(R.id.tvNotificationDesc);
            tvNotificationAgo = itemView.findViewById(R.id.tvNotificationAgo);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Notifications getItem(int id) {
        return this.arrayListNotification.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
