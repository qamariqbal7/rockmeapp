package com.qamar.rockme.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.StoreArtworkViewFragment;
import com.qamar.rockme.models.Artwork;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class RecentArtworksAdapter extends RecyclerView.Adapter<RecentArtworksAdapter.ViewHolder> {

    private ArrayList<Artwork> arrayListRecentArtworks = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private Artwork mArtwork;

    // data is passed into the constructor
    public RecentArtworksAdapter(Context context, ArrayList<Artwork> arrayListRecentArtworks) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListRecentArtworks = arrayListRecentArtworks;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recent_artworks_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mArtwork = arrayListRecentArtworks.get(position);
        if(mArtwork.getArtworkImage() != null) {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, holder.ivPic
                    ,mArtwork.getArtworkImage(),
                    holder.pbPic, R.drawable.profile);
        } else {
            ((HomeActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, holder.ivPic
                    , "",
                    holder.pbPic, R.drawable.profile);
        }

        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new StoreArtworkViewFragment();
                Bundle args = new Bundle();
                args.putSerializable(context.getResources().getString(R.string.artwork_object), arrayListRecentArtworks.get(position));
                fragment.setArguments(args);

                ((HomeActivity) context).replaceFragment(fragment, (HomeActivity) context);
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListRecentArtworks.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivPic;
        private ProgressBar pbPic;

        private RelativeLayout rlMain;

        public ViewHolder(View itemView) {
            super(itemView);
            ivPic = (ImageView) itemView.findViewById(R.id.ivPic);
            pbPic = (ProgressBar) itemView.findViewById(R.id.pbPic);

            rlMain = itemView.findViewById(R.id.rlMain);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Artwork getItem(int id) {
        return this.arrayListRecentArtworks.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
