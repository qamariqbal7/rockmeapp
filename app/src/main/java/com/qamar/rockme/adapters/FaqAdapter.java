package com.qamar.rockme.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.models.FAQs;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.ViewHolder> {

    private ArrayList<FAQs> arrayListFaq = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private FAQs mFAQs;

    private static int currentPosition = 0;

    // data is passed into the constructor
    public FaqAdapter(Context context, ArrayList<FAQs> arrayListFaq) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListFaq = arrayListFaq;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.faq_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mFAQs = arrayListFaq.get(position);
        holder.text.setText(String.valueOf(position+1));
        holder.tvFaqQuestion.setText(context.getString(R.string.Question) + " " + mFAQs.getQuestion());
        holder.tvFaqAnswer.setText(context.getString(R.string.Answer) + " " + mFAQs.getAnswer());

        holder.tvFaqAnswer.setVisibility(View.GONE);

        if (currentPosition == position) {

            Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);
            holder.tvFaqAnswer.setVisibility(View.VISIBLE);
            holder.tvFaqAnswer.startAnimation(slideDown);
        }

        holder.llFaqbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = position;
                notifyDataSetChanged();
            }
        });
 //       holder.tvFaqQuestion.setText(mFAQs.getQuestion());
    }

    // total number of cells
    @Override
    public int getItemCount() {
       //
        return this.arrayListFaq.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {

//        private TextView tvFaqQuestion, tvFaqAnswer;
        private TextView tvFaqQuestion,text,tvFaqAnswer;
        private RelativeLayout llFaqbg;
        public ViewHolder(View itemView) {
            super(itemView);

            tvFaqQuestion = itemView.findViewById(R.id.tvFaqQuestion);
            text = itemView.findViewById(R.id.text);
            tvFaqAnswer = itemView.findViewById(R.id.tvFaqAnswer);
            llFaqbg = itemView.findViewById(R.id.llFaqbg);

        }

    }

    // convenience method for getting data at click position
    public FAQs getItem(int id) {
        return this.arrayListFaq.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
