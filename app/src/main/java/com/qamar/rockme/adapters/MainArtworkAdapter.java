package com.qamar.rockme.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.fragments.ProfileViewAllFragment;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.MainArtwork;

import java.util.ArrayList;


/**
 * Created by Ishaq on 4/18/2017.
 */

public class MainArtworkAdapter extends RecyclerView.Adapter<MainArtworkAdapter.ViewHolder> {

    private ArrayList<MainArtwork> arrayListMainArtwork = new ArrayList<>();
    private ArrayList<Artwork> arrayListArtwork = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private MainArtwork mMainArtwork;

    // data is passed into the constructor
    public MainArtworkAdapter(Context context, ArrayList<MainArtwork> arrayListMainArtwork) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayListMainArtwork = arrayListMainArtwork;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.main_artwork_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        mMainArtwork = arrayListMainArtwork.get(position);
        holder.tvArtWork.setText(mMainArtwork.getLabelArtwork());
        holder.tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileViewAllFragment();
                Bundle args = new Bundle();
                args.putSerializable(context.getString(R.string.artwork_arraylist), arrayListMainArtwork.get(position).getArrayListArtwork());
                args.putString(context.getString(R.string.label_artwork), arrayListMainArtwork.get(position).getLabelArtwork());
                fragment.setArguments(args);

                ((HomeActivity) context).replaceFragment(fragment, (HomeActivity) context);
            }
        });
        arrayListArtwork = arrayListMainArtwork.get(position).getArrayListArtwork();
        holder.adapterSetForArtworks();
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrayListMainArtwork.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvArtWork, tvViewAll;
        private RecyclerView rvArtWork;
        private ArtworksAdapter mArtworksAdapter;

        public ViewHolder(View itemView) {
            super(itemView);

            tvArtWork = itemView.findViewById(R.id.tvArtWork);
            tvViewAll = itemView.findViewById(R.id.tvViewAll);
            rvArtWork = itemView.findViewById(R.id.rvArtWork);

            setUpArtWorkRecyclerView();

            itemView.setOnClickListener(this);
        }

        private void setUpArtWorkRecyclerView() {
            rvArtWork.setLayoutManager(new GridLayoutManager(context, 4));
        }

        private void adapterSetForArtworks() {
            mArtworksAdapter = new ArtworksAdapter(context, arrayListArtwork);
            rvArtWork.setAdapter(mArtworksAdapter);
            mArtworksAdapter.notifyDataSetChanged();
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItem2Click(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public MainArtwork getItem(int id) {
        return this.arrayListMainArtwork.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItem2Click(View view, int position);
    }
}
