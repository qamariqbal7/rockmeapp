package com.qamar.rockme.interfaces;

/**
 * Created by Ishaq on 1/18/2017.
 */

public interface RefreshWebView {

    public void refreshWebView();

}
