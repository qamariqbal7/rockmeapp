package com.qamar.rockme.interfaces;

import java.io.File;

/**
 * Created by Ishaq on 1/18/2017.
 */

public interface PictureResize {

    public void resizePicture(File file);
    public void cropStart();

}
