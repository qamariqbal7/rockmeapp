package com.qamar.rockme.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ActivityVerification extends BaseActivity implements View.OnTouchListener, View.OnClickListener, ServiceResponse{
   TextView tvSignup;
   Button btnVerify;
   EditText etVerificationCode;
    private ModelParsedResponse modelParsedResponse = null;
    private UserData mUserData = null;
    private KProgressHUD mKProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        initializeControls();
        setClickListeners(this);
    }
    private void initializeControls() {
        mKProgressHUD = KProgressHUD.create(ActivityVerification.this);
        tvSignup=(TextView)findViewById(R.id.tvSignUp);
        btnVerify=(Button) findViewById(R.id.btnVerify) ;
        etVerificationCode=(EditText) findViewById(R.id.etVerificaitonCode);
    }
    private void setClickListeners(View.OnClickListener onClick) {
        findViewById(R.id.tvSignUp).setOnClickListener(onClick);
        findViewById(R.id.btnVerify).setOnClickListener(onClick);
        //findViewById(R.id.btFacebook).setOnClickListener(onClick);
        //findViewById(R.id.btTwitter).setOnClickListener(onClick);
        // findViewById(R.id.ivInstaBtn).setOnClickListener(onClick);
        // findViewById(R.id.ivGoogleBtn).setOnClickListener(onClick);
    }
    public void startNewActivity(Activity activity, Class<?> clsStartActivty, int enterAnim, int exitAnim) {
        startActivity(new Intent(activity, clsStartActivty));
        overridePendingTransition(enterAnim, exitAnim);
        activity.finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnVerify:
                if (etVerificationCode.getText().length()>0) {
                    mKProgressHUD = KProgressHUD.create(ActivityVerification.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(1, getPostParameters());
                }
                break;

            case R.id.tvSignUp:
                startNewActivity(ActivityVerification.this,SignUpActivity.class,R.anim.enter,R.anim.exit);

                break;

        }
    }
    private String getPostParameters() {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("Code", etVerificationCode.getText().toString().trim());
//            jsonObject.put("SocialId", null);
//            jsonObject.put("EmailAddress", etEmail.getText().toString().trim());
//            jsonObject.put("IsSocial", false);
//            jsonObject.put("Password", etPassword.getText().toString().trim());
//            jsonObject.put("FirebaseToken", "");
//            jsonObject.put("Udid", Settings.Secure.getString(this.getContentResolver(),
//                    Settings.Secure.ANDROID_ID));
//            jsonObject.put("DeviceType", "android");
//            jsonObject.put("Token", "" + FrontEngine.getInstance().deviceToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.6f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
      //  if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1 && getIntent().getExtras().get("actiontype").equals("1")) {
                    if (o.getResponseData().contains("true")) {
                       // prefs.saveValueInSharedPreference(prefs.check_login, true);
                        Intent forgetPassword=new Intent(ActivityVerification.this,ForgotPasswordActivity.class);
                        forgetPassword.putExtra("email",getIntent().getExtras().get("email").toString());
                        startActivity(forgetPassword);
                       // startNewActivity(ActivityVerification.this, ForgotPasswordActivity.class,
                         //       R.anim.enter, R.anim.exit);
                        //startNewActivity(SignUpActivity.this, VerifyActivity.class,
                        //    R.anim.enter, R.anim.exit);
                    } else {
                        etVerificationCode.setError("Invalid Code");
                        //   startNewActivity(SignUpActivity.this,ActivityVerification.class,R.anim.enter,R.anim.exit);
                    }
                }
                else if (type == 1 && getIntent().getExtras().get("actiontype").equals("2")) {
                    if (o.getResponseData().contains("true")) {
                        prefs.saveValueInSharedPreference(prefs.check_login, true);
                        startNewActivity(ActivityVerification.this, HomeActivity.class,
                                R.anim.enter, R.anim.exit);
                        //startNewActivity(SignUpActivity.this, VerifyActivity.class,
                        //    R.anim.enter, R.anim.exit);
                    } else {
                        etVerificationCode.setError("Invalid Code");
                        //   startNewActivity(SignUpActivity.this,ActivityVerification.class,R.anim.enter,R.anim.exit);
                    }
                }
            }

       // } else {
         //   customDialogSomethingWent(type);
        //}
    }
    public void customDialogSomethingWent(int type) {
        Toast.makeText(this, getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {
        Toast.makeText(this, e.getLocalizedMessage().toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type,final String... params) {
        final HashMap map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.content_type),
                getString(R.string.app_json)});

        CheckInternet.getInstance().internetCheckTask(this, new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {

                    if (type == 1) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                "account/VerifyCode",
                                jsonObject,
                                new CallBackRetrofit(type,
                                        ActivityVerification.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                }
            }
        });

    }
}
