package com.qamar.rockme.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by Development on 7/5/2018.
 */

public class InstagramOAuthActivity extends BaseActivity implements ServiceResponse {

    private static final String TAG = InstagramOAuthActivity.class.getName();
    private String AUTH_URI;
    private String accessToken;
    private WebView webView;
    private KProgressHUD mKProgressHUD;

    /** */
    public InstagramOAuthActivity() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram_oauth);

        clearCookie();

        mKProgressHUD = KProgressHUD.create(this);
        mKProgressHUD = KProgressHUD.create(InstagramOAuthActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        webView = findViewById(R.id.webView);
        webView.setWebChromeClient(new WebChromeClient());
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        setClient(this, webView);
        AUTH_URI = getString(R.string.authUrlInstagram) + "?client_id="
                + getString(R.string.clientIdInstagram) + "&redirect_uri=" + getString(R.string.redirectUriInstagram) + "&response_type=token";
        webView.loadUrl(AUTH_URI);
    }

    private void clearCookie() {
        CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
    }

    private void setClient(final Activity act, WebView webView) {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d(TAG, "URL : " + url);
                if (url.startsWith(getString(R.string.redirectUriInstagram))) {
                    if (url.contains("access_token")) {
                        accessToken = url.split("#access_token=")[1];
                        Log.d(TAG, "Instagram TOKEN: " + accessToken);
                        mKProgressHUD = KProgressHUD.create(InstagramOAuthActivity.this)
                                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                .setBackgroundColor(getResources().getColor(R.color.black_50))
                                .setAnimationSpeed(2)
                                .setCancellable(false)
                                .show();
                        requestHttpCall(1, "");
                    } else if (url.contains("error_reason")) {
                        String error = url.contains("user_denied") ? "User denied access" : "Authentication failed";
                        Toast.makeText(act, error, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                mKProgressHUD.dismiss();
            }
        });
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
        Log.d("InstaTestApi", "" + o);
        Intent intent=new Intent();
        intent.putExtra(getString(R.string.instagram_data), "" + o.getResponseData());
        setResult(4,intent);
        finish();
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        Log.d("InstaTestApi", "" + o);
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {
        mKProgressHUD.dismiss();
        Log.d("InstaTestApi", "" + o);
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {
        CheckInternet.getInstance().internetCheckTask(this, new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    if (type == 1) {
                        map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.content_type)});

                        FrontEngine.getInstance().getRetrofitFactory().requestServiceInstagramApi(RetrofitFactory.GET,
                                map,
                                "self?access_token=" + accessToken,
                                null,
                                new CallBackRetrofit(type,
                                        InstagramOAuthActivity.this, true
                                ));
                    }
                } else {
                    mKProgressHUD.dismiss();
                    objGlobalHelperNormal.callDialog(InstagramOAuthActivity.this,
                            getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
