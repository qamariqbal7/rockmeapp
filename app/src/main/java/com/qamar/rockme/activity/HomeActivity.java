package com.qamar.rockme.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.qamar.rockme.R;
import com.qamar.rockme.fragments.ArtCreatorFragment;
import com.qamar.rockme.fragments.HelpAndSupportFragment;
import com.qamar.rockme.fragments.NotificationFragment;
import com.qamar.rockme.fragments.ProfileFragment;
import com.qamar.rockme.fragments.RockMeFragment;
import com.qamar.rockme.fragments.SettingsFragment;
import com.qamar.rockme.fragments.WorkPlaceFragment;
import com.qamar.rockme.helper.BottomNavigationViewHelper;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.UserData;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class HomeActivity extends BaseActivity
        implements View.OnClickListener, DrawerLayout.DrawerListener,
        View.OnTouchListener {

    //Side Menu Initialization
    private ImageView ivProfileImage;
    private ProgressBar pbProfileImage;
    private TextView tvProfileName, tvProfileEmail, tvHeart, tvCoins;

 //   private DrawerLayout drawerLayout;
    private String fragmentName = "";

    private UserData mUserData = null;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_home);

        initializeSideMenuControls();
        //initializeControlsAndSetDrawer();
        //setMenusOnClickListeners();
        setDataFromFile();
//        setProfileDataAndPicture();
       /* if (savedInstanceState == null) {
            SelectItem(1);
        }*/

//        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("12:23:23:ew");
//        myRef.child("status").child("AF").setValue("1");
//        myRef.child("user").child("A").setValue("1");
//        myRef.child("user").child("B").setValue("10");

        //myRef.setValue("Hello, World!");


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadFragment(new WorkPlaceFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragmentNav = null;
            switch (item.getItemId()) {
                case R.id.home:
                    fragmentNav = new WorkPlaceFragment();
                    loadFragment(fragmentNav);
                    return true;
                case R.id.rockMe:
                    fragmentNav = new RockMeFragment();
                    loadFragment(fragmentNav);
                    return true;
                case R.id.createArt:
                    fragmentNav = new ArtCreatorFragment();
                    loadFragment(fragmentNav);
                    return true;
                case R.id.notification:
                    fragmentNav = new NotificationFragment();
                    loadFragment(fragmentNav);
                    return true;
                case R.id.settings:
                    fragmentNav = new SettingsFragment();
                    loadFragment(fragmentNav);
                    return true;
            }

            return false;

        }

    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
       // transaction.addToBackStack(null);
        transaction.commit();
    }


    private void initializeSideMenuControls() {
        ivProfileImage = (ImageView) findViewById(R.id.ivProfileImage);
        pbProfileImage = (ProgressBar) findViewById(R.id.pbProfileImage);
        tvProfileName = (TextView) findViewById(R.id.tvProfileName);
        tvProfileEmail = (TextView) findViewById(R.id.tvProfileEmail);
        tvHeart = (TextView) findViewById(R.id.tvHeart);
        tvCoins = (TextView) findViewById(R.id.tvCoins);
    }

   /* public void initializeControlsAndSetDrawer() {
        drawerLayout = findViewById(R.id.drawerLayout);
        drawerLayout.addDrawerListener(this);
    }*/

    private void setMenusOnClickListeners() {
        findViewById(R.id.llMenuWorkplace).setOnClickListener(this);
        findViewById(R.id.llMenuNotification).setOnClickListener(this);
        findViewById(R.id.llMenuFeedback).setOnClickListener(this);
        findViewById(R.id.llMenuHelp).setOnClickListener(this);
        findViewById(R.id.llMenuRockMe).setOnClickListener(this);
        findViewById(R.id.llMenuSettings).setOnClickListener(this);
        findViewById(R.id.llMenuLogout).setOnClickListener(this);

        findViewById(R.id.llProfile).setOnClickListener(this);
        findViewById(R.id.llProfile).setOnTouchListener(this);
    }

    private void setDataFromFile() {
        mUserData = FrontEngine.getInstance().mUserData;
        if (mUserData == null) {
            FrontEngine.getInstance().initializeUser(this);
            mUserData = FrontEngine.getInstance().mUserData;
        }
    }

    public void setProfileDataAndPicture() {
        if (mUserData.getUserName() != null) {
            tvProfileName.setText(mUserData.getUserName());
        }
        if (mUserData.getEmailAddress() != null) {
            tvProfileEmail.setText(mUserData.getEmailAddress());
        }
        tvCoins.setText("" + mUserData.getCurrentCoinCount());
        tvHeart.setText("" + mUserData.getUserLikes());
        if (mUserData.getProfilePicture() != null) {
            objGlobalHelperNormal.setGlideCircularImage(this, ivProfileImage
                    , mUserData.getProfilePicture(),
                    pbProfileImage, R.drawable.profile);
            Log.e("mUserData",mUserData.getProfilePicture());
        } else {
            objGlobalHelperNormal.setGlideCircularImage(this, ivProfileImage
                    , "",
                    pbProfileImage, R.drawable.profile);
        }
    }

    public void setProfileDataAndPicture(ImageView imageView) {
        if (mUserData.getProfilePicture() != null) {
            objGlobalHelperNormal.setGlideCircularImage(this, imageView
                    , mUserData.getProfilePicture(),
                    pbProfileImage, R.drawable.profile);
            Log.e("mUserData",mUserData.getProfilePicture());
        } else {
            objGlobalHelperNormal.setGlideCircularImage(this, imageView
                    , "",
                    pbProfileImage, R.drawable.profile);
        }
    }

    public void setProfileDataAndPicture(TextView textView) {
        if (mUserData.getUserName() != null) {
            textView.setText(mUserData.getUserName());
        }
    }


    @Override
    public void onBackPressed() {
        try {
//            objGlobalHelperNormal.windowImmersive(this);
            objGlobalHelperNormal.hideSoftKeyboard(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            } else {
                fragmentName = getSupportFragmentManager().getBackStackEntryAt(
                        getSupportFragmentManager().getBackStackEntryCount() - 2).getName();

            }
        }*/
        super.onBackPressed();
    }

    /**
     * Used to replace fragment
     *
     * @param currentFragment
     * @param fragmentActivity
     */
    public void replaceFragment(Fragment currentFragment, FragmentActivity fragmentActivity) {
        try {
//            objGlobalHelperNormal.windowImmersive(this);
            objGlobalHelperNormal.hideSoftKeyboard(fragmentActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (currentFragment != null && !fragmentName.equals(currentFragment.getClass().getName())) {
            try {
                String backStateName = currentFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = false;
//                if (backStateName.equals("com.digitonics.themall.fragments.HomeFragment")
//                        || backStateName.equals("com.digitonics.themall.fragments.OffersFragment")
//                        || backStateName.equals("com.digitonics.themall.fragments.ElevatorFragment")
//                        || backStateName.equals("com.digitonics.themall.fragments.FavoriteFragment")
//                        || backStateName.equals("com.digitonics.themall.fragments.ProfileFragment")) {
//                    fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
//                }
                if (!fragmentPopped) { //fragment not in back stack, create it.
                    FragmentTransaction ft = manager.beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    ft.replace(R.id.content_frame, currentFragment, backStateName);
                    ft.addToBackStack(backStateName);
                    ft.commit();
                }
                fragmentName = backStateName;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//            closeDrawer();
        }
    }

    public void replaceFragment2(Fragment currentFragment, FragmentActivity fragmentActivity) {

        String backStateName = currentFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction ft = manager.beginTransaction();
                    ft.addToBackStack(backStateName);
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    ft.replace(R.id.content_frame, currentFragment, backStateName);
                    ft.addToBackStack(backStateName);
                    ft.commit();
        fragmentName = backStateName;
    }

    public void SelectItem(int position) {
        Fragment fragmentNav = null;
        switch (position) {

            case 0:
                fragmentNav = new ProfileFragment();
                break;

            case 1:
                fragmentNav = new WorkPlaceFragment();
                break;

            case 2:
                fragmentNav = new NotificationFragment();
                break;

            case 3:
                fragmentNav = new RockMeFragment();
                break;

            case 4:
                fragmentNav = new SettingsFragment();
                break;

            case 5:
                fragmentNav = new HelpAndSupportFragment();
                break;
        }
        replaceFragment(fragmentNav, this);
    }

    /*public void openAndCloseDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }*/

    public void closeDrawer() {
       /* if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }*/
    }

    public void logOutWorkPerform() {
        FrontEngine.getInstance().deleteUserFile(this);
        objGlobalHelperNormal.logOut(HomeActivity.this, LoginActivity.class, prefs);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
             //   openAndCloseDrawer();
                break;

            case R.id.llMenuWorkplace:
                SelectItem(1);
                break;

            case R.id.llMenuNotification:
                SelectItem(2);
                break;

            case R.id.llMenuRockMe:
                SelectItem(3);
                break;

            case R.id.llMenuSettings:
                SelectItem(4);
                break;

            case R.id.llMenuHelp:
                SelectItem(5);
                break;

            case R.id.llMenuFeedback:
                closeDrawer();
                break;

            case R.id.llMenuLogout:
                closeDrawer();
                dialog = objGlobalHelperNormal.showDialog(R.layout.dialog_alert, this,
                        true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationLeftRight);
                initializeControlsAndSetLogoutData();
                break;

            case R.id.llProfile:
                SelectItem(0);
                break;

            case R.id.btnOne:
                dialog.dismiss();
                logOutWorkPerform();
//                        requestHttpCall(1, getPostParameters());
                break;

            case R.id.btnTwo:
                dialog.dismiss();
                break;
        }
    }

    public void initializeControlsAndSetLogoutData() {
        ((TextView) dialog.findViewById(R.id.tvHeading)).setText(getString(R.string.alert));
        ((TextView) dialog.findViewById(R.id.tvDesc)).setText(getString(R.string.Logout_msg));
        ((TextView) dialog.findViewById(R.id.btnOne)).setText(getString(R.string.yes));
        ((TextView) dialog.findViewById(R.id.btnTwo)).setText(getString(R.string.no));
        dialog.findViewById(R.id.btnOne).setOnClickListener(this);
        dialog.findViewById(R.id.btnOne).setOnTouchListener(this);
        dialog.findViewById(R.id.btnTwo).setOnClickListener(this);
        dialog.findViewById(R.id.btnTwo).setOnTouchListener(this);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
//        drawerLayout.getChildAt(0).setTranslationX(slideOffset * drawerView.getWidth());
//        drawerLayout.bringChildToFront(drawerView);
//        drawerLayout.requestLayout();
    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }
}
