package com.qamar.rockme.activity;

import android.os.Handler;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.qamar.rockme.R;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity {

    private Handler handler = new Handler();
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

       //setContentView(R.layout.activity_splash);

       // setStatusBarGradiant(this, R.drawable.loginbg, (RelativeLayout) findViewById(R.id.rlMain));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getStringValue(prefs.language).equals("")) {
            prefs.saveValueInSharedPreference(prefs.language, "en");
        }

        handlerTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            handler.removeCallbacks(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handlerTimer() {
        runnable = new Runnable() {
            @Override
            public void run() {
                startNewActivity(SplashActivity.this, LoginActivity.class,
                        R.anim.enter, R.anim.exit);
            }
        };
        handler.postDelayed(runnable, 2000);
    }
}
