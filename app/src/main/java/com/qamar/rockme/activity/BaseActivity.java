package com.qamar.rockme.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.helper.GlobalHelperNormal;
import com.qamar.rockme.helper.SharedPreference;
import com.qamar.rockme.helper.Validation;

import java.util.Locale;

/**
 * Created by Ishaq on 5/25/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public GlobalHelperNormal objGlobalHelperNormal;
    public SharedPreference prefs;
    public Validation objValidation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        objGlobalHelperNormal = GlobalHelperNormal.getInstance();
        prefs = SharedPreference.getInstance(this);
        objValidation = Validation.getInstance();

        objGlobalHelperNormal.ignoreFileUriExposure();
//        objGlobalHelperNormal.windowImmersive(this);
    }

    public Configuration languageChange(String languageToLoad) {
        Configuration config = getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }
        if (!languageToLoad.equals("") && !sysLocale.getLanguage().equals(languageToLoad)) {
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setSystemLocale(config, locale);
            } else {
                setSystemLocaleLegacy(config, locale);
            }
            getBaseContext().getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }

        return config;
    }

    @SuppressWarnings("deprecation")
    public Locale getSystemLocaleLegacy(Configuration config){
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getSystemLocale(Configuration config){
        return config.getLocales().get(0);
    }

    @SuppressWarnings("deprecation")
    public void setSystemLocaleLegacy(Configuration config, Locale locale){
        config.locale = locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public void setSystemLocale(Configuration config, Locale locale){
        config.setLocale(locale);
    }

    public void startNewActivity(Activity activity, Class<?> clsStartActivty, int enterAnim, int exitAnim) {
        startActivity(new Intent(activity, clsStartActivty));
        overridePendingTransition(enterAnim, exitAnim);
        activity.finish();
    }

    public void setStatusBarGradiant(Activity activity, int drawable, RelativeLayout rl) {
        Window window = activity.getWindow();
        Drawable background = activity.getResources().getDrawable(drawable);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_10));
            window.setNavigationBarColor(ContextCompat.getColor(this, R.color.black_100));
        } else {
            if(rl != null)
                rl.setBackgroundResource(drawable);
        }
        window.setBackgroundDrawable(background);

    }

    public void changeStatusBarColor(int color) {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        objGlobalHelperNormal.windowImmersive(this);
        return super.onTouchEvent(event);
    }

    public void showToast(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
