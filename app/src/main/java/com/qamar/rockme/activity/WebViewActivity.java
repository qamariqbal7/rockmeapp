package com.qamar.rockme.activity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.qamar.rockme.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.File;

/**
 * Created by Development on 8/8/2018.
 */
public class WebViewActivity extends BaseActivity implements View.OnClickListener,
        View.OnTouchListener{

    private WebView webView;
//    private WorkPlace mWorkPlace;
    private KProgressHUD mKProgressHUD;
    private File file = null;

//    private void readBundle(Bundle bundle) {
//        if (bundle != null) {
//            mWorkPlace = (WorkPlace) bundle.getSerializable(getString(R.string.workplace_object));
//        }
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_webview);

        changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        intializeControlsAndListener();
        setUpWebView();
    }

    private void initialWork() {

    }

    private void intializeControlsAndListener() {
        findViewById(R.id.ivMenu1).setOnTouchListener(this);
        findViewById(R.id.ivMenu1).setOnClickListener(this);
        findViewById(R.id.ivMenu2).setOnTouchListener(this);
        findViewById(R.id.ivMenu2).setOnClickListener(this);

        webView = findViewById(R.id.webView);
        mKProgressHUD = KProgressHUD.create(this);
    }

    private void setUpWebView() {
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setAppCacheEnabled(false);
        webView.clearCache(true);
        webView.loadUrl("about:blank");
        webView.reload();
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setSupportZoom(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setDisplayZoomControls(false);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            // chromium, enable hardware acceleration
//            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        } else {
//            // older android version, disable hardware acceleration
//            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }
//        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return super.shouldOverrideUrlLoading(view, url);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                mKProgressHUD.dismiss();
            }
        });

        mKProgressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();

        webView.loadUrl("file:///android_asset/slipper.html");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                onBackPressed();
                break;

            case R.id.ivMenu2:
                webView.loadUrl("javascript:hide();");
                captureWebViewScreenShot();
//                Fragment fragment = new WorkPlaceMenuFragment();
//                Bundle args = new Bundle();
//                args.putSerializable(getResources().getString(R.string.workplace_object), mWorkPlace);
//                args.putString(getResources().getString(R.string.webview_screenshot_path), file.getAbsolutePath());
//                fragment.setArguments(args);
//
//                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;
        }
    }

    private void captureWebViewScreenShot() {

        webView.loadUrl("javascript:svghtml();");
        webView.setDrawingCacheEnabled(true);
        webView.buildDrawingCache(true);

        file = objGlobalHelperNormal.
                saveBitmapViewInFile(this, webView.getDrawingCache());
        webView.setDrawingCacheEnabled(false);
    }
}