package com.qamar.rockme.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.FBUser;
import com.qamar.rockme.models.InstagramUser;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;


/**
 * Created by Ishaq on 6/2/2018.
 */

public class LoginActivity extends BaseActivity implements View.OnTouchListener, View.OnClickListener, ServiceResponse {

    private EditText etUsername, etPassword;
    private LinearLayout llProfile;
    private TextView tvProfileName, tvFacebook;
    private ImageView ivProfileImage;
    private ProgressBar pbProfileImage;
    private Call<JsonElement> jCall = null;
    private ModelParsedResponse modelParsedResponse = null;
    private UserData mUserData = null;
    private KProgressHUD mKProgressHUD;
    private FBUser fb_user = null;
    private CallbackManager callbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN_SOCIAL_MEDIA;
    private GoogleSignInAccount googleUserData = null;
    //twitter auth client required for custom login
    private TwitterAuthClient client;
    private User user;
    private InstagramUser mInstagramUser = null;


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (prefs.getBooleanValue(prefs.check_login) == true) {
            startNewActivity(LoginActivity.this, HomeActivity.class,
                    R.anim.enter, R.anim.exit);
        }
        setDeviceToken();
    }

    private void setDeviceToken() {
        FrontEngine.getInstance().deviceToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("ishaq", "" + FrontEngine.getInstance().deviceToken);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        setStatusBarGradiant(this, R.drawable.loginbg, (RelativeLayout) findViewById(R.id.rlMain));
        initializeControls();
        setClickListeners(this);
        //setTouchListeners();
        //setProfileNameAndPicture();

    }

    private void initializeControls() {
        mKProgressHUD = KProgressHUD.create(LoginActivity.this);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        tvProfileName = findViewById(R.id.tvProfileName);
        tvFacebook = findViewById(R.id.tvFacebook);
        ivProfileImage = findViewById(R.id.ivProfileImage);
        pbProfileImage = findViewById(R.id.pbProfileImage);
        llProfile = findViewById(R.id.llProfile);
        initGoogleLogin();
        initTwitterLogin();
    }

    //init google login button start
    @SuppressLint("RestrictedApi")
    private void initGoogleLogin() {
//        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
//        signInButton.setSize(SignInButton.SIZE_STANDARD);
//        signInButton.setOnClickListener(this);

        // Configure sign-in to request the user's ID, email address, and basic profile. ID and
        // basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }
    //init google login button close

    private void initTwitterLogin() {
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))//enable logging when app is in debug mode
                .twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))//pass the created app Consumer KEY and Secret also called API Key and Secret
                .debug(true)//enable debug mode
                .build();

        //finally initialize twitter with created configs
        Twitter.initialize(config);
        //initialize twitter auth client
        client = new TwitterAuthClient();
    }

    /**
     * method to do Default Twitter Login
     */
    public void loginTwitter() {
        //check if user is already authenticated or not
        if (getTwitterSession() == null) {

            //if user is not authenticated start authenticating
            client.authorize(this, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {

                    // Do something with result, which provides a TwitterSession for making API calls
                    TwitterSession twitterSession = result.data;

                    //call fetch email only when permission is granted
//                    fetchTwitterData(twitterSession);
                    fetchTwitterDataWithImage();
                }

                @Override
                public void failure(TwitterException e) {
                    setClickListeners(LoginActivity.this);
                    // Do something on failure
                    Log.e("TwitterException",e.toString());
                    Toast.makeText(LoginActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //if user is already authenticated direct call fetch twitter email api
            Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show();
//            fetchTwitterData(getTwitterSession());
            fetchTwitterDataWithImage();
        }
    }


    /**
     * Before using this feature, ensure that “Request email addresses from users” is checked for your Twitter app.
     *
     * @param twitterSession user logged in twitter session
     */
    public void fetchTwitterData(final TwitterSession twitterSession) {
        client.requestEmail(twitterSession, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                //here it will give u only email and rest of other information u can get from TwitterSession
                Log.d("TwitterData",
                        "User Id : " + twitterSession.getUserId() + "\nScreen Name : "
                                + twitterSession.getUserName() + "\nEmail Id : " + result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(LoginActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * call Verify Credentials API when Twitter Auth is successful else it will go in exception block
     * this metod will provide you User model which contain all user information
     */
    public void fetchTwitterDataWithImage() {
        //check if user is already authenticated or not
        if (getTwitterSession() != null) {

            //fetch twitter image with other information if user is already authenticated

            //initialize twitter api client
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();

            //Link for Help : https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-verify_credentials

            //pass includeEmail : true if you want to fetch Email as well
            Call<User> call = twitterApiClient.getAccountService().verifyCredentials(true, false, true);

            call.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    user = result.data;
                    Log.d("TwitterData",
                            "User Id : " + user.id + "\nUser Name : " + user.name + "\nEmail Id : " +
                                    user.email + "\nScreen Name : " + user.screenName + "\nProfile Image : "
                                    + user.profileImageUrl.replace("_normal", ""));
                    mKProgressHUD = KProgressHUD.create(LoginActivity.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(3, getPostParametersTwitter());
                }

                @Override
                public void failure(TwitterException exception) {
                    Log.e("exceptionTwitter",""+exception);
                    Toast.makeText(LoginActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //if user is not authenticated first ask user to do authentication
            Toast.makeText(this, "First to Twitter auth to Verify Credentials.", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * get authenticates user session
     *
     * @return twitter session
     */
    private TwitterSession getTwitterSession() {
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();

        //NOTE : if you want to get token and secret too use uncomment the below code
        /*TwitterAuthToken authToken = session.getAuthToken();
        String token = authToken.token;
        String secret = authToken.secret;*/

        return session;
    }

    private void setClickListeners(View.OnClickListener onClick) {
        findViewById(R.id.btLogin).setOnClickListener(onClick);
        findViewById(R.id.tvSignUp).setOnClickListener(onClick);
        findViewById(R.id.btFacebook).setOnClickListener(onClick);
        findViewById(R.id.btTwitter).setOnClickListener(onClick);
       //  findViewById(R.id.ivInstaBtn).setOnClickListener(onClick);
        // findViewById(R.id.ivGoogleBtn).setOnClickListener(onClick);
        findViewById(R.id.tvForgotPassword).setOnClickListener(onClick);
    }

    private void setTouchListeners() {
        findViewById(R.id.rlLogin).setOnTouchListener(this);
        findViewById(R.id.tvSignUp).setOnTouchListener(this);
        findViewById(R.id.ivFaceBookBtn).setOnTouchListener(this);
        findViewById(R.id.ivTwitterBtn).setOnTouchListener(this);
        //   findViewById(R.id.ivInstaBtn).setOnTouchListener(this);
        //   findViewById(R.id.ivGoogleBtn).setOnTouchListener(this);
        findViewById(R.id.tvForgotPassword).setOnTouchListener(this);
    }

    private void setProfileNameAndPicture() {
        tvProfileName.setText("John Doe");
//        if (mCheckUsernameOrEmail.getImage_urls() != null) {
//            objGlobalHelperNormal.setGlideCircularImage(this, ivProfileImage
//                    , mCheckUsernameOrEmail.getImage_urls().getOne(),
//                    pbProfileImage, R.drawable.profile);
//        } else {
//            objGlobalHelperNormal.setGlideCircularImage(this, ivProfileImage
//                    , "",
//                    pbProfileImage, R.drawable.profile);
//        }
        objGlobalHelperNormal.setGlideRoundedImage(this, ivProfileImage
                , "http://2.bp.blogspot.com/-PGA8jn3WdMU/Uc-IgtfXu9I/AAAAAAAAB80/jg9kc8H7WW0/s324/Cristiano+Ronaldo+Profile+And+Biography.jpg",
                pbProfileImage, R.drawable.profile, getResources().getDimension(R.dimen._5sdp));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.6f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!objValidation.hasText(etUsername)) {
            ret = false;
        }
        if (!objValidation.hasText(etPassword)) {
            ret = false;
        }

        return ret;
    }

    //get parameters start
    private String getPostParameters() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("SocialId", null);
            jsonObject.put("EmailAddress", etUsername.getText().toString().trim());
            jsonObject.put("IsSocial", false);
            jsonObject.put("Password", etPassword.getText().toString().trim());
            jsonObject.put("FirebaseToken", "");
            jsonObject.put("Udid", Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID));
            jsonObject.put("DeviceType", "android");
            jsonObject.put("Token", "" + FrontEngine.getInstance().deviceToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    //get parameters start
    private String getPostParametersFB() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("FullName", fb_user.getFullName());
            jsonObject.put("SocialId", fb_user.getFBId());
            jsonObject.put("EmailAddress", null);
            jsonObject.put("IsSocial", true);
            jsonObject.put("FirebaseToken", ""+ FrontEngine.getInstance().deviceToken);
            jsonObject.put("ProfilePicture", fb_user.getImageURL());
            jsonObject.put("DeviceType", "android");
            jsonObject.put("Token", "" + FrontEngine.getInstance().deviceToken);
            jsonObject.put("Udid", Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    //get parameters start
    private String getPostParametersTwitter() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("FullName", user.name);
            jsonObject.put("SocialId", "" + user.id);
            jsonObject.put("EmailAddress", null);
            jsonObject.put("IsSocial", true);
            jsonObject.put("FirebaseToken", "");
            jsonObject.put("ProfilePicture", user.profileImageUrl.replace("_normal", ""));
            jsonObject.put("DeviceType", "android");
            jsonObject.put("Token", "" + FrontEngine.getInstance().deviceToken);
            jsonObject.put("Udid", Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    //get parameters start
    private String getPostParametersGooglePlus() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("SocialId", googleUserData.getId());
            jsonObject.put("IsSocial", true);
            jsonObject.put("EmailAddress", null);
            jsonObject.put("FullName", googleUserData.getDisplayName());
            jsonObject.put("ProfilePicture", googleUserData.getPhotoUrl());
            jsonObject.put("FirebaseToken", "");
            jsonObject.put("DeviceType", "android");
            jsonObject.put("Token", "" + FrontEngine.getInstance().deviceToken);
            jsonObject.put("Udid", Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    //get parameters start
    private String getPostParametersInstagram() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("SocialId", mInstagramUser.getData().getId());
            jsonObject.put("IsSocial", true);
            jsonObject.put("EmailAddress", null);
            jsonObject.put("FullName", mInstagramUser.getData().getFull_name());
            jsonObject.put("ProfilePicture", mInstagramUser.getData().getProfile_picture());
            jsonObject.put("FirebaseToken", "");
            jsonObject.put("DeviceType", "android");

            jsonObject.put("Token", "" + FrontEngine.getInstance().deviceToken);
            jsonObject.put("Udid", Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btLogin:
                if (checkValidation()) {
                    mKProgressHUD = KProgressHUD.create(LoginActivity.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(1, getPostParameters());
                }
                break;

            case R.id.tvSignUp:
                startNewActivity(LoginActivity.this, SignUpActivity.class,
                        R.anim.enter, R.anim.exit);
                break;

            case R.id.tvForgotPassword:
                startNewActivity(LoginActivity.this, ForgetPasswordEmail.class, R.anim.enter, R.anim.exit);
                break;

            case R.id.btFacebook:
                CheckInternet.getInstance().internetCheckTask(this, new CheckInternet.ConnectionCallBackInternet() {
                    @Override
                    public void intenetConnected(boolean status) {
                        if (status) {
                            LoginManager.getInstance().logOut();
                            setClickListeners(null);
                            RC_SIGN_IN_SOCIAL_MEDIA = 1;
                            fbFunctionality();
                        } else {
                            objGlobalHelperNormal.callDialog(LoginActivity.this, getString(R.string.alert), getString(R.string.please_connect));
                        }
                    }
                });
                break;

            case R.id.btTwitter:
//                CheckInternet.getInstance().internetCheckTask(this, new CheckInternet.ConnectionCallBackInternet() {
//                    @Override
//                    public void intenetConnected(boolean status) {
//                        if (status) {
//                            setClickListeners(null);
//                            RC_SIGN_IN_SOCIAL_MEDIA = 2;
//                            loginTwitter();
//                            //loginToTwitter();
//                        } else {
//                            objGlobalHelperNormal.callDialog(LoginActivity.this, getString(R.string.alert), getString(R.string.please_connect));
//                        }
//                    }
//                });
//                break;
                RC_SIGN_IN_SOCIAL_MEDIA = 4;
                startActivityForResult(new Intent(this, InstagramOAuthActivity.class), RC_SIGN_IN_SOCIAL_MEDIA);
                break;

            case R.id.ivInstaBtn:
                RC_SIGN_IN_SOCIAL_MEDIA = 4;
                startActivityForResult(new Intent(this, InstagramOAuthActivity.class), RC_SIGN_IN_SOCIAL_MEDIA);
                break;

            case R.id.ivGoogleBtn:
                CheckInternet.getInstance().internetCheckTask(this, new CheckInternet.ConnectionCallBackInternet() {
                    @Override
                    public void intenetConnected(boolean status) {
                        if (status) {
                            setClickListeners(null);
                            RC_SIGN_IN_SOCIAL_MEDIA = 1021;
                            loginGoogleClick();
                        } else {
                            objGlobalHelperNormal.callDialog(LoginActivity.this, getString(R.string.alert), getString(R.string.please_connect));
                        }
                    }
                });
                break;
        }
    }

    //login with Google Plus start
    public void loginGoogleClick() {
        @SuppressLint("RestrictedApi") Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN_SOCIAL_MEDIA);
    }
    //login with Google Plus end

    public void fbFunctionality() {

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        //LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));

        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                if (loginResult != null) {

                    getUserDetails(loginResult.getAccessToken().getUserId());
                    Log.e("FB", loginResult.getAccessToken().getToken());
                    Log.e("FB", loginResult.getAccessToken().getDeclinedPermissions().toString());
                    Log.e("FB", loginResult.getAccessToken().getExpires().getTime() + "");
                    Log.e("FB", loginResult.getAccessToken().getUserId() + "");
                    Log.e("FB", loginResult.getRecentlyDeniedPermissions().toString());
                    Log.e("FB", loginResult.getRecentlyGrantedPermissions().toString());
                    Log.e("FB", loginResult.getRecentlyGrantedPermissions().toString());
                    //Toast.makeText(LoginActivity.this, "" + loginResult.getAccessToken().getUserId(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancel() {
                // App code
                getUserDetails("cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                getUserDetails("cancel");
            }
        });
    }

    public void getUserDetails(String userID) {
        /* make the API call */

        //user cancel the facebook screen at the time of login/signup
        if (!userID.equals("cancel")) {
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {

                            Log.v("LoginActivity Response ", response.toString());
                            FBUser fb_user = new FBUser();

                            if (object != null) {

                                try {
                                    if (object.has("name"))
                                        fb_user.setFullName(object.getString("name"));
                                    if (object.has("email"))
                                        fb_user.setEmail(object.getString("email"));
                                    if (object.has("id"))
                                        fb_user.setFBId(object.getString("id"));
                                    if (object.has("picture")) {
                                        JSONObject jsonObject = object.getJSONObject("picture");
                                        if (jsonObject.has("data")) {
                                            JSONObject data = jsonObject.getJSONObject("data");
                                            if (data.has("url")) {
                                                fb_user.setImageURL(data.getString("url"));
                                            }
                                        }

                                    }
                                    fb_user.setFbAccessToken(AccessToken.getCurrentAccessToken().getToken());
                                    onFetchFBData(fb_user, 1); //1 for success fb request

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    onFetchFBData(null, 2); //2 for exception
                                }

                            } else {
                                onFetchFBData(null, 2); //2 for exception null
                            }


                        }
                    });


            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,picture.type(large),about,link,email");
            request.setParameters(parameters);
            request.executeAsync();

        } else {
            if (userID.equals("cancel")) {
                onFetchFBData(null, 0); //0 for fb request cancel from user
            }

        }


    }

    public void onFetchFBData(FBUser fb_user, int checkService) {
        if (checkService == 1) {
            Log.e("FB", fb_user.getEmail());
            Log.e("FB", fb_user.getFullName());
            Log.e("FB", fb_user.getImageURL());
            Log.e("FB", fb_user.getFBId());
            Log.e("FB", fb_user.getFbAccessToken());
            this.fb_user = fb_user;
            mKProgressHUD = KProgressHUD.create(LoginActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setBackgroundColor(getResources().getColor(R.color.black_50))
                    .setAnimationSpeed(2)
                    .setCancellable(false)
                    .show();
            requestHttpCall(3, getPostParametersFB());
        } else {
            setClickListeners(this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (RC_SIGN_IN_SOCIAL_MEDIA == 1021) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            @SuppressLint("RestrictedApi") Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else if (RC_SIGN_IN_SOCIAL_MEDIA == 1) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (RC_SIGN_IN_SOCIAL_MEDIA == 2) {
            // Pass the activity result to the twitterAuthClient.
            if (client != null)
                client.onActivityResult(requestCode, resultCode, data);
        } else if (RC_SIGN_IN_SOCIAL_MEDIA == 4) {
            if (data != null) {
                mInstagramUser = new Gson().fromJson(data.getStringExtra(getString(R.string.instagram_data)), InstagramUser.class);
                mKProgressHUD = KProgressHUD.create(LoginActivity.this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setBackgroundColor(getResources().getColor(R.color.black_50))
                        .setAnimationSpeed(2)
                        .setCancellable(false)
                        .show();
                requestHttpCall(3, getPostParametersInstagram());
            }
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            googleUserData = completedTask.getResult(ApiException.class);

            Log.d("Google Name", "" + googleUserData.getDisplayName());
            Log.d("Google Email", "" + googleUserData.getEmail());
            Log.d("Google Given Name", "" + googleUserData.getGivenName()); //first name
            Log.d("Google Family Name", "" + googleUserData.getFamilyName()); //last name
            Log.d("Google Display Name", "" + googleUserData.getDisplayName()); //full name
            Log.d("Google Id", "" + googleUserData.getId());
            Log.d("Google Picture", "" + googleUserData.getPhotoUrl());         //picture url
            Log.d("Google Server Auth", "" + googleUserData.getServerAuthCode());  //server auth token
            Log.d("Google Account", "" + googleUserData.getAccount());
            mKProgressHUD = KProgressHUD.create(LoginActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setBackgroundColor(getResources().getColor(R.color.black_50))
                    .setAnimationSpeed(2)
                    .setCancellable(false)
                    .show();
            requestHttpCall(3, getPostParametersGooglePlus());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Google Plus Error", "signInResult:failed code=" + e.getStatusCode());
            setClickListeners(this);
        }
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();

        if (modelParsedResponse != null) { Log.e("loginResponse",o.getResponseData());

            if (o.getResponseCode() == 200) {
                if (type == 1 || type == 3) {
                    try {
                        JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                        JSONObject response = jsonObject.getJSONObject("response");
                        JSONObject data = response.getJSONObject("data");
                        mUserData = new Gson().fromJson(data.toString(), UserData.class);
                        FrontEngine.getInstance().saveUser(this, mUserData);
                    }
                    catch (JSONException j){}
                    if(mUserData.getIsVerified()=="true") {
                        prefs.saveValueInSharedPreference(prefs.check_login, true);
                        startNewActivity(LoginActivity.this, HomeActivity.class,
                                R.anim.enter, R.anim.exit);
                        //startNewActivity(SignUpActivity.this, VerifyActivity.class,
                        //    R.anim.enter, R.anim.exit);
                    }
                    else
                    {
                        startNewActivity(LoginActivity.this,ActivityVerification.class,R.anim.enter,R.anim.exit);
                    }

                } else if (type == 2) {
                    llProfile.setVisibility(View.VISIBLE);
                    setProfileNameAndPicture();
                }
            } else {
                if (modelParsedResponse.getMessages() != null && type != 2) {
                    objGlobalHelperNormal.callDialog(this,
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent(type);
                }
            }

        } else {
            customDialogSomethingWent(type);
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {
                setClickListeners(this);
                if (type == 1 || type == 3) {
                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                    mUserData = new Gson().fromJson(jsonObject.toString(), UserData.class);
                    FrontEngine.getInstance().saveUser(this, mUserData);
                } else if (type == 2) {
                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
//                    mCheckUsernameOrEmail =
//                            new Gson().fromJson(modelParsedResponse.getResponse().getData(), CheckUsernameOrEmail.class);
                }

//                JsonObject jsonObject = modelParsedResponse.getResponse().getData().getAsJsonObject();
//
//                int i = Integer.parseInt(jsonObject.get("activity_count").toString());
//
//                Log.e("Data",""+i);
//
//                ShortcutBadger.applyCount(this, i);
//
//                FrontEngine.getInstance().notificationCount = i;

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {
        Log.e("response",o.getResponseData());
        try {
            setClickListeners(this);
            mKProgressHUD.dismiss();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null && type != 2) {

                        if (modelParsedResponse.getMessages() != null) {
                            if (type == 3 || type == 1 && modelParsedResponse.getMessages().equals("User already exist.")) {
                                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                                mUserData = new Gson().fromJson(modelParsedResponse.getResponse().getData(), UserData.class);
                                FrontEngine.getInstance().saveUser(this, mUserData);
                                prefs.saveValueInSharedPreference(prefs.check_login, true);
                                startNewActivity(LoginActivity.this, HomeActivity.class,
                                        R.anim.enter, R.anim.exit);
                            } else {
                                //do changes according to login response change
                                 objGlobalHelperNormal.callDialog(this, getString(R.string.alert), modelParsedResponse.getMessages());
                               /* try {
                                    JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                                   *//* JSONObject response = jsonObject.getJSONObject("response");
                                    JSONObject data = response.getJSONObject("data");*//*
                                    mUserData = new Gson().fromJson(jsonObject.toString(), UserData.class);
                                    FrontEngine.getInstance().saveUser(this, mUserData);
                                }
                                catch (JSONException j){}
                                prefs.saveValueInSharedPreference(prefs.check_login, true);
                                startNewActivity(LoginActivity.this, HomeActivity.class, R.anim.enter, R.anim.exit);
*/                            }
                        } else {
                            customDialogSomethingWent(type);
                        }

                    }
                    else {
                        customDialogSomethingWent(type);
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent(type);
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent(type);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent(int type) {
        if (type != 2) {
            Toast.makeText(this, getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        final HashMap map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.content_type),
                getString(R.string.app_json)});

        CheckInternet.getInstance().internetCheckTask(this, new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {

                    if (type == 1) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        jCall = FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.signinUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        LoginActivity.this
                                ));
                    } else if (type == 2) {
//                        if (jCall != null) {
//                            jCall.cancel();
//                        }
//                        jCall = FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
//                                map,
//                                getString(R.string.checkUsernameOrEmailUrl)
//                                        + "?email=" + etEmail.getText().toString().trim(),
//                                null,
//                                new CallBackRetrofit(type,
//                                        LoginActivity.this
//                                ));
                    } else if (type == 3) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        jCall = FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.signupUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        LoginActivity.this
                                ));
                    }
                } else {
                    mKProgressHUD.dismiss();
                    if (type != 2) {
                        objGlobalHelperNormal.callDialog(
                                LoginActivity.this, getString(R.string.alert), getString(R.string.please_connect));
                    }
                }
            }
        });


    }
}

