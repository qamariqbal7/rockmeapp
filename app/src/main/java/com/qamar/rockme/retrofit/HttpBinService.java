package com.qamar.rockme.retrofit;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by Ishaq on 1/20/2017.
 */
public interface HttpBinService {


    @POST("{url}")
    Call<JsonElement> postData(@Path(value = "url", encoded = true) String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    @POST
    Call<JsonElement> postData(@Url String url, @HeaderMap HashMap<String, String> header);

    @POST("{url}")
    Call<JsonElement> postData(@Path(value = "url", encoded = true) String url, @HeaderMap HashMap<String, String> header, @Body JsonArray jsonArray);

    @PUT
    Call<JsonElement> putData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    @PUT
    Call<JsonElement> putData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonArray jsonArray);

    @GET
    Call<JsonElement> getData(@Url String url, @HeaderMap HashMap<String, String> header);

    //@DELETE
    @HTTP(method = "DELETE", /*path = "post/delete",*/ hasBody = true)
    Call<JsonElement> deleteData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    //@DELETE
    @HTTP(method = "DELETE", /*path = "post/delete",*/ hasBody = true)
    Call<JsonElement> deleteData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonArray jsonArray);

    //@DELETE
    @HTTP(method = "DELETE", /*path = "post/delete",*/ hasBody = false)
    Call<JsonElement> deleteData(@Url String url, @HeaderMap HashMap<String, String> header);

    @POST("account/UpdateProfilePicture")
    @Multipart
    Call<JsonElement> updateProfilePicture(@HeaderMap HashMap<String, String> header,
                                           @Part MultipartBody.Part body
    );

    @POST("artwork/uploaduserartwork")
    @Multipart
    Call<JsonElement> uploadArtwork(@HeaderMap HashMap<String, String> header,
                                    @Part MultipartBody.Part body,
                                    @Part("artworkName") RequestBody artworkName,
                                    @Part("categoryId") RequestBody categoryId,
                                    @Part("createDate") RequestBody createDate,
                                    @Part("artworkAmount") RequestBody artworkAmount,
                                    @Part("gadgetCommand") RequestBody gadgetCommand,
                                    @Part("artworkFrame") RequestBody artworkFrame,
                                    @Part("gadgetId") RequestBody gadgetId,
                                    @Part("artworkId") RequestBody artworkId
                                    );

}
