package com.qamar.rockme.retrofit;

import com.qamar.rockme.helper.FrontEngine;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ishaq on 1/20/2017.
 */

public class RetrofitFactory {
    public static final int POST = 1, GET = 2, PUT = 3, DELETE = 4;
    Retrofit retrofit;
    Call<JsonElement> j = null;
    HttpBinService service;
    private static OkHttpClient okHttpClient;

    public Call<JsonElement> requestService(int methodType, HashMap map, String postURL, JsonObject jsonElement, CallBackRetrofit callback) {
        HttpBinService service = getServiceInstance();

        if (methodType == POST) {
            map.put("Content-Type", "application/json");
            if (jsonElement != null) {
                j = service.postData(postURL, map, jsonElement);
            } else {
                j = service.postData(postURL, map);
            }
        } else if (methodType == PUT) {
            map.put("Content-Type", "application/json");
            j = service.putData(postURL, map, jsonElement);
        } else if (methodType == GET) {
            map.put("Content-Type", "application/json");
            j = service.getData(postURL, map);
        } else if (methodType == DELETE) {
            map.put("Content-Type", "application/json");
            if (jsonElement != null) {
                j = service.deleteData(postURL, map, jsonElement);
            } else {
                j = service.deleteData(postURL, map);
            }
        }

        j.enqueue(callback);

        return j;
    }

    public void requestService(int methodType, HashMap map, String postURL, JsonArray jsonArray, boolean check
            , CallBackRetrofit callback) {
        HttpBinService service = getServiceInstance();

        if (methodType == POST) {
            map.put("Content-Type", "application/json");
            j = service.postData(postURL, map, jsonArray);
        } else if (methodType == PUT) {
            map.put("Content-Type", "application/json");
            j = service.putData(postURL, map, jsonArray);
        } else if (methodType == GET) {
            map.put("Content-Type", "application/json");
            j = service.getData(postURL, map);
        } else if (methodType == DELETE) {
            map.put("Content-Type", "application/json");
            j = service.deleteData(postURL, map, jsonArray);
        }

        j.enqueue(callback);
    }

    public void requestServiceInstagramApi(int methodType, HashMap map, String postURL,
                                           JsonObject jsonElement, CallBackRetrofit callback) {
        HttpBinService service = getServiceInstanceInstagramApi();

        if (methodType == GET)
            j = service.getData(postURL, map);

        j.enqueue(callback);
    }

    public HttpBinService getServiceInstanceInstagramApi() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
       /* OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
        okHttpClient.readTimeout(1, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(1, TimeUnit.MINUTES);
        okHttpClient.followRedirects(false);
        okHttpClient.followSslRedirects(false);
        OkHttpClient okHttpClient_ = okHttpClient.build();*/
        if (okHttpClient == null)
            initOkHttp();
        retrofit = new Retrofit.Builder()
                .baseUrl(FrontEngine.getInstance().InstagramApiUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(HttpBinService.class);


        return service;
    }

    private static void initOkHttp() {
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .followRedirects(false)
                .followSslRedirects(false);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(interceptor);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json");



                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        okHttpClient = httpClient.build();
    }

    public void updateProfilePicture(HashMap<String, String> header,
                                     MultipartBody.Part body,
                                     CallBackRetrofit callBack) {

        HttpBinService service = getServiceInstance();
        Call<JsonElement> j = service.updateProfilePicture(header, body);

        j.enqueue(callBack);

    }

    public void uploadArtwork(HashMap<String, String> header,
                              MultipartBody.Part body,
                              RequestBody artworkName,
                              RequestBody categoryId,
                              RequestBody createDate,
                              RequestBody artworkAmount,
                              RequestBody gadgetCommand,
                              RequestBody artworkFrame,
                              RequestBody gadgetId,
                              RequestBody artworkId,
                              CallBackRetrofit callBack) {

        HttpBinService service = getServiceInstance();
        Call<JsonElement> j = service.uploadArtwork(header,
                body,
                artworkName,
                categoryId,
                createDate,
                artworkAmount,
                gadgetCommand,
                artworkFrame,
                gadgetId,
                artworkId);

        j.enqueue(callBack);

    }

    public HttpBinService getServiceInstance() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
        okHttpClient.readTimeout(1, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(1, TimeUnit.MINUTES);
        okHttpClient.followRedirects(false);
        okHttpClient.followSslRedirects(false);
        OkHttpClient okHttpClient_ = okHttpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(FrontEngine.getInstance().MainApiUrl)
                .client(okHttpClient_)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(HttpBinService.class);


        return service;
    }

    public void cancelRequest() {
        j.cancel();
    }

    public boolean isExceutedCall() {
        if (j.isExecuted()) {
            return true;
        } else {
            return false;
        }
    }
}
