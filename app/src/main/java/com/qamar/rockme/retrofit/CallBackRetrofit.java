package com.qamar.rockme.retrofit;

import android.os.AsyncTask;

import com.qamar.rockme.models.ModelParsedResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import org.json.JSONException;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ishaq on 1/26/2017.
 */

public class CallBackRetrofit implements Callback<JsonElement> {
    ServiceResponse response;
    int type;
    boolean check = false;

    public CallBackRetrofit(int type, ServiceResponse response) {
        this.response = response;
        this.type = type;
    }

    public CallBackRetrofit(int type, ServiceResponse response, boolean check) {
        this.response = response;
        this.type = type;
        this.check = check;
    }

    @Override
    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

        HttpResponse httpResponse = new HttpResponse();

        if (response != null) {

            if (response.body() != null)
                httpResponse.setResponseData(response.body().toString());
            else try {
                httpResponse.setResponseData(response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (check) {
                httpResponse.setResponseCode(response.code());
                if (response.isSuccessful()) {
                    asyncTask(httpResponse);
                } else try {
                    if (response != null) this.response.onError(type, httpResponse, null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    ModelParsedResponse modelParsedResponse = new Gson().fromJson
                            (httpResponse.getResponseData(), ModelParsedResponse.class);
                    if (modelParsedResponse.getResponse() != null) {
                        if (modelParsedResponse.getResponse().getCode() == 200) {
                            httpResponse.setResponseCode(200);
                        } else {
                            if (modelParsedResponse.getError() != null) {
                                httpResponse.setResponseCode(modelParsedResponse.getError().getCode());
                            } else if (modelParsedResponse.getResponse() != null) {
                                httpResponse.setResponseCode(modelParsedResponse.getResponse().getCode());
                            }
                        }
                    }
                    if (httpResponse.getResponseCode() == 200) {
                        asyncTask(httpResponse);
                    } else try {
                        if (response != null) this.response.onError(type, httpResponse, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        this.response.onError(type, httpResponse, null);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }
            }

        }else{

            try {

                this.response.onError(type, null, new Exception());

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onFailure(Call<JsonElement> call, Throwable t) {
        t.printStackTrace();

        if (call.isCanceled()) {
            try {
                String response = "request_cancel";
                HttpResponse httpResponse = new HttpResponse();
                httpResponse.setResponseData(response);
                if (response != null) this.response.onError(type, httpResponse, new Exception());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (response != null) this.response.onError(type, null, new Exception());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    public void asyncTask(final HttpResponse res) {
        AsyncTask task = new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] params) {
                if (response != null)
                    response.parseDataInBackground(type, res);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (res.getResponseCode() == 200) {
                    if (response != null) response.onResult(type, res);
                } else try {
                    if (response != null) response.onError(type, res, null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
