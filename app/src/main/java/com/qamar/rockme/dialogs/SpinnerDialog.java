package com.qamar.rockme.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.qamar.rockme.activity.BaseActivity;
import com.qamar.rockme.R;


public class SpinnerDialog extends Dialog {

    private Activity activity;
    private ProgressBar progressBar;

    public SpinnerDialog(Activity activity) {
        super(activity, R.style.dialogSpinner);
        this.activity = activity;
    }

    public SpinnerDialog(BaseActivity activity, int style) {
        super(activity, style);
        this.activity = activity;
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullScreenCode();
        setContentView(R.layout.dialog_spinner);

        initViews();

    }

    protected void fullScreenCode() {

        Window window = this.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    }

    private void initViews() {

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

//        int currentapiVersion = android.os.Build.VERSION.SDK_INT;

        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(activity ,
                R.color.colorPrimary), PorterDuff.Mode.SRC_IN);


    }




}
