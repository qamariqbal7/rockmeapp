package com.qamar.rockme.helper;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.qamar.rockme.R;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Ishaq on 2/27/2017.
 */

public class FrontEngine extends MultiDexApplication {

    public static FrontEngine frontEngine = null;

    public static FrontEngine getInstance() {
        if (frontEngine == null)
            frontEngine = new FrontEngine();
        return frontEngine;
    }

    public String MainApiUrl = "http://rockme.info/RockMeApi/api/";
    public String gadgetURL = "http://192.168.4.1/";
    public String baseURLForImages = "http://rock-me.co/RockMeApi/";
    public String InstagramApiUrl = "https://api.instagram.com/v1/users/";

    private File userDetailFile = null;
    private String userFileName = "user_detail.ser";
    public String accessToken = "";
    public UserData mUserData = null;
    public float dpi;
    public String deviceToken = "";

    @Override
    public void onCreate() {
        super.onCreate();
        dpi = this.getResources().getDisplayMetrics().density;

    }


    public UserData initializeUser(Context context) {


        try {
            userDetailFile = new File(context.getFilesDir(), userFileName);

            if (userDetailFile != null) {
                String userString = (String) SerializationUtil.deserialize(userDetailFile);
                mUserData = new Gson().fromJson(userString, UserData.class);
                this.accessToken = mUserData.getAccessToken();
                SharedPreference.getInstance(context).saveValueInSharedPreference(context.getString(R.string.sp_accesstoken), accessToken);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }



        return mUserData;
    }

    public boolean saveUser(Context context, UserData mUserData) {

        File userDetailFile = new File(context.getFilesDir(), userFileName);
        if (mUserData != null) {
            try {

                mUserData.setAccessToken("" + mUserData.getAccessToken());
                SerializationUtil.serialize(new Gson().toJson(mUserData), userDetailFile);
                this.mUserData = mUserData;
                this.accessToken = mUserData.getAccessToken();
                SharedPreference.getInstance(context).saveValueInSharedPreference(context.getString(R.string.sp_accesstoken), accessToken);

            } catch (IOException e) {
                //e.printStackTrace();
                return false;
            } catch (ClassNotFoundException e) {
                //e.printStackTrace();
                return false;
            }

            return true;
        } else {
            return false;
        }

    }

    public boolean deleteUserFile(Context context) {
        boolean deleted = false;
        userDetailFile = new File(context.getFilesDir(), userFileName);

        if (userDetailFile != null) {
            deleted = userDetailFile.delete();

        }
        FrontEngine.getInstance().mUserData = null;
        return deleted;
    }

//    public String getUserProfilePicture(Context context) {
//        String url = null;
//        if (mUserData == null)
//            FrontEngine.getInstance().initializeUser(context);
//
//        if (mUserData.getUser().getImage_urls() != null)
//            if (mUserData.getUser().getImage_urls().getScaledImageURL() != null) {
//                url = mUserData.getUser().getImage_urls().getScaledImageURL();
//            }
//
//        return url;
//    }

    private RetrofitFactory retrofitFactory;

    public RetrofitFactory getRetrofitFactory() {
        retrofitFactory = new RetrofitFactory();
        return retrofitFactory;
    }


    public HashMap getMap(String[] strings) {
        HashMap<String, String> map = new HashMap<>();
        if (strings != null && strings.length > 1) {
            for (int i = 0; i < strings.length; i += 2)
                map.put(strings[i], strings[i + 1]);
        }
        return map;
    }


}
