package com.qamar.rockme.helper;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by Ishaq on 5/11/2017.
 */

public class GalleryModel {

    private String ImagesUrl;
    private int type;
    private boolean ImageSelected;
    private Bitmap ImagesBitmap;
    private Uri imageUri;

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public Bitmap getImagesBitmap() {
        return ImagesBitmap;
    }

    public void setImagesBitmap(Bitmap imagesBitmap) {
        ImagesBitmap = imagesBitmap;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImagesUrl() {
        return ImagesUrl;
    }

    public void setImagesUrl(String imagesUrl) {
        ImagesUrl = imagesUrl;
    }

    public boolean isImageSelected() {
        return ImageSelected;
    }

    public void setImageSelected(boolean imageSelected) {
        ImageSelected = imageSelected;
    }
}
