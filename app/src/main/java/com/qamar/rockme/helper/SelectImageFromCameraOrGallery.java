package com.qamar.rockme.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import com.qamar.rockme.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Ishaq on 5/7/2017.
 */

public class SelectImageFromCameraOrGallery {
    public static String userChoosenTask;
    public static Uri fileUri;

    /**
     * fetch all images and videos from gallery
     *
     * @param c
     * @return
     */
    public ArrayList<GalleryModel> fetchGalleryImagesAndVideos(Context c) {
        String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID,
                MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};//get all columns of type images and videos
        String orderBy = MediaStore.Video.Media.DATE_TAKEN;//order data by date
        Cursor cursor = c.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");//get all data in Cursor by sorting in DESC order
        int dataColumnIndex;
        GalleryModel objGalleryModel;
        ArrayList<GalleryModel> galleryImageUrls = new ArrayList<GalleryModel>();//Init array

        //Loop to cursor count
        for (int i = 0; i < cursor.getCount(); i++) {
            objGalleryModel = new GalleryModel();
            cursor.moveToPosition(i);
            dataColumnIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATA);//get column index
            objGalleryModel.setImageSelected(false);
            objGalleryModel.setType(1);
            objGalleryModel.setImagesUrl(cursor.getString(dataColumnIndex));
            galleryImageUrls.add(objGalleryModel);
        }

        orderBy = MediaStore.Images.Media.DATE_TAKEN;//order data by date
        cursor = c.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");//get all data in Cursor by sorting in DESC order

        //Loop to cursor count
        for (int i = 0; i < cursor.getCount(); i++) {
            objGalleryModel = new GalleryModel();
            cursor.moveToPosition(i);
            dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);//get column index
            objGalleryModel.setImageSelected(false);
            objGalleryModel.setType(0);
            objGalleryModel.setImagesUrl(cursor.getString(dataColumnIndex));
            galleryImageUrls.add(objGalleryModel);
        }

        return galleryImageUrls;
    }

    /**
     * fetch all images from gallery
     *
     * @param c
     * @return
     */
    public ArrayList<GalleryModel> fetchGalleryImages(Context c) {
        String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};//get all columns of type images
        String orderBy = MediaStore.Images.Media.DATE_TAKEN;//order data by date
        Cursor cursor = c.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");//get all data in Cursor by sorting in DESC order
        int dataColumnIndex;
        GalleryModel objGalleryModel;
        ArrayList<GalleryModel> galleryImageUrls = new ArrayList<GalleryModel>();//Init array

        //Loop to cursor count
        for (int i = 0; i < cursor.getCount(); i++) {
            objGalleryModel = new GalleryModel();
            cursor.moveToPosition(i);
            dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);//get column index
            objGalleryModel.setImageSelected(false);
            objGalleryModel.setType(0);
            objGalleryModel.setImagesBitmap(null);
            objGalleryModel.setImageUri(ContentUris
                    .withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID))));
            objGalleryModel.setImagesUrl(cursor.getString(dataColumnIndex));
            galleryImageUrls.add(objGalleryModel);
        }

        return galleryImageUrls;
    }

    /**
     * fetch all videos from gallery
     *
     * @param c
     * @return
     */
    public ArrayList<GalleryModel> fetchGalleryVideos(Context c) {
        String[] columns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};//get all columns of type videos
        String orderBy = MediaStore.Video.Media.DATE_TAKEN;//order data by date
        Cursor cursor = c.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");//get all data in Cursor by sorting in DESC order
        int dataColumnIndex;
        GalleryModel objGalleryModel;
        ArrayList<GalleryModel> galleryImageUrls = new ArrayList<GalleryModel>();//Init array

        //Loop to cursor count
        for (int i = 0; i < cursor.getCount(); i++) {
            objGalleryModel = new GalleryModel();
            cursor.moveToPosition(i);
            dataColumnIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATA);//get column index
            objGalleryModel.setImageSelected(false);
            objGalleryModel.setType(1);
            objGalleryModel.setImagesBitmap(ThumbnailUtils.createVideoThumbnail(
                    cursor.getString(dataColumnIndex), MediaStore.Video.Thumbnails.MICRO_KIND));
            objGalleryModel.setImagesUrl(cursor.getString(dataColumnIndex));
            galleryImageUrls.add(objGalleryModel);
        }

        return galleryImageUrls;
    }

    /**
     * Used to get real path form URI
     *
     * @param uri
     * @param c
     * @return
     */
    public String getRealPathFromURI(Uri uri, Context c) {
        String result = null;
        try {
            Cursor cursor = c.getContentResolver().query(uri, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = uri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Used to get result path of captured image or video
     *
     * @param c
     * @return
     */
    public String onCaptureImageOrVideoResult(Context c) {

        galleryAddFile(c);

        return getRealPathFromURI(fileUri, c);
    }

    /**
     * Used to add file in gallery(to show)
     *
     * @param c
     */
    private void galleryAddFile(Context c) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(fileUri);
        c.sendBroadcast(mediaScanIntent);
    }

    /**
     * Used to add file in gallery(to show)
     *
     * @param c
     */
    public void galleryAddFile(Context c, String filePath, String fileType) {
        MediaScannerConnection.scanFile(c, new String[]{filePath}, new String[]{fileType}, null);
    }

    /**
     * Used to add file in gallery(to show)
     *
     * @param c
     */
    private void galleryAddPic(Context c, String path) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        c.sendBroadcast(mediaScanIntent);
    }

    /**
     * Used to get result path of selected image or video from gallery
     *
     * @param data
     * @param c
     * @return
     */
    public String onSelectFromGalleryResult(Intent data, Context c) {

//        Bitmap bm=null;
//        if (data != null) {
//            try {
//                bm = MediaStore.Images.Media.getBitmap(c.getContentResolver(), data.getData());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        return getRealPathFromURI(data.getData(), c);
    }

    /**
     * Used to show choose image option dialog in fragment
     *
     * @param context
     * @param SELECT_PICTURE
     * @param REQUEST_CAMERA
     * @param fragment
     */
    public void selectImage(final Context context, final int SELECT_PICTURE, final int REQUEST_CAMERA, final Fragment fragment) {
        final CharSequence[] items = {context.getString(R.string.take_photo), context.getString(R.string.choose_from_gallery),
                context.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = new CheckPermissions().checkPermissionExternelStorageAndCamera(context, fragment);

                if (items[item].equals(context.getString(R.string.take_photo))) {
                    userChoosenTask = context.getString(R.string.take_photo);
                    if (result)
                        cameraIntent(REQUEST_CAMERA, fragment);

                } else if (items[item].equals(context.getString(R.string.choose_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_from_gallery);
                    if (result)
                        galleryIntent(SELECT_PICTURE, fragment);

                } else if (items[item].equals(context.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Used to show choose image option dialog in activity
     *
     * @param context
     * @param SELECT_PICTURE
     * @param REQUEST_CAMERA
     * @param activity
     */
    public void selectImage(final Context context, final int SELECT_PICTURE, final int REQUEST_CAMERA, final Activity activity) {
        final CharSequence[] items = {context.getString(R.string.take_photo), context.getString(R.string.choose_from_gallery),
                context.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = new CheckPermissions().checkPermissionExternelStorageAndCamera(context, activity);

                if (items[item].equals(context.getString(R.string.take_photo))) {
                    userChoosenTask = context.getString(R.string.take_photo);
                    if (result)
                        cameraIntent(REQUEST_CAMERA, activity);

                } else if (items[item].equals(context.getString(R.string.choose_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_from_gallery);
                    if (result)
                        galleryIntent(SELECT_PICTURE, activity);

                } else if (items[item].equals(context.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Used to show choose video option dialog in fragment
     *
     * @param context
     * @param SELECT_VIDEO
     * @param REQUEST_VIDEO
     * @param fragment
     */
    public void selectVideo(final Context context, final int SELECT_VIDEO,
                            final int REQUEST_VIDEO, final Fragment fragment,
                            final int duration, final int sizeLimit) {
        final CharSequence[] items = {context.getString(R.string.take_video), context.getString(R.string.choose_video_from_gallery),
                context.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.add_video));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = new CheckPermissions().checkPermissionExternelStorageAndCamera(context, fragment);

                if (items[item].equals(context.getString(R.string.take_video))) {
                    userChoosenTask = context.getString(R.string.take_video);
                    if (result)
                        cameraIntentForVideo(REQUEST_VIDEO, fragment, duration, sizeLimit);

                } else if (items[item].equals(context.getString(R.string.choose_video_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_video_from_gallery);
                    if (result)
                        galleryIntentForVideo(SELECT_VIDEO, fragment);

                } else if (items[item].equals(context.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Used to show choose video option dialog in activity
     *
     * @param context
     * @param SELECT_VIDEO
     * @param REQUEST_VIDEO
     * @param activity
     */
    public void selectVideo(final Context context, final int SELECT_VIDEO,
                            final int REQUEST_VIDEO, final Activity activity,
                            final int duration, final int sizeLimit) {
        final CharSequence[] items = {context.getString(R.string.take_video), context.getString(R.string.choose_video_from_gallery),
                context.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.add_video));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = new CheckPermissions().checkPermissionExternelStorageAndCamera(context, activity);

                if (items[item].equals(context.getString(R.string.take_video))) {
                    userChoosenTask = context.getString(R.string.take_video);
                    if (result)
                        cameraIntentForVideo(REQUEST_VIDEO, activity, duration, sizeLimit);

                } else if (items[item].equals(context.getString(R.string.choose_video_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_video_from_gallery);
                    if (result)
                        galleryIntentForVideo(SELECT_VIDEO, activity);

                } else if (items[item].equals(context.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Used to show choose image and video option dialog in fragment
     *
     * @param context
     * @param SELECT_PICTURE
     * @param REQUEST_CAMERA
     * @param SELECT_VIDEO
     * @param REQUEST_VIDEO
     * @param duration
     * @param sizeLimit
     * @param fragment
     */
    public void selectImageAndVideo(final Context context, final int REQUEST_CAMERA, final int SELECT_PICTURE,
                                    final int REQUEST_VIDEO, final int SELECT_VIDEO,
                                    final int duration, final int sizeLimit,
                                    final Fragment fragment) {
        final CharSequence[] items =
                {context.getString(R.string.take_photo), context.getString(R.string.choose_photo_from_gallery),
                        context.getString(R.string.take_video), context.getString(R.string.choose_video_from_gallery),
                        context.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.add_photo_or_video));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = new CheckPermissions().checkPermissionExternelStorageAndCamera(context, fragment);

                if (items[item].equals(context.getString(R.string.take_photo))) {
                    userChoosenTask = context.getString(R.string.take_photo);
                    if (result)
                        cameraIntent(REQUEST_CAMERA, fragment);

                } else if (items[item].equals(context.getString(R.string.choose_photo_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_photo_from_gallery);
                    if (result)
                        galleryIntent(SELECT_PICTURE, fragment);

                } else if (items[item].equals(context.getString(R.string.take_video))) {
                    userChoosenTask = context.getString(R.string.take_video);
                    if (result)
                        cameraIntentForVideo(REQUEST_VIDEO, fragment, duration, sizeLimit);

                } else if (items[item].equals(context.getString(R.string.choose_video_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_video_from_gallery);
                    if (result)
                        galleryIntentForVideo(SELECT_VIDEO, fragment);

                } else if (items[item].equals(context.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Used to show choose image and video option dialog in activity
     *
     * @param context
     * @param SELECT_PICTURE
     * @param REQUEST_CAMERA
     * @param SELECT_VIDEO
     * @param REQUEST_VIDEO
     * @param duration
     * @param sizeLimit
     * @param activity
     */
    public void selectImageAndVideo(final Context context, final int REQUEST_CAMERA, final int SELECT_PICTURE,
                                    final int REQUEST_VIDEO, final int SELECT_VIDEO,
                                    final int duration, final int sizeLimit,
                                    final Activity activity) {
        final CharSequence[] items =
                {context.getString(R.string.take_photo), context.getString(R.string.choose_photo_from_gallery),
                        context.getString(R.string.take_video), context.getString(R.string.choose_video_from_gallery),
                        context.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.add_photo_or_video));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = new CheckPermissions().checkPermissionExternelStorageAndCamera(context, activity);

                if (items[item].equals(context.getString(R.string.take_photo))) {
                    userChoosenTask = context.getString(R.string.take_photo);
                    if (result)
                        cameraIntent(REQUEST_CAMERA, activity);

                } else if (items[item].equals(context.getString(R.string.choose_photo_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_photo_from_gallery);
                    if (result)
                        galleryIntent(SELECT_PICTURE, activity);

                } else if (items[item].equals(context.getString(R.string.take_video))) {
                    userChoosenTask = context.getString(R.string.take_video);
                    if (result)
                        cameraIntentForVideo(REQUEST_VIDEO, activity, duration, sizeLimit);

                } else if (items[item].equals(context.getString(R.string.choose_video_from_gallery))) {
                    userChoosenTask = context.getString(R.string.choose_video_from_gallery);
                    if (result)
                        galleryIntentForVideo(SELECT_VIDEO, activity);

                } else if (items[item].equals(context.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Used to call gallery intent for image in fragment
     *
     * @param SELECT_PICTURE
     * @param f
     */
    public void galleryIntent(int SELECT_PICTURE, Fragment f) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        f.startActivityForResult(Intent.createChooser(intent, f.getString(R.string.select_picture)), SELECT_PICTURE);
    }

    /**
     * Used to call gallery intent for video in fragment
     *
     * @param SELECT_VIDEO
     * @param f
     */
    public void galleryIntentForVideo(int SELECT_VIDEO, Fragment f) {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_PICK);
        f.startActivityForResult(Intent.createChooser(intent, f.getString(R.string.select_video)), SELECT_VIDEO);
    }

    /**
     * Used to call camera intent for image in fragment
     *
     * @param REQUEST_CAMERA
     * @param f
     */
    public void cameraIntent(int REQUEST_CAMERA, Fragment f) {
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.LOLLIPOP) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = Uri.fromFile(getOutputImageFile(f));

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            f.startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f.startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Used to call camera intent for video in fragment
     *
     * @param REQUEST_VIDEO
     * @param f
     */
    public void cameraIntentForVideo(int REQUEST_VIDEO, Fragment f, int duration, long sizeLimit) {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = Uri.fromFile(getOutputVideoFile(f));
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, duration);
        //intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 25000000L);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        f.startActivityForResult(intent, REQUEST_VIDEO);
    }

    /**
     * Used to call gallery intent for image in activity
     *
     * @param SELECT_PICTURE
     * @param activity
     */
    public void galleryIntent(int SELECT_PICTURE, Activity activity) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.select_picture)), SELECT_PICTURE);
    }

    /**
     * Used to call gallery intent for video in activity
     *
     * @param SELECT_VIDEO
     * @param activity
     */
    public void galleryIntentForVideo(int SELECT_VIDEO, Activity activity) {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_PICK);
        activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.select_video)), SELECT_VIDEO);
    }

    /**
     * Used to call camera intent for image in activity
     *
     * @param REQUEST_CAMERA
     * @param activity
     */
    public void cameraIntent(int REQUEST_CAMERA, Activity activity) {
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.LOLLIPOP) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = Uri.fromFile(getOutputImageFile(activity));

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            activity.startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            activity.startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Used to call camera intent for video in activity
     *
     * @param REQUEST_VIDEO
     * @param activity
     */
    public void cameraIntentForVideo(int REQUEST_VIDEO, Activity activity, int duration, long sizeLimit) {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = Uri.fromFile(getOutputVideoFile(activity));
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, duration);
        //intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 25000000L);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        activity.startActivityForResult(intent, REQUEST_VIDEO);
    }

    /**
     * Used to get Image output file path in fragment
     *
     * @param f
     * @return
     */
    private File getOutputImageFile(Fragment f) {

        File mediaFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath()
                + File.separator
                + f.getResources().getString(R.string.app_name)
                + "_IMG_" + System.currentTimeMillis() + ".jpg");

        return mediaFile;
    }

    /**
     * Used to get Video output file path in fragment
     *
     * @param f
     * @return
     */
    private File getOutputVideoFile(Fragment f) {

        File mediaFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath()
                + File.separator
                + f.getResources().getString(R.string.app_name)
                + "_VID_" + System.currentTimeMillis() + ".mp4");

        return mediaFile;
    }

    /**
     * Used to get Image output file path in activity
     *
     * @param activity
     * @return
     */
    private File getOutputImageFile(Activity activity) {

        File mediaFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath()
                + File.separator
                + activity.getResources().getString(R.string.app_name)
                + "_IMG_" + System.currentTimeMillis() + ".jpg");

        galleryAddPic(activity, mediaFile.getPath());

        return mediaFile;
    }

    /**
     * Used to get Video output file path in activity
     *
     * @param activity
     * @return
     */
    private File getOutputVideoFile(Activity activity) {

        File mediaFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath()
                + File.separator
                + activity.getResources().getString(R.string.app_name)
                + "_VID_" + System.currentTimeMillis() + ".mp4");

        galleryAddPic(activity, mediaFile.getPath());

        return mediaFile;
    }
}
