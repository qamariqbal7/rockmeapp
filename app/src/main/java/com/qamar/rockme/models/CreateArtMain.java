package com.qamar.rockme.models;

import java.util.ArrayList;

public class CreateArtMain {
    private String cat;
    private ArrayList<CreateArt> createArts;

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public ArrayList<CreateArt> getCreateArts() {
        return createArts;
    }

    public void setCreateArts(ArrayList<CreateArt> createArts) {
        this.createArts = createArts;
    }
}
