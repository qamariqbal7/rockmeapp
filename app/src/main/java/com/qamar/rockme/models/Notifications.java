package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Development on 8/1/2018.
 */

public class Notifications {

    private long timeAgo;

    public long getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(long timeAgo) {
        this.timeAgo = timeAgo;
    }

    @SerializedName("ID")
    @Expose
    private int iD;
    @SerializedName("Notification")
    @Expose
    private String notification;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("IsActive")
    @Expose
    private int isActive;
    @SerializedName("IsView")
    @Expose
    private int isView;

    public int getiD() {
        return iD;
    }

    public void setiD(int iD) {
        this.iD = iD;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsView() {
        return isView;
    }

    public void setIsView(int isView) {
        this.isView = isView;
    }
}
