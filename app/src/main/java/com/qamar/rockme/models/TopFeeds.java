package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Development on 6/29/2018.
 */

public class TopFeeds {

    @SerializedName("User")
    @Expose
    private User user;
    @SerializedName("Artwork")
    @Expose
    private Artwork artwork;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Artwork getArtwork() {
        return artwork;
    }

    public void setArtwork(Artwork artwork) {
        this.artwork = artwork;
    }
}
