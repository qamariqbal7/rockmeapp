package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Development on 6/29/2018.
 */

public class RockMeFeeds {
    private long timeAgo;

    public long getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(long timeAgo) {
        this.timeAgo = timeAgo;
    }

    @SerializedName("NewsFeedId")
    @Expose
    private int NewsFeedId;
    @SerializedName("IsLiked")
    @Expose
    private String IsLiked;
    @SerializedName("LikesCount")
    @Expose
    private String LikesCount;

    public String getIsLiked() {
        return IsLiked;
    }

    public void setIsLiked(String isLiked) {
        IsLiked = isLiked;
    }

    public String getLikesCount() {
        return LikesCount;
    }

    public void setLikesCount(String likesCount) {
        LikesCount = likesCount;
    }

    public String getCommentsCount() {
        return CommentsCount;
    }

    public void setCommentsCount(String commentsCount) {
        CommentsCount = commentsCount;
    }

    @SerializedName("CommentsCount")
    @Expose
    private String CommentsCount;
    @SerializedName("NewsFeedTitle")
    @Expose
    private String NewsFeedTitle;
    @SerializedName("NewsFeedText")
    @Expose
    private String NewsFeedText;
    @SerializedName("NewsFeedImage")
    @Expose
    private String NewsFeedImage;
    @SerializedName("ArtworkId")
    @Expose
    private int ArtworkId;
    @SerializedName("ArtworkName")
    @Expose
    private String ArtworkName;
    @SerializedName("ArtworkImage")
    @Expose
    private String ArtworkImage;
    @SerializedName("StartDate")
    @Expose
    private String StartDate;

    public String getInsertDate() {
        return InsertDate;
    }

    public void setInsertDate(String insertDate) {
        InsertDate = insertDate;
    }

    @SerializedName("NewsFeedType")
    @Expose
    private String NewsFeedType;
    @SerializedName("InsertDate")
    @Expose
    private String InsertDate;
    @SerializedName("PostType")
    @Expose
    private String PostType;
    @SerializedName("User")
    @Expose
    private User User;

    public int getNewsFeedId() {
        return NewsFeedId;
    }

    public void setNewsFeedId(int newsFeedId) {
        NewsFeedId = newsFeedId;
    }

    public String getNewsFeedTitle() {
        return NewsFeedTitle;
    }

    public void setNewsFeedTitle(String newsFeedTitle) {
        NewsFeedTitle = newsFeedTitle;
    }

    public String getNewsFeedText() {
        return NewsFeedText;
    }

    public void setNewsFeedText(String newsFeedText) {
        NewsFeedText = newsFeedText;
    }

    public String getNewsFeedImage() {
        return NewsFeedImage;
    }

    public void setNewsFeedImage(String newsFeedImage) {
        NewsFeedImage = newsFeedImage;
    }

    public int getArtworkId() {
        return ArtworkId;
    }

    public void setArtworkId(int artworkId) {
        ArtworkId = artworkId;
    }

    public String getArtworkName() {
        return ArtworkName;
    }

    public void setArtworkName(String artworkName) {
        ArtworkName = artworkName;
    }

    public String getArtworkImage() {
        return ArtworkImage;
    }

    public void setArtworkImage(String artworkImage) {
        ArtworkImage = artworkImage;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getNewsFeedType() {
        return NewsFeedType;
    }

    public void setNewsFeedType(String newsFeedType) {
        NewsFeedType = newsFeedType;
    }

    public String getPostType() {
        return PostType;
    }

    public void setPostType(String postType) {
        PostType = postType;
    }

    public com.qamar.rockme.models.User getUser() {
        return User;
    }

    public void setUser(com.qamar.rockme.models.User user) {
        User = user;
    }
}
