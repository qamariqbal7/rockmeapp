package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ishaq on 6/26/2018.
 */

public class UserData implements Serializable {

    @SerializedName("UserId")
    @Expose
    private int userId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("SocialId")
    @Expose
    private String socialId;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("IsSocial")
    @Expose
    private Boolean isSocial;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("PreviousCoinCount")
    @Expose
    private int previousCoinCount;
    @SerializedName("CurrentCoinCount")
    @Expose
    private int currentCoinCount;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("Status")
    @Expose
    private int status;
    @SerializedName("AccessToken")
    @Expose
    private String accessToken;
    @SerializedName("UserLikes")
    @Expose
    private int userLikes;
    @SerializedName("FirebaseToken")
    @Expose
    private String firebaseToken;
    @SerializedName("HasAdded")
    @Expose
    private int hasAdded;
    @SerializedName("IsLiked")
    @Expose
    private int isLiked;
    @SerializedName("Udid")
    @Expose
    private String udid;
    @SerializedName("DeviceType")
    @Expose
    private String deviceType;

    public String getVerificationCode() {
        return verificationCode;
    }

    public UserData setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
        return this;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public UserData setIsVerified(String isVerified) {
        this.isVerified = isVerified;
        return this;
    }

    @SerializedName("Token")
    @Expose
    private String token;

    @SerializedName("VerificationCode")
    @Expose
    private String verificationCode;

    @SerializedName("IsVerified")
    @Expose
    private String isVerified;


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Boolean getSocial() {
        return isSocial;
    }

    public void setSocial(Boolean social) {
        isSocial = social;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public int getPreviousCoinCount() {
        return previousCoinCount;
    }

    public void setPreviousCoinCount(int previousCoinCount) {
        this.previousCoinCount = previousCoinCount;
    }

    public int getCurrentCoinCount() {
        return currentCoinCount;
    }

    public void setCurrentCoinCount(int currentCoinCount) {
        this.currentCoinCount = currentCoinCount;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getUserLikes() {
        return userLikes;
    }

    public void setUserLikes(int userLikes) {
        this.userLikes = userLikes;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public int getHasAdded() {
        return hasAdded;
    }

    public void setHasAdded(int hasAdded) {
        this.hasAdded = hasAdded;
    }

    public int getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
