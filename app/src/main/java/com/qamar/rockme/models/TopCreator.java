package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopCreator {

    /*
                "UserID": 1,
                "UserName": "@rockme",
                "FullName": "rockme",
                "ProfilePicture": "",
                "Ranking": 1,
                "ArtworkCount": 40*/

    @SerializedName("UserID")
    @Expose
    private String userId;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("Ranking")
    @Expose
    private String ranking;
    @SerializedName("ArtworkCount")
    @Expose
    private String artworkCount;

    public TopCreator() {
    }

    public TopCreator(String userId, String userName, String profilePicture, String artworkCount) {
        this.userId = userId;
        this.userName = userName;
        this.profilePicture = profilePicture;
        this.artworkCount = artworkCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getArtworkCount() {
        return artworkCount;
    }

    public void setArtworkCount(String artworkCount) {
        this.artworkCount = artworkCount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }
}
