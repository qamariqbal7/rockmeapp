package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Development on 7/13/2017.
 */

public class MainArtwork implements Serializable {

    private ArrayList<Artwork> arrayListArtwork = null;

    public ArrayList<Artwork> getArrayListArtwork() {
        return arrayListArtwork;
    }

    public void setArrayListArtwork(ArrayList<Artwork> arrayListArtwork) {
        this.arrayListArtwork = arrayListArtwork;
    }

    private String labelArtwork;

    public String getLabelArtwork() {
        return labelArtwork;
    }

    public void setLabelArtwork(String labelArtwork) {
        this.labelArtwork = labelArtwork;
    }

    @SerializedName("PrivateArtworks")
    @Expose
    private ArrayList<Artwork> PrivateArtworks = null;
    @SerializedName("SharedArtworks")
    @Expose
    private ArrayList<Artwork> SharedArtworks = null;
    @SerializedName("PublicArtworks")
    @Expose
    private ArrayList<Artwork> PublicArtworks = null;
    @SerializedName("PendingArtworks")
    @Expose
    private ArrayList<Artwork> PendingArtworks = null;
    @SerializedName("PurchasedArtworks")
    @Expose
    private ArrayList<Artwork> PurchasedArtworks = null;

    public ArrayList<Artwork> getPrivateArtworks() {
        return PrivateArtworks;
    }

    public void setPrivateArtworks(ArrayList<Artwork> privateArtworks) {
        PrivateArtworks = privateArtworks;
    }

    public ArrayList<Artwork> getSharedArtworks() {
        return SharedArtworks;
    }

    public void setSharedArtworks(ArrayList<Artwork> sharedArtworks) {
        SharedArtworks = sharedArtworks;
    }

    public ArrayList<Artwork> getPublicArtworks() {
        return PublicArtworks;
    }

    public void setPublicArtworks(ArrayList<Artwork> publicArtworks) {
        PublicArtworks = publicArtworks;
    }

    public ArrayList<Artwork> getPendingArtworks() {
        return PendingArtworks;
    }

    public void setPendingArtworks(ArrayList<Artwork> pendingArtworks) {
        PendingArtworks = pendingArtworks;
    }

    public ArrayList<Artwork> getPurchasedArtworks() {
        return PurchasedArtworks;
    }

    public void setPurchasedArtworks(ArrayList<Artwork> purchasedArtworks) {
        PurchasedArtworks = purchasedArtworks;
    }
}
