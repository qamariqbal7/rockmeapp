package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Development on 7/6/2018.
 */

public class MyGadgets implements Serializable {

    @SerializedName("userGadgetId")
    @Expose
    private int userGadgetId;
    @SerializedName("userGadgetName")
    @Expose
    private String userGadgetName;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("gadgetStockId")
    @Expose
    private int gadgetStockId;
    @SerializedName("FirebaseToken")
    @Expose
    private String firebaseToken;
    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("macAddress")
    @Expose
    private String macAddress;
    @SerializedName("gadgetPicture")
    @Expose
    private String gadgetPicture;
    @SerializedName("gadgetSvg")
    @Expose
    private String gadgetSvg;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("SharedGadgetId")
    @Expose
    private int sharedGadgetId;
    @SerializedName("SharedWith")
    @Expose
    private int sharedWith;
    @SerializedName("SharedDate")
    @Expose
    private String sharedDate;
    @SerializedName("GadgetName")
    @Expose
    private String gadgetName;

    public int getSharedGadgetId() {
        return sharedGadgetId;
    }

    public void setSharedGadgetId(int sharedGadgetId) {
        this.sharedGadgetId = sharedGadgetId;
    }

    public int getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(int sharedWith) {
        this.sharedWith = sharedWith;
    }

    public String getSharedDate() {
        return sharedDate;
    }

    public void setSharedDate(String sharedDate) {
        this.sharedDate = sharedDate;
    }

    public String getGadgetName() {
        return gadgetName;
    }

    public void setGadgetName(String gadgetName) {
        this.gadgetName = gadgetName;
    }

    public int getUserGadgetId() {
        return userGadgetId;
    }

    public void setUserGadgetId(int userGadgetId) {
        this.userGadgetId = userGadgetId;
    }

    public String getUserGadgetName() {
        return userGadgetName;
    }

    public void setUserGadgetName(String userGadgetName) {
        this.userGadgetName = userGadgetName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGadgetStockId() {
        return gadgetStockId;
    }

    public void setGadgetStockId(int gadgetStockId) {
        this.gadgetStockId = gadgetStockId;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getGadgetPicture() {
        return gadgetPicture;
    }

    public void setGadgetPicture(String gadgetPicture) {
        this.gadgetPicture = gadgetPicture;
    }

    public String getGadgetSvg() {
        return gadgetSvg;
    }

    public void setGadgetSvg(String gadgetSvg) {
        this.gadgetSvg = gadgetSvg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
