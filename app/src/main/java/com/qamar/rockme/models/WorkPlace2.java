package com.qamar.rockme.models;

import java.util.ArrayList;

public class WorkPlace2 {

    private String title;
    private ArrayList<WorkPlace> workPlaces;

    public WorkPlace2() {
    }

    public WorkPlace2(String title, ArrayList<WorkPlace> workPlaces) {
        this.title = title;
        this.workPlaces = workPlaces;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<WorkPlace> getWorkPlaces() {
        return workPlaces;
    }

    public void setWorkPlaces(ArrayList<WorkPlace> workPlaces) {
        this.workPlaces = workPlaces;
    }
}
