package com.qamar.rockme.models;

public class Popular {
    private String ArtworkID,ArtworkName,ArtworkPicture,Category,UserName,ProfilePicture,ArtworkViews
            ,ArtworkAmount;

    public Popular() {
    }

    public Popular(String artworkID, String artworkName, String artworkPicture, String category, String userName, String profilePicture, String artworkViews, String artworkAmount) {
        ArtworkID = artworkID;
        ArtworkName = artworkName;
        ArtworkPicture = artworkPicture;
        Category = category;
        UserName = userName;
        ProfilePicture = profilePicture;
        ArtworkViews = artworkViews;
        ArtworkAmount = artworkAmount;
    }

    public String getArtworkID() {
        return ArtworkID;
    }

    public void setArtworkID(String artworkID) {
        ArtworkID = artworkID;
    }

    public String getArtworkName() {
        return ArtworkName;
    }

    public void setArtworkName(String artworkName) {
        ArtworkName = artworkName;
    }

    public String getArtworkPicture() {
        return ArtworkPicture;
    }

    public void setArtworkPicture(String artworkPicture) {
        ArtworkPicture = artworkPicture;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getArtworkViews() {
        return ArtworkViews;
    }

    public void setArtworkViews(String artworkViews) {
        ArtworkViews = artworkViews;
    }

    public String getArtworkAmount() {
        return ArtworkAmount;
    }

    public void setArtworkAmount(String artworkAmount) {
        ArtworkAmount = artworkAmount;
    }
}
