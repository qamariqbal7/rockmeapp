package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Development on 8/2/2018.
 */

public class SharedGadgetUsersList {

    @SerializedName("GadgetId")
    @Expose
    private int gadgetId;
    @SerializedName("SharedDate")
    @Expose
    private String sharedDate;
    @SerializedName("User")
    @Expose
    private User user;

    public int getGadgetId() {
        return gadgetId;
    }

    public void setGadgetId(int gadgetId) {
        this.gadgetId = gadgetId;
    }

    public String getSharedDate() {
        return sharedDate;
    }

    public void setSharedDate(String sharedDate) {
        this.sharedDate = sharedDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SharedGadgetUsersList(int gadgetId, String sharedDate, User user) {
        this.gadgetId = gadgetId;
        this.sharedDate = sharedDate;
        this.user = user;
    }

    public SharedGadgetUsersList() {
    }

    @SerializedName("UserId")
    @Expose
    private String UserId;

    @SerializedName("Username")
    @Expose
    private String Username;

    @SerializedName("FullName")
    @Expose
    private String FullName;

    @SerializedName("ProfilePicture")
    @Expose
    private String ProfilePicture;

    @SerializedName("HasShared")
    @Expose
    private String HasShared;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getHasShared() {
        return HasShared;
    }

    public void setHasShared(String hasShared) {
        HasShared = hasShared;
    }
}
