package com.qamar.rockme.models;

import java.io.Serializable;

/**
 * Created by Development on 11/19/2017.
 */

public class SelectColorModel implements Serializable {

    private int red;
    private int green;
    private int blue;

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    private int selectColor;
    private boolean checkSelected;

    public SelectColorModel() {
    }

    public int getSelectColor() {
        return selectColor;
    }

    public void setSelectColor(int selectColor) {
        this.selectColor = selectColor;
    }

    public boolean isCheckSelected() {
        return checkSelected;
    }

    public void setCheckSelected(boolean checkSelected) {
        this.checkSelected = checkSelected;
    }
}
