package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Development on 7/13/2017.
 */

public class MainGadgets implements Serializable {

    private ArrayList<MyGadgets> arrayListGadgets = new ArrayList<>();

    public ArrayList<MyGadgets> getArrayListGadgets() {
        return arrayListGadgets;
    }

    public void setArrayListGadgets(ArrayList<MyGadgets> arrayListGadgets) {
        this.arrayListGadgets = arrayListGadgets;
    }

    private String labelGadgets;

    public String getLabelGadgets() {
        return labelGadgets;
    }

    public void setLabelGadgets(String labelGadgets) {
        this.labelGadgets = labelGadgets;
    }

    @SerializedName("PrivateGadgets")
    @Expose
    private ArrayList<MyGadgets> PrivateGadgets = null;
    @SerializedName("SharedGadgets")
    @Expose
    private ArrayList<MyGadgets> SharedGadgets = null;

    @SerializedName("Angry Bird")
    @Expose
    private ArrayList<MyGadgets> angryBirds = null;

    public ArrayList<MyGadgets> getPrivateGadgets() {
        return PrivateGadgets;
    }

    public void setPrivateGadgets(ArrayList<MyGadgets> privateGadgets) {
        PrivateGadgets = privateGadgets;
    }

    public ArrayList<MyGadgets> getSharedGadgets() {
        return SharedGadgets;
    }

    public void setSharedGadgets(ArrayList<MyGadgets> sharedGadgets) {
        SharedGadgets = sharedGadgets;
    }

    public ArrayList<MyGadgets> getAngryBirds() {
        return angryBirds;
    }

    public void setAngryBirds(ArrayList<MyGadgets> angryBirds) {
        this.angryBirds = angryBirds;
    }
}
