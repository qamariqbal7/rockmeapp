package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Development on 7/10/2018.
 */

public class Packages {

    @SerializedName("BundleId")
    @Expose
    private int bundleId;
    @SerializedName("BundleName")
    @Expose
    private String bundleName;
    @SerializedName("BundleImage")
    @Expose
    private String bundleImage;
    @SerializedName("BundleAmount")
    @Expose
    private int bundleAmount;
    @SerializedName("BundleCoints")
    @Expose
    private int bundleCoints;
    @SerializedName("Status")
    @Expose
    private int status;

    public int getBundleId() {
        return bundleId;
    }

    public void setBundleId(int bundleId) {
        this.bundleId = bundleId;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getBundleImage() {
        return bundleImage;
    }

    public void setBundleImage(String bundleImage) {
        this.bundleImage = bundleImage;
    }

    public int getBundleAmount() {
        return bundleAmount;
    }

    public void setBundleAmount(int bundleAmount) {
        this.bundleAmount = bundleAmount;
    }

    public int getBundleCoints() {
        return bundleCoints;
    }

    public void setBundleCoints(int bundleCoints) {
        this.bundleCoints = bundleCoints;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
