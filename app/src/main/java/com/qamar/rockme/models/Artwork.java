package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Development on 6/29/2018.
 */
public class Artwork implements Serializable {

    private long timeAgo;

    public long getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(long timeAgo) {
        this.timeAgo = timeAgo;
    }

    @SerializedName("ArtworkId")
    @Expose
    private int artworkId;
    @SerializedName("ArtworkName")
    @Expose
    private String artworkName;
    @SerializedName("CharacterId")
    @Expose
    private int characterId;
    @SerializedName("CharacterName")
    @Expose
    private String characterName;
    @SerializedName("CategoryId")
    @Expose
    private int categoryId;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("UserId")
    @Expose
    private int userId;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("LastModified")
    @Expose
    private String lastModified;
    @SerializedName("ArtworkAmount")
    @Expose
    private int artworkAmount;
    @SerializedName("Status")
    @Expose
    private int status;
    @SerializedName("ArtworkFile")
    @Expose
    private String artworkFile;
    @SerializedName("ArtworkImage")
    @Expose
    private String artworkImage;
    @SerializedName("UserGadgetId")
    @Expose
    private int userGadgetId;
    @SerializedName("GadgetCommand")
    @Expose
    private String gadgetCommand;
    @SerializedName("Likes")
    @Expose
    private int likes;
    @SerializedName("Shares")
    @Expose
    private int shares;
    @SerializedName("GadgetId")
    @Expose
    private int gadgetId;
    @SerializedName("GadgetName")
    @Expose
    private String gadgetName;
    @SerializedName("IsFeatured")
    @Expose
    private int isFeatured;
    @SerializedName("IsLiked")
    @Expose
    private int isLiked;
    @SerializedName("IsShared")
    @Expose
    private int isShared;
    @SerializedName("IsPurchased")
    @Expose
    private int isPurchased;

    public Artwork() {
    }

    public Artwork(long timeAgo, int artworkId, String artworkName, int characterId, String characterName, int categoryId, String categoryName, int userId, String createDate, String lastModified, int artworkAmount, int status, String artworkFile, String artworkImage, int userGadgetId, String gadgetCommand, int likes, int shares, int gadgetId, String gadgetName, int isFeatured, int isLiked, int isShared, int isPurchased) {
        this.timeAgo = timeAgo;
        this.artworkId = artworkId;
        this.artworkName = artworkName;
        this.characterId = characterId;
        this.characterName = characterName;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.userId = userId;
        this.createDate = createDate;
        this.lastModified = lastModified;
        this.artworkAmount = artworkAmount;
        this.status = status;
        this.artworkFile = artworkFile;
        this.artworkImage = artworkImage;
        this.userGadgetId = userGadgetId;
        this.gadgetCommand = gadgetCommand;
        this.likes = likes;
        this.shares = shares;
        this.gadgetId = gadgetId;
        this.gadgetName = gadgetName;
        this.isFeatured = isFeatured;
        this.isLiked = isLiked;
        this.isShared = isShared;
        this.isPurchased = isPurchased;
    }

    public int getArtworkId() {
        return artworkId;
    }

    public void setArtworkId(int artworkId) {
        this.artworkId = artworkId;
    }

    public String getArtworkName() {
        return artworkName;
    }

    public void setArtworkName(String artworkName) {
        this.artworkName = artworkName;
    }

    public int getCharacterId() {
        return characterId;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public int getArtworkAmount() {
        return artworkAmount;
    }

    public void setArtworkAmount(int artworkAmount) {
        this.artworkAmount = artworkAmount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getArtworkFile() {
        return artworkFile;
    }

    public void setArtworkFile(String artworkFile) {
        this.artworkFile = artworkFile;
    }

    public String getArtworkImage() {
        return artworkImage;
    }

    public void setArtworkImage(String artworkImage) {
        this.artworkImage = artworkImage;
    }

    public int getUserGadgetId() {
        return userGadgetId;
    }

    public void setUserGadgetId(int userGadgetId) {
        this.userGadgetId = userGadgetId;
    }

    public String getGadgetCommand() {
        return gadgetCommand;
    }

    public void setGadgetCommand(String gadgetCommand) {
        this.gadgetCommand = gadgetCommand;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public int getGadgetId() {
        return gadgetId;
    }

    public void setGadgetId(int gadgetId) {
        this.gadgetId = gadgetId;
    }

    public String getGadgetName() {
        return gadgetName;
    }

    public void setGadgetName(String gadgetName) {
        this.gadgetName = gadgetName;
    }

    public int getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(int isFeatured) {
        this.isFeatured = isFeatured;
    }

    public int getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public int getIsShared() {
        return isShared;
    }

    public void setIsShared(int isShared) {
        this.isShared = isShared;
    }

    public int getIsPurchased() {
        return isPurchased;
    }

    public void setIsPurchased(int isPurchased) {
        this.isPurchased = isPurchased;
    }
}
