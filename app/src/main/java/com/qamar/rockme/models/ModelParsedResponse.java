package com.qamar.rockme.models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ishaq on 3/8/2017.
 */

public class ModelParsedResponse {
    @SerializedName("error")
    @Expose
    private Error error;

    @SerializedName("response")
    @Expose
    private Response response;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public String getMessages() {
        String messages = "";

        ArrayList<String> l = response != null ? response.getMessages() : error.getMessages();
        if (l != null && l.size() > 0)
            for (String s : l)
                if(messages.equals(""))
                    messages = s;
                else
                    messages = messages + "\n" + s;

        return messages;
    }

    public int getCode() {
        try {
            return response != null ? response.getCode() : error.getCode();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class Error {

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("messages")
        @Expose
        private ArrayList<String> messages = new ArrayList<>();

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public ArrayList<String> getMessages() {
            return messages;
        }

        public void setMessages(ArrayList<String> messages) {
            this.messages = messages;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }

    }


    public class Response {

        @SerializedName("code")
        @Expose
        private Integer code;

        @SerializedName("messages")
        @Expose
        private ArrayList<String> messages = new ArrayList<>();

        @SerializedName("data")
        @Expose
        private JsonElement data = null;

        @SerializedName("pagination")
        @Expose
        private Pagination pagination;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public ArrayList<String> getMessages() {
            return messages;
        }

        public void setMessages(ArrayList<String> messages) {
            this.messages = messages;
        }

        public JsonElement getData() {
            return data;
        }

        public void setData(JsonElement data) {
            this.data = data;
        }

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }

    }

    public class Pagination implements Serializable {

        private static final long serialVersionUID = 7091149282267667438L;

        @SerializedName("total")
        private int total = 0;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getCurrent() {
            return current;
        }

        public void setCurrent(int current) {
            this.current = current;
        }

        public int getFirst() {
            return first;
        }

        public void setFirst(int first) {
            this.first = first;
        }

        public int getLast() {
            return last;
        }

        public void setLast(int last) {
            this.last = last;
        }

        public int getPervious() {
            return pervious;
        }

        public void setPervious(int pervious) {
            this.pervious = pervious;
        }

        public int getNext() {
            return next;
        }

        public void setNext(int next) {
            this.next = next;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public ArrayList<Integer> getPages() {
            return pages;
        }

        public void setPages(ArrayList<Integer> pages) {
            this.pages = pages;
        }

        @SerializedName("current")
        private int current = 0;

        @SerializedName("first")
        private int first = 0;

        @SerializedName("last")
        private int last = 0;

        @SerializedName("pervious")
        private int pervious = 0;

        @SerializedName("next")
        private int next = 0;

        @SerializedName("from")
        private int from = 0;

        @SerializedName("to")
        private int to = 0;

        @SerializedName("pages")
        private ArrayList<Integer> pages;

    }
}
