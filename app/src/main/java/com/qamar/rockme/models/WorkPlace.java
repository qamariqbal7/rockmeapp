package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Development on 7/9/2018.
 */

public class WorkPlace implements Serializable {

   /* private int img;
    private String webViewUrl;
    private int index;*/

    @SerializedName("ArtworkID")
    @Expose
    private int artworkId;
    @SerializedName("ArtworkPicture")
    @Expose
    private String artworkImage;
    @SerializedName("Category")
    @Expose
    private String Category;
    @SerializedName("ArtworkFile")
    @Expose
    private String artworkFile;
    @SerializedName("ArtworkFileiOS")
    @Expose
    private String artworkFileiOS;
    @SerializedName("GadgetCommand")
    @Expose
    private String gadgetCommand;

    @SerializedName("ArtworkName")
    @Expose
    private String ArtworkName;
    @SerializedName("ArtworkAmount")
    @Expose
    private String ArtworkAmount;
    @SerializedName("ArtworkViews")
    @Expose
    private String ArtworkViews;
    @SerializedName("UserName")
    @Expose
    private String UserName;

    public WorkPlace() {
    }

    public WorkPlace(int artworkId, String artworkImage, String artworkFile, String artworkFileiOS, String gadgetCommand, String artworkName, String artworkAmount) {
        this.artworkId = artworkId;
        this.artworkImage = artworkImage;
        this.artworkFile = artworkFile;
        this.artworkFileiOS = artworkFileiOS;
        this.gadgetCommand = gadgetCommand;
        ArtworkName = artworkName;
        ArtworkAmount = artworkAmount;
    }

    public int getArtworkId() {
        return artworkId;
    }

    public void setArtworkId(int artworkId) {
        this.artworkId = artworkId;
    }

    public String getArtworkImage() {
        return artworkImage;
    }

    public void setArtworkImage(String artworkImage) {
        this.artworkImage = artworkImage;
    }

    public String getArtworkFile() {
        return artworkFile;
    }

    public void setArtworkFile(String artworkFile) {
        this.artworkFile = artworkFile;
    }

    public String getArtworkFileiOS() {
        return artworkFileiOS;
    }

    public void setArtworkFileiOS(String artworkFileiOS) {
        this.artworkFileiOS = artworkFileiOS;
    }

    public String getGadgetCommand() {
        return gadgetCommand;
    }

    public void setGadgetCommand(String gadgetCommand) {
        this.gadgetCommand = gadgetCommand;
    }

    public String getArtworkName() {
        return ArtworkName;
    }

    public void setArtworkName(String artworkName) {
        ArtworkName = artworkName;
    }

    public String getArtworkAmount() {
        return ArtworkAmount;
    }

    public void setArtworkAmount(String artworkAmount) {
        ArtworkAmount = artworkAmount;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getArtworkViews() {
        return ArtworkViews;
    }

    public void setArtworkViews(String artworkViews) {
        ArtworkViews = artworkViews;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    @SerializedName("GadgetId")
    @Expose
    private int gadgetId;
    @SerializedName("GadgetPicture")
    @Expose
    private String gadgetPicture;
    @SerializedName("GadgetHtml")
    @Expose
    private String gadgetHtml;
    @SerializedName("GadgetHtmliOS")
    @Expose
    private String gadgetHtmliOS;

    public int getGadgetId() {
        return gadgetId;
    }

    public void setGadgetId(int gadgetId) {
        this.gadgetId = gadgetId;
    }

    public String getGadgetPicture() {
        return gadgetPicture;
    }

    public void setGadgetPicture(String gadgetPicture) {
        this.gadgetPicture = gadgetPicture;
    }

    public String getGadgetHtml() {
        return gadgetHtml;
    }

    public void setGadgetHtml(String gadgetHtml) {
        this.gadgetHtml = gadgetHtml;
    }

    public String getGadgetHtmliOS() {
        return gadgetHtmliOS;
    }

    public void setGadgetHtmliOS(String gadgetHtmliOS) {
        this.gadgetHtmliOS = gadgetHtmliOS;
    }
}
