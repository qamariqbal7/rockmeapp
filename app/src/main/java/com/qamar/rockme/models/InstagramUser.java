package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Development on 7/5/2018.
 */

public class InstagramUser implements Serializable {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("profile_picture")
        @Expose
        private String profile_picture;
        @SerializedName("full_name")
        @Expose
        private String full_name;
        @SerializedName("bio")
        @Expose
        private String bio;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("is_business")
        @Expose
        private Boolean is_business;
        @SerializedName("counts")
        @Expose
        private Counts counts;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getProfile_picture() {
            return profile_picture;
        }

        public void setProfile_picture(String profile_picture) {
            this.profile_picture = profile_picture;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public Boolean getIs_business() {
            return is_business;
        }

        public void setIs_business(Boolean is_business) {
            this.is_business = is_business;
        }

        public Counts getCounts() {
            return counts;
        }

        public void setCounts(Counts counts) {
            this.counts = counts;
        }

    }

    public class Counts {

        @SerializedName("media")
        @Expose
        private Integer media;
        @SerializedName("follows")
        @Expose
        private Integer follows;
        @SerializedName("followed_by")
        @Expose
        private Integer followed_by;

        public Integer getMedia() {
            return media;
        }

        public void setMedia(Integer media) {
            this.media = media;
        }

        public Integer getFollows() {
            return follows;
        }

        public void setFollows(Integer follows) {
            this.follows = follows;
        }

        public Integer getFollowed_by() {
            return followed_by;
        }

        public void setFollowed_by(Integer followed_by) {
            this.followed_by = followed_by;
        }

    }
}
