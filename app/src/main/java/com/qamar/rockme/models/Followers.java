package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Development on 7/1/2018.
 */

public class Followers {

    private boolean select;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    private String firstCharacter = "";

    public String getFirstCharacter() {
        return firstCharacter;
    }

    public void setFirstCharacter(String firstCharacter) {
        this.firstCharacter = firstCharacter;
    }

    @SerializedName("FollowerId")
    @Expose
    private int followerId;
    @SerializedName("UserId")
    @Expose
    private int userId;
    @SerializedName("FollowedBy")
    @Expose
    private int followedBy;
    @SerializedName("FollowerName")
    @Expose
    private String followerName;
    @SerializedName("FollowDate")
    @Expose
    private String followDate;
    @SerializedName("SocialId")
    @Expose
    private String socialId;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("HasAdded")
    @Expose
    private int hasAdded;

    public int getFollowerId() {
        return followerId;
    }

    public void setFollowerId(int followerId) {
        this.followerId = followerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFollowedBy() {
        return followedBy;
    }

    public void setFollowedBy(int followedBy) {
        this.followedBy = followedBy;
    }

    public String getFollowerName() {
        return followerName;
    }

    public void setFollowerName(String followerName) {
        this.followerName = followerName;
    }

    public String getFollowDate() {
        return followDate;
    }

    public void setFollowDate(String followDate) {
        this.followDate = followDate;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public int getHasAdded() {
        return hasAdded;
    }

    public void setHasAdded(int hasAdded) {
        this.hasAdded = hasAdded;
    }

    public Followers() {
    }

    public Followers(boolean select, String firstCharacter, int followerId, int userId, int followedBy, String followerName, String followDate, String socialId, String emailAddress, String profilePicture, int hasAdded) {
        this.select = select;
        this.firstCharacter = firstCharacter;
        this.followerId = followerId;
        this.userId = userId;
        this.followedBy = followedBy;
        this.followerName = followerName;
        this.followDate = followDate;
        this.socialId = socialId;
        this.emailAddress = emailAddress;
        this.profilePicture = profilePicture;
        this.hasAdded = hasAdded;
    }
}
