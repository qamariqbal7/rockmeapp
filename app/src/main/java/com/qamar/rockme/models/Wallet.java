package com.qamar.rockme.models;

public class Wallet {

    private String History,ActionDate,Amount;

    public Wallet() {
    }

    public Wallet(String history, String actionDate, String amount) {
        History = history;
        ActionDate = actionDate;
        Amount = amount;
    }

    public String getHistory() {
        return History;
    }

    public void setHistory(String history) {
        History = history;
    }

    public String getActionDate() {
        return ActionDate;
    }

    public void setActionDate(String actionDate) {
        ActionDate = actionDate;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
