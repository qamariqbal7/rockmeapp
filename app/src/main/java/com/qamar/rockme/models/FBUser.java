package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ishaq on 1/17/2017.
 */

public class FBUser implements Serializable {


    private static final long serialVersionUID = 1082272362609277108L;

    @SerializedName("password")
    static String password = "";

    @SerializedName("fullname")
    private String fullName = "";

    static String title = "";

    @SerializedName("fbConnected")
    static boolean fbConnected = false;

    @SerializedName("facebook_id")
    private String FBId;

    @SerializedName("image_url")
    private String imageURL;

    @SerializedName("fb_access_token")
    private String fbAccessToken;

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("describe")
    @Expose
    private String describe="";
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;

    @SerializedName("signup_via")
    @Expose
    private String signupVia;

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        FBUser.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public static String getTitle() {
        return title;
    }

    public static void setTitle(String title) {
        FBUser.title = title;
    }

    public static boolean isFbConnected() {
        return fbConnected;
    }

    public static void setFbConnected(boolean fbConnected) {
        FBUser.fbConnected = fbConnected;
    }

    public String getFBId() {
        return FBId;
    }

    public void setFBId(String FBId) {
        this.FBId = FBId;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getFbAccessToken() {
        return fbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken) {
        this.fbAccessToken = fbAccessToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getSignupVia() {
        return signupVia;
    }

    public void setSignupVia(String signupVia) {
        this.signupVia = signupVia;
    }




}