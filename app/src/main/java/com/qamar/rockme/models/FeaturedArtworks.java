package com.qamar.rockme.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Development on 6/29/2018.
 */

public class FeaturedArtworks {

    @SerializedName("FeaturedArtworkId")
    @Expose
    private int featuredArtworkId;
    @SerializedName("FeaturedAmount")
    @Expose
    private int featuredAmount;
    @SerializedName("InsertId")
    @Expose
    private int insertId;
    @SerializedName("InsertDate")
    @Expose
    private String insertDate;
    @SerializedName("EditId")
    @Expose
    private int editId;
    @SerializedName("EditDate")
    @Expose
    private String editDate;
    @SerializedName("Status")
    @Expose
    private int status;
    @SerializedName("Artwork")
    @Expose
    private Artwork artwork;

    public int getFeaturedArtworkId() {
        return featuredArtworkId;
    }

    public void setFeaturedArtworkId(int featuredArtworkId) {
        this.featuredArtworkId = featuredArtworkId;
    }

    public int getFeaturedAmount() {
        return featuredAmount;
    }

    public void setFeaturedAmount(int featuredAmount) {
        this.featuredAmount = featuredAmount;
    }

    public int getInsertId() {
        return insertId;
    }

    public void setInsertId(int insertId) {
        this.insertId = insertId;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public int getEditId() {
        return editId;
    }

    public void setEditId(int editId) {
        this.editId = editId;
    }

    public String getEditDate() {
        return editDate;
    }

    public void setEditDate(String editDate) {
        this.editDate = editDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Artwork getArtwork() {
        return artwork;
    }

    public void setArtwork(Artwork artwork) {
        this.artwork = artwork;
    }
}
