package com.qamar.rockme.models;

public class CreateArt {

    private String GadgetID;
    private String GadgetName;
    private String GadgetPicture;

    public String getGadgetString() {
        return GadgetString;
    }

    public void setGadgetString(String gadgetString) {
        GadgetString = gadgetString;
    }

    private String GadgetString;

    public String getGadgetID() {
        return GadgetID;
    }

    public void setGadgetID(String gadgetID) {
        GadgetID = gadgetID;
    }

    public String getGadgetName() {
        return GadgetName;
    }

    public void setGadgetName(String gadgetName) {
        GadgetName = gadgetName;
    }

    public String getGadgetPicture() {
        return GadgetPicture;
    }

    public void setGadgetPicture(String gadgetPicture) {
        GadgetPicture = gadgetPicture;
    }
}
