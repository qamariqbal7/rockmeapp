package com.qamar.rockme.models;

public class Like {
    private String ArtworkID,ArtworkName,ArtworkPicture,Likes;

    public String getArtworkID() {
        return ArtworkID;
    }

    public void setArtworkID(String artworkID) {
        ArtworkID = artworkID;
    }

    public String getArtworkName() {
        return ArtworkName;
    }

    public void setArtworkName(String artworkName) {
        ArtworkName = artworkName;
    }

    public String getArtworkPicture() {
        return ArtworkPicture;
    }

    public void setArtworkPicture(String artworkPicture) {
        ArtworkPicture = artworkPicture;
    }

    public String getLikes() {
        return Likes;
    }

    public void setLikes(String likes) {
        Likes = likes;
    }
}
