package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.AdapterArtCreator;
import com.qamar.rockme.adapters.WorkPlaceAdapter2;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.CreateArt;
import com.qamar.rockme.models.CreateArtMain;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.WorkPlace;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArtCreatorFragment extends BaseFragment implements ServiceResponse {

    private View rootView;
    private WorkPlaceAdapter2 mWorkPlaceAdapter;
    private RecyclerView rvArtWork;
    private ArrayList<WorkPlace> arrayListWorkPlace = new ArrayList<>();
    private WorkPlace mWorkPlace;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private ArrayList<CreateArtMain> createArtMains;
    private AdapterArtCreator adapterArtCreator;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_are_creator,null,false);

        setRecyclerView();
        intializeControlsAndListener();
        return rootView;
    }

    private void intializeControlsAndListener()
    {
        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
    }

    private void setRecyclerView()
    {
        rvArtWork = rootView.findViewById(R.id.rvArtWork);
        rvArtWork.setLayoutManager(new LinearLayoutManager(getActivity()));
        createArtMains = new ArrayList<>();
        adapterArtCreator = new AdapterArtCreator(getActivity(),createArtMains);
        rvArtWork.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvArtWork.setAdapter(adapterArtCreator);

        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();

        requestHttpCall(1, "");
    }


    @Override
    public void onResult(int type, HttpResponse o) {
      if(type==1)
      {
          try {
              JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
              //while (jsonObject.keys().hasNext())

              Iterator<String> iter = jsonObject.keys();

              while (iter.hasNext()) {
                String key = iter.next();
                ArrayList<CreateArt> createArts = new ArrayList<>();
                JSONArray jsonArray = jsonObject.getJSONArray(key);
                 for(int i = 0; i<jsonArray.length(); i++)
                 {
                     JSONObject object = jsonArray.getJSONObject(i);
                     CreateArt createArt = new Gson().fromJson((object.toString()), CreateArt.class);
                     createArts.add(createArt);
                 }
                 if(!createArts.isEmpty())
                 {
                     CreateArtMain createArtMain = new CreateArtMain();
                     createArtMain.setCat(key);
                     createArtMain.setCreateArts(createArts);
                     createArtMains.add(createArtMain);
                 }

              }
              adapterArtCreator.notifyDataSetChanged();

          } catch (JSONException e) {
              e.printStackTrace();
          }
      }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {
            mKProgressHUD.dismiss();
            try {
                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.createart),
                                null,
                                new CallBackRetrofit(type,
                                        ArtCreatorFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
