package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.LikesAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreatorProfile extends BaseFragment implements ServiceResponse ,View.OnClickListener {

    private View rootView;
    String UserId = "";

    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private ArrayList<Artwork> artworks = new ArrayList<>();
    LikesAdapter likesAdapter;
    private CircleImageView profile;
    private TextView tvName,tvAddFollower,tvUserName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_service_provider_profile,null);
        intializeControlsAndListener();
        setRecyclerView();

        return rootView;
    }

    private void intializeControlsAndListener()
    {
        Bundle bundle = getArguments();
        UserId = bundle.getString("UserId");

        ImageView ivMenu1 = rootView.findViewById(R.id.ivMenu1);

        profile = rootView.findViewById(R.id.profile_image2);
        tvName = rootView.findViewById(R.id.tvFeedTittleOld);
        tvUserName = rootView.findViewById(R.id.tvUserName);
        tvAddFollower = rootView.findViewById(R.id.tvAddFollower);
        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
        setBackEvent(ivMenu1);
        mKProgressHUD = KProgressHUD.create(getActivity());
        tvAddFollower.setOnClickListener(this);
    }


    private void setRecyclerView()
    {
        RecyclerView rvArtWork = rootView.findViewById(R.id.rvArtWork);
        rvArtWork.setLayoutManager(new GridLayoutManager(getActivity(),2));
        likesAdapter = new LikesAdapter(getActivity(),artworks,false);
        rvArtWork.setAdapter(likesAdapter);
        ViewCompat.setNestedScrollingEnabled(rvArtWork,false);
        requestHttpCall(1,"");
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();

        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {
                    Log.e("responseData",o.getResponseData());
                    try {
                        JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                        String Username = jsonObject.getString("Username");
                        String FullName = jsonObject.getString("FullName");
                        String ProfilePicture = jsonObject.getString("ProfilePicture");
                        String Follower=jsonObject.getString("HasAdded");
                        JSONArray CreatedArtworks = jsonObject.getJSONArray("CreatedArtworks");
                        for(int i = 0; i<CreatedArtworks.length() ; i++)
                        {
                            JSONObject object = CreatedArtworks.getJSONObject(i);
                            Artwork artwork = new Gson().fromJson(object.toString(),Artwork.class);
                            artworks.add(artwork);
                        }
                        likesAdapter.notifyDataSetChanged();
                        tvName.setText(FullName);
                        tvUserName.setText(Username);
                        if(Follower.equals("1"))
                        {
                            tvAddFollower.setText(R.string.unfollowing);
                        }
                        else {
                            tvAddFollower.setText(R.string.follow);
                        }
                        if(!ProfilePicture.equals(""))
                        Glide.with(getActivity()).load(ProfilePicture).into(profile);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(type == 2)
                {
                  //  tvRemoveFollver.setVisibility(View.VISIBLE);
                    if(tvAddFollower.getText().equals("Follow")) {
                        tvAddFollower.setText(R.string.unfollowing);
                    }
                    else
                    {
                        tvAddFollower.setText(R.string.follow);
                    }
                   // showToast("user followed successfully");
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }
    }
    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }
    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {
        mKProgressHUD.dismiss();
        Log.e("HttpResponse",o.getResponseData());
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    mKProgressHUD.show();
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                "account/appuserdetail?id="+UserId,
                                null,
                                new CallBackRetrofit(type,
                                        CreatorProfile.this
                                ));
                    }
                    else if (type == 2) {
                        if(tvAddFollower.getText().equals("Follow"))
                        {
                            JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                            FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                    map,
                                    getString(R.string.AddFollowerUrl) + UserId,
                                    jsonObject,
                                    new CallBackRetrofit(type,
                                            CreatorProfile.this
                                    ));
                        }
                        else
                            {
                                FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                        map,
                                        getString(R.string.UnfollowUserUrl)+""+UserId,
                                        null,
                                        new CallBackRetrofit(type,
                                                CreatorProfile.this
                                        ));
                        }
                    }

                } else {
                     mKProgressHUD.dismiss();
//                    helper.spinnerStop(spinnerDialog);
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v == tvAddFollower)requestHttpCall(2,"");
    }
}
