package com.qamar.rockme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Followers;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class ContactsFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse {

    private View rootView;
//    private EditText etSearch;
//    private TextView tvSuggestedContacts;
//    private ImageView ivSearch;

    private ImageView ivProfileImage, ivMenu2;
    private ProgressBar pbProfileImage;
    private TextView tvProfileName, tvDone;

    private ContactsAdapter mContactsAdapter;
    private RecyclerView rvContacts;
    private ArrayList<Followers> arrayListContacts = new ArrayList<>();
    private ArrayList<Followers> arrayListContactsTemp = new ArrayList<>();
    private ArrayList<String> arrayListNames = new ArrayList<>();
    private ArrayList<String> arrayListFirstCharacter = new ArrayList<>();
    private UserData mUserData = null;
    private boolean contactCheck;
    private int gadgetId;
    private ShareAccessFragment mShareAccessFragment;
    private WorkPlaceMenuFragment mWorkPlaceMenuFragment;

    private ModelParsedResponse modelParsedResponse = null;
    private KProgressHUD mKProgressHUD;

    public ContactsFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mUserData = (UserData) bundle.getSerializable(getString(R.string.user_object));
            contactCheck = bundle.getBoolean(getString(R.string.contact_check));
            gadgetId = bundle.getInt(getString(R.string.gadget_id));
            mShareAccessFragment = (ShareAccessFragment) bundle.getSerializable(getString(R.string.refrence));
            mWorkPlaceMenuFragment = (WorkPlaceMenuFragment) bundle.getSerializable(getString(R.string.refrence_2));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_contacts, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        readBundle(getArguments());
        intializeControlsAndListener();
        setProfileDataAndPicture();

        setUpContactsRecyclerView();
        adapterSetForContacts();

        if (arrayListContacts.size() == 0) {
            loadingContacts();
        }


        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
//        etSearch = rootView.findViewById(R.id.etSearch);
//        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_SEARCH)) {
//                    ((HomeActivity) getActivity()).objGlobalHelperNormal.hideSoftKeyboard(getActivity());
//                    if (!etSearch.getText().toString().trim().equals(getString(R.string.empty))) {
//                        tvSuggestedContacts.setText(getString(R.string.All_Search)
//                                + " " + etSearch.getText().toString().trim() + ")");
//                        ivSearch.setTag("1");
//                    }
//                }
//                return false;
//            }
//        });
//
//        tvSuggestedContacts = rootView.findViewById(R.id.tvSuggestedContacts);

        ivProfileImage = rootView.findViewById(R.id.ivProfileImage);
        pbProfileImage = rootView.findViewById(R.id.pbProfileImage);
        tvProfileName = rootView.findViewById(R.id.tvProfileName);
        tvDone = rootView.findViewById(R.id.tvDone);
        ivMenu2 = rootView.findViewById(R.id.ivMenu2);

        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);
        ivMenu2.setOnTouchListener(this);
        ivMenu2.setOnClickListener(this);
        tvDone.setOnTouchListener(this);
        tvDone.setOnClickListener(this);

//        ivSearch = rootView.findViewById(R.id.ivSearch);
//        ivSearch.setOnTouchListener(this);
//        ivSearch.setOnClickListener(this);

        if (contactCheck) {
            ivMenu2.setVisibility(View.VISIBLE);
            tvDone.setVisibility(View.GONE);
        } else {
            tvDone.setVisibility(View.VISIBLE);
            ivMenu2.setVisibility(View.GONE);
        }
    }

    private void setProfileDataAndPicture() {
        if (mUserData.getUserName() != null) {
            tvProfileName.setText(mUserData.getUserName());
        }
        if (mUserData.getProfilePicture() != null) {
            ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage
                    , mUserData.getProfilePicture(),
                    pbProfileImage, R.drawable.profile);
        } else {
            ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage
                    , "",
                    pbProfileImage, R.drawable.profile);
        }
    }

    private void loadingContacts() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListContacts = new ArrayList<>();
        arrayListContactsTemp = new ArrayList<>();
        arrayListNames = new ArrayList<>();
        arrayListFirstCharacter = new ArrayList<>();
        requestHttpCall(1, "");
    }

    private void setUpContactsRecyclerView() {
        rvContacts = rootView.findViewById(R.id.rvContacts);
        rvContacts.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void adapterSetForContacts() {
        mContactsAdapter = new ContactsAdapter(getActivity());
        rvContacts.setAdapter(mContactsAdapter);
        mContactsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.ivMenu2:
                ((HomeActivity) getActivity()).replaceFragment(new AddContactsFragment(), getActivity());
                break;

            case R.id.tvDone:
                String userId = "";

                for (Followers objFollowers: arrayListContacts) {
                    if (objFollowers.isSelect()) {
                        userId = userId + objFollowers.getFollowedBy() + ",";
                    }
                }

                if (!userId.equals("")) {
                    if (mWorkPlaceMenuFragment == null) {
                        mKProgressHUD = KProgressHUD.create(getActivity())
                                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                .setBackgroundColor(getResources().getColor(R.color.black_50))
                                .setAnimationSpeed(2)
                                .setCancellable(false)
                                .show();

                        requestHttpCall(2, getPostParameters(userId));
                    } else {
                        mWorkPlaceMenuFragment.sendUserIds(userId);
                        getActivity().onBackPressed();
                    }
                } else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                            getString(R.string.please_select_any_contact_msg));
                }
                break;

//            case R.id.ivSearch:
//                if (ivSearch.getTag().toString().trim().equals("1")) {
//                    tvSuggestedContacts.setText(getString(R.string.Suggested_Contacts));
//                    etSearch.setText("");
//                    ivSearch.setTag("0");
//                }
//                ((HomeActivity) getActivity()).objGlobalHelperNormal.hideSoftKeyboard(getActivity());
//                if (!etSearch.getText().toString().trim().equals(getString(R.string.empty))) {
//                    tvSuggestedContacts.setText(getString(R.string.All_Search)
//                            + " " + etSearch.getText().toString().trim() + ")");
//                    ivSearch.setTag("1");
//                }
//                break;
        }
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {
                    if (arrayListContacts.size() > 0) {
                        adapterSetForContacts();
    //                            tvMsg.setVisibility(View.INVISIBLE);
    //                        } else {
    //                            materialRefreshList.setLoadMore(false);
    //                            tvMsg.setVisibility(View.VISIBLE);
                    }
                } else if (type == 2) {
                    mShareAccessFragment.refreshList();
                    getActivity().onBackPressed();
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                if (type == 1) {
                    for (int i = 0; i < modelParsedResponse.getResponse().getData().getAsJsonArray().size(); i++) {
                        Followers objFollowers;
                        objFollowers = new Gson().fromJson
                                ((modelParsedResponse.getResponse().getData().getAsJsonArray().get(i)), Followers.class);
                        arrayListContactsTemp.add(objFollowers);
                    }

                    for (Followers mFollowers : arrayListContactsTemp) {
                        if (!arrayListNames.contains(mFollowers.getFollowerName())) {
                            arrayListNames.add(mFollowers.getFollowerName());
                        }
                    }

                    Collections.sort(arrayListNames, new Comparator<String>()
                    {
                        @Override
                        public int compare(String text1, String text2)
                        {
                            return text1.compareToIgnoreCase(text2);
                        }
                    });

                    for (int i = 0; i < arrayListNames.size(); i++) {
                        for (int j = 0; j < arrayListContactsTemp.size(); j++) {
                            if (arrayListContactsTemp.get(j).getFollowerName().equals(arrayListNames.get(i))) {
                                Followers objFollowers;
                                objFollowers = arrayListContactsTemp.get(j);
                                if (!arrayListFirstCharacter.contains(("" + objFollowers.getFollowerName().charAt(0)).toUpperCase())) {
                                    objFollowers.setFirstCharacter(("" + objFollowers.getFollowerName().charAt(0)).toUpperCase());
                                } else {
                                    objFollowers.setFirstCharacter("");
                                }
                                arrayListFirstCharacter.add(("" + objFollowers.getFollowerName().charAt(0)).toUpperCase());
                                arrayListContacts.add(objFollowers);
                            }
                        }
                    }
                }

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.FollowersListUrl),
                                null,
                                new CallBackRetrofit(type,
                                        ContactsFragment.this
                                ));
                    } else  if (type == 2) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.sharedGadgetBulkUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        ContactsFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
//                    helper.spinnerStop(spinnerDialog);
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    //get parameters start
    private String getPostParameters(String userId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("gadgetId", gadgetId);
            jsonObject.put("userIds", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

        private LayoutInflater mInflater;
        private Context context;
        private Followers mFollowers;

        // data is passed into the constructor
        public ContactsAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.context = context;
        }

        // inflates the cell layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.contacts_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            mFollowers = arrayListContacts.get(position);
            if(mFollowers.getProfilePicture() != null) {
                ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivContactsPic
                        ,mFollowers.getProfilePicture(),
                        holder.pbContacts, R.drawable.profile);
            } else {
                ((HomeActivity) context).objGlobalHelperNormal.setGlideCircularImage(context, holder.ivContactsPic
                        , "",
                        holder.pbContacts, R.drawable.profile);
            }

            holder.tvContactsName.setText(mFollowers.getFollowerName());

            if(mFollowers.getFirstCharacter().equals("")) {
                holder.tvIndex.setText("S");
                holder.tvIndex.setVisibility(View.INVISIBLE);
            } else {
                holder.tvIndex.setText(mFollowers.getFirstCharacter());
                holder.tvIndex.setVisibility(View.VISIBLE);
            }

            if(contactCheck) {
                holder.tvContactsSelect.setVisibility(View.GONE);
            } else {
                holder.tvContactsSelect.setVisibility(View.VISIBLE);
            }

            holder.tvContactsSelect.setSelected(mFollowers.isSelect());

            holder.llContactsbg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (contactCheck) {
                        Fragment fragment = new ProfileOtherFragment();
                        Bundle args = new Bundle();
                        args.putInt(context.getResources().getString(R.string.user_id), arrayListContacts.get(position).getFollowedBy());
                        fragment.setArguments(args);

                        ((HomeActivity) context).replaceFragment(fragment, (HomeActivity) context);
                    }
                }
            });

            holder.tvContactsSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (arrayListContacts.get(position).isSelect()) {
                        arrayListContacts.get(position).setSelect(false);
                        holder.tvContactsSelect.setSelected(false);
                    } else {
                        arrayListContacts.get(position).setSelect(true);
                        holder.tvContactsSelect.setSelected(true);
                    }

                }
            });

        }

        // total number of cells
        @Override
        public int getItemCount() {
            return arrayListContacts.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView ivContactsPic;
            private ProgressBar pbContacts;
            private TextView tvContactsName, tvIndex, tvContactsSelect;
            private LinearLayout llContactsbg;

            public ViewHolder(View itemView) {
                super(itemView);

                ivContactsPic = itemView.findViewById(R.id.ivContactsPic);
                pbContacts = itemView.findViewById(R.id.pbContacts);
                tvContactsName = itemView.findViewById(R.id.tvContactsName);
                tvIndex = itemView.findViewById(R.id.tvIndex);
                tvContactsSelect = itemView.findViewById(R.id.tvContactsSelect);
                llContactsbg = itemView.findViewById(R.id.llContactsbg);

//                itemView.setOnClickListener(this);
            }

//            @Override
//            public void onClick(View view) {
//                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
//            }
        }

        // convenience method for getting data at click position
        public Followers getItem(int id) {
            return arrayListContacts.get(id);
        }

//        // allows clicks events to be caught
//        public void setClickListener(ItemClickListener itemClickListener) {
//            this.mClickListener = itemClickListener;
//        }
//
//        // parent activity will implement this method to respond to click events
//        public interface ItemClickListener {
//            void onItemClick(View view, int position);
//        }
    }
}
