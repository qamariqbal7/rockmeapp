package com.qamar.rockme.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.interfaces.ShareArtWorkWithFriends;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class WorkPlaceMenuFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, Serializable, ShareArtWorkWithFriends, ServiceResponse {

    private View rootView;

    private Dialog dialogWorkPlaceMenuStart, dialogWorkPlaceMenuSave;
    private ImageView ivPic, ivImg, ivWorkplace;
    private ProgressBar pbPic, pbImg;
    private EditText etArtName,etArtworkName;
    private TextView tvArtNameCount;
    private RadioButton rdbPrivate, rdbWithFriends, rdbPublic;
  //  private WorkPlace mWorkPlace;
    private String wvScreenShotPath, allFrames, artworkFrame, gadgetCommond = "", userId = "";
    private ArrayList<String> arrListFrames = new ArrayList<>();
    private UserData mUserData;
    private Artwork mArtwork;
//    private WebViewFragment mWebViewFragment;

    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private MultipartBody.Part imageBody = null;
    private Spinner spSave;

    public WorkPlaceMenuFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
          //  mWorkPlace = (WorkPlace) bundle.getSerializable(getString(R.string.workplace_object));
//            mWebViewFragment = (WebViewFragment) bundle.getSerializable(getString(R.string.refrence));
            wvScreenShotPath = bundle.getString(getString(R.string.webview_screenshot_path));
            allFrames = bundle.getString(getString(R.string.all_frames));
            artworkFrame = bundle.getString(getString(R.string.all_frames));
            allFrames = allFrames.replace("[[[", "");
            allFrames = allFrames.replace("]]]", "");
            allFrames = allFrames.replace("\"", "");
            allFrames = allFrames.replace("[", "");
            arrListFrames = new ArrayList<String>(Arrays.asList(allFrames.split("],")));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_workplace_menu, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        readBundle(getArguments());
        initialWork();
        setDataFromFile();
        intializeControlsAndListener();
        setData();
        makeGadgetSimpleCommand();
        makeGadgetComplexCommand();
        if (dialogWorkPlaceMenuSave == null) {
            dialogWorkPlaceMenuSave = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                    showDialog(R.layout.dialog_workplace_menu_save, getActivity(),
                            true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationFade);
            initAndSetListeners2();
            dialogWorkPlaceMenuSave.dismiss();
        }

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void setDataFromFile() {
        mUserData = FrontEngine.getInstance().mUserData;
        if (mUserData == null) {
            FrontEngine.getInstance().initializeUser(getActivity());
            mUserData = FrontEngine.getInstance().mUserData;
        }
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());
        ivWorkplace = rootView.findViewById(R.id.ivWorkplace);
        spSave = rootView.findViewById(R.id.spSave);
        etArtworkName = (EditText) rootView.findViewById(R.id.etArtworkName);


        rootView.findViewById(R.id.ivMenu).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu).setOnClickListener(this);

        rootView.findViewById(R.id.tvPreview).setOnTouchListener(this);
        rootView.findViewById(R.id.tvPreview).setOnClickListener(this);

        rootView.findViewById(R.id.llSaveArtWork).setOnTouchListener(this);
        rootView.findViewById(R.id.llSaveArtWork).setOnClickListener(this);

        rootView.findViewById(R.id.llStartOver).setOnTouchListener(this);
        rootView.findViewById(R.id.llStartOver).setOnClickListener(this);

        rootView.findViewById(R.id.llShareArtwork).setOnTouchListener(this);
        rootView.findViewById(R.id.llShareArtwork).setOnClickListener(this);


    }

    private void setData() {
        ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideNormalImage(getActivity(), ivWorkplace,
                wvScreenShotPath,
                null, R.drawable.profile);
    }

    private void makeGadgetSimpleCommand() {
        for (int i = 0; i < arrListFrames.size(); i++) {
            String arrlistData = arrListFrames.get(i);
            String[] colorArrOne = arrlistData.split(",m");
            String[] colorArrTwo = colorArrOne[0].split(",");
            String hexColor = String.format("#%02x%02x%02x",
                    Integer.parseInt(colorArrTwo[0].replace("]", "")), Integer.parseInt(colorArrTwo[1].replace("]", "")),
                    Integer.parseInt(colorArrTwo[2].replace("]", "")));
            arrlistData = arrlistData.replace(colorArrOne[0], hexColor);
            arrlistData = arrlistData.replace(",m", "$");
            arrListFrames.remove(i);
            arrListFrames.add(i, arrlistData);
        }
    }

    private void makeGadgetComplexCommand() {
        ArrayList<String> arrListFrames2 = new ArrayList<>();
        arrListFrames2.addAll(arrListFrames);

        HashSet<String> hashSetFrames = new HashSet<String>();

        for (int i = 0; i < arrListFrames.size(); i++) {
            ArrayList<Integer> arrListLeds = new ArrayList<>();
            HashSet<Integer> hashSetLeds = new HashSet<Integer>();
            String colorPlusLeds = "";
            colorPlusLeds = arrListFrames.get(i);
            colorPlusLeds = colorPlusLeds.split("\\$")[0];
            for (int j = 0; j < arrListFrames2.size(); j++) {
                if (arrListFrames2.get(j).split("\\$").length >= 2) {
                    if (colorPlusLeds.equals(arrListFrames2.get(j).split("\\$")[0])) {
                        arrListLeds.add(Integer.parseInt(arrListFrames2.get(j).split("\\$")[1]));
                    }
                }
            }

            hashSetLeds.addAll(arrListLeds);
            arrListLeds.clear();
            arrListLeds.addAll(hashSetLeds);
            Collections.sort(arrListLeds);

            colorPlusLeds += "$";
            boolean isDifference = false;

            for (int k = 0; k < arrListLeds.size(); k++) {
                if (k < arrListLeds.size()-1) {
                    if ((arrListLeds.get(k+1) - arrListLeds.get(k)) == 1) {
                        if (!isDifference) {
                            isDifference = true;
                            colorPlusLeds += arrListLeds.get(k) + "-";
                        }
                    } else {
                        isDifference = false;
                        colorPlusLeds += arrListLeds.get(k) + ",";
                    }
                } else {
                    isDifference = false;
                    colorPlusLeds += arrListLeds.get(k);
                }
            }

            hashSetFrames.add(colorPlusLeds);
        }

        gadgetCommond = "#000000$0-250;";

        for (String s : hashSetFrames) {
            gadgetCommond += s + ";";
        }

        gadgetCommond = gadgetCommond;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        Fragment fragment;
        Bundle args;
        switch (v.getId()) {

            case R.id.ivMenu:
                getActivity().onBackPressed();
                break;

            case R.id.llStartOver:
                dialogWorkPlaceMenuStart = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                        showDialog(R.layout.dialog_workplace_menu_start, getActivity(),
                                true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationFade);
                initAndSetListeners();
                break;

            case R.id.llSaveArtWork:
                //optionSelect(true, false, false);
               // dialogWorkPlaceMenuSave.show();
                mKProgressHUD = KProgressHUD.create(getActivity())
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setBackgroundColor(getResources().getColor(R.color.black_50))
                        .setAnimationSpeed(2)
                        .setCancellable(false)
                        .show();
                requestHttpCall(1, "");
                break;

            case R.id.llShareArtwork:
                ((HomeActivity) getActivity()).objGlobalHelperNormal.shareCodeIntent
                        ((HomeActivity) getActivity(), getActivity(),
                                getActivity().getString(R.string.app_name), getActivity().getString(R.string.app_name));
                break;

            case R.id.tvCancel:
                dialogWorkPlaceMenuStart.dismiss();
                break;

            case R.id.tvYes:
                dialogWorkPlaceMenuStart.dismiss();
                getActivity().onBackPressed();
//                mWebViewFragment.refreshWebView();
                break;

            case R.id.tvCANCEL:
                dialogWorkPlaceMenuSave.dismiss();
                break;

            case R.id.tvSAVE:
                if (checkValidation()) {
                    dialogWorkPlaceMenuSave.dismiss();
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(1, "");
                }
                break;

            case R.id.rdbPrivate:
                optionSelect(true, false, false);
                break;

            case R.id.rdbWithFriends:
                dialogWorkPlaceMenuSave.dismiss();
                fragment = new ContactsFragment();
                args = new Bundle();
                args.putSerializable(getString(R.string.user_object), mUserData);
                args.putBoolean(getString(R.string.contact_check), false);
                args.putSerializable(getString(R.string.refrence_2), WorkPlaceMenuFragment.this);
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;

            case R.id.rdbPublic:
                optionSelect(false, false, true);
                break;

            case R.id.tvPreview:
                fragment = new MyGadgetsFragment();
                args = new Bundle();
                args.putString(getString(R.string.gadget_command), gadgetCommond);
                args.putBoolean(getString(R.string.isPreview), true);
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;

        }
    }

    private void initAndSetListeners() {
        ivPic = dialogWorkPlaceMenuStart.findViewById(R.id.ivPic);
        pbPic = dialogWorkPlaceMenuStart.findViewById(R.id.pbPic);

        dialogWorkPlaceMenuStart.findViewById(R.id.tvCancel).setOnTouchListener(this);
        dialogWorkPlaceMenuStart.findViewById(R.id.tvCancel).setOnClickListener(this);
        dialogWorkPlaceMenuStart.findViewById(R.id.tvYes).setOnTouchListener(this);
        dialogWorkPlaceMenuStart.findViewById(R.id.tvYes).setOnClickListener(this);
    }

    private void initAndSetListeners2() {
        ivImg = dialogWorkPlaceMenuSave.findViewById(R.id.ivImg);
        pbImg = dialogWorkPlaceMenuSave.findViewById(R.id.pbImg);

        etArtName = dialogWorkPlaceMenuSave.findViewById(R.id.etArtName);
        tvArtNameCount = dialogWorkPlaceMenuSave.findViewById(R.id.tvArtNameCount);

        rdbPrivate = dialogWorkPlaceMenuSave.findViewById(R.id.rdbPrivate);
        rdbWithFriends = dialogWorkPlaceMenuSave.findViewById(R.id.rdbWithFriends);
        rdbPublic = dialogWorkPlaceMenuSave.findViewById(R.id.rdbPublic);

        rdbPrivate.setOnClickListener(this);
        rdbWithFriends.setOnClickListener(this);
        rdbPublic.setOnClickListener(this);

        dialogWorkPlaceMenuSave.findViewById(R.id.tvCANCEL).setOnTouchListener(this);
        dialogWorkPlaceMenuSave.findViewById(R.id.tvCANCEL).setOnClickListener(this);
        dialogWorkPlaceMenuSave.findViewById(R.id.tvSAVE).setOnTouchListener(this);
        dialogWorkPlaceMenuSave.findViewById(R.id.tvSAVE).setOnClickListener(this);

        etArtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvArtNameCount.setText(etArtName.getText().length() + " / 50");
            }
        });
    }

    private void optionSelect(boolean one, boolean two, boolean three) {
        rdbPrivate.setChecked(one);
        rdbWithFriends.setChecked(two);
        rdbPublic.setChecked(three);
        userId = "";
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!((HomeActivity) getActivity()).objValidation.hasText(etArtName)) {
            ret = false;
        }

        return ret;
    }

    @Override
    public void sendUserIds(String userId) {
        optionSelect(false, true, false);
        this.userId = userId;
        dialogWorkPlaceMenuSave.show();
    }

    @Override
    public void onResult(int type, HttpResponse o) {

        try {
            mKProgressHUD.dismiss();
            if (modelParsedResponse != null) {

                if (o.getResponseCode() == 200) {
                    if (type == 1) {
                        if (userId.equals("")) {
                            getActivity().getSupportFragmentManager().
                                    popBackStackImmediate(new WorkPlaceFragment().getClass().getName(), 0);
                            getActivity().finishAffinity();
                            startActivity(getActivity().getIntent());

                        } else {
                            mKProgressHUD = KProgressHUD.create(getActivity())
                                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                    .setBackgroundColor(getResources().getColor(R.color.black_50))
                                    .setAnimationSpeed(2)
                                    .setCancellable(false)
                                    .show();

                            requestHttpCall(2, getPostParameters());
                        }
                    } else {
                        getActivity().getSupportFragmentManager().
                                popBackStackImmediate(new WorkPlaceFragment().getClass().getName(), 0);
                    }
                }
            } else {
                customDialogSomethingWent();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                if (type == 1) {
                    mArtwork = new Gson().fromJson(modelParsedResponse.getResponse().getData(), Artwork.class);
                }

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception e) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }
            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        File file = new File(wvScreenShotPath);
                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                        imageBody = MultipartBody.Part.createFormData("imageFile", file.getName(), requestFile);

                        RequestBody artworkName, categoryId = null, createDate, artworkAmount, gadgetCommandRB, artworkFrameRB,
                                gadgetId, artworkId;
                        artworkName = RequestBody.create(MediaType.parse("text/plain"), etArtworkName.getText().toString().trim());
                        if (spSave.getSelectedItemPosition()==0)
                        {
                            categoryId = RequestBody.create(MediaType.parse("text/plain"), "1");
                        } else if (spSave.getSelectedItemPosition()==1) {
                            categoryId = RequestBody.create(MediaType.parse("text/plain"), "4");
                        } /*else if (rdbWithFriends.isChecked()) {
                            categoryId = RequestBody.create(MediaType.parse("text/plain"), "2");
                        }*/

                        createDate = RequestBody.create(MediaType.parse("text/plain"), ((HomeActivity) getActivity()).
                                objGlobalHelperNormal.getCurrentTimeOrDate(getString(R.string.backEndJustDateFormat)));
                        artworkAmount = RequestBody.create(MediaType.parse("text/plain"), "20");
                        gadgetCommandRB = RequestBody.create(MediaType.parse("text/plain"), gadgetCommond);
                        artworkFrameRB = RequestBody.create(MediaType.parse("text/plain"), artworkFrame);
                      //  gadgetId = RequestBody.create(MediaType.parse("text/plain"), "" + mWorkPlace.getIndex());
                        gadgetId = RequestBody.create(MediaType.parse("text/plain"), "" + 1);
                        artworkId = RequestBody.create(MediaType.parse("text/plain"), "0");

                        FrontEngine.getInstance().getRetrofitFactory().uploadArtwork(
                                map,
                                imageBody,
                                artworkName,
                                categoryId,
                                createDate,
                                artworkAmount,
                                gadgetCommandRB,
                                artworkFrameRB,
                                gadgetId,
                                artworkId,
                                new CallBackRetrofit(type,
                                        WorkPlaceMenuFragment.this
                                ));
                    } else if (type == 2) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.shareArtWorkBulkUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        WorkPlaceMenuFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    //get parameters start
    private String getPostParameters() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("artworkId", "" + mArtwork.getArtworkId());
            jsonObject.put("userIds", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
