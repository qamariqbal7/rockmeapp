package com.qamar.rockme.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.MainArtworkAdapter;
import com.qamar.rockme.adapters.RecentArtworksAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.CheckPermissions;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.helper.SelectImageFromCameraOrGallery;
import com.qamar.rockme.interfaces.PictureResize;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.MainArtwork;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, PictureResize, ServiceResponse {

    private View rootView;
    private ImageView ivProfileImage;
    private ProgressBar pbProfileImage;
    private TextView tvProfileName, tvMyGadges, tvMyContacts, tvProfileEmail, tvHeart, tvCoins;

    private RecentArtworksAdapter mRecentArtworksAdapter;
    private RecyclerView rvRecentArtWorks;
    private ArrayList<Artwork> arrayListRecentArtworks = new ArrayList<>();

    private MainArtworkAdapter mMainArtworkAdapter;
    private RecyclerView rvMainArtWork;
    private ArrayList<MainArtwork> arrayListMainArtwork = new ArrayList<>();

    private SelectImageFromCameraOrGallery mSelectImageFromCameraOrGallery = new SelectImageFromCameraOrGallery();
    private int SELECT_PICTURE = 0, REQUEST_CAMERA = 1;
    private boolean result;
    private String imgPath;
    private File file;
    private Dialog dialog;
    private UserData mUserData = null;
    private UserData mUserDataLocal = null;

    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private ModelParsedResponse modelParsedResponse2 = null;
    private ModelParsedResponse modelParsedResponse3 = null;
    private MultipartBody.Part imageBody = null;

    public ProfileFragment() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile, container,
                false);
        ((HomeActivity) getActivity()).setStatusBarGradiant(getActivity(), R.mipmap.bg_profile, (RelativeLayout) rootView.findViewById(R.id.rlMain));
        initialWork();
        intializeControlsAndListener();
        setDataFromFile();
        setProfileDataAndPicture(mUserDataLocal);

        setUpRecentArtworksRecyclerView();
        setUpArtworkRecyclerView();

        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        requestHttpCall(1, "");

        loadingRecentArtWorks();
        loadingArtWorksOwn();

        return rootView;
    }

    private void loadingRecentArtWorks() {
        arrayListRecentArtworks = new ArrayList<>();
        requestHttpCall(2, "");
    }

    private void loadingArtWorksOwn() {
        arrayListMainArtwork = new ArrayList<>();
        requestHttpCall(3, "");
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        rootView.findViewById(R.id.ivMenu).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu).setOnClickListener(this);
        tvMyGadges = rootView.findViewById(R.id.tvMyGadges);
        tvMyGadges.setOnTouchListener(this);
        tvMyGadges.setOnClickListener(this);
        tvMyContacts = rootView.findViewById(R.id.tvMyContacts);
        tvMyContacts.setOnTouchListener(this);
        tvMyContacts.setOnClickListener(this);

        ivProfileImage = rootView.findViewById(R.id.ivProfileImage);
        ivProfileImage.setOnTouchListener(this);
        ivProfileImage.setOnClickListener(this);
        pbProfileImage = rootView.findViewById(R.id.pbProfileImage);
        tvProfileName = rootView.findViewById(R.id.tvProfileName);
        tvProfileEmail = rootView.findViewById(R.id.tvProfileEmail);
        tvHeart = rootView.findViewById(R.id.tvHeart);
        tvCoins = rootView.findViewById(R.id.tvCoins);
    }

    private void setDataFromFile() {
        mUserDataLocal = FrontEngine.getInstance().mUserData;
        if (mUserDataLocal == null) {
            FrontEngine.getInstance().initializeUser(getActivity());
            mUserDataLocal = FrontEngine.getInstance().mUserData;
        }
    }

    private void setProfileDataAndPicture(UserData mUserData) {
        if (mUserData.getUserName() != null) {
            tvProfileName.setText(mUserData.getUserName());
        }
        if (mUserData.getEmailAddress() != null) {
            tvProfileEmail.setText(mUserData.getEmailAddress());
        }
        tvCoins.setText("" + mUserData.getCurrentCoinCount());
        tvHeart.setText("" + mUserData.getUserLikes());
        if (mUserData.getProfilePicture() != null) {
            ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage
                    , mUserData.getProfilePicture(),
                    pbProfileImage, R.drawable.profile);
        } else {
            ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage
                    , "",
                    pbProfileImage, R.drawable.profile);
        }
    }

    private void setUpRecentArtworksRecyclerView() {
        rvRecentArtWorks = rootView.findViewById(R.id.rvRecentArtWorks);
        rvRecentArtWorks.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }

    private void adapterSetForRecentArtworks() {
        mRecentArtworksAdapter = new RecentArtworksAdapter(getActivity(), arrayListRecentArtworks);
        rvRecentArtWorks.setAdapter(mRecentArtworksAdapter);
        mRecentArtworksAdapter.notifyDataSetChanged();
    }

    private void setUpArtworkRecyclerView() {
        rvMainArtWork = rootView.findViewById(R.id.rvMainArtWork);
        rvMainArtWork.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void adapterSetForArtwork() {
        mMainArtworkAdapter = new MainArtworkAdapter(getActivity(), arrayListMainArtwork);
        rvMainArtWork.setAdapter(mMainArtworkAdapter);
        mMainArtworkAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:

                break;

            case R.id.ivMenu2:

                break;

            case R.id.ivMenu:
                getActivity().onBackPressed();
                break;

            case R.id.ivProfileImage:
                dialog = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                        showDialog(R.layout.dialog_picture_change, getActivity(),
                                true, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen, R.style.DialogAnimationSlideUp);
                setListeners();
                break;

            case R.id.llGallery:
                dialog.dismiss();

                result = new CheckPermissions().
                        checkPermissionExternelStorageAndCamera(getActivity(), ProfileFragment.this);

                mSelectImageFromCameraOrGallery.userChoosenTask = getString(R.string.choose_from_gallery);
                if (result)
                    mSelectImageFromCameraOrGallery.galleryIntent(SELECT_PICTURE, ProfileFragment.this);
                break;

            case R.id.llCamera:
                dialog.dismiss();

                result = new CheckPermissions().
                        checkPermissionExternelStorageAndCamera(getActivity(), ProfileFragment.this);

                mSelectImageFromCameraOrGallery.userChoosenTask = getString(R.string.take_photo);
                if (result)
                    mSelectImageFromCameraOrGallery.cameraIntent(REQUEST_CAMERA, ProfileFragment.this);
                break;

            case R.id.tvMyContacts:
                Fragment fragment = new ContactsFragment();
                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.user_object), mUserData);
                args.putBoolean(getString(R.string.contact_check), true);
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;

            case R.id.tvMyGadges:
                ((HomeActivity) getActivity()).replaceFragment(new MyGadgetsFragment(), getActivity());
                break;
        }
    }

    private void setListeners() {
        dialog.findViewById(R.id.llGallery).setOnTouchListener(this);
        dialog.findViewById(R.id.llCamera).setOnTouchListener(this);

        dialog.findViewById(R.id.llGallery).setOnClickListener(this);
        dialog.findViewById(R.id.llCamera).setOnClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CheckPermissions.MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    if (mSelectImageFromCameraOrGallery.userChoosenTask.equals("Take Photo"))
                        mSelectImageFromCameraOrGallery.cameraIntent(REQUEST_CAMERA, this);
                    else if (mSelectImageFromCameraOrGallery.userChoosenTask.equals("Choose from Gallery"))
                        mSelectImageFromCameraOrGallery.galleryIntent(SELECT_PICTURE, this);
                } else {
                    //code for deny
                    ((HomeActivity) getActivity()).objGlobalHelperNormal
                            .callDialog(getActivity(), getString(R.string.alert),
                                    getString(R.string.externel_storage_and_camera_permission_msg));

                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                imgPath = ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage,
                        mSelectImageFromCameraOrGallery.onSelectFromGalleryResult(data,
                                getActivity()), pbProfileImage, R.drawable.profile);

                if (imgPath != null) {
                    file = new File(imgPath);
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.compressImage(
                            ProfileFragment.this, file, 720, 1280, ProfileFragment.this);

                    Log.d("gallery path", "" + imgPath);
                } else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal
                            .callDialog(getActivity(), getString(R.string.upload_failed), getString(R.string.invalid_file));
                }

            } else if (requestCode == REQUEST_CAMERA) {
                if (Build.VERSION.SDK_INT != Build.VERSION_CODES.LOLLIPOP) {
                    imgPath = ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage,
                            mSelectImageFromCameraOrGallery.onCaptureImageOrVideoResult(getActivity()),
                            pbProfileImage, R.drawable.profile);

                    if (imgPath != null) {
                        file = new File(imgPath);
                        ((HomeActivity) getActivity()).objGlobalHelperNormal.compressImage(
                                ProfileFragment.this, file, 720, 1280, ProfileFragment.this);

                        Log.d("capture path", "" + imgPath);
                    } else {
                        ((HomeActivity) getActivity()).objGlobalHelperNormal
                                .callDialog(getActivity(), getString(R.string.upload_failed), getString(R.string.invalid_file));
                    }
                } else {
                    if (data != null) {
                        imgPath = ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage,
                                mSelectImageFromCameraOrGallery.getRealPathFromURI(data.getData(), getActivity()),
                                pbProfileImage, R.drawable.profile);
                        if (imgPath != null) {
                            file = new File(imgPath);
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.compressImage(
                                    ProfileFragment.this, file, 720, 1280, ProfileFragment.this);

                            Log.d("capture path", "" + imgPath);
                        } else {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.upload_failed), getString(R.string.invalid_file));
                        }
                    }
                }


//            } else if (requestCode == PLACE_PICKER_REQUEST) {
//                place = PlacePicker.getPlace(getActivity(), data);
//                if (countLocation <= checkLocationCount) {
//                    ivCross.setVisibility(View.VISIBLE);
//                    mHashMap = new HashMap<String, String>();
//                    mHashMap.put("longitude", "" + place.getLatLng().longitude);
//                    mHashMap.put("lattitude", "" + place.getLatLng().latitude);
//                    mHashMap.put("name", "" + place.getName());
//                    arrayListHashMap.add(mHashMap);
//                    tvLocation.setText(tvLocation.getText() + "" + countLocation + ") " + place.getName() + "\n");
//                    countLocation++;
//                } else {
//                    helper.callDialog(getActivity(), getString(R.string.alert),
//                            "You can add atmost " + checkLocationCount + " locations.");
//                }
            }
        }
    }

    @Override
    public void resizePicture(File file) {
        if (file == null) {
            imgPath = ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage,
                    "", pbProfileImage, R.drawable.profile);
            ((HomeActivity) getActivity()).objGlobalHelperNormal
                    .callDialog(getActivity(), getString(R.string.upload_failed), getString(R.string.invalid_file));
        } else {
            imgPath = file.toString();
            this.file = file;
            mKProgressHUD = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setBackgroundColor(getResources().getColor(R.color.black_50))
                    .setAnimationSpeed(2)
                    .setCancellable(false)
                    .show();
            requestHttpCall(4, "");
        }
    }

    @Override
    public void cropStart() {

    }

    @Override
    public void onResult(int type, HttpResponse o) {

        try {
//            helper.spinnerStop(spinnerDialog);
            if (type == 1) {
                if (modelParsedResponse != null) {

                    if (o.getResponseCode() == 200) {
                        setProfileDataAndPicture(mUserData);
                    }
                } else {
                    customDialogSomethingWent();
                }
            } else if (type == 2) {
                if (modelParsedResponse2 != null) {

                    if (o.getResponseCode() == 200) {
                        if (arrayListRecentArtworks.size() > 0) {
                            adapterSetForRecentArtworks();
//                            tvMsg.setVisibility(View.INVISIBLE);
//                        } else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    customDialogSomethingWent();
                }
            } else if (type == 3) {
                mKProgressHUD.dismiss();
                if (modelParsedResponse3 != null) {

                    if (o.getResponseCode() == 200) {
                        if (arrayListMainArtwork.size() > 0) {
                            adapterSetForArtwork();
//                            tvMsg.setVisibility(View.INVISIBLE);
//                        } else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    customDialogSomethingWent();
                }
            } else if(type == 4) {
                mKProgressHUD.dismiss();
                if (modelParsedResponse3 != null) {

                    if (o.getResponseCode() == 200) {
                        ((HomeActivity) getActivity()).setProfileDataAndPicture();
                        setProfileDataAndPicture(mUserData);
                    }
                } else {
                    customDialogSomethingWent();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (type == 1) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    mUserData = new Gson().fromJson(modelParsedResponse.getResponse().getData(), UserData.class);

                } catch (JsonParseException exp) {
                    modelParsedResponse = null;
                } catch (Exception e) {
                    modelParsedResponse = null;
                }


            } else modelParsedResponse = null;
        } else if (type == 2) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    for (int i = 0; i < modelParsedResponse2.getResponse().getData().getAsJsonArray().size(); i++) {
                        Artwork objArtwork;
                        objArtwork = new Gson().fromJson
                                ((modelParsedResponse2.getResponse().getData().getAsJsonArray().get(i)), Artwork.class);
                        arrayListRecentArtworks.add(objArtwork);
                    }

                } catch (JsonParseException exp) {
                    modelParsedResponse2 = null;
                } catch (Exception e) {
                    modelParsedResponse2 = null;
                }


            } else modelParsedResponse2 = null;
        } else if (type == 3) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse3 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    MainArtwork objMainArtwork, objMainArtwork2;
                    objMainArtwork = new Gson().fromJson((modelParsedResponse3.getResponse().getData()), MainArtwork.class);

                    objMainArtwork2 = new MainArtwork();
                    if (objMainArtwork.getPrivateArtworks().size() > 0) {
                        objMainArtwork2.setLabelArtwork("Saved Artworks");
                        objMainArtwork2.setArrayListArtwork(objMainArtwork.getPrivateArtworks());
                        arrayListMainArtwork.add(objMainArtwork2);
                    }

                    objMainArtwork2 = new MainArtwork();
                    if (objMainArtwork.getSharedArtworks().size() > 0) {
                        objMainArtwork2.setLabelArtwork("Shared Artworks");
                        objMainArtwork2.setArrayListArtwork(objMainArtwork.getSharedArtworks());
                        arrayListMainArtwork.add(objMainArtwork2);
                    }

                    objMainArtwork2 = new MainArtwork();
                    if (objMainArtwork.getPublicArtworks().size() > 0) {
                        objMainArtwork2.setLabelArtwork("Public Artworks");
                        objMainArtwork2.setArrayListArtwork(objMainArtwork.getPublicArtworks());
                        arrayListMainArtwork.add(objMainArtwork2);
                    }

                    objMainArtwork2 = new MainArtwork();
                    if (objMainArtwork.getPendingArtworks().size() > 0) {
                        objMainArtwork2.setLabelArtwork("Pending Artworks");
                        objMainArtwork2.setArrayListArtwork(objMainArtwork.getPendingArtworks());
                        arrayListMainArtwork.add(objMainArtwork2);
                    }

                    objMainArtwork2 = new MainArtwork();
                    if (objMainArtwork.getPurchasedArtworks().size() > 0) {
                        objMainArtwork2.setLabelArtwork("Purchased Artworks");
                        objMainArtwork2.setArrayListArtwork(objMainArtwork.getPurchasedArtworks());
                        arrayListMainArtwork.add(objMainArtwork2);
                    }

                } catch (JsonParseException exp) {
                    modelParsedResponse3 = null;
                } catch (Exception e) {
                    modelParsedResponse3 = null;
                }


            } else modelParsedResponse3 = null;
        } else if(type == 4) {
            modelParsedResponse3 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
            UserData mUserData2 = FrontEngine.getInstance().mUserData;
            if (mUserData2 == null) {
                FrontEngine.getInstance().initializeUser(getActivity());
                mUserData2 = FrontEngine.getInstance().mUserData;
            }
            mUserData2.setProfilePicture(modelParsedResponse3.getResponse().getData().getAsString());
            mUserData.setProfilePicture(modelParsedResponse3.getResponse().getData().getAsString());
            FrontEngine.getInstance().saveUser(getActivity(), mUserData2);
        }

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            if (type == 1) {
                if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                    try {

                        modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    } catch (JsonParseException exp) {
                        customDialogSomethingWent();
                    }
                } else {
                    modelParsedResponse = null;
                    customDialogSomethingWent();
                }
            } else if (type == 2) {
                if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                    try {

                        modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    } catch (JsonParseException exp) {
                        customDialogSomethingWent();
                    }
                } else {
                    modelParsedResponse2 = null;
                    customDialogSomethingWent();
                }
            } else if (type == 3 || type == 4) {
                mKProgressHUD.dismiss();
                if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                    try {

                        modelParsedResponse3 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    } catch (JsonParseException exp) {
                        customDialogSomethingWent();
                    }
                } else {
                    modelParsedResponse3 = null;
                    customDialogSomethingWent();
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.profileUserInfoUrl),
                                null,
                                new CallBackRetrofit(type,
                                        ProfileFragment.this
                                ));
                    } else if (type == 2) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.profileArtworkRecentArtworksUrl),
                                null,
                                new CallBackRetrofit(type,
                                        ProfileFragment.this
                                ));
                    } else if (type == 3) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.profileArtworksUrl),
                                null,
                                new CallBackRetrofit(type,
                                        ProfileFragment.this
                                ));
                    } else if (type == 4) {
                        if (file != null) {
                            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                            imageBody = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
                        }

                        FrontEngine.getInstance().getRetrofitFactory().updateProfilePicture(
                                map,
                                imageBody,
                                new CallBackRetrofit(type,
                                        ProfileFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
