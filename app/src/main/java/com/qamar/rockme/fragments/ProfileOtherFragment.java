package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.ArtworksAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class ProfileOtherFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse {

    private View rootView;
    private ImageView ivProfileImage;
    private ProgressBar pbProfileImage;
    private TextView tvProfileName, tvProfileEmail, tvHeart, tvArtWork, tvLike, tvAddFriend;

    private ArtworksAdapter mArtworksAdapter;
    private RecyclerView rvArtWork;
    private ArrayList<Artwork> arrayListArtwork = new ArrayList<>();

    private UserData mUserData = null;
    private int userId;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private ModelParsedResponse modelParsedResponse2 = null;

    public ProfileOtherFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            userId = bundle.getInt(getString(R.string.user_id));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile_other, container,
                false);
        ((HomeActivity) getActivity()).setStatusBarGradiant(getActivity(), R.mipmap.bg_profile, (RelativeLayout) rootView.findViewById(R.id.rlMain));
        initialWork();
        intializeControlsAndListener();
        readBundle(getArguments());

        setUpArtworkRecyclerView();

        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        requestHttpCall(1, "");

        loadingArtWorksOther();

        return rootView;
    }

    private void loadingArtWorksOther() {
        arrayListArtwork = new ArrayList<>();
        requestHttpCall(2, "");
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        rootView.findViewById(R.id.ivMenu).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu).setOnClickListener(this);

        rootView.findViewById(R.id.tvViewAll).setOnTouchListener(this);
        rootView.findViewById(R.id.tvViewAll).setOnClickListener(this);

        ivProfileImage = rootView.findViewById(R.id.ivProfileImage);
        pbProfileImage = rootView.findViewById(R.id.pbProfileImage);
        tvProfileName = rootView.findViewById(R.id.tvProfileName);
        tvProfileEmail = rootView.findViewById(R.id.tvProfileEmail);
        tvHeart = rootView.findViewById(R.id.tvHeart);
        tvArtWork = rootView.findViewById(R.id.tvArtWork);
        tvLike = rootView.findViewById(R.id.tvLike);
        tvAddFriend = rootView.findViewById(R.id.tvAddFriend);

        tvLike.setOnTouchListener(this);
        tvLike.setOnClickListener(this);
        tvAddFriend.setOnTouchListener(this);
        tvAddFriend.setOnClickListener(this);
    }

    private void setProfileDataAndPicture() {
        if (mUserData.getUserName() != null) {
            tvProfileName.setText(mUserData.getUserName());
        }
        if (mUserData.getEmailAddress() != null) {
            tvProfileEmail.setText(mUserData.getEmailAddress());
        }
        tvHeart.setText("" + mUserData.getUserLikes());
        tvArtWork.setText(mUserData.getUserName() + "'s " + getString(R.string.Artwork));
        if (mUserData.getProfilePicture() != null) {
            ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage
                    , mUserData.getProfilePicture(),
                    pbProfileImage, R.drawable.profile);
        } else {
            ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideCircularImage(getActivity(), ivProfileImage
                    , "",
                    pbProfileImage, R.drawable.profile);
        }

        if (mUserData.getHasAdded() == 0) {
            tvAddFriend.setText(getString(R.string.ADD_FRIEND));
        } else {
            tvAddFriend.setText(getString(R.string.UNFRIEND));
        }
    }

    private void setUpArtworkRecyclerView() {
        rvArtWork = rootView.findViewById(R.id.rvArtWork);
        rvArtWork.setLayoutManager(new GridLayoutManager(getActivity(), 4));
    }

    private void adapterSetForArtwork() {
        mArtworksAdapter = new ArtworksAdapter(getActivity(), arrayListArtwork);
        rvArtWork.setAdapter(mArtworksAdapter);
        mArtworksAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu:
                getActivity().onBackPressed();
                break;

            case R.id.tvViewAll:
                Fragment fragment = new ProfileViewAllFragment();
                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.artwork_arraylist), arrayListArtwork);
                args.putString(getString(R.string.label_artwork), tvArtWork.getText().toString());
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;

            case R.id.tvAddFriend:
                if (mUserData.getHasAdded() == 0) {
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(3, "");
                } else {
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(4, "");
                }
                break;

            case R.id.tvLike:
                if (mUserData.getIsLiked() == 0) {
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(5, "");
                } else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), getString(R.string.alreadyLikeProfile));
                }
                break;
        }
    }

    @Override
    public void onResult(int type, HttpResponse o) {

        try {
//            helper.spinnerStop(spinnerDialog);
            if (type == 1) {
                if (modelParsedResponse != null) {

                    if (o.getResponseCode() == 200) {
                        setProfileDataAndPicture();
                    }
                } else {
                    customDialogSomethingWent();
                }
            } else if (type == 2) {
                mKProgressHUD.dismiss();
                if (modelParsedResponse2 != null) {

                    if (o.getResponseCode() == 200) {
                        if (arrayListArtwork.size() > 0) {
                            adapterSetForArtwork();
//                            tvMsg.setVisibility(View.INVISIBLE);
//                        } else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    customDialogSomethingWent();
                }
            } else if (type == 3) {
                mKProgressHUD.dismiss();
                if (modelParsedResponse2 != null) {

                    if (o.getResponseCode() == 200) {
                        mUserData.setHasAdded(1);
                        setProfileDataAndPicture();
                    }
                } else {
                    customDialogSomethingWent();
                }
            } else if (type == 4) {
                mKProgressHUD.dismiss();
                if (modelParsedResponse2 != null) {

                    if (o.getResponseCode() == 200) {
                        mUserData.setHasAdded(0);
                        setProfileDataAndPicture();
                    }
                } else {
                    customDialogSomethingWent();
                }
            } else if (type == 5) {
                mKProgressHUD.dismiss();
                if (modelParsedResponse2 != null) {

                    if (o.getResponseCode() == 200) {
                        mUserData.setIsLiked(1);
                        mUserData.setUserLikes(mUserData.getUserLikes() + 1);
                        setProfileDataAndPicture();
                    }
                } else {
                    customDialogSomethingWent();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (type == 1) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    mUserData = new Gson().fromJson(modelParsedResponse.getResponse().getData(), UserData.class);

                } catch (JsonParseException exp) {
                    modelParsedResponse = null;
                } catch (Exception e) {
                    modelParsedResponse = null;
                }


            } else modelParsedResponse = null;
        } else if (type == 2) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    for (int i = 0; i < modelParsedResponse2.getResponse().getData().getAsJsonArray().size(); i++) {
                        Artwork objArtwork;
                        objArtwork = new Gson().fromJson
                                ((modelParsedResponse2.getResponse().getData().getAsJsonArray().get(i)), Artwork.class);
                        arrayListArtwork.add(objArtwork);
                    }

                } catch (JsonParseException exp) {
                    modelParsedResponse2 = null;
                } catch (Exception e) {
                    modelParsedResponse2 = null;
                }


            } else modelParsedResponse2 = null;
        } else if (type == 3 || type == 4 || type == 5) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                } catch (JsonParseException exp) {
                    modelParsedResponse2 = null;
                } catch (Exception e) {
                    modelParsedResponse2 = null;
                }


            } else modelParsedResponse2 = null;
        }

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            if (type == 1) {
                if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                    try {

                        modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    } catch (JsonParseException exp) {
                        customDialogSomethingWent();
                    }
                } else {
                    modelParsedResponse = null;
                    customDialogSomethingWent();
                }
            } else if (type == 2 || type == 3 || type == 4 || type == 5) {
                mKProgressHUD.dismiss();
                if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                    try {

                        modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                        if (modelParsedResponse2.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse2.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } catch (JsonParseException exp) {
                        customDialogSomethingWent();
                    } catch (Exception exp) {
                        customDialogSomethingWent();
                    }
                } else {
                    modelParsedResponse2 = null;
                    customDialogSomethingWent();
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.profileUserInfoUrl) + "?userId=" + userId,
                                null,
                                new CallBackRetrofit(type,
                                        ProfileOtherFragment.this
                                ));
                    } else if (type == 2) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.profileArtworksUrl) + "?userId=" + userId,
                                null,
                                new CallBackRetrofit(type,
                                        ProfileOtherFragment.this
                                ));
                    } else if (type == 3) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.AddFollowerUrl) + "?follow=" + userId,
                                null,
                                new CallBackRetrofit(type,
                                        ProfileOtherFragment.this
                                ));
                    } else if (type == 4) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.UnfollowUserUrl) + "?follower=" + userId,
                                null,
                                new CallBackRetrofit(type,
                                        ProfileOtherFragment.this
                                ));
                    } else if (type == 5) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.SetAppUserUrl) + "?userId=" + userId,
                                null,
                                new CallBackRetrofit(type,
                                        ProfileOtherFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
