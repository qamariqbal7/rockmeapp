package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.AdapterFollowing;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Followers;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FollowingFragment extends BaseFragment implements ServiceResponse ,AdapterFollowing.ItemClickListener {
    private View rootView;
    private ArrayList<Followers> followersArrayList;
    AdapterFollowing adapterFollowing;
    private ListView listView;
    private int disableFollowerId;
    private KProgressHUD mKProgressHUD;
    private int RemoveFollowerPosition;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_like,null,false);
        listView = rootView.findViewById(R.id.listView);
        mKProgressHUD = KProgressHUD.create(getActivity());
        setRecyclerView();
        requestHttpCall(1, "");

        return rootView;

    }

    private void setListView()
    {
        listView.setVisibility(View.VISIBLE);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("No followers yet");
      //  arrayList.add("There is no taste yet");
       // arrayList.add("No artist work");
      //  arrayList.add("No gadget yet available");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.adapter_list_view,R.id.text,arrayList);
        listView.setAdapter(adapter);
    }

    private void setRecyclerView()
    {
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);

        followersArrayList = new ArrayList<>();
        adapterFollowing = new AdapterFollowing(getActivity(),followersArrayList);
        adapterFollowing.setClickListener(this);
        recyclerView.setAdapter(adapterFollowing);
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        adapterFollowing.notifyDataSetChanged();
        if (o.getResponseCode() == 200)
        {
            if (type == 1)
            {
                try {
                    JSONObject jsonObject = new JSONObject(o.getResponseData());
                    JSONObject response = jsonObject.getJSONObject("response");
                    JSONArray data = response.getJSONArray("data");
                    for(int i = 0; i<data.length() ; i++)
                    {
                        JSONObject object = data.getJSONObject(i);
//                String FollowerId = object.getString("FollowerId");
//                String UserId = object.getString("UserId");
//                String FollowedBy = object.getString("FollowedBy");
//                String FollowerName = object.getString("FollowerName");
//                String FollowDate = object.getString("FollowDate");
//                String SocialId = object.getString("SocialId");
//                String EmailAddress = object.getString("EmailAddress");
//                String ProfilePicture = object.getString("ProfilePicture");
//                String HasAdded = object.getString("HasAdded");

                        Followers followers = new Gson().fromJson(object.toString(),Followers.class);

                        // Followers followers = new Followers(false,"",Integer.parseInt(FollowerId),Integer.parseInt(UserId),Integer.parseInt(FollowedBy),FollowerName,FollowDate,SocialId,EmailAddress,ProfilePicture,Integer.parseInt(HasAdded));
                        followersArrayList.add(followers);
                    }
                    if(followersArrayList.isEmpty())
                    {
                        setListView();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    setListView();
                }
            }

        }

        if(type == 2)
        {
            mKProgressHUD.dismiss();
            Toast.makeText(getActivity(), "Remover Follower", Toast.LENGTH_SHORT).show();
            followersArrayList.remove(RemoveFollowerPosition);
            adapterFollowing.notifyItemChanged(RemoveFollowerPosition);
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        Log.e("response",o.getResponseData());


    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {
     if(type==2)
     {
         mKProgressHUD.dismiss();
     }
    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.FollowersListUrl),
                                null,
                                new CallBackRetrofit(type,
                                        FollowingFragment.this
                                ));
                    }
                    if (type == 2) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.UnfollowUserUrl)+""+disableFollowerId,
                                null,
                                new CallBackRetrofit(type,
                                        FollowingFragment.this
                                ));
                    }
                }
            }
        });

    }

    @Override
    public void onItemClick(View view, int position) {
        disableFollowerId = followersArrayList.get(position).getUserId();
        requestHttpCall(2,"");
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        RemoveFollowerPosition = position;
    }
}
