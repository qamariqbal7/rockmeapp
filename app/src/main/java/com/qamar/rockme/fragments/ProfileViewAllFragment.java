package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.ArtworksAdapter;
import com.qamar.rockme.models.Artwork;

import java.util.ArrayList;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class ProfileViewAllFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    private View rootView;
    private String labelArtwork;
    private TextView tvAllArtWork;

    private RecyclerView rvAllArtWork;

    private ArtworksAdapter mArtworksAdapter;
    private ArrayList<Artwork> arrayListArtwork = new ArrayList<>();

    public ProfileViewAllFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            labelArtwork = bundle.getString(getString(R.string.label_artwork));
            arrayListArtwork = (ArrayList<Artwork>) bundle.getSerializable(getString(R.string.artwork_arraylist));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile_view_all, container,
                false);
        ((HomeActivity) getActivity()).setStatusBarGradiant(getActivity(), R.drawable.topbar,
                (RelativeLayout) rootView.findViewById(R.id.rlMain));
        initialWork();
        readBundle(getArguments());
        intializeControlsAndListener();

        setUpAllArtWorkRecyclerView();
        adapterSetForAllArtWork();

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);

        tvAllArtWork = rootView.findViewById(R.id.tvAllArtWork);

        tvAllArtWork.setText(labelArtwork);
    }

    private void setUpAllArtWorkRecyclerView() {
        rvAllArtWork = rootView.findViewById(R.id.rvAllArtWork);
        rvAllArtWork.setLayoutManager(new GridLayoutManager(getActivity(), 4));
    }

    private void adapterSetForAllArtWork() {
        mArtworksAdapter = new ArtworksAdapter(getActivity(), arrayListArtwork);
        rvAllArtWork.setAdapter(mArtworksAdapter);
        mArtworksAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.ivMenu2:

                break;
        }
    }
}
