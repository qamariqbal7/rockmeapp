package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.LikesAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Like;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LikeFragment extends Fragment implements ServiceResponse {
    private View rootView;

    private ArrayList<Like> likes;
    private LikesAdapter likesAdapter;

    private ModelParsedResponse modelParsedResponse = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_like,null,false);
        setRecyclerView();
        return rootView;
    }

    private void setRecyclerView()
    {
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        likes = new ArrayList<>();
        likesAdapter = new LikesAdapter(getActivity(),likes);
        recyclerView.setAdapter(likesAdapter);
        requestHttpCall(1,"");
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        Log.e("responseLike",o.getResponseData());
        try {
            JSONArray jsonArray = new JSONArray(modelParsedResponse.getResponse().getData().toString());
            for (int i = 0; i<jsonArray.length() ; i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Like like = new Gson().fromJson((jsonObject.toString()), Like.class);
                likes.add(like);
            }
            likesAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.likedartworks),
                                null,
                                new CallBackRetrofit(type,
                                        LikeFragment.this
                                ));
                    }

                } else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
