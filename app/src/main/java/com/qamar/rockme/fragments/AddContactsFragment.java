package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.AddContactsAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class AddContactsFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse {

    private View rootView;
    private EditText etSearch;
    private TextView tvSuggestedContacts;
    private ImageView ivSearch;

    private AddContactsAdapter mAddContactsAdapter;
    private RecyclerView rvContacts;
    private ArrayList<UserData> arrayListContacts = new ArrayList<>();

    private ModelParsedResponse modelParsedResponse = null;
    private KProgressHUD mKProgressHUD;
    private boolean checkCall = true;

    public AddContactsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_contacts, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        intializeControlsAndListener();

        setUpContactsRecyclerView();
        adapterSetForContacts();

        loadingContacts();

        return rootView;
    }

    private void loadingContacts() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListContacts = new ArrayList<>();
        requestHttpCall(1, "");
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());
        etSearch = rootView.findViewById(R.id.etSearch);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_SEARCH)) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.hideSoftKeyboard(getActivity());
                    if (!etSearch.getText().toString().trim().equals(getString(R.string.empty))) {
                        tvSuggestedContacts.setText(getString(R.string.All_Search)
                                + " " + etSearch.getText().toString().trim() + ")");
                        ivSearch.setTag("1");
                        if (checkCall) {
                            checkCall = false;
                            loadingContacts();
                        }
                    }
                }
                return false;
            }
        });

        tvSuggestedContacts = rootView.findViewById(R.id.tvSuggestedContacts);

        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);

        ivSearch = rootView.findViewById(R.id.ivSearch);
        ivSearch.setOnTouchListener(this);
        ivSearch.setOnClickListener(this);
    }

    private void setUpContactsRecyclerView() {
        rvContacts = rootView.findViewById(R.id.rvContacts);
        rvContacts.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void adapterSetForContacts() {
        mAddContactsAdapter = new AddContactsAdapter(getActivity(), arrayListContacts, mKProgressHUD);
        rvContacts.setAdapter(mAddContactsAdapter);
        mAddContactsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.ivSearch:
                if (ivSearch.getTag().toString().trim().equals("1")) {
                    tvSuggestedContacts.setText(getString(R.string.Suggested_Contacts));
                    etSearch.setText("");
                    ivSearch.setTag("0");
                }
                ((HomeActivity) getActivity()).objGlobalHelperNormal.hideSoftKeyboard(getActivity());
                if (!etSearch.getText().toString().trim().equals(getString(R.string.empty))) {
                    tvSuggestedContacts.setText(getString(R.string.All_Search)
                            + " " + etSearch.getText().toString().trim() + ")");
                    ivSearch.setTag("1");
                    loadingContacts();
                }
                break;
        }
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        checkCall = true;
        mKProgressHUD.dismiss();
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (arrayListContacts.size() > 0) {
                    adapterSetForContacts();
//                            tvMsg.setVisibility(View.INVISIBLE);
//                        } else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                for (int i = 0; i < modelParsedResponse.getResponse().getData().getAsJsonArray().size(); i++) {
                    UserData objUserData;
                    objUserData = new Gson().fromJson
                            ((modelParsedResponse.getResponse().getData().getAsJsonArray().get(i)), UserData.class);
                    arrayListContacts.add(objUserData);
                }

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            checkCall = true;
            mKProgressHUD.dismiss();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.SearchUserUrl) + "?username=" + etSearch.getText().toString().trim(),
                                null,
                                new CallBackRetrofit(type,
                                        AddContactsFragment.this
                                ));
                    }

                } else {
                    checkCall = true;
                    mKProgressHUD.dismiss();
//                    helper.spinnerStop(spinnerDialog);
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
