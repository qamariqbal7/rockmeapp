package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.CategoriesAdapter;
import com.qamar.rockme.adapters.SliderAdapter;
import com.qamar.rockme.adapters.TopCreatorAdapter;
import com.qamar.rockme.adapters.WorkPlaceAdapter2;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.helper.RecyclerItemClickListener;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.Category;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.Popular;
import com.qamar.rockme.models.TopCreator;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.models.WorkPlace;
import com.qamar.rockme.models.WorkPlace2;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.widget.LinearLayout.VERTICAL;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class WorkPlaceFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener,ServiceResponse {

    private View rootView;

    private WorkPlaceAdapter2 mWorkPlaceAdapter;
    private RecyclerView rvWorkPlace,rvTopCreator,rvCategories;

    private WorkPlace mWorkPlace;
  //  private ImageView ivWorkplace;
    private TextView tvPopular;

    private ArrayList<TopCreator> topCreators;
    TopCreatorAdapter topCreatorAdapter;
    private ModelParsedResponse modelParsedResponse = null;

    ArrayList<WorkPlace2> workPlace2s;

    private ViewPager viewPager;
    private TabLayout indicator;
    private ArrayList<Popular> populars;
    private SliderAdapter sliderAdapter;
    private CountDownTimer countAutoSlider;

    private KProgressHUD mKProgressHUD;

    private CategoriesAdapter categoriesAdapter;
    private ArrayList<Category> categories;
    UserData mUserData=null;

    public WorkPlaceFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_workplace2, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
//        initialWork();
        intializeControlsAndListener();
       // setDataFromFile();
        setUpWorkPlaceRecyclerView();
        setPopularSlider();
        adapterSetForWorkPlace();
        adapterTopCreatorSet();
        setCategories();
        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
    /*    rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);

        rootView.findViewById(R.id.ivMenu2).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu2).setOnClickListener(this);*/

        tvPopular = rootView.findViewById(R.id.tvPopular);
        viewPager = rootView.findViewById(R.id.viewPager);
        indicator = rootView.findViewById(R.id.indicator);
//        ivWorkplace = rootView.findViewById(R.id.ivWorkplace);
//        ivWorkplace.setOnTouchListener(this);
//        ivWorkplace.setOnClickListener(this);
        tvPopular.setOnClickListener(this);
        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
        TextView tvName = rootView.findViewById(R.id.tvFeedTittleOld);


        //setProfileName();
       // tvName.setText("Qamar");
        setProfileName(tvName);
    }

    private void setUpWorkPlaceRecyclerView() {
        rvWorkPlace = rootView.findViewById(R.id.rvWorkPlace);

      //  rvWorkPlace.setNestedScrollingEnabled(false);
        rvWorkPlace.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTopCreator = rootView.findViewById(R.id.rvTopCreator);
        rvTopCreator.setLayoutManager(new LinearLayoutManager(getActivity()));

        rvCategories = rootView.findViewById(R.id.rvCategories);
        rvCategories.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
    }

    private void setCategories()
    {
        categories = new ArrayList<>();
        categoriesAdapter = new CategoriesAdapter(getActivity(),categories);
        rvCategories.setAdapter(categoriesAdapter);
        requestHttpCall(4,"");

        rvCategories.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Category category = categories.get(position);

                for(int i = 0 ; i<categories.size() ; i++)
                {
                    if(i==position)
                    {
                       categories.get(i).setChecked(true);
                    }
                    else {
                        categories.get(i).setChecked(false);
                    }
                }
                categoriesAdapter.notifyDataSetChanged();

                Bundle bundle = new Bundle();
                bundle.putString("catId",category.getCategoryID());
                bundle.putString("titleKey",category.getCategoryName());
                PopularFragment popularFragment = new PopularFragment();
                popularFragment.setArguments(bundle);
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame2, popularFragment);
               // transaction.addToBackStack(null);
                transaction.commit();

            }
        }));
    }

    private void adapterTopCreatorSet()
    {
        topCreators = new ArrayList<>();

        topCreatorAdapter = new TopCreatorAdapter(getActivity(),topCreators);
        DividerItemDecoration itemDecor = new DividerItemDecoration(getActivity(), VERTICAL);
        rvTopCreator.addItemDecoration(itemDecor);
        rvTopCreator.setAdapter(topCreatorAdapter);
        ViewCompat.setNestedScrollingEnabled(rvTopCreator, false);
        requestHttpCall(3,"");
        rvTopCreator.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Fragment fragment = new CreatorProfile();
                Bundle args = new Bundle();
                args.putSerializable(getResources().getString(R.string.workplace_object), mWorkPlace);
                args.putString("UserId", topCreators.get(position).getUserId());
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());
            }
        }));

    }

    private void adapterSetForWorkPlace() {
        workPlace2s = new ArrayList<>();
        mWorkPlaceAdapter = new WorkPlaceAdapter2((AppCompatActivity) getActivity(), workPlace2s, this);
        rvWorkPlace.setAdapter(mWorkPlaceAdapter);
        mWorkPlaceAdapter.notifyDataSetChanged();

      //  setImageAndObject(arrayListWorkPlace.get(0));
        ViewCompat.setNestedScrollingEnabled(rvWorkPlace, false);
        requestHttpCall(1,"");
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();

    }

    public void setImageAndObject(WorkPlace mWorkPlace) {
        //ivWorkplace.setImageResource(mWorkPlace.getImg());
       // this.mWorkPlace = mWorkPlace;
    }

    private void setPopularSlider()
    {
        populars = new ArrayList<>();
        sliderAdapter = new SliderAdapter(getActivity(),populars,this);

        viewPager.setAdapter(sliderAdapter);
        indicator.setupWithViewPager(viewPager, true);

    }

    public void clickViewPager(int position)
    {
        Fragment fragment = new StoreArtworkViewFragment();
        Bundle args = new Bundle();
        Popular popular = populars.get(position);
        Artwork artwork = new Artwork();
        artwork.setArtworkImage(popular.getArtworkPicture());
        artwork.setArtworkName(popular.getArtworkName());
        artwork.setArtworkAmount(Integer.parseInt(popular.getArtworkAmount()));
        artwork.setArtworkId(Integer.parseInt(popular.getArtworkAmount()));

        args.putSerializable(getResources().getString(R.string.workplace_object), artwork);
        args.putString("ArtworkID", populars.get(position).getArtworkID());
        fragment.setArguments(args);

        ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());
    }

    private void autoSlide()
    {
        countAutoSlider = new CountDownTimer(8000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (viewPager.getCurrentItem() < populars.size() - 1) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                } else {
                    viewPager.setCurrentItem(0);
                }
               countAutoSlider.start();
            }
        };
        countAutoSlider.start();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
             //   ((HomeActivity) getActivity()).openAndCloseDrawer();
                break;

            case R.id.ivWorkplace:
                Fragment fragment = new StoreArtworkViewFragment();
                Bundle args = new Bundle();
                args.putSerializable(getResources().getString(R.string.workplace_object), mWorkPlace);
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;
            case R.id.tvPopular:
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame2, new PopularFragment());
                // transaction.addToBackStack(null);
                transaction.commit();
                break;
            /*case R.id.profile_image:
                ((HomeActivity) getActivity()).replaceFragment2(new ProfileFragment2(), getActivity());
                break;*/
        }
    }

    @Override
    public void onResult(int type, HttpResponse o) {

       // if (modelParsedResponse != null)
        {

            if (o.getResponseCode() == 200) {
                if (type == 1) {
                    mKProgressHUD.dismiss();
                    try {
                        modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                        JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                        Iterator iterator = jsonObject.keys();
                        while (iterator.hasNext())
                         {
                             String key = (String)iterator.next();
                             if(key.equals("Popular"))
                             {
                                 {
                                     JSONArray free = jsonObject.getJSONArray(key);
                                     ArrayList<WorkPlace> arrayListWorkPlace = new ArrayList<>();
                                     for(int i = 0; i<free.length() ; i++)
                                     {
                                         JSONObject object = free.getJSONObject(i);
                                         String ArtworkID = object.getString("ArtworkID");
                                         String ArtworkName = object.getString("ArtworkName");
                                         String ArtworkPicture = object.getString("ArtworkPicture");
                                         String Category = object.getString("Category");
                                         String UserName = object.getString("UserName");
                                         String ProfilePicture = object.getString("ProfilePicture");
                                         String ArtworkViews = object.getString("ArtworkViews");
                                         String ArtworkAmount = object.getString("ArtworkAmount");
                                         Popular popular = new Popular(ArtworkID,ArtworkName,ArtworkPicture,Category,UserName,ProfilePicture,ArtworkViews,ArtworkAmount);
                                        populars.add(popular);
                                     }
                                    sliderAdapter.notifyDataSetChanged();
                                    autoSlide();
                                 }
                             }
                             else
                                 {
                                     JSONArray free = jsonObject.getJSONArray(key);
                                     ArrayList<WorkPlace> arrayListWorkPlace = new ArrayList<>();
                                     for(int i = 0; i<free.length() ; i++)
                                     {
                                     JSONObject object = free.getJSONObject(i);
                                      WorkPlace workPlace = new Gson().fromJson(object.toString(),WorkPlace.class);
                                      arrayListWorkPlace.add(workPlace);
                                     }
                                     if(!arrayListWorkPlace.isEmpty())
                                     workPlace2s.add(new WorkPlace2(key,arrayListWorkPlace));
                                     mWorkPlaceAdapter.notifyDataSetChanged();

                                 }
                         }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (type == 3) {
                    try {
                        modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                        JSONArray array = new JSONArray(modelParsedResponse.getResponse().getData().toString());
                        for(int i = 0; i<array.length() ; i++)
                        {
                            JSONObject object = array.getJSONObject(i);
                            TopCreator topCreator = new Gson().fromJson(object.toString(),TopCreator.class);
                            topCreators.add(topCreator);
                        }
                        topCreatorAdapter.notifyDataSetChanged();
                    } catch (JSONException e){}


                }
                else if(type == 4)
                {
                    try {
                        modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                        JSONArray array = new JSONArray(modelParsedResponse.getResponse().getData().toString());
                        for(int i = 0; i<array.length(); i++)
                        {
                            JSONObject jsonObject = array.getJSONObject(i);
                            String CategoryID = jsonObject.getString("CategoryID");
                            String CategoryName = jsonObject.getString("CategoryName");
                            Category category = new Category();
                            category.setCategoryID(CategoryID);
                            category.setCategoryName(CategoryName);
                            categories.add(category);

                        }
                        Collections.sort(categories, new Comparator<Category>() {
                            @Override
                            public int compare(Category category, Category category2) {
                                Integer a = Integer.valueOf(category.getCategoryID());
                                Integer b = Integer.valueOf(category2.getCategoryID());
                                return a < b ? -1 : (a > b ) ? 1 : 0;
                            }
                        });
                        categoriesAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                    mKProgressHUD.dismiss();
                }
            }

        } /*else {
            customDialogSomethingWent();
            mKProgressHUD.dismiss();
        }*/
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

               } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.artworkbycategory),
                                null,
                                new CallBackRetrofit(type,
                                        WorkPlaceFragment.this
                                ));
                    } else if (type == 2) {
                       /* JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.addGadgetUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        WorkPlaceFragment.this
                                ));*/
                    }
                    else if (type == 3) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.topcreators),
                                null,
                                new CallBackRetrofit(type,
                                        WorkPlaceFragment.this
                                ));

                    }
                    else if (type == 4) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.categories),
                                null,
                                new CallBackRetrofit(type,
                                        WorkPlaceFragment.this
                                ));

                    }

                }  else {

                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }
}
