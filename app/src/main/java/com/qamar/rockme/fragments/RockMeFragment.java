package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.RockMeFeedsAdapter;
import com.qamar.rockme.adapters.RockMeFeedsAdapter2;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.FeaturedArtworks;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.RockMeFeeds;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class RockMeFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener ,ServiceResponse {

    private View rootView;
    private TextView tvFeeds, tvMarketplace;
    private LinearLayout llMarketPlace, llFeeds;
    private RelativeLayout.LayoutParams layoutParams;
    private ArrayList<RockMeFeeds> arrayListRockMeFeeds =new ArrayList<>();

     private RockMeFeedsAdapter mRockMeFeedsAdapter;
    RockMeFeedsAdapter2 rockMeFeedsAdapter2;
    private ArrayList<FeaturedArtworks> arrayListFeaturedItems = new ArrayList<>();

    private RecyclerView rvRockMeFeeds;

    private ModelParsedResponse modelParsedResponse = null;
    private ModelParsedResponse modelParsedResponse2 = null;
    private KProgressHUD  mKProgressHUD2;
    private boolean checkFeedsMarketPlace = true;

    public RockMeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_rockme2, container,
                false);
        //   ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);

        intializeControlsAndListener();

       // setUpRockMeFeedsRecyclerView();
       // adapterSetForRockMeFeeds();
        //loadingRockMeFeeds();
//
//        if (checkFeedsMarketPlace) {
//            setFeedsMarketPlaceUI(tvFeeds, R.drawable.selected,
//                    tvMarketplace, R.drawable.unselected,
//                    R.color.mainGreenColor, R.color.mainYellowColor);
//            llMarketPlace.setVisibility(View.GONE);
//            llFeeds.setVisibility(View.VISIBLE);
//
//            if (arrayListTopFeeds.size() == 0) {
//                loadingTopFeeds();
//            }
//
//            if (arrayListRockMeFeeds.size() == 0) {
//                loadingRockMeFeeds();
//            }
//        } else {
//            setFeedsMarketPlaceUI(tvMarketplace, R.drawable.selected,
//                    tvFeeds, R.drawable.unselected,
//                    R.color.mainGreenColor, R.color.mainYellowColor);
//            llMarketPlace.setVisibility(View.VISIBLE);
//            llFeeds.setVisibility(View.GONE);
//
//            if (arrayListFeaturedItems.size() == 0) {
//                loadingFeaturedArtworks();
//            }
//
//            if (arrayListMerchandise.size() == 0) {
//                loadingMerchandiseArtworks();
//            }
//        }*/
        setRootViewRecyclerView();
        return rootView;
    }



    private void intializeControlsAndListener() {
        mKProgressHUD2 = KProgressHUD.create(getActivity());

        //  tvFeeds = rootView.findViewById(R.id.tvFeeds);
        //  tvMarketplace = rootView.findViewById(R.id.tvMarketplace);
        //  tvFeeds.setSelected(true);
//        tvMarketplace.setSelected(false);

//        llMarketPlace = rootView.findViewById(R.id.llMarketPlace);
//        llFeeds = rootView.findViewById(R.id.llFeeds);

//        tvFeeds.setOnClickListener(this);
//        tvMarketplace.setOnClickListener(this);

//        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
//        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);

//        rootView.findViewById(R.id.ivMenu2).setOnTouchListener(this);
//        rootView.findViewById(R.id.ivMenu2).setOnClickListener(this);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
    }

    private void setRootViewRecyclerView()
    {
        rvRockMeFeeds = rootView.findViewById(R.id.rvRockMeFeeds);
        rvRockMeFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
        rockMeFeedsAdapter2 = new RockMeFeedsAdapter2(getActivity(),arrayListRockMeFeeds);
        rvRockMeFeeds.setAdapter(rockMeFeedsAdapter2);
        //adapterSetForRockMeFeeds();
        //loadingRockMeFeeds();
        requestHttpCall(2,"");

        //open next screen using recyclerview item touchListener listner
        /*rvRockMeFeeds.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Fragment fragment = new RockMeDetailFragment();
                Bundle args = new Bundle();
                args.putSerializable(getResources().getString(R.string.artwork_object), arrayListFeaturedItems.get(position).getArtwork());
                args.putString("ArtworkID", arrayListFeaturedItems.get(position).getArtwork().getArtworkId()+"");
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());
            }
        }));*/
    }

    /*private void setUpFeaturedItemsRecyclerView() {
        rvFeaturedItems = rootView.findViewById(R.id.rvFeaturedItems);
        rvFeaturedItems.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }

    private void adapterSetForFeaturedItems() {
        mFeaturedArtworksAdapter = new FeaturedArtworksAdapter(getActivity(), arrayListFeaturedItems);
        rvFeaturedItems.setAdapter(mFeaturedArtworksAdapter);
        mFeaturedArtworksAdapter.notifyDataSetChanged();
    }

    private void setUpMerchandiseRecyclerView() {
        rvMerchandise = rootView.findViewById(R.id.rvMerchandise);
        rvMerchandise.setLayoutManager(new GridLayoutManager(getActivity(), 4));
    }

    private void adapterSetForMerchandise() {
        mMerchandiseAdapter = new MerchandiseAdapter(getActivity(), arrayListMerchandise);
        rvMerchandise.setAdapter(mMerchandiseAdapter);
        mMerchandiseAdapter.notifyDataSetChanged();
    }

    private void setUpTopFeedsRecyclerView() {
        rvTopFeeds = rootView.findViewById(R.id.rvTopFeeds);
        rvTopFeeds.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }

    private void adapterSetForTopFeeds() {
        mTopFeedsAdapter = new TopFeedsAdapter(getActivity(), arrayListTopFeeds);
        rvTopFeeds.setAdapter(mTopFeedsAdapter);
        mTopFeedsAdapter.notifyDataSetChanged();
    }
*/
    private void setUpRockMeFeedsRecyclerView() {
        rvRockMeFeeds = rootView.findViewById(R.id.rvRockMeFeeds);
        rvRockMeFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void adapterSetForRockMeFeeds() {
        mRockMeFeedsAdapter = new RockMeFeedsAdapter(getActivity(), arrayListRockMeFeeds);
        rvRockMeFeeds.setAdapter(mRockMeFeedsAdapter);
        mRockMeFeedsAdapter.notifyDataSetChanged();
    }

  /*  private void loadingTopFeeds() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListTopFeeds = new ArrayList<>();
        requestHttpCall(1, null);
    }
*/
    private void loadingRockMeFeeds() {
        mKProgressHUD2 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListRockMeFeeds = new ArrayList<>();
        requestHttpCall(2, null);
    }
/*
    private void loadingFeaturedArtworks() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListFeaturedItems = new ArrayList<>();
        requestHttpCall(3, null);
    }

    private void loadingMerchandiseArtworks() {
        mKProgressHUD2 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListMerchandise = new ArrayList<>();
        requestHttpCall(4, null);
    }*/

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.ivMenu2:

                break;

          /*  case R.id.tvFeeds:
                checkFeedsMarketPlace = true;
                setFeedsMarketPlaceUI(tvFeeds, R.drawable.selected,
                        tvMarketplace, R.drawable.unselected,
                        R.color.mainGreenColor, R.color.mainYellowColor);
                llMarketPlace.setVisibility(View.GONE);
                llFeeds.setVisibility(View.VISIBLE);

                if (arrayListTopFeeds.size() == 0) {
                    loadingTopFeeds();
                }

                if (arrayListRockMeFeeds.size() == 0) {
                    loadingRockMeFeeds();
                }
                break;

            case R.id.tvMarketplace:
                checkFeedsMarketPlace = false;
                setFeedsMarketPlaceUI(tvMarketplace, R.drawable.selected,
                        tvFeeds, R.drawable.unselected,
                        R.color.mainGreenColor, R.color.mainYellowColor);
                llMarketPlace.setVisibility(View.VISIBLE);
                llFeeds.setVisibility(View.GONE);

                if (arrayListFeaturedItems.size() == 0) {
                    loadingFeaturedArtworks();
                }

                if (arrayListMerchandise.size() == 0) {
                    loadingMerchandiseArtworks();
                }
                break;*/
        }
    }

    /*private void setFeedsMarketPlaceUI(TextView tvOne, int selectedOne,
                                       TextView tvTwo, int unselectedTwo,
                                       int colorTxtOne, int colorTxtTwo) {
        if (!tvOne.isSelected()) {
            tvOne.setBackground(ContextCompat.getDrawable(getActivity(), selectedOne));
            tvOne.setSelected(true);
            tvOne.setTextColor(ContextCompat.getColor(getActivity(), colorTxtOne));
            tvTwo.setBackground(ContextCompat.getDrawable(getActivity(), unselectedTwo));
            tvTwo.setSelected(false);
            tvTwo.setTextColor(ContextCompat.getColor(getActivity(), colorTxtTwo));

            layoutParams = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen._145sdp),
                    (int) getResources().getDimension(R.dimen._28sdp));
            layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
            if (tvOne.getId() == tvMarketplace.getId()) {
                layoutParams.addRule(RelativeLayout.RIGHT_OF, tvFeeds.getId());
                tvMarketplace.bringToFront();
            } else {
                tvFeeds.bringToFront();
            }
            tvOne.setPadding(0, 0, 0, 0);
            tvOne.setLayoutParams(layoutParams);

            layoutParams = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen._155sdp),
                    (int) getResources().getDimension(R.dimen._28sdp));
            layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
            if (tvTwo.getId() == tvMarketplace.getId()) {
                layoutParams.addRule(RelativeLayout.RIGHT_OF, tvFeeds.getId());
                layoutParams.setMargins((int) getResources().getDimension(R.dimen._minus20sdp), 0, 0, 0);
                tvTwo.setPadding((int) getResources().getDimension(R.dimen._20sdp), 0, 0, 0);
            } else {
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen._minus20sdp), 0);
                tvTwo.setPadding(0, 0, (int) getResources().getDimension(R.dimen._20sdp), 0);
            }
            tvTwo.setLayoutParams(layoutParams);
        }
    }*/

//    public void loadData() {
//        if (pageNo == 1) {
//            adapterSetProduct();
//            rvFavoriteProductStoreLists.post(new Runnable() {
//                public void run() {
//                    rvFavoriteProductStoreLists.scrollToPosition(0);
//                }
//
//            });
//        } else {
//            mFavoriteProductAdapter.notifyDataSetChanged();
//            rvFavoriteProductStoreLists.post(new Runnable() {
//                public void run() {
//                    rvFavoriteProductStoreLists.scrollToPosition(arrListFavoriteProduct.size() -
//                            modelParsedResponse.getResponse().getData().getAsJsonArray().size());
//                }
//
//            });
//        }
//
//        try {
//            if (modelParsedResponse.getResponse().getPagination().getLast() ==
//                    modelParsedResponse.getResponse().getPagination().getCurrent()) {
//                materialRefreshList.setLoadMore(false);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void loadData2() {
//        if (pageNo == 1) {
//            adapterSetStore();
//            rvFavoriteProductStoreLists.post(new Runnable() {
//                public void run() {
//                    rvFavoriteProductStoreLists.scrollToPosition(0);
//                }
//
//            });
//        } else {
//            mCategoryAdapter.notifyDataSetChanged();
//            rvFavoriteProductStoreLists.post(new Runnable() {
//                public void run() {
//                    rvFavoriteProductStoreLists.scrollToPosition(arrListFavoriteStore.size() -
//                            modelParsedResponse2.getResponse().getData().getAsJsonArray().size());
//                }
//
//            });
//        }
//
//        try {
//            if (modelParsedResponse2.getResponse().getPagination().getLast() ==
//                    modelParsedResponse2.getResponse().getPagination().getCurrent()) {
//                materialRefreshList.setLoadMore(false);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onResult(int type, HttpResponse o) {
        try {
            // mKProgressHUD.dismiss();
//            helper.spinnerStop(spinnerDialog);
           /* if (type == 1) {
                mKProgressHUD.dismiss();
                if (modelParsedResponse != null) {

                    if (o.getResponseCode() == 200) {
                        if (arrayListTopFeeds.size() > 0) {
                          //  adapterSetForTopFeeds();
//                            tvMsg.setVisibility(View.INVISIBLE);
//                        } else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
                        }
                    } else {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal
                                    .callDialog(getActivity(), getString(R.string.alert), modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }
                    }

                } else {
                    customDialogSomethingWent();
                }
            } else*/ if (type == 2) {
                mKProgressHUD2.dismiss();
                if (modelParsedResponse2 != null) {

                    if (o.getResponseCode() == 200) {
                        if (arrayListRockMeFeeds.size() > 0) {
                            rockMeFeedsAdapter2.notifyDataSetChanged();

                        }
//                        else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
//                        }
                    } else {

                        if (modelParsedResponse2.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal
                                    .callDialog(getActivity(), getString(R.string.alert), modelParsedResponse2.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }
                    }

                } else {
                    customDialogSomethingWent();
                }
            } else if (type == 3) {
                mKProgressHUD2.dismiss();
                if (modelParsedResponse != null) {

                    if (o.getResponseCode() == 200) {
                        if (arrayListFeaturedItems.size() > 0) {
                            rockMeFeedsAdapter2.notifyDataSetChanged();
//                           adapterSetForFeaturedItems();
//                            tvMsg.setVisibility(View.INVISIBLE);
//                        } else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
                        }
                    } else {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal
                                    .callDialog(getActivity(), getString(R.string.alert), modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }
                    }

                } else {
                    customDialogSomethingWent();
                }

            }
           /* else if (type == 4) {
                mKProgressHUD2.dismiss();
                if (modelParsedResponse2 != null) {

                    if (o.getResponseCode() == 200) {
                        if (arrayListMerchandise.size() > 0) {
                      //      adapterSetForMerchandise();
//                            tvMsg.setVisibility(View.INVISIBLE);
//                        } else {
//                            materialRefreshList.setLoadMore(false);
//                            tvMsg.setVisibility(View.VISIBLE);
                        }
                    } else {

                        if (modelParsedResponse2.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal
                                    .callDialog(getActivity(), getString(R.string.alert), modelParsedResponse2.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }
                    }

                } else {
                    customDialogSomethingWent();
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

/*        if (type == 1) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    for (int i = 0; i < modelParsedResponse.getResponse().getData().getAsJsonArray().size(); i++) {
                        TopFeeds objTopFeeds;
                        objTopFeeds = new Gson().fromJson
                                ((modelParsedResponse.getResponse().getData().getAsJsonArray().get(i)), TopFeeds.class);
                        objTopFeeds.getArtwork().setTimeAgo(((HomeActivity) getActivity()).objGlobalHelperNormal.
                                convertStringDateTimeIntoMiliSec(objTopFeeds.getArtwork().getCreateDate(), getString(R.string.backEndDateFormat)));
//                        objTopFeeds.getArtwork().setTimeAgo(((HomeActivity) getActivity()).objGlobalHelperNormal.
//                                convertStringDateTimeIntoMiliSec(
//                                        ((HomeActivity) getActivity()).objGlobalHelperNormal.
//                                                convertTimeZone(objTopFeeds.getArtwork().getCreateDate(),
//                                                        getString(R.string.backEndDateFormat), TimeZone.getTimeZone("UTC"),
//                                                        TimeZone.getDefault()), getString(R.string.backEndDateFormat)));
                        arrayListTopFeeds.add(objTopFeeds);
                    }

                } catch (JsonParseException exp) {
                    modelParsedResponse = null;
                } catch (Exception e) {
                    modelParsedResponse = null;
                }


            } else modelParsedResponse = null;
        } else */
         if (type == 2) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    Log.e("modelParsedResponse2",modelParsedResponse2.getResponse().getData().getAsJsonArray().toString());
                    for (int i = 0; i < modelParsedResponse2.getResponse().getData().getAsJsonArray().size(); i++) {
                        String data = modelParsedResponse2.getResponse().getData().getAsJsonArray().get(i).toString();
                        RockMeFeeds objRockMeFeeds;
                        objRockMeFeeds = new Gson().fromJson(data, RockMeFeeds.class);
                       // objRockMeFeeds.setTimeAgo(((HomeActivity) getActivity()).objGlobalHelperNormal.convertStringDateTimeIntoMiliSec(objRockMeFeeds.getStartDate(), getString(R.string.backEndDateFormat)));
                        //objRockMeFeeds.setTimeAgo();

                        objRockMeFeeds.setTimeAgo(((HomeActivity) getActivity()).objGlobalHelperNormal.
                                convertStringDateTimeIntoMiliSec(
                                        ((HomeActivity) getActivity()).objGlobalHelperNormal.
                                                convertTimeZone(objRockMeFeeds.getInsertDate(),
                                                        getString(R.string.backEndDateFormat), TimeZone.getTimeZone("UTC"),
                                                        TimeZone.getDefault()), getString(R.string.backEndDateFormat)));
                        arrayListRockMeFeeds.add(objRockMeFeeds);
                    }

                } catch (JsonParseException exp) {
                    modelParsedResponse2 = null;
                } catch (Exception e) {
                    modelParsedResponse2 = null;
                }


            } else modelParsedResponse2 = null;
        } else if (type == 3) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    arrayListFeaturedItems.clear();
                    for (int i = 0; i < modelParsedResponse.getResponse().getData().getAsJsonArray().size(); i++) {
                        FeaturedArtworks objFeaturedArtworks;
                        objFeaturedArtworks = new Gson().fromJson
                                ((modelParsedResponse.getResponse().getData().getAsJsonArray().get(i)), FeaturedArtworks.class);
                        arrayListFeaturedItems.add(objFeaturedArtworks);
                    }

                } catch (JsonParseException exp) {
                    modelParsedResponse = null;
                } catch (Exception e) {
                    modelParsedResponse = null;
                }


            } else modelParsedResponse = null;
        }
        /*else if (type == 4) {
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    for (int i = 0; i < modelParsedResponse2.getResponse().getData().getAsJsonArray().size(); i++) {
                        Artwork objArtwork;
                        objArtwork = new Gson().fromJson
                                ((modelParsedResponse2.getResponse().getData().getAsJsonArray().get(i)), Artwork.class);
                        arrayListMerchandise.add(objArtwork);
                    }

                } catch (JsonParseException exp) {
                    modelParsedResponse2 = null;
                } catch (Exception e) {
                    modelParsedResponse2 = null;
                }


            } else modelParsedResponse2 = null;
        }*/

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
//            helper.spinnerStop(spinnerDialog);

            if (type == 1 || type == 3) {
                mKProgressHUD2.dismiss();
                if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                    try {

                        modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                        if (modelParsedResponse != null) {

                            if (modelParsedResponse.getMessages() != null) {
                                ((HomeActivity) getActivity()).objGlobalHelperNormal
                                        .callDialog(getActivity(), getString(R.string.alert),
                                                modelParsedResponse.getMessages());
                            } else {
                                customDialogSomethingWent();
                            }

                        } else {
                            customDialogSomethingWent();
                        }


                    } catch (JsonParseException exp) {
                        customDialogSomethingWent();
                    }


                } else {
                    modelParsedResponse = null;
                    customDialogSomethingWent();
                }
            } else if (type == 2 || type == 4) {
                mKProgressHUD2.dismiss();
                if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                    try {

                        modelParsedResponse2 = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                        if (modelParsedResponse2 != null) {

                            if (modelParsedResponse2.getMessages() != null) {
                                ((HomeActivity) getActivity()).objGlobalHelperNormal
                                        .callDialog(getActivity(), getString(R.string.alert),
                                                modelParsedResponse2.getMessages());
                            } else {
                                customDialogSomethingWent();
                            }

                        } else {
                            customDialogSomethingWent();
                        }


                    } catch (JsonParseException exp) {
                        customDialogSomethingWent();
                    }


                } else {
                    modelParsedResponse2 = null;
                    customDialogSomethingWent();
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    mKProgressHUD2.show();
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.feedsTopFeedsUrl),
                                null,
                                new CallBackRetrofit(type,
                                        RockMeFragment.this
                                ));
                    } else if (type == 2) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.feedsRockMeFeedsUrl),
                                null,
                                new CallBackRetrofit(type,
                                        RockMeFragment.this
                                ));
                    } else if (type == 3) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.featuredArtworksUrl),
                                null,
                                new CallBackRetrofit(type,
                                        RockMeFragment.this
                                ));
                    } else if (type == 4) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.merchandiseArtworksUrl),
                                null,
                                new CallBackRetrofit(type,
                                        RockMeFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD2.dismiss();

//                 helper.spinnerStop(spinnerDialog);
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

//    private void refreshComplete() {
//        mKProgressHUD.dismiss();
//        materialRefreshList.setLoadMore(true);
//        // refresh complete
//        materialRefreshList.finishRefresh();
//        // load more refresh complete
//        materialRefreshList.finishRefreshLoadMore();
//        rvFavoriteProductStoreLists.setOnTouchListener(null);
//        if (favType.equals(getString(R.string.product))) {
//            if (arrListFavoriteProduct.size() > 0) {
//                tvMsg.setVisibility(View.INVISIBLE);
//            } else {
//                materialRefreshList.setLoadMore(false);
//                tvMsg.setVisibility(View.VISIBLE);
//            }
//        } else {
//            if (arrListFavoriteStore.size() > 0) {
//                tvMsg.setVisibility(View.INVISIBLE);
//            } else {
//                materialRefreshList.setLoadMore(false);
//                tvMsg.setVisibility(View.VISIBLE);
//            }
//        }
//    }
}
