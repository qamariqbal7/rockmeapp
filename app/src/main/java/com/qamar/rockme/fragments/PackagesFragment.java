package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.PackagesAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.Packages;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class PackagesFragment extends BaseFragment implements View.OnTouchListener, ServiceResponse, PurchasesUpdatedListener {

    private View rootView;

    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    BillingClient billingClient;
    List<SkuDetails> skuDetailsList;
    private PackagesAdapter mPackagesAdapter;
    private RecyclerView rvPackages;
    private ArrayList<Packages> arrayListPackages = new ArrayList<>();
    private Packages mPackages;

    public PackagesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_packages, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        intializeControlsAndListener();

        setUpPackagesRecyclerView();
        adapterSetForPackages();

        if (arrayListPackages.size() == 0) {
            loadingPackages();
        }

        return rootView;
    }

    @Override
    public void onResume() {

        setUpPackagesRecyclerView();
        adapterSetForPackages();

        super.onResume();
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        ImageView ivMenu1 = rootView.findViewById(R.id.ivMenu1);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
        setBackEvent(ivMenu1);
    }

    private void setUpPackagesRecyclerView() {
        rvPackages = rootView.findViewById(R.id.rvPackages);
        rvPackages.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void adapterSetForPackages() {
        billingClient=BillingClient.newBuilder(getContext()).enablePendingPurchases().setListener(this).build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if(billingResult.getResponseCode()==BillingClient.BillingResponseCode.OK)
                {
                    if(billingClient.isReady())
                    {
                        SkuDetailsParams params= SkuDetailsParams.newBuilder().setSkusList(Arrays.asList("0001","0002","0003")).setType(BillingClient.SkuType.INAPP).build();
                        billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                            @Override
                            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
                                if(billingResult.getResponseCode()==BillingClient.BillingResponseCode.OK)
                                {
                                    mPackagesAdapter = new PackagesAdapter(getActivity(), arrayListPackages, PackagesFragment.this,billingClient,skuDetailsList);
                                    rvPackages.setAdapter(mPackagesAdapter);
                                    mPackagesAdapter.notifyDataSetChanged();
                                }
                                else
                                {

                                }
                            }
                        });
                    }
                   // Toast.makeText(getContext(), "connectioin ho gya", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //Toast.makeText(getContext(), "connection ni ho ra bhai"+ billingResult.getDebugMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                Toast.makeText(getContext(), "Disconnected", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadingPackages() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListPackages = new ArrayList<>();
        requestHttpCall(1, "");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    public void buyCoins (Packages mPackages) {
        this.mPackages = mPackages;
        requestHttpCall(2, "" + mPackages.getBundleId());
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {
                    if (arrayListPackages.size() > 0) {
                        adapterSetForPackages();
                    }
                } else if (type == 2) {
//                    ((HomeActivity) getActivity()).setProfileDataAndPicture();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert),
                            getString(R.string.successfully_purchased_package_msg) + " " + mPackages.getBundleName() + " package.");
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                if (type == 1) {

                    for (int i = 0; i < modelParsedResponse.getResponse().getData().getAsJsonArray().size(); i++) {
                        Packages objPackages;
                        objPackages = new Gson().fromJson
                                ((modelParsedResponse.getResponse().getData().getAsJsonArray().get(i)), Packages.class);
                        arrayListPackages.add(objPackages);
                    }

                } else if (type == 2) {
                    UserData mUserData = FrontEngine.getInstance().mUserData;
                    if (mUserData == null) {
                        FrontEngine.getInstance().initializeUser(getActivity());
                        mUserData = FrontEngine.getInstance().mUserData;
                    }
                    mUserData.setCurrentCoinCount(mUserData.getCurrentCoinCount() + mPackages.getBundleCoints());
                    FrontEngine.getInstance().saveUser(getActivity(), mUserData);
                }

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.packgesUrl),
                                null,
                                new CallBackRetrofit(type,
                                        PackagesFragment.this
                                ));
                    } else if (type == 2) {
                        mKProgressHUD = KProgressHUD.create(getActivity())
                                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                .setBackgroundColor(getResources().getColor(R.color.black_50))
                                .setAnimationSpeed(2)
                                .setCancellable(false)
                                .show();
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.purchaseCoinsUrl) + "?bundle=" + mPackages.getBundleName(),
                                null,
                                new CallBackRetrofit(type,
                                        PackagesFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        //if (purchases != null && ! purchases.isEmpty()) {
            buyCoins(mPackagesAdapter.getItem(PackagesAdapter.currentPosition));
        //} else {
          //   Toast.makeText(getContext(), "You  purchased nothing!!!", Toast.LENGTH_SHORT).show();
       // }
    }
}
