package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.AdapterPopular;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.helper.RecyclerItemClickListener;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.Popular;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PopularFragment extends Fragment implements ServiceResponse {

    private View rootView;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private String endPoint = "",catId = "",titleKey = "";
    private ArrayList<Popular> populars;
    private AdapterPopular adapterPopular;
    private TextView tvTitle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_popular,null,false);

        tvTitle = rootView.findViewById(R.id.tvFeedText);
        Bundle bundle = getArguments();
        catId = bundle.getString("catId");
        titleKey = bundle.getString("titleKey");
        endPoint = "artwork/artworkbycategory?category="+catId;
        setRecyclerView();
        tvTitle.setText(titleKey);
        return rootView;
    }

    private void setRecyclerView()
    {
        RecyclerView rvPopular = rootView.findViewById(R.id.rvPopular);
        populars = new ArrayList<>();
        rvPopular.setLayoutManager(new GridLayoutManager(getActivity(),2));
        adapterPopular = new AdapterPopular(getActivity(),populars);
        rvPopular.setAdapter(adapterPopular);
        ViewCompat.setNestedScrollingEnabled(rvPopular,false);

        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();

        if(catId.equals("0"))
        {
        requestHttpCall(2,"");
        }
        else
        {
        requestHttpCall(1, "");
        }

        rvPopular.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Fragment fragment = new StoreArtworkViewFragment();
                Bundle args = new Bundle();
                Popular popular = populars.get(position);
                Artwork artwork = new Artwork();
                artwork.setArtworkImage(popular.getArtworkPicture());
                artwork.setArtworkName(popular.getArtworkName());
                artwork.setArtworkAmount(Integer.parseInt(popular.getArtworkAmount()));
                artwork.setArtworkId(Integer.parseInt(popular.getArtworkAmount()));
                args.putSerializable(getResources().getString(R.string.artwork_object),artwork);
                args.putString("ArtworkID", populars.get(position).getArtworkID());
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());
            }
        }));

    }

    @Override
    public void onResult(int type, HttpResponse o) {
        Log.e("getResponseData",o.getResponseData());
        if(type==1)
        {
            try {
                JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                JSONArray jsonArray = jsonObject.getJSONArray("Popular");
                for(int i = 0; i<jsonArray.length(); i++)
                {
                    JSONObject object = jsonArray.getJSONObject(i);
                    Popular popular = new Gson().fromJson((object.toString()), Popular.class);
                    populars.add(popular);
                }
                adapterPopular.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(type==2)
        {
            try {
                JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                JSONArray jsonArray = jsonObject.getJSONArray(titleKey);
                for(int i = 0; i<jsonArray.length(); i++)
                {
                    JSONObject object = jsonArray.getJSONObject(i);
                    Popular popular = new Gson().fromJson((object.toString()), Popular.class);
                    populars.add(popular);
                }
                adapterPopular.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {
          mKProgressHUD.dismiss();
            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                endPoint,
                                null,
                                new CallBackRetrofit(type,
                                        PopularFragment.this
                                ));
                    }
                    if (type == 2) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.artworkbycategory),
                                null,
                                new CallBackRetrofit(type,
                                        PopularFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
