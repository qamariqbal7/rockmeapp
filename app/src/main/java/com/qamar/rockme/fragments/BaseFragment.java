package com.qamar.rockme.fragments;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;

public class BaseFragment extends Fragment
{


    public void goProfile(View view)
    {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).replaceFragment2(new ProfileFragment2(), getActivity());

            }
        });
    }

    public void setBackEvent(View view)
    {
        view.findViewById(R.id.ivMenu1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    public void showToast(String msg)
    {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    public void loadProfileImage(ImageView profile_image)
    {
        ((HomeActivity) getActivity()).setProfileDataAndPicture(profile_image);
    }

    public void setProfileName(TextView textView)
    {
        ((HomeActivity) getActivity()).setProfileDataAndPicture(textView);
    }
}
