package com.qamar.rockme.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.CommentAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.Comment;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class RockMeDetailFragment extends BaseFragment implements View.OnClickListener,ServiceResponse {

    private View rootView;
    private KProgressHUD mKProgressHUD;
    private CircleImageView profile_image2;
    private TextView tvName,tvCat,tvTitle2,tvComment,tvFeedTittle;
    private ImageView ivPic;
    private CheckBox cbLike,cbCheat,cbShare;
    private RecyclerView recyclerView;
    private EditText etComment;
    private CommentAdapter commentAdapter;
    private ArrayList<Comment> comments = new ArrayList<>();

    private ModelParsedResponse modelParsedResponse = null;
    private String mArtWorkEndPoint = "";
    private Artwork mArtwork;
    private String ItemID,IsLike;
    private UserData mUserData = null;

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            ///mArtwork = (Artwork) bundle.getSerializable(getString(R.string.artwork_object));
            String ArtworkID = bundle.getString("ArtworkID");
            mArtWorkEndPoint = "artwork/artworkdetail?artwork="+ArtworkID;
            ItemID = bundle.getString("FeedId");
            IsLike=bundle.getString("IsLiked");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_rock_me_detail, container,false);
        readBundle(getArguments());
        intializeControlsAndListener();
        setDataFromFile();
        return rootView;
    }

    private void setDataFromFile() {
        mUserData = FrontEngine.getInstance().mUserData;
        if (mUserData == null) {
            FrontEngine.getInstance().initializeUser(getActivity());
            mUserData = FrontEngine.getInstance().mUserData;
        }
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        CircleImageView profile_image2 = rootView.findViewById(R.id.profile_image2);
        tvName = rootView.findViewById(R.id.tvName);
        tvFeedTittle = rootView.findViewById(R.id.tvFeedTittleOld);
        tvCat = rootView.findViewById(R.id.tvCat);
        tvTitle2 = rootView.findViewById(R.id.tvTitle2);
        tvComment = rootView.findViewById(R.id.tvComment);
        etComment = rootView.findViewById(R.id.etComment);

        cbLike = rootView.findViewById(R.id.cbLike);
        cbCheat = rootView.findViewById(R.id.cbCheat);
        cbShare = rootView.findViewById(R.id.cbShare);

        ivPic = rootView.findViewById(R.id.ivPic);
        recyclerView = rootView.findViewById(R.id.recyclerView);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);

        cbLike.setOnClickListener(this);
        cbShare.setOnClickListener(this);
        cbCheat.setOnClickListener(this);
        tvComment.setOnClickListener(this);
        setRecyclerView();
        tvName.setText(getArguments().get("Name").toString());
        tvFeedTittle.setText(getArguments().get("FeedTittle").toString());
        tvTitle2.setText(getArguments().get("FeedText").toString());
        Glide.with(this).load(getArguments().get("FeedImage").toString())
                .centerCrop()
                .override(600,600)
                .into(ivPic);
        cbLike.setText(getArguments().get("FeedLikes").toString());
        cbCheat.setText(getArguments().get("FeedCount").toString());
        if(IsLike.equals("true"))
        {
            cbLike.setChecked(true);
        }
        else
        {
            cbLike.setChecked(false);
        }
      //  Toast.makeText(getContext(), IsLike, Toast.LENGTH_SHORT).show();
//        requestHttpCall(4,"");
//        mKProgressHUD = KProgressHUD.create(getActivity())
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setBackgroundColor(getResources().getColor(R.color.black_50))
//                .setAnimationSpeed(2)
//                .setCancellable(false)
//                .show();
        requestHttpCall(7,"");
    }

    private void setRecyclerView()
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        commentAdapter = new CommentAdapter(getActivity(),comments);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(commentAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.cbLike:
                // if (mArtwork.getIsLiked() == 0)
            {
                mKProgressHUD = KProgressHUD.create(getActivity())
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setBackgroundColor(getResources().getColor(R.color.black_50))
                        .setAnimationSpeed(2)
                        .setCancellable(false)
                        .show();
                //requestHttpCall(1, getPostParametersLikeView(2));
                //if(cbLike.isChecked()==true)
                //{
                  //  requestHttpCall(2, "");
                //}
               // else {
                    requestHttpCall(1, getPostParametersLikeView(2));
                //}
            }
                /*else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), getString(R.string.alreadyLike));
                }*/
            break;


            case R.id.cbShare:
               // ((HomeActivity) getActivity()).replaceFragment(new ShareAccessFragment(), getActivity());

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                intent.putExtra(Intent.EXTRA_TEXT, "This is my text");
                startActivity(Intent.createChooser(intent, "choose one"));

                break;
         /*   case R.id.profile_image2:
                ((HomeActivity)getActivity()).replaceFragment(new CreatorProfile(),getActivity());
                break;*/

            case R.id.tvComment:
                //  cvComment.setVisibility(View.VISIBLE);
                // nestScrollView.smoothScrollTo(0,tvComment.getBottom());
                postComment();
                break;
        }
    }

    private void postComment()
    {
        String comment = etComment.getText().toString();
        if(comment.equals(""))
        {
            Toast.makeText(getActivity(), "please enter comment", Toast.LENGTH_SHORT).show();
            return;
        }
        //cvComment.setVisibility(View.GONE);
        requestHttpCall(5,getPostParameters());
        mKProgressHUD.show();

    }

    private String getPostParameters() {
        JSONObject jsonObject = new JSONObject();
        try {
            String comment = etComment.getText().toString();
            jsonObject.put("Comment", comment);
            jsonObject.put("CommentTypeID", "2");
            jsonObject.put("ItemID", ItemID);
            etComment.setText("");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
        // Log.e("responseData",o.getResponseData());
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {
                    //  mArtwork.setIsLiked(1);
                    //  mArtwork.setLikes(mArtwork.getLikes() + 1);
                    // seedData();
                    requestHttpCall(4,null);
                } else if (type == 3) {
                    //   mArtwork.setIsPurchased(1);
                    //  seedData();
                    ((HomeActivity)getActivity()).replaceFragment(new PurchaseFragment(),getActivity());
                }
                else if (type == 4) {
                    Log.e("responseData",o.getResponseData());
                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    try {
                        JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                        String ArtworkID = jsonObject.getString("ArtworkID");
                        String ArtworkName = jsonObject.getString("ArtworkName");
                        String ArtworkPicture = jsonObject.getString("ArtworkPicture");
                        String UserID = jsonObject.getString("UserID");
                        String UserName = jsonObject.getString("UserName");
                        String FullName = jsonObject.getString("FullName");
                        String HasAdded = jsonObject.getString("HasAdded");
                        String ProfilePicture = jsonObject.getString("ProfilePicture");
                        String ArtworkViews = jsonObject.getString("ArtworkViews");
                        String ArtworkLikes = jsonObject.getString("ArtworkLikes");
                        String ArtworkAmount = jsonObject.getString("ArtworkAmount");
                        String IsPurchased = jsonObject.getString("IsPurchased");
                        String IsLiked=jsonObject.getString("IsLiked");
                        String CommentCount=jsonObject.getString("ArtworkComments");
//                        isBuy = Boolean.parseBoolean(IsPurchased);
//                        isLiked = Boolean.parseBoolean(IsLiked);
//                        isHasAdded=Boolean.parseBoolean(HasAdded);
//                        if(isBuy)
//                        {
//                            btBuy.setText("Preview");
//                        }
//                        else
//                        {
//                            btBuy.setText("Buy Now");
//                        }
//                        if(isLiked)
//                        {
//                            cbFav.setChecked(true);
//                            //cbFav.setEnabled(false);
//                        }
//                        else
//                        {
//                            cbFav.setChecked(false);
//                        }
                        tvName.setText(UserName);
//                        tvArtWork.setText(ArtworkName);
//                        // tvMoney.setText(ArtworkAmount);
//                        tvTitle.setText(ArtworkName);
//                        cbChate.setText(CommentCount);
//                        cbFav.setText(ArtworkLikes);
//                        cbShare.setText(" 0");
                        //ivHeart.setImageResource(R.drawable.heart2_fill);
                        //  cbFav.setButtonDrawable(R.drawable.heart_fill);

                        //addFollower = UserID;
                        if(!ArtworkPicture.equals(""))
                            Glide.with(this).load(ArtworkPicture)

                                    .override(500,500)
                                    .centerCrop()
                                    .into(ivPic);
                        if(!ProfilePicture.equals(""))
                            Glide.with(this).load(ProfilePicture).into(profile_image2);

//                        if(isHasAdded)
//                        {
//                            tvRemoveFollver.setVisibility(View.VISIBLE);
//                            tvAddFollower.setVisibility(View.GONE);
//                        }
//                        else
//                        //if(HasAdded.equals("true"))
//                        {
//                            tvRemoveFollver.setVisibility(View.GONE);
//                            tvAddFollower.setVisibility(View.VISIBLE);
//                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                else if(type == 5)
                {
                    requestHttpCall(7,"");
                }
               else if(type == 7)
                {
                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    Log.e("comentsresponse",modelParsedResponse.getResponse().getData().toString());
                    try {
                        comments.clear();
                        // modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                        JSONArray jsonArray = new JSONArray(modelParsedResponse.getResponse().getData().toString());
                        for(int i = 0; i<jsonArray.length(); i++)
                        {
                            JSONObject object = jsonArray.getJSONObject(i);
                            Comment comment = new Gson().fromJson(object.toString(),Comment.class);
                            comments.add(comment);
                        }
                        commentAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {
            try {
                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }
        }
        else modelParsedResponse = null;
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {
        try {
            mKProgressHUD.dismiss();
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }

            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {
        final HashMap map;
        map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                 if (type == 1) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                "feeds/FeedAction",
                                jsonObject,
                                new CallBackRetrofit(type,
                                        RockMeDetailFragment.this
                                ));
                    }
                    else if (type == 2) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                "feeds/UnLikeFeed?id="+ItemID,
                                jsonObject,
                                new CallBackRetrofit(type,
                                        RockMeDetailFragment.this
                                ));
                    }
                 else if (type == 4) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                mArtWorkEndPoint,
                                null,
                                new CallBackRetrofit(type,
                                        RockMeDetailFragment.this
                                ));
                    }
                    else if (type == 5) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.addComment),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        RockMeDetailFragment.this
                                ));

                    // setRecyclerView();
                    }
                    else if (type == 7) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                "feeds/FeedComments?feed="+ItemID+"&lastid=null",
                                jsonObject,
                                new CallBackRetrofit(type,
                                        RockMeDetailFragment.this
                                ));
                    }

                }  else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(
                            getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    private String getPostParametersLikeView(int actionType) {
        JSONObject jsonObject = new JSONObject();
        try {
            // jsonObject.put("artworkId", mArtwork.getArtworkId());
            jsonObject.put("FeedID", ItemID);
          //  jsonObject.put("actionBy", mUserData.getUserId());
            jsonObject.put("ActionType", actionType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
