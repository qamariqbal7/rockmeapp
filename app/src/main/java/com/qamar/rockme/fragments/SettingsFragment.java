package com.qamar.rockme.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class SettingsFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener {

    private View rootView;
    private Dialog dialogLangauge;
    private RadioButton rdbEnglish, rdbArabic, rdbSpanish, rdbFrench, rdbItalian;
    private TextView tvLanguage,tvTerms,tvPrivacyPolicy;

    public SettingsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_settings, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        intializeControlsAndListener();

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        tvLanguage = rootView.findViewById(R.id.tvLanguage);
        tvTerms = rootView.findViewById(R.id.tvTerms);
        tvPrivacyPolicy = rootView.findViewById(R.id.tvPrivacyPolicy);

        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);

        rootView.findViewById(R.id.rlLanguage).setOnTouchListener(this);
        rootView.findViewById(R.id.rlLanguage).setOnClickListener(this);

        rootView.findViewById(R.id.rlMyGadgets).setOnTouchListener(this);
        rootView.findViewById(R.id.rlMyGadgets).setOnClickListener(this);

      //  rootView.findViewById(R.id.rlNotification).setOnTouchListener(this);
     //   rootView.findViewById(R.id.rlNotification).setOnClickListener(this);

        rootView.findViewById(R.id.rlPackages).setOnTouchListener(this);
        rootView.findViewById(R.id.rlPackages).setOnClickListener(this);
        rootView.findViewById(R.id.rlHelp).setOnClickListener(this);

        rootView.findViewById(R.id.tvLogout).setOnClickListener(this);
        rootView.findViewById(R.id.rlMyWallet).setOnClickListener(this);

        tvPrivacyPolicy.setOnClickListener(this);
        tvTerms.setOnClickListener(this);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
        setTvLanguage();
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.rlLanguage:
                dialogLangauge = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                        showDialog(R.layout.dialog_language_select, getActivity(),
                                true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationFade);
                initAndSetListeners();
                checkLanguage();
                break;

            case R.id.tvOK:
                dialogLangauge.dismiss();
                languageStore();
                setTvLanguage();
                // Reload current fragment
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(this).attach(this).commit();
                break;

            case R.id.tvCANCEL:
                dialogLangauge.dismiss();
                break;

            case R.id.llEnglish:
                languageSelect(true, false, false, false, false);
                break;

            case R.id.llArabic:
                languageSelect(false, true, false, false, false);
                break;

            case R.id.llSpanish:
                languageSelect(false, false, true, false, false);
                break;

            case R.id.llFrench:
                languageSelect(false, false, false, true, false);
                break;

            case R.id.llItalian:
                languageSelect(false, false, false, false, true);
                break;

            case R.id.rlMyGadgets:
                ((HomeActivity) getActivity()).replaceFragment2(new MyGadgetsFragment(), getActivity());
                break;

         /*   case R.id.rlNotification:
                ((HomeActivity) getActivity()).replaceFragment(new NotificationFragment(), getActivity());
                break;*/
            case R.id.rlMyWallet:
                ((HomeActivity) getActivity()).replaceFragment2(new MyWalletFragment(), getActivity());
                break;

            case R.id.rlPackages:
                ((HomeActivity) getActivity()).replaceFragment2(new PackagesFragment(), getActivity());
                break;
            case R.id.rlHelp:
                ((HomeActivity) getActivity()).replaceFragment2(new HelpAndSupportFragment(), getActivity());
                break;
            case R.id.tvTerms:
                showTermsAndcondiction();
                break;
            case R.id.tvPrivacyPolicy:
                showPrivacyPolicy();
                break;
            case R.id.tvLogout:
                ((HomeActivity) getActivity()).logOutWorkPerform();
                break;
        }
    }

    private void checkLanguage() {
        if (((HomeActivity) getActivity()).prefs.
                getStringValue(((HomeActivity) getActivity()).prefs.language).equals("en")) {
            languageSelect(true, false, false, false, false);
        } else if (((HomeActivity) getActivity()).prefs.
                getStringValue(((HomeActivity) getActivity()).prefs.language).equals("ar")) {
            languageSelect(false, true, false, false, false);
        } else if (((HomeActivity) getActivity()).prefs.
                getStringValue(((HomeActivity) getActivity()).prefs.language).equals("es")) {
            languageSelect(false, false, true, false, false);
        } else if (((HomeActivity) getActivity()).prefs.
                getStringValue(((HomeActivity) getActivity()).prefs.language).equals("fr")) {
            languageSelect(false, false, false, true, false);
        } else if (((HomeActivity) getActivity()).prefs.
                getStringValue(((HomeActivity) getActivity()).prefs.language).equals("it")) {
            languageSelect(false, false, false, false, true);
        }
    }

    private void languageStore() {
        if (rdbEnglish.isChecked()) {
            ((HomeActivity) getActivity()).prefs.saveValueInSharedPreference
                    (((HomeActivity) getActivity()).prefs.language, "en");
            ((HomeActivity) getActivity()).languageChange("en");
        } else if (rdbArabic.isChecked()) {
            ((HomeActivity) getActivity()).prefs.saveValueInSharedPreference
                    (((HomeActivity) getActivity()).prefs.language, "ar");
            ((HomeActivity) getActivity()).languageChange("ar");
        } else if (rdbSpanish.isChecked()) {
            ((HomeActivity) getActivity()).prefs.saveValueInSharedPreference
                    (((HomeActivity) getActivity()).prefs.language, "es");
            ((HomeActivity) getActivity()).languageChange("es");
        } else if (rdbFrench.isChecked()) {
            ((HomeActivity) getActivity()).prefs.saveValueInSharedPreference
                    (((HomeActivity) getActivity()).prefs.language, "fr");
            ((HomeActivity) getActivity()).languageChange("fr");
        } else if (rdbItalian.isChecked()) {
            ((HomeActivity) getActivity()).prefs.saveValueInSharedPreference
                    (((HomeActivity) getActivity()).prefs.language, "it");
            ((HomeActivity) getActivity()).languageChange("it");
        }
    }

    private void setTvLanguage()
    {
        String language =  ((HomeActivity) getActivity()).prefs.
                getStringValue(((HomeActivity) getActivity()).prefs.language);
        switch (language)
        {
            case "en":
                tvLanguage.setText(getString(R.string.English));
                break;
            case "ar":
                tvLanguage.setText(getString(R.string.Arabic));
                break;
            case "es":
                tvLanguage.setText(getString(R.string.Spanish));
                break;
            case "fr":
                tvLanguage.setText(getString(R.string.French));
                break;
            case "it":
                tvLanguage.setText(getString(R.string.Italian));
                break;

        }
    }

    private void initAndSetListeners() {
        rdbEnglish = dialogLangauge.findViewById(R.id.rdbEnglish);
        rdbArabic = dialogLangauge.findViewById(R.id.rdbArabic);
        rdbSpanish = dialogLangauge.findViewById(R.id.rdbSpanish);
        rdbFrench = dialogLangauge.findViewById(R.id.rdbFrench);
        rdbItalian = dialogLangauge.findViewById(R.id.rdbItalian);

        dialogLangauge.findViewById(R.id.tvOK).setOnTouchListener(this);
        dialogLangauge.findViewById(R.id.tvOK).setOnClickListener(this);
        dialogLangauge.findViewById(R.id.tvCANCEL).setOnTouchListener(this);
        dialogLangauge.findViewById(R.id.tvCANCEL).setOnClickListener(this);

        dialogLangauge.findViewById(R.id.llEnglish).setOnClickListener(this);
        dialogLangauge.findViewById(R.id.llArabic).setOnClickListener(this);
        dialogLangauge.findViewById(R.id.llSpanish).setOnClickListener(this);
        dialogLangauge.findViewById(R.id.llFrench).setOnClickListener(this);
        dialogLangauge.findViewById(R.id.llItalian).setOnClickListener(this);
    }

    private void languageSelect(boolean one, boolean two, boolean three, boolean four, boolean five) {
        rdbEnglish.setChecked(one);
        rdbArabic.setChecked(two);
        rdbSpanish.setChecked(three);
        rdbFrench.setChecked(four);
        rdbItalian.setChecked(five);
    }

    private void showPrivacyPolicy()
    {
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        final AlertDialog dialog = ab.create();
        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_privacy_policy,null);
        Button btnConfrom=(Button) v.findViewById(R.id.btConform);
        WebView webView=(WebView) v.findViewById(R.id.webView);
        TextView tvTittle=(TextView)v.findViewById(R.id.tvTittle);
        tvTittle.setText(R.string.privacy_and_terms_conditions);
        webView.loadUrl("http://rockme.info/privacy-policy-mobile.html");
        btnConfrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setView(v);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

       // dialog.getWindow().setLayout(950, 1500);
    }
    private void showTermsAndcondiction()
    {
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        final AlertDialog dialog = ab.create();
        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_privacy_policy,null);
        Button btnConfrom=(Button) v.findViewById(R.id.btConform);
        WebView webView=(WebView) v.findViewById(R.id.webView);
        TextView tvTittle=(TextView)v.findViewById(R.id.tvTittle);
        tvTittle.setText(R.string.terms_and_conditions);
        webView.loadUrl("http://rockme.info/terms-conditions-mobile.html");
        btnConfrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setView(v);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        // dialog.getWindow().setLayout(950, 1500);
    }
}
