package com.qamar.rockme.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.BaseActivity;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.MyGadgets;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class MyGadgetDetailFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse {

    private View rootView;
    private MyGadgets mMyGadgets;
    private TextView tvDeviceName, tvSerialNumber, tvMACAddress;
    private Dialog dialogAlert, dialogGadgetWifi;
    private EditText etSSID, etPassword;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private boolean checkAlertDialogType, checkAdd = false;
    private MyGadgetsFragment mMyGadgetsFragment;

    public MyGadgetDetailFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mMyGadgets = (MyGadgets) bundle.getSerializable(getString(R.string.gadget_object));
            mMyGadgetsFragment = (MyGadgetsFragment) bundle.getSerializable(getString(R.string.refrence));
            checkAdd = bundle.getBoolean(getString(R.string.gadget_check_add));

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_my_gadgets_detail, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        readBundle(getArguments());
        intializeControlsAndListener();
        seedData();

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);

        rootView.findViewById(R.id.llDeleteDevice).setOnTouchListener(this);
        rootView.findViewById(R.id.llDeleteDevice).setOnClickListener(this);
        rootView.findViewById(R.id.rlUpdateWiFiNetwork).setOnTouchListener(this);
        rootView.findViewById(R.id.rlUpdateWiFiNetwork).setOnClickListener(this);
        rootView.findViewById(R.id.rlShareAccess).setOnTouchListener(this);
        rootView.findViewById(R.id.rlShareAccess).setOnClickListener(this);

        tvDeviceName = rootView.findViewById(R.id.tvDeviceName);
        tvSerialNumber = rootView.findViewById(R.id.tvSerialNumber);
        tvMACAddress = rootView.findViewById(R.id.tvMACAddress);

        mKProgressHUD = KProgressHUD.create(getActivity());
    }

    private void seedData() {
        if (mMyGadgets != null) {
            tvDeviceName.setText(mMyGadgets.getUserGadgetName());
            tvSerialNumber.setText(mMyGadgets.getSerialNumber());
            tvMACAddress.setText(mMyGadgets.getMacAddress());
        }

        if (checkAdd) {
            dialogGadgetWifi = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                    showDialog(R.layout.dialog_gadget_wifi, getActivity(),
                            true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
            initGadgetWifiDialogChild(getString(R.string.SUCCESSFULLY_ADDED));
            setConnectedSSID();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.llDeleteDevice:
                checkAlertDialogType = true;
                dialogAlert = ((HomeActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_alert, getActivity(),
                        true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationFade);
                initializeControlsAndSetData();
                break;

            case R.id.btnOne:
                dialogAlert.dismiss();
                if (checkAlertDialogType) {
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(1, "");
                }
                break;

            case R.id.btnTwo:
                dialogAlert.dismiss();
                if (!checkAlertDialogType) {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
                break;

            case R.id.rlUpdateWiFiNetwork:
                dialogGadgetWifi = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                        showDialog(R.layout.dialog_gadget_wifi, getActivity(),
                                true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
                initGadgetWifiDialogChild(getString(R.string.WIFI_UPDATE));
                setConnectedSSID();
                break;

            case R.id.ivClose2:
                dialogGadgetWifi.dismiss();
                break;

            case R.id.tvConnectToGadget:
                checkAlertDialogType = false;
                if (checkValidation2()) {
                    dialogAlert = ((HomeActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_alert, getActivity(),
                            true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
                    initializeControlsAndSetData();
                }
                break;

            case R.id.rlShareAccess:
                Fragment fragment = new ShareAccessFragment();
                Bundle args = new Bundle();
                args.putInt(getString(R.string.gadget_id), mMyGadgets.getUserGadgetId());
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;
        }
    }

    private boolean checkValidation2() {
        boolean ret = true;
        if (!((HomeActivity) getActivity()).objValidation.hasText(etSSID)) {
            ret = false;
        }
        if (!((HomeActivity) getActivity()).objValidation.hasText(etPassword)) {
            ret = false;
        }

        return ret;
    }

    private void initializeControlsAndSetData() {
        if (checkAlertDialogType) {
            ((TextView) dialogAlert.findViewById(R.id.tvHeading)).setText(getString(R.string.alert));
            ((TextView) dialogAlert.findViewById(R.id.tvDesc)).setText(getString(R.string.delete_device_msg));
            ((TextView) dialogAlert.findViewById(R.id.btnOne)).setText(getString(R.string.yes));
            ((TextView) dialogAlert.findViewById(R.id.btnTwo)).setText(getString(R.string.no));
            dialogAlert.findViewById(R.id.btnOne).setOnClickListener(this);
            dialogAlert.findViewById(R.id.btnOne).setOnTouchListener(this);
            dialogAlert.findViewById(R.id.btnTwo).setOnClickListener(this);
            dialogAlert.findViewById(R.id.btnTwo).setOnTouchListener(this);
        } else {
            ((TextView) dialogAlert.findViewById(R.id.tvHeading)).setText(getString(R.string.alert));
            ((TextView) dialogAlert.findViewById(R.id.tvDesc)).setText(getString(R.string.gadget_goto_settings_msg));
            ((TextView) dialogAlert.findViewById(R.id.btnOne)).setText(getString(R.string.cancel));
            ((TextView) dialogAlert.findViewById(R.id.btnTwo)).setText(getString(R.string.Goto_Settings));
            dialogAlert.findViewById(R.id.btnOne).setOnClickListener(this);
            dialogAlert.findViewById(R.id.btnOne).setOnTouchListener(this);
            dialogAlert.findViewById(R.id.btnTwo).setOnClickListener(this);
            dialogAlert.findViewById(R.id.btnTwo).setOnTouchListener(this);
        }
    }

    private void initGadgetWifiDialogChild(String heading) {
        etSSID = dialogGadgetWifi.findViewById(R.id.etSSID);
        etPassword = dialogGadgetWifi.findViewById(R.id.etPassword);
        dialogGadgetWifi.findViewById(R.id.tvConnectToGadget).setOnClickListener(this);
        dialogGadgetWifi.findViewById(R.id.tvConnectToGadget).setOnTouchListener(this);
        dialogGadgetWifi.findViewById(R.id.ivClose2).setOnClickListener(this);
        dialogGadgetWifi.findViewById(R.id.ivClose2).setOnTouchListener(this);
        ((TextView) dialogGadgetWifi.findViewById(R.id.tvHeading)).setText(heading);
    }

    private void setConnectedSSID() {
        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        etSSID.setText(info.getSSID().replace("\"", ""));
        etSSID.setSelection(etSSID.getText().length());
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                mMyGadgetsFragment.refreshList();
                getActivity().onBackPressed();
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                HashMap map;
                if (status) {
                    switch (type) {

                        case 1:
                            map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                                    ((BaseActivity) getActivity()).prefs.getStringValue(getString(R.string.sp_accesstoken))});

                            FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.DELETE,
                                    map,
                                    getString(R.string.deleteGadgetUrl)
                                            + "?id=" + mMyGadgets.getUserGadgetId(),
                                    null,
                                    new CallBackRetrofit(type,
                                            MyGadgetDetailFragment.this
                                    ));
                            break;
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((BaseActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
