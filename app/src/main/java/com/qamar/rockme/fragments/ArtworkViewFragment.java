package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class ArtworkViewFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener , ServiceResponse {

    private Button btSaveWork,btStartOver,btShareArtWork;
    private View rootView;

    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;

    public ArtworkViewFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_artwork_view, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        intializeControlsAndListener();

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());
        ImageView ivMenu1 = rootView.findViewById(R.id.ivMenu1);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        setBackEvent(ivMenu1);

        btSaveWork = rootView.findViewById(R.id.btSaveWork);
        btStartOver = rootView.findViewById(R.id.btStartOver);
        btShareArtWork = rootView.findViewById(R.id.btShareArtWork);

        btSaveWork.setOnClickListener(this);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if(v==btSaveWork)
        {
            mKProgressHUD.show();
            requestHttpCall(1,getPostParameters());
        }
        if(v==btStartOver)
        {
            ((HomeActivity) getActivity()).replaceFragment2(new ProfileFragment2(), getActivity());
        }
        if(v==btShareArtWork)
        {
            ((HomeActivity) getActivity()).replaceFragment2(new ShareAccessFragment(), getActivity());
        }
    }

    //get parameters start
    private String getPostParameters() {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("artworkId", "86");
            jsonObject.put("artworkName", "testByAhmad");
            jsonObject.put("imageFile", "");
            jsonObject.put("categoryId", "2");
            jsonObject.put("artworkAmount", "25");
            jsonObject.put("gadgetCommand", "adfja;ljfa;lfj;aj");
            jsonObject.put("artworkFrame", "asdfasdfas464asdf65asdf6a" );
            jsonObject.put("userGadgetId", "53");
            jsonObject.put("gadgetId", "2");
            jsonObject.put("htmlFile", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
        Log.e("responseData",o.getResponseData());
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {

                }

            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.uploadArtworkUrl),
                                null,
                                new CallBackRetrofit(type,
                                        ArtworkViewFragment.this
                                ));
                    }

                }  else {

                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
