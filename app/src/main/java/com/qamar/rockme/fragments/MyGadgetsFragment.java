package com.qamar.rockme.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.MainGadgetAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.interfaces.RefreshList;
import com.qamar.rockme.models.MainGadgets;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.MyGadgets;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class MyGadgetsFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse,Serializable, RefreshList {

    private View rootView;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private SwipeRefreshLayout swipeToRefresh;
    private TextView tvMsg , tvManually,tvQRCode;

    private MainGadgetAdapter mMainGadgetAdapter;
    private RecyclerView rvMainGadgetList;
    ArrayList<MainGadgets> arrayListMainGadget = new ArrayList<>();

    private Dialog dialogAddGadget, dialogGadgetWifi, dialogGotoSettings;
    private LinearLayout llManually;
    private EditText etName, etSerialNumber, etSSID, etPassword;
//    private boolean checkAdd = false;
    public boolean isPreview = false;
    public String gadgetCommond;

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            gadgetCommond = bundle.getString(getString(R.string.gadget_command));
            isPreview = bundle.getBoolean(getString(R.string.isPreview));
        }
    }

    public MyGadgetsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_my_gadgets, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        readBundle(getArguments());
        intializeControlsAndListener();

        setRefreshListener();
        setUpGadgetRecyclerView();
        adapterSetForGadget();

        if (arrayListMainGadget.size() == 0) {
            loadingGadgets();
        }

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);
        rootView.findViewById(R.id.ivMenu2).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu2).setOnClickListener(this);

        tvMsg = rootView.findViewById(R.id.tvMsg);
        swipeToRefresh = rootView.findViewById(R.id.swipeToRefresh);

        swipeToRefresh.setSize(CircularProgressDrawable.LARGE);
        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.mainYellowColor),
                ContextCompat.getColor(getActivity(), R.color.mainYellowHoverColor),
                ContextCompat.getColor(getActivity(), R.color.mainGreenColor),
                ContextCompat.getColor(getActivity(), R.color.mainGreenHoverColor));
    }

    public void setRefreshListener() {
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadRefreshData();
            }
        });
    }

    private void setUpGadgetRecyclerView() {
        rvMainGadgetList = rootView.findViewById(R.id.rvMainGadgetList);
        rvMainGadgetList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void adapterSetForGadget() {
        mMainGadgetAdapter = new MainGadgetAdapter(getActivity(), arrayListMainGadget, MyGadgetsFragment.this);
        rvMainGadgetList.setAdapter(mMainGadgetAdapter);
        mMainGadgetAdapter.notifyDataSetChanged();

    }

    private void loadRefreshData() {
        arrayListMainGadget = new ArrayList<>();
        requestHttpCall(1, "");
        rvMainGadgetList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    private void loadingGadgets() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListMainGadget = new ArrayList<>();
        requestHttpCall(1, "");
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.ivMenu2:
                dialogAddGadget = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                        showDialog(R.layout.dialog_add_gadget, getActivity(),
                                true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
                initAddGadgetDialogChild();
                tvManually.setVisibility(View.VISIBLE);
                tvQRCode.setVisibility(View.VISIBLE);
                break;

            case R.id.tvManually:
                llManually.setVisibility(View.VISIBLE);
                tvManually.setVisibility(View.GONE);
                tvQRCode.setVisibility(View.GONE);
                break;

            case R.id.tvQRCode:

                break;

            case R.id.ivClose:
                dialogAddGadget.dismiss();
                break;

            case R.id.tvSubmit:
                if (checkValidation()) {
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(2, getPostParameters());
                }
                break;

            case R.id.tvConnectToGadget:
                if (checkValidation2()) {
                    dialogGotoSettings = ((HomeActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_alert, getActivity(),
                            true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
                    initGotoSettingsDialogChildAndSetData();
                }
                break;

            case R.id.ivClose2:
                dialogGadgetWifi.dismiss();
                break;

            case R.id.btnOne:
                dialogGotoSettings.dismiss();
                break;

            case R.id.btnTwo:
                dialogGotoSettings.dismiss();
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                break;
        }
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!((HomeActivity) getActivity()).objValidation.hasText(etName)) {
            ret = false;
        }
        if (!((HomeActivity) getActivity()).objValidation.hasText(etSerialNumber)) {
            ret = false;
        }

        return ret;
    }

    private boolean checkValidation2() {
        boolean ret = true;
        if (!((HomeActivity) getActivity()).objValidation.hasText(etSSID)) {
            ret = false;
        }
        if (!((HomeActivity) getActivity()).objValidation.hasText(etPassword)) {
            ret = false;
        }

        return ret;
    }

    private void initAddGadgetDialogChild() {
        llManually = dialogAddGadget.findViewById(R.id.llManually);
        etName = dialogAddGadget.findViewById(R.id.etName);
        etSerialNumber = dialogAddGadget.findViewById(R.id.etSerialNumber);
        tvManually =  dialogAddGadget.findViewById(R.id.tvManually);
        tvManually.setOnClickListener(this);
        dialogAddGadget.findViewById(R.id.tvManually).setOnTouchListener(this);
        tvQRCode = dialogAddGadget.findViewById(R.id.tvQRCode);
        tvQRCode.setOnClickListener(this);
        dialogAddGadget.findViewById(R.id.tvQRCode).setOnTouchListener(this);
        dialogAddGadget.findViewById(R.id.ivClose).setOnClickListener(this);
        dialogAddGadget.findViewById(R.id.ivClose).setOnTouchListener(this);
        dialogAddGadget.findViewById(R.id.tvSubmit).setOnClickListener(this);
        dialogAddGadget.findViewById(R.id.tvSubmit).setOnTouchListener(this);
    }

    private void initGadgetWifiDialogChild() {
        etSSID = dialogGadgetWifi.findViewById(R.id.etSSID);
        etPassword = dialogGadgetWifi.findViewById(R.id.etPassword);
        dialogGadgetWifi.findViewById(R.id.tvConnectToGadget).setOnClickListener(this);
        dialogGadgetWifi.findViewById(R.id.tvConnectToGadget).setOnTouchListener(this);
        dialogGadgetWifi.findViewById(R.id.ivClose2).setOnClickListener(this);
        dialogGadgetWifi.findViewById(R.id.ivClose2).setOnTouchListener(this);
    }

    private void initGotoSettingsDialogChildAndSetData() {
        ((TextView) dialogGotoSettings.findViewById(R.id.tvHeading)).setText(getString(R.string.alert));
        ((TextView) dialogGotoSettings.findViewById(R.id.tvDesc)).setText(getString(R.string.gadget_goto_settings_msg));
        ((TextView) dialogGotoSettings.findViewById(R.id.btnOne)).setText(getString(R.string.cancel));
        ((TextView) dialogGotoSettings.findViewById(R.id.btnTwo)).setText(getString(R.string.Goto_Settings));
        dialogGotoSettings.findViewById(R.id.btnOne).setOnClickListener(this);
        dialogGotoSettings.findViewById(R.id.btnOne).setOnTouchListener(this);
        dialogGotoSettings.findViewById(R.id.btnTwo).setOnClickListener(this);
        dialogGotoSettings.findViewById(R.id.btnTwo).setOnTouchListener(this);
    }

    private void setConnectedSSID() {
        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        etSSID.setText(info.getSSID().replace("\"", ""));
        etSSID.setSelection(etSSID.getText().length());
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        refreshComplete();
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {

                   /* MainGadgets objMainGadgets;
                    objMainGadgets = new Gson().fromJson((modelParsedResponse.getResponse().getData()), MainGadgets.class);

                    if (objMainGadgets.getAngryBirds().size() > 0) {
                            //                    objMainGadgets2.setLabelGadgets("Private Gadgets (" + objMainGadgets.getPrivateGadgets().size() + ")");
                            objMainGadgets.setLabelGadgets("Saved Gadgets");
                            objMainGadgets.setArrayListGadgets(objMainGadgets.getAngryBirds());
                            arrayListMainGadget.add(objMainGadgets);
                     }*/
                    try {
                        JSONObject issueObj = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                        Iterator iterator = issueObj.keys();
                        while(iterator.hasNext()){
                            MainGadgets mainGadgets = new MainGadgets();
                            ArrayList<MyGadgets> myGadgets = new ArrayList<>();
                            String key = (String)iterator.next();
                            JSONArray jsonArray = issueObj.getJSONArray(key);
                           for(int i = 0; i<jsonArray.length() ; i++)
                           {
                            JSONObject object = jsonArray.getJSONObject(i);
                               MyGadgets myGadget = new Gson().fromJson(object.toString(),MyGadgets.class);
                               myGadgets.add(myGadget);
                           }
                            mainGadgets.setLabelGadgets("Saved Gadgets");
                            mainGadgets.setArrayListGadgets(myGadgets);
                            mainGadgets.setAngryBirds(myGadgets);
                            arrayListMainGadget.add(mainGadgets);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (arrayListMainGadget.size() > 0) {
                        adapterSetForGadget();
                        tvMsg.setVisibility(View.INVISIBLE);
                    } else {
                        adapterSetForGadget();
                        tvMsg.setVisibility(View.VISIBLE);
                    }
                } else if (type == 2) {
                    dialogAddGadget.dismiss();
                    Fragment fragment = new MyGadgetDetailFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(getResources().getString(R.string.gadget_object),
                            arrayListMainGadget.get(0)
                                    .getArrayListGadgets().get(arrayListMainGadget.get(0)
                                    .getArrayListGadgets().size() -1));
                    args.putBoolean(getString(R.string.gadget_check_add), true);
                    args.putSerializable(getString(R.string.refrence),MyGadgetsFragment.this );
                    fragment.setArguments(args);

                    ((HomeActivity) getActivity()).replaceFragment(fragment, (HomeActivity) getActivity());


                    dialogGadgetWifi = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            showDialog(R.layout.dialog_gadget_wifi, getActivity(),
                                    true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
                    initGadgetWifiDialogChild();
                    setConnectedSSID();
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                if (type == 1) {
                    MainGadgets objMainGadgets, objMainGadgets2;
                    objMainGadgets = new Gson().fromJson((modelParsedResponse.getResponse().getData()), MainGadgets.class);

                    objMainGadgets2 = new MainGadgets();



                    if (objMainGadgets.getPrivateGadgets().size() > 0) {
                        //                    objMainGadgets2.setLabelGadgets("Private Gadgets (" + objMainGadgets.getPrivateGadgets().size() + ")");
                        objMainGadgets2.setLabelGadgets("Saved Gadgets");
                        objMainGadgets2.setArrayListGadgets(objMainGadgets.getPrivateGadgets());
                        arrayListMainGadget.add(objMainGadgets2);
                    }

                    objMainGadgets2 = new MainGadgets();
                    if (objMainGadgets.getSharedGadgets().size() > 0) {
                        //                    objMainGadgets2.setLabelGadgets("Shared Gadgets (" + objMainGadgets.getSharedGadgets().size() + ")");
                        objMainGadgets2.setLabelGadgets("Shared Gadgets");
                        objMainGadgets2.setArrayListGadgets(objMainGadgets.getSharedGadgets());
                        arrayListMainGadget.add(objMainGadgets2);
                    }
                } else if (type == 2) {
                    MyGadgets objMyGadgets;
                    objMyGadgets = new Gson().fromJson((modelParsedResponse.getResponse().getData()), MyGadgets.class);
                    if (arrayListMainGadget.size() != 0) {
                        arrayListMainGadget.get(0).getArrayListGadgets().add(objMyGadgets);
                    } else {
                        MainGadgets objMainGadgets = new MainGadgets();
                        ArrayList<MyGadgets> arrayListGadgets = new ArrayList<>();
                        arrayListGadgets.add(objMyGadgets);
                        objMainGadgets.setLabelGadgets("Saved Gadgets");
                        objMainGadgets.setArrayListGadgets(arrayListGadgets);
                        arrayListMainGadget.add(objMainGadgets);
                    }
                }

            } catch (JsonParseException exp) {
             //   modelParsedResponse = null;
            } catch (Exception exp) {
             //   modelParsedResponse = null;
            }
        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            refreshComplete();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {
                try {
                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization), ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});
                   // map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization), "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIyIiwibmJmIjoxNTQ1NjI5MjU2LCJleHAiOjE1NzcxNjUyNTYsImlhdCI6MTU0NTYyOTI1NiwiaXNzIjoic2VsZiIsImF1ZCI6Imh0dHA6Ly9yb2NrLW1lLmNvLyJ9.IdmxMDKx96Xfphz1r_jloewJWHmPRMXZAfQZEcEJ8s4"});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.gadgetListUrl),
                                null,
                                new CallBackRetrofit(type,
                                        MyGadgetsFragment.this
                                ));
                    } else if (type == 2) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.addGadgetUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        MyGadgetsFragment.this
                                ));
                    }

                } else {
                    refreshComplete();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    private void refreshComplete() {
        mKProgressHUD.dismiss();
        swipeToRefresh.setRefreshing(false);
        rvMainGadgetList.setOnTouchListener(null);
        setRefreshListener();
//        if (arrayListShareAccess.size() > 0) {
//            tvMsg.setVisibility(View.INVISIBLE);
//        } else {
//            tvMsg.setVisibility(View.VISIBLE);
//        }
    }

    //get parameters start
    private String getPostParameters() {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("UserGadgetName", etName.getText().toString().trim());
            jsonObject.put("FirebaseToken", "0");
            jsonObject.put("SerialNumber", etSerialNumber.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void refreshList() {
        arrayListMainGadget = new ArrayList<>();
    }
}
