package com.qamar.rockme.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.colouring.Colour;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.CheckPermissions;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.SelectColorModel;
import com.qamar.rockme.models.WorkPlace;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class WebViewFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener, Serializable ,ServiceResponse {

    private View rootView;
    private WebView webView;
    private ImageView ivSelectedColor;
    private WorkPlace mWorkPlace;
    private String gadgetId="",gadetString="";
    private KProgressHUD mKProgressHUD;
    private File file = null;
    private String allFrames = "", allFramesJson = "", base64Image;
    private Bitmap decodedImage = null;
    private Handler handler = new Handler();
    private Runnable runnable;

    private ArrayList<SelectColorModel> arrListSelectColor = new ArrayList<>();
    private SelectColorAdapter mSelectColorAdapter;
    private RecyclerView rvSelectColorLists;

    private ArrayList<SelectColorModel> arrListSelectColorScheme = new ArrayList<>();
    private SelectColorSchemeAdapter mSelectColorSchemeAdapter;
    private RecyclerView rvSelectSchemeColorLists;

    private SelectColorModel mSelectColorModel;

    private ModelParsedResponse modelParsedResponse = null;
    private ImageView ivPlus,ivMinus;

    public WebViewFragment() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            handler.removeCallbacks(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mWorkPlace = (WorkPlace) bundle.getSerializable(getString(R.string.workplace_object));
            gadgetId = bundle.getString("gadgetId");
            gadetString=bundle.getString("gadgetString");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_webview, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        readBundle(getArguments());
        initialWork();
        intializeControlsAndListener();
        setUpWebView();

        setUpSelectColorRecyclerView();
        adapterSetForSelectColor();

        setUpSelectColorSchemeRecyclerView();
        adapterSetForSelectColorScheme();
      //  requestHttpCall(1,"");
        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);
        rootView.findViewById(R.id.ivMenu2).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu2).setOnClickListener(this);
        rootView.findViewById(R.id.ivPlus).setOnClickListener(this);
        rootView.findViewById(R.id.ivMinus).setOnClickListener(this);
        rootView.findViewById(R.id.tvNext).setOnClickListener(this);
        rootView.findViewById(R.id.tvworking).setOnClickListener(this);
        rootView.findViewById(R.id.ivReset).setOnClickListener(this);
        rootView.findViewById(R.id.ivReturn).setOnClickListener(this);
        ivSelectedColor = rootView.findViewById(R.id.ivSelectedColor);
        ivPlus = rootView.findViewById(R.id.ivSelectedColor);
        ivMinus = rootView.findViewById(R.id.ivSelectedColor);
        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
        mKProgressHUD = KProgressHUD.create(getActivity());
    }

    private void setUpSelectColorRecyclerView() {
        rvSelectColorLists = rootView.findViewById(R.id.rvSelectColorLists);
        rvSelectColorLists.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));
    }

    private void setDataForSelectColor(int color) {
        mSelectColorModel = new SelectColorModel();
        mSelectColorModel.setSelectColor(color);
        if (arrListSelectColor.size() == 0) {
            mSelectColorModel.setCheckSelected(true);
        }
        mSelectColorModel.setRed(Color.red(color));
        mSelectColorModel.setGreen(Color.green(color));
        mSelectColorModel.setBlue(Color.blue(color));
//        if (mSelectColorModel.getRed() >= 128 && mSelectColorModel.getGreen() >= 128
//                && mSelectColorModel.getBlue() >= 128) {
//            arrListSelectColor.add(mSelectColorModel);
//        }
        arrListSelectColor.add(mSelectColorModel);
    }

    private void adapterSetForSelectColor() {
        for (int i = 0; i < Colour.arrColors.length; i++) {
            setDataForSelectColor(Colour.setColor(i));
        }

        mSelectColorAdapter = new SelectColorAdapter(getActivity());
        rvSelectColorLists.setAdapter(mSelectColorAdapter);
    }

    private void setDataForSelectColorSchemeSub(int color) {
        mSelectColorModel = new SelectColorModel();
        mSelectColorModel.setSelectColor(color);
        mSelectColorModel.setRed(Color.red(color));
        mSelectColorModel.setGreen(Color.green(color));
        mSelectColorModel.setBlue(Color.blue(color));
//        if (mSelectColorModel.getRed() >= 128 && mSelectColorModel.getGreen() >= 128
//                && mSelectColorModel.getBlue() >= 128) {
//            arrListSelectColorScheme.add(mSelectColorModel);
//        }
        arrListSelectColorScheme.add(mSelectColorModel);
    }

    private void setDataForSelectColorScheme(int position) {
        arrListSelectColorScheme = new ArrayList<>();
        SelectColorModel selectColorModel = arrListSelectColor.get(position);
        int[] colors = Colour.colorSchemeOfType(selectColorModel.getSelectColor(),
                Colour.ColorScheme.ColorSchemeAnalagous);

        setDataForSelectColorSchemeSub(colors[0]);
        setDataForSelectColorSchemeSub(colors[1]);
        setDataForSelectColorSchemeSub(colors[2]);
        setDataForSelectColorSchemeSub(colors[3]);

        colors = Colour.colorSchemeOfType(
                selectColorModel.getSelectColor(), Colour.ColorScheme.ColorSchemeMonochromatic);

        setDataForSelectColorSchemeSub(colors[0]);
        setDataForSelectColorSchemeSub(colors[1]);
        setDataForSelectColorSchemeSub(colors[2]);
        setDataForSelectColorSchemeSub(colors[3]);

        colors = Colour.colorSchemeOfType(selectColorModel.getSelectColor(),
                Colour.ColorScheme.ColorSchemeTriad);

        setDataForSelectColorSchemeSub(colors[0]);
        setDataForSelectColorSchemeSub(colors[1]);
        setDataForSelectColorSchemeSub(colors[2]);
        setDataForSelectColorSchemeSub(colors[3]);

        colors = Colour.colorSchemeOfType(
                selectColorModel.getSelectColor(), Colour.ColorScheme.ColorSchemeComplementary);

        setDataForSelectColorSchemeSub(colors[0]);
        setDataForSelectColorSchemeSub(colors[1]);
        setDataForSelectColorSchemeSub(colors[2]);
        setDataForSelectColorSchemeSub(colors[3]);
    }

    private void setUpSelectColorSchemeRecyclerView() {
        rvSelectSchemeColorLists = rootView.findViewById(R.id.rvSelectSchemeColorLists);
        rvSelectSchemeColorLists.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));
    }

    private void adapterSetForSelectColorScheme() {
        setDataForSelectColorScheme(0);
        mSelectColorSchemeAdapter = new SelectColorSchemeAdapter(getActivity());
        rvSelectSchemeColorLists.setAdapter(mSelectColorSchemeAdapter);
    }

    private void chooseColorAndShow(SelectColorModel mSelectColorModel) {
        ivSelectedColor.setImageDrawable(new ColorDrawable(mSelectColorModel.getSelectColor()));
        webView.evaluateJavascript("setcolors("+
                mSelectColorModel.getRed() +", "+
                mSelectColorModel.getGreen() +", "+
                mSelectColorModel.getBlue() +");", null);
    }

    private void setUpWebView() {
        webView = rootView.findViewById(R.id.webView);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setAppCacheEnabled(false);
        webView.clearCache(true);
//        webView.loadUrl("about:blank");
//        webView.reload();
//        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.addJavascriptInterface(new WebAppInterface(getActivity()), "Android");
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.setInitialScale(70);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            // chromium, enable hardware acceleration
//            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        } else {
//            // older android version, disable hardware acceleration
//            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }
//        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return super.shouldOverrideUrlLoading(view, url);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                 // gadetString="m0,192.65,552.15,art1,0.7*m1,153.65,524.65,art1,0.7*m2,121.65,490.15,art1,0.7*m3,96.15,447.65,art1,0.7*m4,77.65,401.65,art1,0.7*m5,68.65,352.15,art1,0.7*m6,67.65,298.65,art1,0.7*m7,78.65,245.65,art1,0.7*m8,98.65,192.65,art1,0.7*m9,129.65,147.65,art1,0.7*m10,166.15,113.65,art1,0.7*m11,210.15,86.65,art1,0.7*m12,260.65,69.65,art1,0.7*m13,311.65,67.65,art1,0.7*m14,365.65,79.15,art1,0.7*m15,412.65,101.65,art1,0.7*m16,452.65,134.15,art1,0.7*m17,484.65,173.65,art1,0.7*m18,509.15,220.65,art1,0.7*m19,524.65,268.65,art1,0.7*m20,528.65,321.65,art1,0.7*m21,523.65,375.15,art1,0.7*m22,509.15,428.65,art1,0.7*m23,482.65,475.15,art1,0.7*m24,451.65,515.65,art1,0.7*m25,410.65,547.65,art1,0.7*m26,416.85,516.35,art7,0.30000000000000004*m27,446.85,489.85,art7,0.30000000000000004*m28,472.85,455.35,art7,0.30000000000000004*m29,491.85,414.35,art7,0.30000000000000004*m30,500.85,371.35,art7,0.30000000000000004*m31,504.85,327.35,art7,0.30000000000000004*m32,499.85,284.85,art7,0.30000000000000004*m33,488.85,248.35,art7,0.30000000000000004*m34,472.85,210.85,art7,0.30000000000000004*m35,446.35,180.85,art7,0.30000000000000004*m36,413.35,149.35,art7,0.30000000000000004*m37,379.85,130.85,art7,0.30000000000000004*m38,343.85,119.35,art7,0.30000000000000004*m39,303.85,115.85,art7,0.30000000000000004*m40,268.85,120.85,art7,0.30000000000000004*m41,234.85,131.85,art7,0.30000000000000004*m42,201.85,150.35,art7,0.30000000000000004*m43,171.85,177.35,art7,0.30000000000000004*m44,146.85,209.85,art7,0.30000000000000004*m45,127.35,249.85,art7,0.30000000000000004*m46,115.85,292.85,art7,0.30000000000000004*m47,110.85,334.85,art7,0.30000000000000004*m48,117.35,378.85,art7,0.30000000000000004*m49,127.85,417.35,art7,0.30000000000000004*m50,147.35,455.35,art7,0.30000000000000004*m51,170.85,489.35,art7,0.30000000000000004*m52,203.85,516.85,art7,0.30000000000000004*m53,202.85,476.85,art7,0.30000000000000004*m54,180.85,443.85,art7,0.30000000000000004*m55,160.85,407.85,art7,0.30000000000000004*m56,148.85,367.85,art7,0.30000000000000004*m57,141.3,327.3,art5,0.4*m58,174.8,326.8,art5,0.4*m59,214.8,327.3,art5,0.4*m60,255.3,327.3,art5,0.4*m61,289.8,325.3,art5,0.4*m62,278.3,292.8,art5,0.4*m63,251.8,265.8,art5,0.4*m64,213.3,255.8,art5,0.4*m65,175.8,267.8,art5,0.4*m66,148.3,291.3,art5,0.4*m67,167.85,237.85,art7,0.30000000000000004*m68,191.85,202.35,art7,0.30000000000000004*m69,223.85,175.35,art7,0.30000000000000004*m70,257.85,156.85,art7,0.30000000000000004*m71,295.85,149.85,art7,0.30000000000000004*m72,333.85,152.35,art7,0.30000000000000004*m73,370.85,161.85,art7,0.30000000000000004*m74,404.85,186.35,art7,0.30000000000000004*m75,419.8,218.3,art5,0.4*m76,448.3,237.8,art5,0.4*m77,457.8,268.8,art5,0.4*m78,450.3,302.3,art5,0.4*m79,472.85,331.35,art7,0.30000000000000004*m80,467.85,370.85,art7,0.30000000000000004*m81,456.85,407.35,art7,0.30000000000000004*m82,439.85,444.85,art7,0.30000000000000004*m83,417.85,476.35,art7,0.30000000000000004*m84,426.8,324.8,art5,0.4*m85,391.3,327.3,art5,0.4*m86,365.8,309.3,art5,0.4*m87,351.8,276.3,art5,0.4*m88,360.8,240.8,art5,0.4*m89,387.3,218.8,art5,0.4*m90,360.35,195.85,art7,0.30000000000000004*m91,323.35,186.85,art7,0.30000000000000004*m92,284.85,183.85,art7,0.30000000000000004*m93,249.85,199.35,art7,0.30000000000000004*m94,219.85,223.35,art7,0.30000000000000004*m95,254.35,234.85,art7,0.30000000000000004*m96,293.35,220.85,art7,0.30000000000000004*m97,333.85,220.85,art7,0.30000000000000004*m98,326.35,253.85,art7,0.30000000000000004*m99,288.85,256.85,art7,0.30000000000000004*m100,319.35,287.85,art7,0.30000000000000004*m101,332.35,327.35,art7,0.30000000000000004*m102,320.85,361.35,art7,0.30000000000000004*m103,359.85,360.85,art7,0.30000000000000004*m104,396.85,361.35,art7,0.30000000000000004*m105,434.85,361.35,art7,0.30000000000000004*m106,180.85,361.35,art7,0.30000000000000004*m107,215.35,361.35,art7,0.30000000000000004*m108,250.85,361.35,art7,0.30000000000000004*m109,284.85,360.85,art7,0.30000000000000004*m110,243.8,470.3,art5,0.4*m111,287.3,470.8,art5,0.4*m112,331.3,470.8,art5,0.4*m113,373.8,470.8,art5,0.4*m114,374.3,501.8,art5,0.4*m115,330.3,502.3,art5,0.4*m116,287.3,502.3,art5,0.4*m117,243.3,502.3,art5,0.4*m118,243.8,536.3,art5,0.4*m119,287.3,535.8,art5,0.4*m120,330.3,535.8,art5,0.4*m121,374.3,535.3,art5,0.4*m122,373.8,567.8,art5,0.4*m123,330.3,567.3,art5,0.4*m124,286.3,567.3,art5,0.4*m125,243.8,567.8,art5,0.4*m126,264.3,595.8,art5,0.4*m127,309.3,593.3,art5,0.4*m128,354.3,593.3,art5,0.4*m129,329.3,619.3,art5,0.4*m130,286.3,618.8,art5,0.4*";
                        view.loadUrl("javascript:setFramsToHtml('"+gadetString+"');");

                // do your stuff here
                mKProgressHUD.dismiss();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        chooseColorAndShow(arrListSelectColor.get(0));
                    }
                };
                handler.postDelayed(runnable, 2000);

//                if (!allFramesJson.equals("")) {
//                    runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            webView.evaluateJavascript("setallframes("+ allFramesJson +");", null);
//                        }
//                    };
//                    handler.postDelayed(runnable, 3000);
//                }
            }

        });
        webView.setWebChromeClient(new WebChromeClient()
        {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if(newProgress==100)
                {
                    mKProgressHUD.dismiss();
                }
                else {mKProgressHUD.show();}
            }
        });

      /*  mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();*/

        webView.loadUrl("http://rockme.info/RockMeApi/Uploads/default.html");
        //webView.loadUrl("http://192.168.10.2/uploads/default.html");
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        if(type==1)
        {
            
            ArrayList<Artwork> artworks = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(modelParsedResponse.getResponse().getData().toString());
                //while (jsonObject.keys().hasNext())
                for(int i = 0; i<jsonArray.length() ; i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Artwork artwork = new Gson().fromJson(jsonObject.toString(),Artwork.class);
                    artworks.add(artwork);
                }
                if(!artworks.isEmpty())
                {
                    //artworks.get(0).getArtworkFile()
                    webView.loadUrl(artworks.get(0).getArtworkFile());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {
            mKProgressHUD.dismiss();
            try {
                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        mKProgressHUD.show();
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                "Artwork/ArtisticArtworks?gadgetId="+gadgetId,
                                null,
                                new CallBackRetrofit(type,
                                        WebViewFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public String getallframes() {
            return allFrames;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;
            case R.id.ivReturn:
                webView.loadUrl("javascript:undo();");
                break;
            case R.id.ivReset:
                webView.loadUrl("javascript:setFramsToHtml('"+gadetString+"');");
            case R.id.tvworking:
                webView.loadUrl("http://rockme.info/RockMeApi/Uploads/ios_HappyTounge.html");
                break;
            case R.id.tvNext:
                webView.evaluateJavascript("getallframesarray();", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        if (!s.contains("null")) {
                            if (!s.equals("[[[]]]")) {
                                allFrames = s;
                                boolean result = new CheckPermissions().
                                        checkPermissionExternelStorageAndCamera(getActivity(), WebViewFragment.this);
                                if (result) {
                                    redirect();
                                }
                            }
                        }
                    }
                });

                break;
            case R.id.ivPlus:
            webView.zoomIn();
                break;
            case R.id.ivMinus:
            webView.zoomOut();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CheckPermissions.MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    redirect();
                } else {
                    //code for deny
                    new CheckPermissions().
                            checkPermissionExternelStorageAndCamera(getActivity(), WebViewFragment.this);
                }
                break;
        }
    }

    private void redirect() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        webView.evaluateJavascript("hide();", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                webView.evaluateJavascript("getallframesjson();", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        allFramesJson = s;
                        webView.evaluateJavascript("pic();", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String s) {
                                base64Image = s;
                                base64Image = base64Image.replace("\"", "");
                                base64Image = base64Image.split("base64,")[1];
//                                decode base64 string to image
                                byte[] imageBytes = Base64.decode(base64Image, Base64.DEFAULT);
                        decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                        captureWebViewScreenShot();
                        removeSelectedColor();
                        arrListSelectColor.get(0).setCheckSelected(true);
                        Fragment fragment = new WorkPlaceMenuFragment();
                        Bundle args = new Bundle();
                        // args.putSerializable(getResources().getString(R.string.workplace_object), mWorkPlace);
//        args.putSerializable(getResources().getString(R.string.refrence), WebViewFragment.this);
                        args.putString(getResources().getString(R.string.webview_screenshot_path), file.getAbsolutePath());
                        args.putString(getResources().getString(R.string.all_frames), allFrames);
                        args.putString(getResources().getString(R.string.all_frames_json), allFramesJson);
                        fragment.setArguments(args);

                        ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());
                        mKProgressHUD.dismiss();

                            }
                        });
                    }
                });
            }
        });
    }

    private void captureWebViewScreenShot() {
        if (file != null) {
            file.delete();
        }

        file = ((HomeActivity) getActivity()).objGlobalHelperNormal.
        saveBitmapViewInFile(getActivity(), decodedImage);
    }

    private void removeSelectedColor() {
        SelectColorModel mSelectColorModel;
        for (int i = 0; i < arrListSelectColorScheme.size(); i++) {
            mSelectColorModel = arrListSelectColorScheme.get(i);
            if (mSelectColorModel.isCheckSelected()) {
                mSelectColorModel.setCheckSelected(false);
                break;
            }
        }

        for (int i = 0; i < arrListSelectColor.size(); i++) {
            mSelectColorModel = arrListSelectColor.get(i);
            if (mSelectColorModel.isCheckSelected()) {
                mSelectColorModel.setCheckSelected(false);
                break;
            }
        }
    }

    public class SelectColorAdapter extends RecyclerView.Adapter<SelectColorAdapter.ViewHolder> {

        private LayoutInflater mInflater;
        private Context context;
        private SelectColorModel mSelectColorModel;

        Animation animation;

        // data is passed into the constructor
        public SelectColorAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.context = context;
            animation = AnimationUtils.loadAnimation(context,
                    R.anim.slide_up);
        }

        // inflates the cell layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.select_color_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            mSelectColorModel = arrListSelectColor.get(position);
            holder.ivSelectColor.setImageDrawable(new ColorDrawable(mSelectColorModel.getSelectColor()));
            holder.ivSelectColor2.setImageDrawable(new ColorDrawable(mSelectColorModel.getSelectColor()));
            if (mSelectColorModel.isCheckSelected()) {
                holder.ivSelectColor2.setVisibility(View.VISIBLE);
                holder.ivSelectColor2.startAnimation(animation);
                holder.ivSelectColor.setVisibility(View.GONE);
                holder.ivSelectColor.setBackground(ContextCompat.getDrawable(context, R.drawable.select_circle));
            } else {
                holder.ivSelectColor.setBackground(ContextCompat.getDrawable(context, R.drawable.unselect_circle));
                holder.ivSelectColor2.setVisibility(View.GONE);
                holder.ivSelectColor.setVisibility(View.VISIBLE);
            }

            holder.rlSelectColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!arrListSelectColor.get(position).isCheckSelected()) {
                        setDataForSelectColorScheme(position);
                        removeSelectedColor();
                        checkColorSelected(position);
                        chooseColorAndShow(arrListSelectColor.get(position));

                    }
                }
            });
        }

        private void checkColorSelected(int position) {
            SelectColorModel mSelectColorModel;
            mSelectColorModel = arrListSelectColor.get(position);
            mSelectColorModel.setCheckSelected(true);
            mSelectColorAdapter.notifyItemRangeChanged(0, arrListSelectColor.size());
            mSelectColorSchemeAdapter.notifyItemRangeChanged(0, arrListSelectColorScheme.size());
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return arrListSelectColor.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView ivSelectColor,ivSelectColor2;
            private RelativeLayout rlSelectColor;

            public ViewHolder(View itemView) {
                super(itemView);
                ivSelectColor = itemView.findViewById(R.id.ivSelectColor);
                ivSelectColor2 = itemView.findViewById(R.id.ivSelectColor2);
                rlSelectColor = itemView.findViewById(R.id.rlSelectColor);
            }
        }

        // convenience method for getting data at click position
        public SelectColorModel getItem(int id) {
            return arrListSelectColor.get(id);
        }
    }

    public class SelectColorSchemeAdapter extends RecyclerView.Adapter<SelectColorSchemeAdapter.ViewHolder> {

        private LayoutInflater mInflater;
        private Context context;
        private SelectColorModel mSelectColorModel;

        // data is passed into the constructor
        public SelectColorSchemeAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.context = context;
        }

        // inflates the cell layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.select_color_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            mSelectColorModel = arrListSelectColorScheme.get(position);
            holder.ivSelectColor.setImageDrawable(new ColorDrawable(mSelectColorModel.getSelectColor()));
            if (mSelectColorModel.isCheckSelected()) {
                holder.ivSelectColor.setBackground(ContextCompat.getDrawable(context, R.drawable.select_circle));
            } else {
                holder.ivSelectColor.setBackground(ContextCompat.getDrawable(context, R.drawable.unselect_circle));
            }
            holder.rlSelectColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!arrListSelectColorScheme.get(position).isCheckSelected()) {
                        removeSelectedColor();
                        checkColorSelected(position);
                        chooseColorAndShow(arrListSelectColorScheme.get(position));
                    }
                }
            });
        }

        private void checkColorSelected(int position) {
            SelectColorModel mSelectColorModel;
            mSelectColorModel = arrListSelectColorScheme.get(position);
            mSelectColorModel.setCheckSelected(true);
            mSelectColorSchemeAdapter.notifyItemRangeChanged(0, arrListSelectColorScheme.size());
            mSelectColorAdapter.notifyItemRangeChanged(0, arrListSelectColor.size());
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return arrListSelectColorScheme.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder {
            private CircleImageView ivSelectColor;
            private RelativeLayout rlSelectColor;

            public ViewHolder(View itemView) {
                super(itemView);
                ivSelectColor = itemView.findViewById(R.id.ivSelectColor);
                rlSelectColor = itemView.findViewById(R.id.rlSelectColor);
            }
        }

        // convenience method for getting data at click position
        public SelectColorModel getItem(int id) {
            return arrListSelectColorScheme.get(id);
        }
    }
}
