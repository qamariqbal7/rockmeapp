package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.models.FAQs;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class FaqDetailFragment extends Fragment implements View.OnClickListener,View.OnTouchListener {

    private View rootView;
    private FAQs mFAQs;
    private TextView tvQuestion, tvAnswer;

    public FaqDetailFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mFAQs = (FAQs) bundle.getSerializable(getString(R.string.faq_object));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_faqs_detail, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        readBundle(getArguments());
        initialWork();
        intializeControlsAndListener();
        setData();

        return rootView;
    }

    private void setData() {
        tvQuestion.setText(getString(R.string.Question) + "\n" + mFAQs.getQuestion());
        tvAnswer.setText(getString(R.string.Answer) + "\n" + mFAQs.getAnswer());
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);

        tvQuestion = rootView.findViewById(R.id.tvQuestion);
        tvAnswer = rootView.findViewById(R.id.tvAnswer);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;
        }
    }
}
