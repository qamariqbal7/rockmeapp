package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.LikesAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ArtWorkFragment extends BaseFragment implements ServiceResponse {
    private View rootView;

    private ModelParsedResponse modelParsedResponse = null;
    private ArrayList<Artwork> artworks;
    private LikesAdapter likesAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_like,null,false);
        setRecyclerView();
        return rootView;
    }

    private void setRecyclerView()
    {
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        artworks = new ArrayList<>();
        likesAdapter = new LikesAdapter(getActivity(),artworks,false);
        recyclerView.setAdapter(likesAdapter);
        requestHttpCall(1,"");

      /*  recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Artwork artwork = artworks.get(position);

            }
        }));*/
    }

    @Override
    public void onResult(int type, HttpResponse o) {

        try {
            JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
            Iterator<String> keys = jsonObject.keys();

          /*  MainArtwork
            objMainArtwork = new Gson().fromJson((modelParsedResponse.getResponse().getData()), MainArtwork.class);
//            artworks.addAll(objMainArtwork.getArrayListArtwork());
            artworks.addAll(objMainArtwork.getPendingArtworks());
            artworks.addAll(objMainArtwork.getPrivateArtworks());
            artworks.addAll(objMainArtwork.getPublicArtworks());
            artworks.addAll(objMainArtwork.getSharedArtworks());
            artworks.addAll(objMainArtwork.getPurchasedArtworks());*/

            while (keys.hasNext())
            {
                if(keys.next().equals("PurchasedArtworks"))
                {
                    JSONArray jsonArray = jsonObject.getJSONArray("PurchasedArtworks");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        // Artwork artwork = jsonAdapter.fromJson(object.toString());
                        Artwork artwork = new Gson().fromJson((object.toString()), Artwork.class);

//                        int ArtworkId = object.getInt("ArtworkId");
//                        String ArtworkName = object.getString("ArtworkName");
//                        String CategoryId = object.getString("CategoryId");
//                        String CategoryName = object.getString("CategoryName");
//                        String UserId = object.getString("UserId");
//                        String ArtworkImage = object.getString("ArtworkImage");
//                        artwork.setArtworkId(ArtworkId);
//                        artwork.setArtworkName(ArtworkName);
//                        artwork.setArtworkImage(ArtworkImage);
                        artworks.add(artwork);
                    }

                }
            }
            likesAdapter.notifyDataSetChanged();
           // Log.e("artworks",objMainArtwork.getPurchasedArtworks().size()+"");
        }
        catch (JSONException e){}
       // catch (IOException i){}

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
               // Log.e("responseLike", modelParsedResponse.getResponse().getData().toString());


            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))
                  //"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIyIiwibmJmIjoxNTQ1NjI5MjU2LCJleHAiOjE1NzcxNjUyNTYsImlhdCI6MTU0NTYyOTI1NiwiaXNzIjoic2VsZiIsImF1ZCI6Imh0dHA6Ly9yb2NrLW1lLmNvLyJ9.IdmxMDKx96Xfphz1r_jloewJWHmPRMXZAfQZEcEJ8s4"
                    });

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.userallartworks),
                                null,
                                new CallBackRetrofit(type,
                                        ArtWorkFragment.this
                                ));
                    }

                } else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }
}
