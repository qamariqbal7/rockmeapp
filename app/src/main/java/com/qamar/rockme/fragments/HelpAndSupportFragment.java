package com.qamar.rockme.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.FaqAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.FAQs;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class HelpAndSupportFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse, FaqAdapter.ItemClickListener {

    private View rootView;

    private FaqAdapter mFaqAdapter;
    private RecyclerView rvFaq;
    private ArrayList<FAQs> arrayListFaq = new ArrayList<>();

    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;

    private Dialog dialogCustomQuery;
    private EditText etSubject, etMessage;

    public HelpAndSupportFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_help_and_support, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        intializeControlsAndListener();

        setUpFaqRecyclerView();
     //   adapterSetForFaq();

        if (arrayListFaq.size() == 0) {
            loadingFaqs();
        }

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        ImageView ivMenu1  = rootView.findViewById(R.id.ivMenu1);

        rootView.findViewById(R.id.tvCustomQuery).setOnTouchListener(this);
        rootView.findViewById(R.id.tvCustomQuery).setOnClickListener(this);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
        setBackEvent(ivMenu1);
    }

    private void setUpFaqRecyclerView() {
        rvFaq = rootView.findViewById(R.id.rvFaq);
        rvFaq.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    //private void adapterSetForFaq(ArrayList<FAQs> arrayListFaq)
    private void adapterSetForFaq()
    {
        /*for (int i = 0; i<5; i++)
        {
            arrayListFaq.add(new FAQs(i,"this question this question this question this question","yes yes yes"));
        }*/

        mFaqAdapter = new FaqAdapter(getActivity(),arrayListFaq);
        rvFaq.setAdapter(mFaqAdapter);
        mFaqAdapter.notifyDataSetChanged();
        mFaqAdapter.setClickListener(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        arrayListFaq.clear();
    }

    private void loadingFaqs() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListFaq = new ArrayList<>();
        requestHttpCall(1, "");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvCustomQuery:
                dialogCustomQuery = ((HomeActivity) getActivity()).objGlobalHelperNormal.
                        showDialog(R.layout.dialog_custom_query, getActivity(),
                                true, android.R.style.Theme_Translucent_NoTitleBar, R.style.DialogAnimationSlideUp);
                initCustomQueryDialogChild();
                break;

            case R.id.tvSEND:
                dialogCustomQuery.dismiss();
                break;

            case R.id.tvCANCEL:
                dialogCustomQuery.dismiss();
                break;
        }
    }

    private void initCustomQueryDialogChild() {
        etSubject = dialogCustomQuery.findViewById(R.id.etSubject);
        etMessage = dialogCustomQuery.findViewById(R.id.etMessage);
        dialogCustomQuery.findViewById(R.id.tvSEND).setOnClickListener(this);
        dialogCustomQuery.findViewById(R.id.tvSEND).setOnTouchListener(this);
        dialogCustomQuery.findViewById(R.id.tvCANCEL).setOnClickListener(this);
        dialogCustomQuery.findViewById(R.id.tvCANCEL).setOnTouchListener(this);
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        Log.e("FaQResponse",o.getResponseData());
        mKProgressHUD.dismiss();
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {
                 //   if (arrayListFaq.size() > 0) {
                      //  adapterSetForFaq();
                        try {
                            JSONObject jsonObject = new JSONObject(o.getResponseData());
                            JSONObject response = jsonObject.getJSONObject("response");
                            JSONObject data = response.getJSONObject("data");

                            Iterator<String> iter = data.keys();
                            while (iter.hasNext()) {
                                String key = iter.next();
                                JSONArray Gadget = data.getJSONArray(key);
                                for(int i = 0; i<Gadget.length() ; i++)
                                {
                                    JSONObject object = Gadget.getJSONObject(i);
                                    FAQs objFAQs = new Gson().fromJson(object.toString(),FAQs.class);
                                    objFAQs.setExpand(false);
                                    arrayListFaq.add(objFAQs);
                                }
                            }

                           /* JSONArray Gadget = data.getJSONArray("Gadget Sync - Wi-Fi / Connectivity");
                            for(int i = 0; i<Gadget.length() ; i++)
                            {
                                JSONObject object = Gadget.getJSONObject(i);
                                int faqTopicId = object.getInt("faqTopicId");
                                String question = object.getString("question");
                                String answer = object.getString("answer");
                                FAQs objFAQs = new FAQs(faqTopicId,question,answer);
                                FAQs objFAQs = new Gson().fromJson(object.toString(),FAQs.class);
                                arrayListFaq.add(objFAQs);
                            }
                            JSONArray Battery = data.getJSONArray("Gadgets / Battery / Charging");
                            for(int i = 0; i<Battery.length() ; i++)
                            {
                                JSONObject object = Battery.getJSONObject(i);
                                int faqTopicId = object.getInt("faqTopicId");
                                String question = object.getString("question");
                                String answer = object.getString("answer");
                                FAQs objFAQs = new FAQs(faqTopicId,question,answer);
                                FAQs objFAQs = new Gson().fromJson(object.toString(),FAQs.class);
                                arrayListFaq.add(objFAQs);
                            }*/
                           /* JSONArray Marketplace = data.getJSONArray("RockMe Artwork Marketplace");
                            for(int i = 0; i<Marketplace.length() ; i++)
                            {
                                JSONObject object = Battery.getJSONObject(i);
                                int faqTopicId = object.getInt("faqTopicId");
                                String question = object.getString("question");
                                String answer = object.getString("answer");
                                FAQs objFAQs = new FAQs(faqTopicId,question,answer);
                                arrayListFaq.add(objFAQs);
                            }
                            JSONArray Support = data.getJSONArray("Support / Others");
                            for(int i = 0; i<Support.length() ; i++)
                            {
                                JSONObject object = Battery.getJSONObject(i);
                                int faqTopicId = object.getInt("faqTopicId");
                                String question = object.getString("question");
                                String answer = object.getString("answer");
                                FAQs objFAQs = new FAQs(faqTopicId,question,answer);
                                arrayListFaq.add(objFAQs);
                            }*/
                            //  Log.e("SizeList",arrayListFaq.size()+"");

                            adapterSetForFaq();
                        }
                        catch (JSONException j){}

                 //   }
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                if (type == 1) {

                  /*  for (int i = 0; i < modelParsedResponse.getResponse().getData().getAsJsonArray().size(); i++) {
                        FAQs objFAQs;
                        objFAQs = new Gson().fromJson
                                ((modelParsedResponse.getResponse().getData().getAsJsonArray().get(i)), FAQs.class);
                        arrayListFaq.add(objFAQs);
                    }*/

                }

            }
            catch (JsonParseException exp) {
                modelParsedResponse = null;
            }
            catch (Exception exp) {
                modelParsedResponse = null; }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.faqsAllListUrl),
                                null,
                                new CallBackRetrofit(type,
                                        HelpAndSupportFragment.this
                                ));
                    }

                } else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        Fragment fragment = new FaqDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(getResources().getString(R.string.faq_object), arrayListFaq.get(position));
        fragment.setArguments(args);

        ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());
    }
}
