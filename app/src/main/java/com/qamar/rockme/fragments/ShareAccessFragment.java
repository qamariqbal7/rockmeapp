package com.qamar.rockme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.interfaces.RefreshList;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.SharedGadgetUsersList;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class ShareAccessFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse, Serializable, RefreshList {

    private View rootView;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
   // private SwipeRefreshLayout swipeToRefresh;
    private TextView tvMsg,tvGadgetName;

    private RecyclerView rvShareAccessList;
    ArrayList<SharedGadgetUsersList> arrayListShareAccess = new ArrayList<>();
    private SharedGadgetUserListAdapter mSharedGadgetUserListAdapter;

    private int gadgetId, currentPosition;
    private UserData mUserData;
    private ImageView ivProfilePic;

    public ShareAccessFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            gadgetId = bundle.getInt(getString(R.string.gadget_id));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_share_access, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        initialWork();
        readBundle(getArguments());
        intializeControlsAndListener();
        setDataFromFile();

       // setRefreshListener();
        setUpSharedGadgetRecyclerView();
        loadRefreshData();
        adapterSetForSharedGadget();

        if (arrayListShareAccess.size() == 0) {
            loadingSharedGadget();
        }

        return rootView;
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        rootView.findViewById(R.id.ivMenu1).setOnTouchListener(this);
        rootView.findViewById(R.id.ivMenu1).setOnClickListener(this);
//        rootView.findViewById(R.id.ivMenu2).setOnTouchListener(this);
//        rootView.findViewById(R.id.ivMenu2).setOnClickListener(this);

        tvMsg = rootView.findViewById(R.id.tvMsg);
        tvGadgetName = rootView.findViewById(R.id.tvGadgetName);
        ivProfilePic = rootView.findViewById(R.id.ivProfilePic);
        CircleImageView profile_image = rootView.findViewById(R.id.profile_image);
        loadProfileImage(profile_image);
     /*   swipeToRefresh = rootView.findViewById(R.id.swipeToRefresh);

        swipeToRefresh.setSize(CircularProgressDrawable.LARGE);
        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.mainYellowColor),
                ContextCompat.getColor(getActivity(), R.color.mainYellowHoverColor),
                ContextCompat.getColor(getActivity(), R.color.mainGreenColor),
                ContextCompat.getColor(getActivity(), R.color.mainGreenHoverColor));*/
    }

    private void setDataFromFile() {
        mUserData = FrontEngine.getInstance().mUserData;
        if (mUserData == null) {
            FrontEngine.getInstance().initializeUser(getActivity());
            mUserData = FrontEngine.getInstance().mUserData;
        }
    }

   /* public void setRefreshListener() {
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadRefreshData();
            }
        });
    }*/

    private void setUpSharedGadgetRecyclerView() {
        rvShareAccessList = rootView.findViewById(R.id.rvShareAccessList);
        rvShareAccessList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void adapterSetForSharedGadget() {
        arrayListShareAccess = new ArrayList<>();
        mSharedGadgetUserListAdapter = new SharedGadgetUserListAdapter(getActivity());
        rvShareAccessList.setAdapter(mSharedGadgetUserListAdapter);
        rvShareAccessList.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
        mSharedGadgetUserListAdapter.notifyDataSetChanged();
        ViewCompat.setNestedScrollingEnabled(rvShareAccessList,false);
    }

    private void loadRefreshData() {

        requestHttpCall(1, "");
       /* rvShareAccessList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });*/
    }

    private void loadingSharedGadget() {
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        arrayListShareAccess = new ArrayList<>();
        requestHttpCall(1, "");
       /* swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefresh.setRefreshing(false);
            }
        });*/
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivMenu1:
                getActivity().onBackPressed();
                break;

            case R.id.ivMenu2:
                Fragment fragment = new ContactsFragment();
                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.user_object), mUserData);
                args.putBoolean(getString(R.string.contact_check), false);
                args.putInt(getString(R.string.gadget_id), gadgetId);
                args.putSerializable(getString(R.string.refrence), ShareAccessFragment.this);
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;
        }
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        refreshComplete();
        Log.e("getResponseData",o.getResponseData());
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 1) {
                    if (arrayListShareAccess.size() > 0) {
                        mSharedGadgetUserListAdapter.notifyDataSetChanged();
                        tvMsg.setVisibility(View.INVISIBLE);
                    } else {
                        adapterSetForSharedGadget();
                        tvMsg.setVisibility(View.VISIBLE);
                    }

                    try {
                        JSONObject jsonObject = new JSONObject(modelParsedResponse.toString());
                        JSONObject response = jsonObject.getJSONObject("response");
                        JSONObject data = response.getJSONObject("data");

                        JSONObject UserGadgetInfo = data.getJSONObject("UserGadgetInfo");
                        String UserGadgetId = UserGadgetInfo.getString("UserGadgetId");
                        String UserGadgetName = UserGadgetInfo.getString("UserGadgetName");
                        String GadgetDefaultImage = UserGadgetInfo.getString("GadgetDefaultImage");

                        Glide.with(getActivity()).load(GadgetDefaultImage).into(ivProfilePic);
                        tvGadgetName.setText(UserGadgetName);
                    }
                    catch (JSONException e){}



                } else if (type == 2) {
                    arrayListShareAccess.remove(currentPosition);
                    if (arrayListShareAccess.size() > 0) {
                        adapterSetForSharedGadget();
                        tvMsg.setVisibility(View.INVISIBLE);
                    } else {
                        adapterSetForSharedGadget();
                        tvMsg.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
               Log.e("modelParsedResponse",modelParsedResponse.toString());
                if (type == 1) {

                  JSONObject jsonObject = new JSONObject(modelParsedResponse.toString());
                  JSONObject response = jsonObject.getJSONObject("response");
                  JSONObject data = response.getJSONObject("data");


                  JSONArray Followers = data.getJSONArray("Followers");
                    for (int i = 0; i < Followers.length(); i++) {
                        JSONObject object = Followers.optJSONObject(i);
                        String UserId = object.getString("UserId");
                        String Username = object.getString("Username");
                        String FullName = object.getString("FullName");
                        String ProfilePicture = object.getString("ProfilePicture");
                        String HasShared = object.getString("HasShared");
                        SharedGadgetUsersList objSharedGadgetUsersList = new SharedGadgetUsersList();
                        objSharedGadgetUsersList.setUserId(UserId);
                        objSharedGadgetUsersList.setUsername(Username);
                        objSharedGadgetUsersList.setFullName(FullName);
                        objSharedGadgetUsersList.setProfilePicture(ProfilePicture);
                        objSharedGadgetUsersList.setHasShared(HasShared);
                        //SharedGadgetUsersList objSharedGadgetUsersList;objSharedGadgetUsersList = new Gson().fromJson((Followers.getJSONObject(i).toString()), SharedGadgetUsersList.class);
                        arrayListShareAccess.add(objSharedGadgetUsersList);
                    }
                } else if (type == 2) {


                }

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            refreshComplete();
            if (o != null && o.getResponseCode() != 500 && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                  //  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIyIiwibmJmIjoxNTQ1NjI5MjU2LCJleHAiOjE1NzcxNjUyNTYsImlhdCI6MTU0NTYyOTI1NiwiaXNzIjoic2VsZiIsImF1ZCI6Imh0dHA6Ly9yb2NrLW1lLmNvLyJ9.IdmxMDKx96Xfphz1r_jloewJWHmPRMXZAfQZEcEJ8s4"
                      ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))
                    });

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.sharedGadgetUserListUrl) + "?gadgetId= " + gadgetId,
                                null,
                                new CallBackRetrofit(type,
                                        ShareAccessFragment.this
                                ));
                    } else if (type == 2) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.removeSharedGadgetAccessUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        ShareAccessFragment.this
                                ));
                    }
                    else  if (type == 3) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.sharedGadgetBulkUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        ShareAccessFragment.this
                                ));
                    }

                } else {
                    refreshComplete();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    private void refreshComplete() {
        mKProgressHUD.dismiss();
        //swipeToRefresh.setRefreshing(false);
        rvShareAccessList.setOnTouchListener(null);
        //setRefreshListener();
//        if (arrayListShareAccess.size() > 0) {
//            tvMsg.setVisibility(View.INVISIBLE);
//        } else {
//            tvMsg.setVisibility(View.VISIBLE);
//        }
    }

    //get parameters start
    private String getPostParameters(String gadgetId, String userId) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("gadgetId", gadgetId);
            jsonObject.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void refreshList() {
        arrayListShareAccess = new ArrayList<>();
    }

    public class SharedGadgetUserListAdapter extends RecyclerView.Adapter<SharedGadgetUserListAdapter.ViewHolder> {

        private LayoutInflater mInflater;
        private Context context;
        private SharedGadgetUsersList mSharedGadgetUsersList;

        // data is passed into the constructor
        public SharedGadgetUserListAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.context = context;
        }

        // inflates the cell layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.adapter_share_access, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            mSharedGadgetUsersList = arrayListShareAccess.get(position);

            holder.tvName.setText(mSharedGadgetUsersList.getFullName());
            holder.tvUserName.setText(mSharedGadgetUsersList.getUsername());
            Glide.with(context).load(mSharedGadgetUsersList.getProfilePicture()).into(holder.profile_image2);
         /*   holder.tvDate.setText(mSharedGadgetUsersList.getSharedDate());

            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currentPosition = position;

                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();

                    requestHttpCall(2, getPostParameters("" + arrayListShareAccess.get(position).getGadgetId(),
                            "" + arrayListShareAccess.get(position).getUser().getUserId()));
                }
            });*/


            holder.tvShareArtwork.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mKProgressHUD.show();
                    requestHttpCall(3,getPostParameters(mSharedGadgetUsersList.getUserId()));
                }
            });
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return arrayListShareAccess.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView tvName,tvUserName,tvShareArtwork;
            private Button btnRemove;
            private CircleImageView profile_image2;

            public ViewHolder(View itemView) {
                super(itemView);

                tvName = itemView.findViewById(R.id.tvFeedTittleOld);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvShareArtwork = itemView.findViewById(R.id.tvShareArtwork);
                profile_image2 = itemView.findViewById(R.id.profile_image2);
              //  tvDate = itemView.findViewById(R.id.tvDate);
              //  btnRemove = itemView.findViewById(R.id.btnRemove);

//                itemView.setOnClickListener(this);


            }

//            @Override
//            public void onClick(View view) {
//                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
//            }
        }

        // convenience method for getting data at click position
        public SharedGadgetUsersList getItem(int id) {
            return arrayListShareAccess.get(id);
        }

//        // allows clicks events to be caught
//        public void setClickListener(ItemClickListener itemClickListener) {
//            this.mClickListener = itemClickListener;
//        }
//
//        // parent activity will implement this method to respond to click events
//        public interface ItemClickListener {
//            void onItemClick(View view, int position);
//        }
    }

    private String getPostParameters(String userId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("gadgetId", gadgetId);
            jsonObject.put("userIds", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
