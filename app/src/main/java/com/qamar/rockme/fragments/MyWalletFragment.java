package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.MyWalletAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.Wallet;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyWalletFragment extends BaseFragment implements ServiceResponse {

    private View rootView;
    RecyclerView rvWallet;
    private ModelParsedResponse modelParsedResponse = null;

    private ArrayList<Wallet> wallets;
    private MyWalletAdapter walletAdapter;
    private TextView tvPrice;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_wallet,null);
        setRecyclerView();
        intializeControlsAndListener();
        return rootView;
    }

    private void intializeControlsAndListener()
    {
        ImageView ivMenu1 = rootView.findViewById(R.id.ivMenu1);
        tvPrice = rootView.findViewById(R.id.tvPrice);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        loadProfileImage(circleImageView);
        setBackEvent(ivMenu1);
    }

    private void setRecyclerView()
    {
        rvWallet = rootView.findViewById(R.id.rvWallet);
        rvWallet.setLayoutManager(new LinearLayoutManager(getActivity()));
        wallets = new ArrayList<>();
        walletAdapter = new MyWalletAdapter(getActivity(),wallets);
        rvWallet.setAdapter(walletAdapter);
        requestHttpCall(1,"");
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        if (modelParsedResponse != null) {
         //  Log.e("responseData",o.getResponseData());
            if (o.getResponseCode() == 200) {
                if (type == 1) {
                    Log.e("ResponseData",o.getResponseData());

                    try {
                        JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());

                        String UserID = jsonObject.getString("UserID");
                        String UserName = jsonObject.getString("UserName");
                        String TotalCoins = jsonObject.getString("TotalCoins");
                        tvPrice.setText(TotalCoins);
                        JSONArray History_Arr = jsonObject.getJSONArray("History");
                        for(int i = 0; i<History_Arr.length(); i++)
                        {
                            JSONObject object = History_Arr.getJSONObject(i);
                            String History = object.getString("History");
                            String ActionDate = object.getString("ActionDate");
                            String Amount = object.getString("Amount");
                            Wallet wallet = new Wallet(History,ActionDate,Amount);
                            wallets.add(wallet);
                        }
                        walletAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }
    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {
        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

    }

    @Override
    public void requestHttpCall(final int type, String... params) {
        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    HashMap map;
                    map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                            ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

                    if (type == 1) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                getString(R.string.userWallet),
                                null,
                                new CallBackRetrofit(type,
                                        MyWalletFragment.this
                                ));
                    }

                }  else {

                    ((HomeActivity) getActivity()).objGlobalHelperNormal.
                            callDialog(getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }
}
