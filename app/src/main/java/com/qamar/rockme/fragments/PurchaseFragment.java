package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class PurchaseFragment extends BaseFragment implements View.OnClickListener {
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_purchase,null);
        intializeControlsAndListener();
        return rootView;
    }

    private void intializeControlsAndListener() {
        rootView.findViewById(R.id.tvNext).setOnClickListener(this);
        ImageView ivMenu1 = rootView.findViewById(R.id.ivMenu1);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        setBackEvent(ivMenu1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvNext:
                ((HomeActivity)getActivity()).replaceFragment(new ArtworkViewFragment(),getActivity());
                break;
        }
    }
}
