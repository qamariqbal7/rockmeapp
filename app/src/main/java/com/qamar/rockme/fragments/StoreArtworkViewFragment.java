package com.qamar.rockme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.qamar.rockme.R;
import com.qamar.rockme.activity.HomeActivity;
import com.qamar.rockme.adapters.CommentAdapter;
import com.qamar.rockme.helper.CheckInternet;
import com.qamar.rockme.helper.FrontEngine;
import com.qamar.rockme.models.Artwork;
import com.qamar.rockme.models.Comment;
import com.qamar.rockme.models.ModelParsedResponse;
import com.qamar.rockme.models.UserData;
import com.qamar.rockme.retrofit.CallBackRetrofit;
import com.qamar.rockme.retrofit.HttpResponse;
import com.qamar.rockme.retrofit.RetrofitFactory;
import com.qamar.rockme.retrofit.ServiceResponse;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ishaq on 11/14/2017.
 */

public class StoreArtworkViewFragment extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener, ServiceResponse {

    private View rootView;
    private Artwork mArtwork;
    private TextView tvTitle, tvHeartCount, tvPurchaseCoin, tvPurchase,tvName,tvArtWork,
            tvViews,tvMoney,tvComment,tvAddFollower,tvRemoveFollver;
    private ImageView ivPic, ivHeart, ivCoins,ivSend;
    private ProgressBar pbPic;
    private KProgressHUD mKProgressHUD;
    private ModelParsedResponse modelParsedResponse = null;
    private UserData mUserData = null;
    private CheckBox cbFav,cbChate,cbShare;
    private String mArtWorkEndPoint = "";
    private String macAddress;
    private String gadgetCommand;
    private JSONObject isGadgetDefault;
    private String ItemID;
    private CardView cvComment;
    private EditText etComment;
    private NestedScrollView nestScrollView;
    private String addFollower;

    private CommentAdapter commentAdapter;
    private ArrayList<Comment> comments = new ArrayList<>();
    private CircleImageView profile_image2;
    private boolean isBuy,isLiked,isHasAdded;
    private Button btBuy;

    public StoreArtworkViewFragment() {

    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mArtwork = (Artwork) bundle.getSerializable(getString(R.string.artwork_object));
            String ArtworkID = bundle.getString("ArtworkID");
            mArtWorkEndPoint = "artwork/artworkdetail?artwork="+ArtworkID;
            ItemID = ArtworkID;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_store_artwork_view, container,
                false);
        ((HomeActivity) getActivity()).changeStatusBarColor(R.color.mainGreenColor);
        readBundle(getArguments());
        setDataFromFile();
        initialWork();
        intializeControlsAndListener();
        seedData();

       /* mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();*/
       // requestHttpCall(2, getPostParametersLikeView(3));

        setRecyclerView();

        return rootView;
    }

    private void setRecyclerView()
    {
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        commentAdapter = new CommentAdapter(getActivity(),comments);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(commentAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void setDataFromFile() {
        mUserData = FrontEngine.getInstance().mUserData;
        if (mUserData == null) {
            FrontEngine.getInstance().initializeUser(getActivity());
            mUserData = FrontEngine.getInstance().mUserData;
        }
    }

    private void initialWork() {
        ((HomeActivity) getActivity()).closeDrawer();
    }

    private void intializeControlsAndListener() {
        mKProgressHUD = KProgressHUD.create(getActivity());

        tvTitle = rootView.findViewById(R.id.tvFeedText);
        tvHeartCount = rootView.findViewById(R.id.tvHeartCount);
        tvPurchaseCoin = rootView.findViewById(R.id.tvPurchaseCoin);
        tvPurchase = rootView.findViewById(R.id.tvPurchase);
        tvName = rootView.findViewById(R.id.tvFeedTittleOld);
        tvArtWork = rootView.findViewById(R.id.tvArtWork);
        tvViews = rootView.findViewById(R.id.tvFeedTittleOld);
        tvMoney = rootView.findViewById(R.id.tvMoney);
        tvComment = rootView.findViewById(R.id.tvComment);
        tvAddFollower = rootView.findViewById(R.id.tvAddFollower);
        tvRemoveFollver = rootView.findViewById(R.id.tvRemoveFollver);
        cbFav = rootView.findViewById(R.id.cbFav);
        cbChate = rootView.findViewById(R.id.cbChate);
        cbShare = rootView.findViewById(R.id.cbShare);
        profile_image2 = rootView.findViewById(R.id.profile_image2);

        etComment = rootView.findViewById(R.id.etComment);
        cvComment = rootView.findViewById(R.id.cvComment);
        ivPic = rootView.findViewById(R.id.ivPic);
        ivHeart = rootView.findViewById(R.id.ivHeart);
        ivCoins = rootView.findViewById(R.id.ivCoins);
        ivSend = rootView.findViewById(R.id.ivSend);
        pbPic = rootView.findViewById(R.id.pbPic);
        nestScrollView = rootView.findViewById(R.id.nestScrollView);
        btBuy = rootView.findViewById(R.id.btBuy);

        rootView.findViewById(R.id.llGadgetPreview).setOnTouchListener(this);
        rootView.findViewById(R.id.llGadgetPreview).setOnClickListener(this);
        rootView.findViewById(R.id.llPurchase).setOnTouchListener(this);
        rootView.findViewById(R.id.llPurchase).setOnClickListener(this);
        rootView.findViewById(R.id.cbShare).setOnClickListener(this);
        rootView.findViewById(R.id.cbFav).setOnClickListener(this);
        rootView.findViewById(R.id.profile_image2).setOnClickListener(this);
        rootView.findViewById(R.id.tvFeedTittleOld).setOnClickListener(this);
        btBuy.setOnClickListener(this);
        ivHeart.setOnClickListener(this);
        tvComment.setOnClickListener(this);
        ivSend.setOnClickListener(this);
        tvAddFollower.setOnClickListener(this);
        tvRemoveFollver.setOnClickListener(this);

        ImageView ivMenu1 = rootView.findViewById(R.id.ivMenu1);

        CircleImageView circleImageView = rootView.findViewById(R.id.profile_image);
        goProfile(circleImageView);
        setBackEvent(ivMenu1);
        loadProfileImage(circleImageView);

        requestHttpCall(4,"");
        requestHttpCall(1, getPostParametersLikeView(3));
        mKProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(getResources().getColor(R.color.black_50))
                .setAnimationSpeed(2)
                .setCancellable(false)
                .show();
        requestHttpCall(7,"");

        etComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                ivSend.performClick();
                return false;
            }
        });
    }
    public void pushCommandtoFirebase(String MacAddress,String Command)
    {
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(MacAddress);
        myRef.child("status").child("AF").setValue("1");
        myRef.child("user").child("A").setValue("1");
        myRef.child("user").child("B").setValue("10");
        myRef.child("user").child("C").setValue(Command);
    }
    private void seedData() {
        if (mArtwork != null) {
            tvTitle.setText(mArtwork.getArtworkName());

            if (mArtwork.getArtworkImage() != null) {
                ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideNormalImageFitCenter(getActivity(), ivPic
                        , mArtwork.getArtworkImage(),
                        pbPic, R.drawable.profile);
            } else {
                ((HomeActivity) getActivity()).objGlobalHelperNormal.setGlideNormalImageFitCenter(getActivity(), ivPic
                        , "",
                        pbPic, R.drawable.profile);
            }

            //if (mArtwork.getIsLiked() == 0) {
              //  ivHeart.setImageResource(R.drawable.heart2);
            //} else {
              //  ivHeart.setImageResource(R.drawable.heart2_fill);
            //}
           // cbFav.setButtonDrawable(R.drawable.heart2_fill);
            cbFav.setChecked(true);
            tvHeartCount.setText("" + mArtwork.getLikes());
            tvPurchaseCoin.setText("" + mArtwork.getArtworkAmount());

            if (mArtwork.getIsPurchased() == 0) {
                ivCoins.setVisibility(View.VISIBLE);
                tvPurchaseCoin.setVisibility(View.VISIBLE);
                tvPurchase.setText(getString(R.string.PURCHASE));
            } else {
                ivCoins.setVisibility(View.GONE);
                tvPurchaseCoin.setVisibility(View.GONE);
                tvPurchase.setText(getString(R.string.PURCHASED));
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.setAlpha(0.4f);
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                v.setAlpha(1f);
                break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.cbFav:
               // if (mArtwork.getIsLiked() == 0)
                {
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                     if(cbFav.isChecked()==true) {
                         requestHttpCall(1, getPostParametersLikeView(2));
                     }
                     else
                     {
                         requestHttpCall(0,"");
                     }
                }
                /*else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), getString(R.string.alreadyLike));
                }*/
                break;

            case R.id.llPurchase:
                if (mArtwork.getIsPurchased() == 0) {
                    mKProgressHUD = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(getResources().getColor(R.color.black_50))
                            .setAnimationSpeed(2)
                            .setCancellable(false)
                            .show();
                    requestHttpCall(3, "");
                } else {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), getString(R.string.purchasedLike));
                }
                break;

            case R.id.llGadgetPreview:
                Fragment fragment = new MyGadgetsFragment();
                Bundle args = new Bundle();
                args.putString(getString(R.string.gadget_command), mArtwork.getGadgetCommand());
                args.putBoolean(getString(R.string.isPreview), true);
                fragment.setArguments(args);

                ((HomeActivity) getActivity()).replaceFragment(fragment, getActivity());
                break;
            case R.id.cbShare:
                //((HomeActivity) getActivity()).replaceFragment(new ShareAccessFragment(), getActivity());
                ((HomeActivity) getActivity()).objGlobalHelperNormal.shareCodeIntent
                        ((HomeActivity) getActivity(), getActivity(),
                                getActivity().getString(R.string.app_name), getActivity().getString(R.string.app_name));
                break;
            case R.id.profile_image2:
                fragment = new CreatorProfile();
                args = new Bundle();
                //args.putSerializable(getResources().getString(R.string.workplace_object), mArtwork);
                args.putString("UserId", addFollower);
                fragment.setArguments(args);
                ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());
                //((HomeActivity)getActivity()).replaceFragment(new CreatorProfile(),getActivity());
                break;
            case R.id.tvFeedTittleOld:
                fragment = new CreatorProfile();
                args = new Bundle();
                //args.putSerializable(getResources().getString(R.string.workplace_object), mArtwork);
                args.putString("UserId", addFollower);
                fragment.setArguments(args);
                ((HomeActivity) getActivity()).replaceFragment2(fragment, getActivity());

                //((HomeActivity)getActivity()).replaceFragment(new CreatorProfile(),getActivity());
                break;
            case R.id.btBuy:
                if(isBuy)
                {
                   //((HomeActivity)getActivity()).replaceFragment2(new WebViewFragment(),getActivity());
                   // mKProgressHUD.show();
                    if(isGadgetDefault!=null)
                    {
                        pushCommandtoFirebase(macAddress,gadgetCommand);
                    }
                    else
                    {
                        showToast("There is no gadget added against this artwork.");
                    }

                }
                else {
                    requestHttpCall(3,"");
                    mKProgressHUD.show();
                }

                break;
            case R.id.tvComment:
              //  cvComment.setVisibility(View.VISIBLE);
               // nestScrollView.smoothScrollTo(0,tvComment.getBottom());
                postComment();
                break;
            case R.id.ivSend:
                postComment();
                break;
            case R.id.tvAddFollower:
                requestHttpCall(6,"");
                mKProgressHUD.show();
                break;
            case R.id.tvRemoveFollver:
                requestHttpCall(8,"");
                mKProgressHUD.show();
                break;
        }
    }

    private void postComment()
    {
        String comment = etComment.getText().toString();
        if(comment.equals(""))
        {
            Toast.makeText(getActivity(), "please enter comment", Toast.LENGTH_SHORT).show();
            return;
        }
        //cvComment.setVisibility(View.GONE);
        requestHttpCall(5,getPostParameters());
        mKProgressHUD.show();
    }

    private String getPostParameters() {

        JSONObject jsonObject = new JSONObject();
        try {
            String comment = etComment.getText().toString();
            jsonObject.put("Comment", comment);
            jsonObject.put("CommentTypeID", "1");
            jsonObject.put("ItemID", ItemID);
            etComment.setText("");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void onResult(int type, HttpResponse o) {
        mKProgressHUD.dismiss();
       // Log.e("responseData",o.getResponseData());
        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {
                if (type == 0) {
//                    mArtwork.setIsLiked(1);
                    //mArtwork.setIsLiked(1);
                    // mArtwork.setLikes(mArtwork.getLikes() + 1);
                    //cbFav.setButtonDrawable(R.drawable.heart2_fill);
                    seedData();
                    requestHttpCall(4,null);
                }
                if (type == 1) {
//                    mArtwork.setIsLiked(1);
                    //mArtwork.setIsLiked(1);
                   // mArtwork.setLikes(mArtwork.getLikes() + 1);
                    //cbFav.setButtonDrawable(R.drawable.heart2_fill);
                    seedData();
                    requestHttpCall(4,null);
                }

                else if (type == 3) {
                 //   mArtwork.setIsPurchased(1);
                  //  seedData();
                    showToast("Artwork Purchased Successfully!!!");
                    requestHttpCall(4,null);
                    //((HomeActivity)getActivity()).replaceFragment(new PurchaseFragment(),getActivity());
                }
                else if (type == 4) {
                    Log.e("responseData",o.getResponseData());
                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    try {
                        JSONObject jsonObject = new JSONObject(modelParsedResponse.getResponse().getData().toString());
                        String ArtworkID = jsonObject.getString("ArtworkID");
                        String ArtworkName = jsonObject.getString("ArtworkName");
                        String ArtworkPicture = jsonObject.getString("ArtworkPicture");
                        String UserID = jsonObject.getString("UserID");
                        String UserName = jsonObject.getString("UserName");
                        String FullName = jsonObject.getString("FullName");
                        String HasAdded = jsonObject.getString("HasAdded");
                        String ProfilePicture = jsonObject.getString("ProfilePicture");
                        String ArtworkViews = jsonObject.getString("ArtworkViews");
                        String ArtworkLikes = jsonObject.getString("ArtworkLikes");
                        String ArtworkAmount = jsonObject.getString("ArtworkAmount");
                        String IsPurchased = jsonObject.getString("IsPurchased");
                        String IsLiked=jsonObject.getString("IsLiked");
                        String CommentCount=jsonObject.getString("ArtworkComments");
                        gadgetCommand=jsonObject.getString("GadgetCommand");
                        if(!jsonObject.isNull("UserDefaultGadget")) {
                            isGadgetDefault = jsonObject.getJSONObject("UserDefaultGadget");
                        }
                        if(isGadgetDefault!=null)
                        {
                            macAddress=isGadgetDefault.getString("macAddress");
                        }
                        isBuy = Boolean.parseBoolean(IsPurchased);
                        isLiked = Boolean.parseBoolean(IsLiked);
                        isHasAdded=Boolean.parseBoolean(HasAdded);
                        if(isBuy)
                        {
                            btBuy.setText("Preview");
                        }
                        else
                            {
                            btBuy.setText("Buy Now");
                        }
                        if(isLiked)
                        {
                            cbFav.setChecked(true);
                            //cbFav.setEnabled(false);
                        }
                        else
                        {
                            cbFav.setChecked(false);
                        }
                        tvName.setText(UserName);
                       // tvArtWork.setText(ArtworkName);
                       // tvMoney.setText(ArtworkAmount);
                        tvViews.setText(ArtworkViews+" Views");
                        tvTitle.setText(ArtworkName);
                        cbChate.setText(CommentCount);
                        cbFav.setText(ArtworkLikes);
                        cbShare.setText(" 0");
                        //ivHeart.setImageResource(R.drawable.heart2_fill);
                      //  cbFav.setButtonDrawable(R.drawable.heart_fill);

                        addFollower = UserID;
                        if(!ArtworkPicture.equals(""))
                            Glide.with(this).load(ArtworkPicture)

                                   .override(500,500)
                                   .centerCrop()
                                   .into(ivPic);
                        if(!ProfilePicture.equals(""))
                            Glide.with(this).load(ProfilePicture).into(profile_image2);

                        if(isHasAdded)
                        {
                            tvRemoveFollver.setVisibility(View.VISIBLE);
                            tvAddFollower.setVisibility(View.GONE);
                        }
                        else
                            //if(HasAdded.equals("true"))
                        {
                            tvRemoveFollver.setVisibility(View.GONE);
                            tvAddFollower.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(type == 5)
                {
                    Toast.makeText(getActivity(), modelParsedResponse.getMessages(), Toast.LENGTH_SHORT).show();
                    requestHttpCall(7,"");
                }

                else if(type == 6)
                {
                   // showToast("user followed successfully");
                    requestHttpCall(4,"");
                }
                else if(type == 7)
                {
                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                    Log.e("comentsresponse",modelParsedResponse.getResponse().getData().toString());
                    try {
                        comments.clear();
                       // modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);
                        JSONArray jsonArray = new JSONArray(modelParsedResponse.getResponse().getData().toString());
                        for(int i = 0; i<jsonArray.length(); i++)
                        {
                         JSONObject object = jsonArray.getJSONObject(i);
                         Comment comment = new Gson().fromJson(object.toString(),Comment.class);
                         comments.add(comment);
                        }
                        commentAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(type == 8)
                {
                    //showToast("remove follower add success");
                    requestHttpCall(4,"");
                }
            } else {
                if (modelParsedResponse.getMessages() != null) {
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(),
                            getString(R.string.alert), modelParsedResponse.getMessages());
                } else {
                    customDialogSomethingWent();
                }
            }

        } else {
            customDialogSomethingWent();
        }

    }

    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            try {

                modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

            } catch (JsonParseException exp) {
                modelParsedResponse = null;
            } catch (Exception exp) {
                modelParsedResponse = null;
            }


        } else modelParsedResponse = null;

    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {

        try {
            mKProgressHUD.dismiss();
            if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

                try {

                    modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParsedResponse.class);

                    if (modelParsedResponse != null) {

                        if (modelParsedResponse.getMessages() != null) {
                            ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(getActivity(), getString(R.string.alert),
                                    modelParsedResponse.getMessages());
                        } else {
                            customDialogSomethingWent();
                        }

                    } else {
                        customDialogSomethingWent();
                    }


                } catch (JsonParseException exp) {
                    customDialogSomethingWent();
                }


            } else {
                modelParsedResponse = null;
                customDialogSomethingWent();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void customDialogSomethingWent() {
        Toast.makeText(getActivity(), getString(R.string.something_wentwrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        final HashMap map;
        map = FrontEngine.getInstance().getMap(new String[]{getString(R.string.authorization),
                ((HomeActivity) getActivity()).prefs.getStringValue(getResources().getString(R.string.sp_accesstoken))});

        CheckInternet.getInstance().internetCheckTask(getActivity(), new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {
                    if (type == 0) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.artworkUnlike)+ItemID,
                                jsonObject,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));
                    }
                    else if (type == 1) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.artworkActionUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));
                    } else if (type == 2) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.artworkActionUrl),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));
                    } else if (type == 3) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.purchaseArtworkUrl)
                                        + "?artworkId=" + ItemID
                                        + "&isFeatured=" + 0,
                                null,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));
                    }
                    else if (type == 4) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                mArtWorkEndPoint,
                                null,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));

                    }
                    else if (type == 5) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.addComment),
                                jsonObject,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));

                    }
                    else if (type == 6) {
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.AddFollowerUrl)+addFollower,
                                jsonObject,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));

                    }
                    else if (type == 7) {
                        Integer lastId = null;
                        JsonObject jsonObject = new Gson().fromJson(params[0], JsonObject.class);
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                "artwork/ArtworkComments?artwork="+ItemID+"&lastId="+lastId,
                                jsonObject,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));

                    }
                    if (type == 8) {
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.POST,
                                map,
                                getString(R.string.UnfollowUserUrl)+""+addFollower,
                                null,
                                new CallBackRetrofit(type,
                                        StoreArtworkViewFragment.this
                                ));
                    }
                }  else {
                    mKProgressHUD.dismiss();
                    ((HomeActivity) getActivity()).objGlobalHelperNormal.callDialog(
                            getActivity(), getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });


    }

    //get parameters start
    private String getPostParametersLikeView(int actionType) {

        JSONObject jsonObject = new JSONObject();
        try {

           // jsonObject.put("artworkId", mArtwork.getArtworkId());
            jsonObject.put("artworkId", ItemID);
            jsonObject.put("actionBy", mUserData.getUserId());
            jsonObject.put("actionType", actionType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
