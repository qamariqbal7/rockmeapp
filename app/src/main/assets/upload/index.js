(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"index_atlas_", frames: [[533,1498,360,75],[579,1362,201,70],[336,1278,241,218],[616,1575,61,73],[533,1575,81,75],[336,1498,195,190],[0,1278,334,222],[782,1362,96,96],[641,1434,50,52],[579,1434,60,62],[0,0,803,1276],[579,1278,242,82]]}
];


// symbols:



(lib.Bitmap11 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap14 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap18 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap22 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap24 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap25 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap26 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.download0 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.element_radio_off2x = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.element_small_weapon2x = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.KakaoTalk_20180517_170256231 = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.status_bar_money2x = function() {
	this.spriteSheet = ss["index_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.transparent = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,0,0,0.008)").s().p("AjbDJIAAmRIG3AAIAAGRg");
	this.shape.setTransform(22,20.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.transparent, new cjs.Rectangle(0,0,44,40.2), null);


(lib.an_TextInput = function(options) {
	this._element = new $.an.TextInput(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,22);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.pop0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.download0();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.654,0.665);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.pop0, new cjs.Rectangle(0,0,62.8,63.8), null);


(lib.Shape_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A5A5A5").s().p("ABsE6QhCgngshFQguBFhCAnQhFAohNAAQh0AAhbhXQhYhWgWiCQBEAnBNAAQBNAABEgoQBCgmAuhEQAtBEBCAmQBEAoBNAAQCGAABfhsQBehtAAibQABglgHgiQBSAvAwBYQAxBbAABsQAACaheBtQhfBuiGAAQhNAAhFgog");
	this.shape.setTransform(57.8,35.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shape_8, new cjs.Rectangle(0,0,115.5,70.9), null);


(lib.Shape_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3B2906").s().p("AgrB2IgEAAQgiAAgXgXQgXgXAAgiQAAgRAHgPQgOgUAAgXQAAgiAYgXQAXgXAhAAQAJAAAHACQAUgOAYAAQAWAAATAMIABAAQAhAAAWAWQAYAWABAfIAAAFQAAARgHAPQAOATAAAYIAAAFQgCAfgXAWQgYAWggAAIgLgBQgUANgXAAQgXAAgUgMg");
	this.shape.setTransform(13.5,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shape_10, new cjs.Rectangle(0,0,26.9,26), null);


(lib.an_CSS = function(options) {
	this._element = new $.an.CSS(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,22);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.an_Button = function(options) {
	this._element = new $.an.Button(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,22);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.AnimateCC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#280600").s().p("AhBB9IAAgEQAjh4Adh+IAAgDQAfCAAjB8IABADQgbACgdAAQgkAAgngEg");
	this.shape.setTransform(-20.1,-14.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FD3E1D").s().p("ACKE1IgDAAQgahVgYhWIgBgDIitAAIgDAAQgUBBgTBDIgKAnIgCAAQguAHgtgMQgBAAAAgGIA6jCIA6jCIAMgqQAVhPAQhWIABgIIB6AAIADAAIAMAoIBoFaQAiBxAeBzQghAEgkAAIgdgBgAgFjVQgdB/gjB4IAAAEQBFAGA+gFIgBgCQgjh8gfiBIAAADg");
	this.shape_1.setTransform(-19.7,-5.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.514)").s().p("AqfgIIH4AAIH2gBIFRABIgEADQg3AOhGgBQj8gDj7AAIn3AAQg/AAg/AEIgRABQgsAAgVgSg");
	this.shape_2.setTransform(0,65.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FE3B1A").s().p("AFPKSIn2AAIn4AAQgFiXAAieQACj8AAj7IAAn3IH3AAIH3AAIFXAAIAAH3QAAD7ACD8QAACegFCXIlRAAgAJrJaIgBjFIABn3IAAn3IjnAAIn2AAIn3AAIgBDFIABH2IAAH4IDmAAIH2AAIH4AAIAAAAg");
	this.shape_3.setTransform(0,-0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FE3615").s().p("ABIDlIAAgCIAAgeQgEiSgGiSQAAgIgFgIQgfgthOAUQgHACgLAAQAAC0gCC1IgBAAQgsAFg0gDIAAgCIgFmiIAAgCQBZguCAANIAJABQA1AFAcAkQAyBAgJB+QgDA4ABA4QABA6gFAzQgbADgcAAIgpgBg");
	this.shape_4.setTransform(25.1,2.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#260600").s().p("ABzJaIn2AAIjmAAIAAn4IgBn2IABjFIH3AAIH2AAIDnAAIAAH3IgBH3IABDFIn4AAgAkeBaICuAAIABADQAYBWAaBVIADAAQA0ACAugFQgfhzgghxIhplaIgLgoIgDAAIh8AAIgBAIQgPBWgWBPIgMAqIg6DDIg6DBQAAAGACAAQAsAMAvgHIABAAIAKgmQAThEAUhBIADAAgAE0hNQAFAIAAAIQAHCSADCTIAAAdIAAACQAzADAtgFQAFgzgBg6QgBg4AEg4QAIh+gyhAQgcgkg1gFIgIgBQiCgNhZAuIAAACIAGGiIAAACQAzADAsgFIABAAQACi1AAi0QALAAAHgCQAYgGAUAAQAsAAAWAfg");
	this.shape_5.setTransform(0,-0.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(0,0,0,0.514)").s().p("ArKgJIIYAAIIWAAIFnAAQg+AShLgBQkNgDkLAAIoWAAQhEAAhDAFIgTAAQguAAgWgTg");
	this.shape_6.setTransform(0,70);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#280600").s().p("AhFCEIAAgEQAmiAAeiGQAiCFAlCEQgjAEglAAQggAAgjgDg");
	this.shape_7.setTransform(-21.4,-15);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FD3E1D").s().p("ACTFJQgfhbgahcQhcgDhdAAQgYBGgUBHIgLApQgzAIgwgNQgBgBAAgFIA+jOIA+jQIAMgrQAXhVAQhcQBDgJBBAAQAJAWAHAVIBuFxQAkB3AgB7QgkAFgnAAIgdgBgAhKAkIAAAEQBKAHBBgIQgliEgiiGQgeCHgmCAg");
	this.shape_8.setTransform(-20.9,-5.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FE3615").s().p("ABNDzIgBghQgDicgHibQgBgIgFgJQghgwhTAVQgHACgMAAQAADAgCDBQgwAFg3gEIgGm+IAAgDQBfgwCJANQBCAHAdAmQA2BEgKCGQgEA8ABA7QABA/gFA2QgbADgeAAQgVAAgXgCg");
	this.shape_9.setTransform(26.8,2.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#260600").s().p("AB7KBIoXAAIj1AAIAAoZIgBoWIABjRIIXAAIIXAAQB7gBB7ABIAAIXIgBIXIABDSIoYAAgAh2BjQAZBcAfBbQA4ABAwgFQghh7gih4IhvlxQgGgUgKgWQhCAAhCAIQgRBcgWBVIgNAsIg+DPIg+DOQAAAGACABQAwAMAygHIALgpQAUhIAYhFQBeAABdADgAFIhSQAFAJAAAIQAHCcAECcIAAAgQA2AFAwgGQAFg2gBg/QgBg7AEg8QAJiGg1hEQgegmhBgGQiKgOhfAwIAAADIAGG+QA2AFAxgGQABjAAAjBQAMAAAIgCQAZgGAVAAQAwAAAXAhg");
	this.shape_10.setTransform(0,-1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FE3B1A").s().p("AFkK8IoWAAIoYAAQgGigABipQACkMAAkLIAAoXIIXAAIIYAAIFsAAIAAIXQAAELACEMQABCpgGCgIlnAAgAqSmuIABIWIAAIZID1AAIIXAAIIYAAIgBjSIABoXIAAoXQh7gBh7ABIoXAAIoXAAIgBDRg");
	this.shape_11.setTransform(0,-1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(0,0,0,0.514)").s().p("Ar1gKII4AAII2AAIF9AAQhDAUhOgBQkdgEkcAAIo3AAQhHAAhHAFIgTABQgxAAgYgVg");
	this.shape_12.setTransform(0,74.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#280600").s().p("AhJCMIAAgFQAoiHAgiPQAkCOAnCMQgmAFgoAAQgiAAgjgEg");
	this.shape_13.setTransform(-22.7,-15.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FD3E1D").s().p("ACbFcQgghggbhhQhigEhiAAQgaBKgWBNIgMArQg1AIgzgOQgCAAAAgGIBDjbIBBjcIANgvQAYhZAShhQBHgKBEABQAKAWAHAXIB1GHQAnB/AhCCQgkAEgoAAIgjgBgAhOAlIAAAFQBOAHBFgIQgniMgkiOQggCQgoCGg");
	this.shape_14.setTransform(-22.2,-6.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FE3615").s().p("ABRECIAAgjQgEimgHikQAAgKgGgJQgjgzhYAXQgIACgNAAQAADMgBDMQg0AGg6gEIgGnaIAAgDQBlgzCRAOQBGAHAfApQA5BIgLCOQgEA/ACA/QABBCgGA6QgeAEghAAQgWAAgXgCg");
	this.shape_15.setTransform(28.4,2.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#260600").s().p("ACDKnIo3AAIkFAAIAAo4IAAo3IAAjeII4AAII3AAIEEAAIAAI4IAAI3IAADeIo3AAgAh9BpQAbBhAgBgQA8ADAygGQghiCgmh/Ih1mHQgHgXgKgWQhFgBhHAKQgSBhgYBZIgNAvIhBDcIhDDbQAAAGACAAQAzAOA1gIIAMgrQAWhNAahKQBiAABjAEgAFbhXQAHAJAAAKQAHCkAECmIAAAjQA6AEAygGQAGg6gBhCQgCg/AEg/QAKiOg4hIQgfgphGgHQiSgOhlAzIAAADIAGHaQA6AEA0gGQABjMAAjMQAMAAAIgCQAcgHAWAAQAyAAAYAjg");
	this.shape_16.setTransform(0,-1.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FE3B1A").s().p("AF5LmIo2AAIo4AAQgGiqABiyQABkdAAkbIAAo4II4AAII3AAIGDAAIAAI4QAAEbACEdQABCygGCqIl9AAgAq6nJIABI4IAAI4IEEAAII3AAII4AAIAAjeIAAo4IAAo3IkEAAIo3AAIo4AAIgBDdg");
	this.shape_17.setTransform(0,-1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(0,0,0,0.514)").s().p("AshgKIJZAAIJXgBIGTABQhHAUhUgBQksgEksAAIpXAAQhMAAhLAGIgTABQg1AAgagWg");
	this.shape_18.setTransform(0,78.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#280600").s().p("AhOCUIAAgFQAriPAiiXQAmCWAqCUQgoAFgoAAQglAAgogEg");
	this.shape_19.setTransform(-24,-16.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FD3E1D").s().p("ACkFxQgihmgdhnQhngEhpAAQgbBPgXBQIgMAuQg4AIg2gOQgBgBAAgGIBFjnIBFjpIAPgxQAZheAShnQBLgLBIABQALAYAHAYIB8GdQApCGAjCKQgoAEguAAIgfAAgAhTAoIAAAFQBTAIBJgJQgqiUgmiWQghCXgrCPg");
	this.shape_20.setTransform(-23.4,-6.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FE3615").s().p("ABWERIAAglQgEivgIiuQAAgKgGgJQglg3hdAYQgJADgNAAQAADXgCDYQg2AGg9gEIgHn1IAAgDQBrg2CZAPQBJAIAiAqQA7BNgKCWQgFBCACBDQABBGgGA9QggADgjAAQgXAAgYgBg");
	this.shape_21.setTransform(30,2.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#260600").s().p("ACJLNIpXAAIkTAAIAApXIAApYIAAjqIJYAAIJXAAQCKgBCKABIAAJYIgBJXIABDqIpZAAgAiFBvQAdBnAjBmQA+ACA2gHQgkiJgoiHIh7mdQgIgYgKgXQhKgBhLALQgSBngZBeIgOAxIhGDoIhFDoQAAAGABABQA2AOA5gJIAMgtQAXhQAbhPQBoAABoAEgAFvhcQAGAKABAKQAHCuAECuIABAmQA8AEA1gHQAGg8gBhGQgBhDAEhDQALiVg8hNQghgqhKgJQiagOhrA2IAAADIAHH1QA9AEA3gHQABjYAAjXQAOAAAIgCQAdgHAXAAQA2AAAZAlg");
	this.shape_22.setTransform(0,-1.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FE3B1A").s().p("AGPMQIpXABIpZAAQgFi1ABi8QABktAAkrIAApYIJZAAIJXAAIGZAAIAAJYQAAErABEtQABC8gFC1ImTgBgArhniIABJXIAAJYIESAAIJYAAIJZAAIgCjqIACpYIAApXQiLgBiJABIpYAAIpXAAIgBDqg");
	this.shape_23.setTransform(0,-1.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(0,0,0,0.514)").s().p("AsSgKIJOAAIJMAAIGLAAQhFAUhSgBQkngDknAAIpNAAQhKAAhJAFIgVAAQgyAAgZgVg");
	this.shape_24.setTransform(0,77);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#280600").s().p("AhMCSIAAgFQAqiNAhiUQAlCTApCRQgoAFgpAAQgjAAglgDg");
	this.shape_25.setTransform(-23.6,-16.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FD3E1D").s().p("AChFqQghhkgdhlQhlgDhmAAQgbBMgXBQIgLAsQg4AJg0gOQgCgBAAgGIBEjjIBEjkIAOgxQAYhdAThlQBJgKBHABQALAXAHAYIB6GVQAnCEAjCHQgoAFgsAAIgfgBgAhRAnIAAAFQBRAIBHgJQgoiRgliTQghCUgqCMg");
	this.shape_26.setTransform(-23,-6.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FE3615").s().p("ABVEMIgBglQgEisgHiqQgBgKgGgJQgkg1hbAXQgJACgNAAQAADUgBDUQg2AGg8gEIgGnsIAAgDQBpg1CWAPQBIAHAhAqQA7BLgLCUQgEBBABBCQABBEgGA8QgfADgiAAQgXAAgXgBg");
	this.shape_27.setTransform(29.4,2.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#260600").s().p("ACHLBIpNAAIkNAAIAApOIgBpNIABjlIJNAAIJNAAQCHgBCHABIAAJNIgBJMIABDnIpOAAgAiCBtQAcBlAiBkQA+ACA0gGQgkiHgmiEIh5mWQgIgXgKgYQhJAAhIAKQgTBlgYBdIgPAwIhDDkIhFDjQAAAGACABQA1AOA3gIIAMgtQAXhPAahNQBmAABnAEgAFphaQAGAJAAAKQAHCqAFCsIAAAlQA8AEAzgGQAGg8AAhEQgChCAEhBQALiUg7hLQgggqhIgHQiYgPhpA1IAAADIAGHsQA9AEA1gGQABjUABjUQAMAAAJgCQAcgHAXAAQA1AAAZAlg");
	this.shape_28.setTransform(0,-1.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FE3B1A").s().p("AGIMCIpMABIpOAAQgGiyABi5QACknAAklIAApOIJNAAIJNAAIGRAAIAAJOQAAElACEnQABC5gGCyImLgBgArUnaIABJNIAAJOIENAAIJNAAIJOAAIgBjnIABpMIAApNQiHgBiHABIpNAAIpNAAIgBDlg");
	this.shape_29.setTransform(0,-1.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("rgba(0,0,0,0.514)").s().p("ArZgJIIjAAIIhgBIFvABQhAAShMgBQkSgDkRAAIohAAQhGAAhEAFIgRABQgwAAgYgUg");
	this.shape_30.setTransform(0,71.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#280600").s().p("AhGCHIAAgFQAmiCAfiJQAjCIAmCHQglAEglAAQghAAgjgDg");
	this.shape_31.setTransform(-21.8,-15.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FD3E1D").s().p("ACWFPQgghcgaheQhegDhfAAQgZBHgUBJIgMAqQgzAIgxgNQgCgBABgGIA/jSIA/jUIANgtQAXhVARheQBEgKBCABQAJAWAHAVIBxF5QAkB6AiB9QgjAEgmAAIgigBgAhLAkIAAAFQBLAHBDgIQgmiHgjiIQgfCKgmCBg");
	this.shape_32.setTransform(-21.3,-5.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FE3615").s().p("ABOD4IAAghQgDiggHieQgBgJgFgIQgigyhVAWQgHACgMAAQAADEgCDFQgxAFg4gEIgGnHIAAgDQBhgxCLANQBDAHAfAnQA2BFgKCJQgEA9ABA9QABA/gFA4QgcADgfAAQgWAAgXgCg");
	this.shape_33.setTransform(27.3,2.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#260600").s().p("AB9KNIohAAIj6AAIAAoiIgBoiIABjVIIiAAIIhAAID7AAIAAIiIgBIhIABDWIojAAgAh5BlQAbBeAfBcQA5ACAxgFQghh+gjh6Ihyl4QgGgWgKgVQhDgBhEAJQgRBegWBWIgOAtIg/DUIg/DSQAAAGABAAQAxANA0gHIAMgqQAUhJAZhIQBfAABeAEgAFPhTQAFAIAAAJQAICeACCgIABAhQA3AEAxgFQAFg4gBg/QgBg9AEg9QAKiJg2hFQgfgnhDgHQiNgNhgAxIAAADIAFHHQA4AEAygFQACjFgBjEQAMAAAIgCQAagHAVAAQAxAAAYAjg");
	this.shape_34.setTransform(0,-1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FE3B1A").s().p("AFrLKIohABIojAAQgFilABisQABkRAAkQIAAojIIjAAIIiAAIF0AAIAAIjQAAEQABERQABCsgFClIlvgBgAqfm3IABIhIAAIjID6AAIIhAAIIjAAIgBjWIABoiIAAohIj7AAIohAAIoiAAIgBDVg");
	this.shape_35.setTransform(0,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5,p:{scaleX:1,scaleY:1,y:-0.9}},{t:this.shape_4,p:{scaleX:1,scaleY:1,x:25.1,y:2.5}},{t:this.shape_3,p:{scaleX:1,scaleY:1,y:-0.9}},{t:this.shape_2,p:{scaleX:1,scaleY:1,y:65.8}},{t:this.shape_1,p:{scaleX:1,scaleY:1,x:-19.7,y:-5.4}},{t:this.shape,p:{scaleX:1,scaleY:1,x:-20.1,y:-14.2}}]}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},1).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]},1).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18}]},1).to({state:[{t:this.shape_5,p:{scaleX:1.256,scaleY:1.256,y:-1.2}},{t:this.shape_4,p:{scaleX:1.256,scaleY:1.256,x:31.6,y:3.1}},{t:this.shape_3,p:{scaleX:1.256,scaleY:1.256,y:-1.2}},{t:this.shape_2,p:{scaleX:1.256,scaleY:1.256,y:82.7}},{t:this.shape_1,p:{scaleX:1.256,scaleY:1.256,x:-24.7,y:-6.8}},{t:this.shape,p:{scaleX:1.256,scaleY:1.256,x:-25.3,y:-17.9}}]},1).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24}]},1).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30}]},1).to({state:[{t:this.shape_5,p:{scaleX:1,scaleY:1,y:-0.9}},{t:this.shape_4,p:{scaleX:1,scaleY:1,x:25.1,y:2.5}},{t:this.shape_3,p:{scaleX:1,scaleY:1,y:-0.9}},{t:this.shape_2,p:{scaleX:1,scaleY:1,y:65.8}},{t:this.shape_1,p:{scaleX:1,scaleY:1,x:-19.7,y:-5.4}},{t:this.shape,p:{scaleX:1,scaleY:1,x:-20.1,y:-14.2}}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.6,-66.7,135.3,133.6);


(lib.miniGameover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag0AFQgCg2gHgsIAxgKQAEAfACAdQAkg6AhAAIgFBAQgYAAgNAEQgOAEgNANIAAAPIAEBfIg3AKg");
	this.shape.setTransform(196.3,30.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag6BTQgVgaAAgnQAAgxAdgmQAdgmAmAAQAeAAARARQAQARAAAeQAAASgGAXIgIAIIhmAJQAHAuAkAAQAMAAANgGQANgGAHgGIAIgGIATAXQgFAHgIAJQgJAJgJAGQgIAGgPAGQgOAFgPAAQghAAgVgZgAgWgyQgNAPgCAWIBFgJIABgPQAAgcgaAAQgRAAgMAPg");
	this.shape_1.setTransform(178.9,31);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag8AFIgdhiIA3gKIASBaIAPBCIAFAAIANg4IAWhlIAzAGIgfBlIgaBgIhFAGg");
	this.shape_2.setTransform(160.5,30.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhaBsQgggmAAg4QAAhFAmgtQAmgtA6AAQAxAAAfAlQAfAlAAA5QAABHglAsQglAtg8AAQgvAAgggmgAgwhIQgSAYAAAuQAAAtATAeQATAdAeAAQAgAAARgZQARgZAAgsQAAhnhEAAQgfAAgRAXg");
	this.shape_3.setTransform(137.4,27.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag6BTQgVgaAAgnQAAgxAdgmQAdgmAmAAQAeAAARARQAQARAAAeQAAASgGAXIgIAIIhmAJQAHAuAkAAQAMAAANgGQANgGAHgGIAIgGIATAXQgFAHgIAJQgJAJgJAGQgIAGgPAGQgOAFgPAAQghAAgVgZgAgWgyQgNAPgCAWIBFgJIABgPQAAgcgaAAQgRAAgMAPg");
	this.shape_4.setTransform(108.4,31);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("ABeAAQAAgcgFgJQgFgKgSAAQgRAAgWAOIgCAfIAEBhIg2AKIAFhpQAAgcgFgJQgFgKgSAAQgSAAgXAPIAAAeIAFBfIg3AKIAFhjQgCg2gHgsIAxgKIAFAuQAFgIAIgIQAIgIARgKQARgKARAAQARAAALALQAMALAEATQAhgpAjAAQAWAAAMAOQANAPABAZIgDAuIAEBhIg3AKg");
	this.shape_5.setTransform(82.8,31);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAaBHQgWAighAAQgZAAgPgPQgPgQAAgaQAAgaAXgTQAYgSAiAAIAbAAIAAgHQAAgUgIgIQgIgHgWAAQgJAAgNAEQgNAEgQAIIgMggQASgMAZgLQAZgJAPAAQBBAAAAA/IAABPQAAAWAOAdIgrASQgLgTgFgQgAgkAlQAAAMAHAHQAGAHALAAQAPAAAVgQIAAgcQgWgFgMAAQgaAAAAAXg");
	this.shape_6.setTransform(56.7,30.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhOBuQghgkAAg7QAAhDAqgvQApguA6AAQAYAAAUAEQAVAFAIAEIAJAFIgYA4QgNgHgUgHQgVgIgQAAQggAAgVAXQgUAYAAAmQAAAzATAdQATAdAhAAQAXAAATgFIAAhAIg1AAIAFgpIBiAAIgEBDIADBGQg8ASgnAAQg1AAghgkg");
	this.shape_7.setTransform(33.4,27.2);

	this.b12 = new lib.an_Button({'id': 'b12', 'label':'->', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b12.setTransform(129.7,197.7,0.597,2.761);

	this.b11 = new lib.an_Button({'id': 'b11', 'label':'C', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b11.setTransform(35.2,197.7,0.597,2.761);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.b11},{t:this.b12},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.miniGameover, new cjs.Rectangle(0,0,223.8,259.8), null);


(lib.mcmole = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(5.4,0,0,4).p("AlughQF1DQFmjy");
	this.shape.setTransform(46.6,61.3,0.486,0.486);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(3,0,0,4).p("AhYiHICxAAIAAEPIixAAg");
	this.shape_1.setTransform(50.2,70.5,0.486,0.486);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#8A8A8A","#F1F1F1","#FFFFFF"],[0,0.286,1],-10.2,0,10.3,0).s().p("AhYCIIAAkPICxAAIAAEPg");
	this.shape_2.setTransform(50.2,70.5,0.486,0.486);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(3,0,0,4).p("AhYiHICxAAIAAEPIixAAg");
	this.shape_3.setTransform(41.4,70.8,0.486,0.486);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#8A8A8A","#F1F1F1","#FFFFFF"],[0,0.286,1],-10.2,0,10.3,0).s().p("AhYCIIAAkPICxAAIAAEPg");
	this.shape_4.setTransform(41.4,70.8,0.486,0.486);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AhvBwQguguAAhCQAAhAAuguQAvgvBAAAQBBAAAvAvQAuAuAABAQAABCguAuQguAuhCAAQhAAAgvgug");
	this.shape_5.setTransform(46.3,47.6,0.486,0.486);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(2.7,0,0,4).p("AgOkyQhWAAhHAnQhFAmgjA+QhQAZguAkQgvAmAAAqQAAAXAPAWQgLAZAAAbQAABiCDBFQCDBFC4AAQC5AACDhFQCDhFAAhiQAAgegPgeQAKgRAAgUQAAgug4goQg2gnhegYQgkg7hDgkQhGgkhRAAg");
	this.shape_6.setTransform(46.8,58.7,0.486,0.486);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FFCEAE","#FFB280"],[0,1],-25.3,37.9,30.9,-26.1).s().p("Ak5DuQiDhFAAhiQAAgcALgYQgPgWAAgXQAAgqAvgmQAugkBQgZQAjg+BFgmQBHgnBWAAQBRAABGAkQBDAkAkA7QBeAYA2AnQA4AoAAAuQAAATgKASQAPAeAAAeQAABiiDBFQiDBFi5AAQi4AAiDhFg");
	this.shape_7.setTransform(46.8,58.7,0.486,0.486);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7F7F8D").s().p("AAbH4QB5jXAAkZQAAkxiNjfQiLjejMgTIAAgFQEZAKDEDgQDEDfAAE4QAAEliwDbQivDZkDAjQC1gzB3jUg");
	this.shape_8.setTransform(58.3,95.3,0.486,0.486);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(1.3,0,0,4).p("ABLiZIARAbQATAiACAlQAJBziTBeQgigigHgGQgRgPgegYIAAgdQAbgPATgrQAchAArg3g");
	this.shape_9.setTransform(85.7,19.8,0.486,0.486);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#F1AAC2","#E5638F"],[0,1],12,0,-11.9,0).s().p("AhCByQgRgPgegYIAAgdQAbgPATgqQAchBArg3IBHgWIARAbQATAjACAkQAJB0iTBdIgpgog");
	this.shape_10.setTransform(85.7,19.8,0.486,0.486);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(1.3,0,0,4).p("AhKiZIgSAbQgSAigDAlQgIBzCTBeQAigiAHgGQARgPAegYIAAgdQgbgOgTgsQgbhAgsg3g");
	this.shape_11.setTransform(7,19.5,0.486,0.486);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#F1AAC2","#E5638F"],[0,1],-11.9,0,12,0).s().p("Ahxg3QADglASgiIASgbIBHAWQAsA3AbBBQATArAbAOIAAAdQgeAYgRAPIgpAoQiTheAIhzg");
	this.shape_12.setTransform(7,19.5,0.486,0.486);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgPARQgIgHABgKQgBgIAIgIQAGgHAJAAQAKAAAGAHQAIAIgBAIQABAKgIAHQgGAGgKAAQgJAAgGgGg");
	this.shape_13.setTransform(49.9,31.1,0.486,0.486);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(3.5,0,0,4).p("ABaAAQAAAmgaAaQgaAbgmAAQglAAgagbQgbgaAAgmQAAglAbgaQAagaAlAAQAmAAAaAaQAaAaAAAlg");
	this.shape_14.setTransform(51.5,32.7,0.486,0.486);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("Ag/BAQgbgaAAgmQAAgkAbgbQAagbAlAAQAmAAAaAbQAbAbAAAkQAAAmgbAaQgaAagmAAQglAAgagag");
	this.shape_15.setTransform(51.5,32.7,0.486,0.486);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgPARQgIgHABgKQgBgIAIgIQAGgHAJAAQAKAAAGAHQAIAIgBAIQABAKgIAHQgGAGgKAAQgJAAgGgGg");
	this.shape_16.setTransform(38.8,31.1,0.486,0.486);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(3.5,0,0,4).p("ABaAAQAAAmgaAaQgaAbgmAAQglAAgagbQgagaAAgmQAAglAagaQAagaAlAAQAmAAAaAaQAaAaAAAlg");
	this.shape_17.setTransform(40.4,32.7,0.486,0.486);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("Ag/BAQgbgaAAgmQAAgkAbgbQAagbAlAAQAmAAAaAbQAbAbAAAkQAAAmgbAaQgaAagmAAQglAAgagag");
	this.shape_18.setTransform(40.4,32.7,0.486,0.486);

	this.instance = new lib.Shape_8();
	this.instance.parent = this;
	this.instance.setTransform(46.1,32.8,0.486,0.486,0,0,0,57.8,35.4);
	this.instance.alpha = 0.34;

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(5.5,0,0,4).p("Aj4lpQiBAAhcBqQhcBqAACVQAACWBcBqQBcBqCBAAQBLAABCgnQBAglArhDQAsBDBAAlQBCAnBLAAQCCAABbhqQBchqAAiWQAAiVhchqQhbhqiCAAQhLAAhCAnQhAAmgsBCQgrhChAgmQhCgnhLAAg");
	this.shape_19.setTransform(45.7,31.5,0.486,0.486);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("ABsFDQhBglgrhDQgrBDhAAlQhCAnhLAAQiBAAhchqQhchqAAiWQAAiVBchqQBchqCBAAQBLAABCAnQBAAmArBCQArhCBBgmQBCgnBLAAQCBAABcBqQBcBqAACVQAACWhcBqQhcBqiBAAQhLAAhCgng");
	this.shape_20.setTransform(45.7,31.5,0.486,0.486);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#C1C1C8").s().p("AigIDQiwjbAAklQAAk3DFjgQDDjgEZgKIAAAFQjMATiLDeQiNDfAAExQAAEZB5DXQB3DUC2AzQkEgjivjZg");
	this.shape_21.setTransform(29.9,93.6,0.486,0.486);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#7F7F8D").s().p("AAbH4QB5jXAAkZQAAkxiNjfQiLjejMgTIAAgFQEZAKDDDgQDFDfAAE4QAAEliwDbQivDZkEAjQC2gzB3jUg");
	this.shape_22.setTransform(62.7,94.2,0.486,0.486);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(2.7,0,0,4).p("AK5AAQAAE/jMDhQhhBrh9A7QiBA8iOAAQiNAAiBg8Qh9g7hhhrQhghqg1iKQg3iPAAidQAAicA3iPQA1iKBghrQBhhqB9g7QCBg8CNAAQCOAACBA8QB9A7BhBqQDMDiAAE+g");
	this.shape_23.setTransform(46.9,93.9,0.486,0.486);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#93939F").s().p("AkOLGQh9g7hhhrQhghqg1iKQg3iQAAicQAAibA3iQQA1iKBghrQBhhqB9g7QCBg8CNAAQCOAACBA8QB9A7BhBqQDMDiAAE+QAAE/jMDhQhhBrh9A7QiBA8iOAAQiNAAiBg8g");
	this.shape_24.setTransform(46.9,93.9,0.486,0.486);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(5.8,0,0,4).p("AgCxEIgIAAIAAABQi1ACiOA2QjhBYhVDPQghgrgSgPQg2gqhphjQhCBAgVBYQgqCvDhBzQAhEwALGzQAKF4AUB4QAjDFBvBOQB4BUEOgGQAqgBBfACIAWABIAAgBQBfgCApABQEOAGB4hVQBwhOAijFQAVh3AJl5QALmyAhkxQDhhygpiwQgVhXhChBQhpBkg3AqQgSAOghAsQhXjVjshXQiPg0i0AAg");
	this.shape_25.setTransform(46.1,53.1,0.486,0.486);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#333333").s().p("AgKRFQhfgDgqABQkOAGh4hUQhvhOgjjFQgUh4gKl4QgLmzghkwQjhhyAqiwQAVhXBChBQBpBkA2ApQASAPAhArQBVjPDhhYQCOg2C1gCIAAAAIAIgBQC0AACPA1QDsBWBXDVQAhgsASgOQA3gqBphjQBCBAAVBYQApCvjhBzQghEwgLGyQgJF5gVB3QgiDGhwBOQh4BUkOgGQgpgBhfACIAAABg");
	this.shape_26.setTransform(46.1,53.1,0.486,0.486);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.instance},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mcmole, new cjs.Rectangle(-2.9,-4,97.9,136.7), null);


(lib.MC_WALLET = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.BTNclose = new lib.transparent();
	this.BTNclose.name = "BTNclose";
	this.BTNclose.parent = this;
	this.BTNclose.setTransform(318.7,95.3,0.861,0.954,0,0,0,0.4,0.1);

	this.instance = new lib.Bitmap24();
	this.instance.parent = this;
	this.instance.setTransform(100,165);

	this._CASH = new cjs.Text("300", "21px 'Acme'", "#26264B");
	this._CASH.name = "_CASH";
	this._CASH.lineHeight = 29;
	this._CASH.lineWidth = 137;
	this._CASH.parent = this;
	this._CASH.setTransform(182.6,190);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26264B").s().p("AAAAUIghA3IgXgMIApg/Igsg3IAagSIAhA1IAgg2IAWANIgmA8IAsA7IgaARg");
	this.shape.setTransform(339.8,112.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("AgMA5QgGgIAAgLIABgZIAAggIgPAAIACgQIANAAIAAgYIAbgFIgDAdIAaAAIgDAQIgYAAIgBArQAAALACADQACADAFAAQAFAAAJgCIABAPQgOALgKAAQgKAAgHgIg");
	this.shape_1.setTransform(68.2,113.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXASAAQAGAAAGgDQAHgDAEgDIAEgDIAJALQgCAEgEAFIgJAIQgFADgHACQgHADgIAAQgQAAgLgNgAgLgZQgGAHgCAMIAjgFIABgIQAAgOgNAAQgJAAgGAIg");
	this.shape_2.setTransform(59.7,114.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("AgLAeIgChpIAcgFIgEBrIADAxIgbAFg");
	this.shape_3.setTransform(52.4,111.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("AgLAeIgChpIAcgFIgDBrIACAxIgbAFg");
	this.shape_4.setTransform(47.1,111.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_5.setTransform(39.4,114.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("AAGARIgGgaIgGAXIgOA5IgmACIgMg4IgXhUIAcgGIARBQIAJAnIACAAIAMgnIAPg3IgEgTIAagGIARBQIAJAnIACAAIAKgkIAUhTIAYAEIgXBTIgOA5IgmACg");
	this.shape_6.setTransform(24.9,112.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AgSArQgIgDgDgEIgDgEIAOgQIAEADIAIAGQAFAEAFAAQAJAAAAgJQAAgDgEgCIgJgFIgMgGQgFgDgEgFQgEgFgBgHQAAgMALgJQAKgJANAAQAHAAAGABQAHACAFADIgMAWIgDgDIgHgEQgGgDgFAAQgDAAgCACQgCACAAADQAAAEAGADIANAGQAHADAGAFQAHAHAAAJQAAAMgLAKQgKAJgNAAQgJAAgHgEg");
	this.shape_7.setTransform(249.2,270.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AgSArQgIgDgDgEIgDgEIAOgQIAEADIAIAGQAGAEAEAAQAJAAAAgJQAAgDgEgCIgJgFIgMgGQgFgDgFgFQgDgFAAgHQgBgMALgJQAKgJANAAQAHAAAGABQAGACAGADIgMAWIgDgDIgHgEQgGgDgEAAQgDAAgDACQgCACAAADQAAAEAGADIANAGQAHADAGAFQAHAHgBAJQABAMgLAKQgKAJgNAAQgIAAgIgEg");
	this.shape_8.setTransform(242.1,270.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_9.setTransform(234.6,270.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#26264B").s().p("AgWACQgBgXgDgTIAVgFIADAaQAPgZAOAAIgCAcQgKAAgGACQgGACgFAFIAAAHIACApIgYAFg");
	this.shape_10.setTransform(227.8,270.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#26264B").s().p("AAOAzQgOASgJAAQgNAAgIgLQgIgKAAgRQAAgWAMgOQAMgQARAAIALABIAAgDIgDgqIAZgFIgCBcIAAAQQAAALAFAOIgTAIQgGgLAAgJgAgOAFQgEAGAAALQAAAMAEAGQAEAHAIAAQAHAAAJgHIAAgnIgPgBQgJAAgEAFg");
	this.shape_11.setTransform(219.7,268.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#26264B").s().p("AAOAzQgOASgJAAQgNAAgIgLQgIgKAAgRQAAgWAMgOQAMgQARAAIALABIAAgDIgDgqIAZgFIgCBcIAAAQQAAALAFAOIgTAIQgGgLAAgJgAgOAFQgEAGAAALQAAAMAEAGQAEAHAIAAQAHAAAJgHIAAgnIgPgBQgJAAgEAFg");
	this.shape_12.setTransform(210.7,268.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgHAAgLQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgDgJAAIgJABIgNAGIgFgPQAIgFALgFQALgDAGAAQAcAAAAAbIAAAiQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADAEAEAAQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_13.setTransform(202,270.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#26264B").s().p("AgKAxQgFgGgBgLIACgUIAAgcIgNAAIACgOIALAAIAAgUIAWgFIgCAZIAWAAIgCAOIgUAAIgBAlQAAAIABADQACADAFAAIALgCIABANQgMAKgJAAQgIAAgGgHg");
	this.shape_14.setTransform(192,269.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_15.setTransform(184.6,270.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#26264B").s().p("AgJAaIgChaIAXgFIgCBcIACAqIgXAFg");
	this.shape_16.setTransform(178.3,268.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#26264B").s().p("AgJAaIgChaIAXgFIgCBcIACAqIgXAFg");
	this.shape_17.setTransform(173.8,268.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgHAAgLQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgDgJAAIgJABIgNAGIgFgPQAIgFALgFQALgDAGAAQAcAAAAAbIAAAiQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADAEAEAAQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_18.setTransform(167.2,270.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#26264B").s().p("AAAADIAAgDIgMArIgcADIgKgrIgNgrIAYgFIAHAoIAGAcIADAAIAPgzIgEgMIAXgFIAHAoIAGAcIADAAIAGgYIAJgsIAWACIgNAsIgLAqIgdADg");
	this.shape_19.setTransform(156.5,270.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#26264B").s().p("AgWACQgBgXgDgTIAVgFIADAaQAPgZAOAAIgCAcQgKAAgGACQgGACgFAFIAAAHIACApIgYAFg");
	this.shape_20.setTransform(144.3,270.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#26264B").s().p("AAMAbQgPATgMgBQgJABgGgHQgGgGAAgKIABgWIgCgpIAYgFIgDA1QAAAJADADQADADAGABQAFAAALgHIAAgKIgCguIAYgFIgCAsIAAAPQAAALAGAMIgSAIQgGgLgCgIg");
	this.shape_21.setTransform(136.3,270.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#26264B").s().p("AgbAkQgKgLAAgRQAAgWAMgQQANgQAPAAQARAAAJAKQAJALAAASQAAAXgLAPQgNAQgRAAQgPAAgJgLgAgRAAQAAAMAFAIQAFAHAIAAQAIAAAFgHQAEgGAAgNQAAgZgRAAQgSAAAAAYg");
	this.shape_22.setTransform(127.2,270.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#26264B").s().p("AgMA+IACgqIgWguIgSgfIAagFIALAbIANAbIACAAIANgYIAOgeIAWAEIgRAdIgXAtIACAug");
	this.shape_23.setTransform(117.8,269);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_24.setTransform(216.3,369.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#26264B").s().p("AgKAXIgCgqIAXgFIgCAsIACAqIgXAFgAgKgoQgDgEAAgGQAAgGAFgFQAEgFAFAAQAHAAADADQADAEAAAHQAAAGgFAFQgEAFgFAAQgGAAgEgEg");
	this.shape_25.setTransform(209.5,367.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#26264B").s().p("AgbAkQgKgLAAgRQAAgWAMgQQANgQAPAAQARAAAJAKQAJALAAASQAAAXgLAPQgNAQgRAAQgPAAgJgLgAgRAAQAAAMAFAIQAFAHAIAAQAIAAAFgHQAEgGAAgNQAAgZgRAAQgSAAAAAYg");
	this.shape_26.setTransform(202.8,369.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#26264B").s().p("AggAvQgNgRAAgYQAAgdAQgUQAQgUAXAAQAKAAAIABIALAFIAEABIgNAaQgEgEgHgDQgIgEgHABQgLAAgHAKQgIAKAAATQAAASAIANQAIAMAMgBQAJAAAHgFQAHgGACgIIAPAJQgFAPgNAJQgMAJgPAAQgTAAgOgRg");
	this.shape_27.setTransform(193.8,367.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_28.setTransform(181.8,369.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#26264B").s().p("AgWACQgBgXgDgTIAVgFIADAaQAPgZAOAAIgCAcQgKAAgGACQgGACgFAFIAAAHIACApIgYAFg");
	this.shape_29.setTransform(175,369.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgLAKgJQAKgHAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJAAIgJACIgNAFIgFgNQAIgGALgFQALgEAGABQAcAAAAAaIAAAjQAAAKAGAMIgTAIQgEgIgCgHgAgPAQQAAAGADADQADACAEAAQAGAAAKgHIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_30.setTransform(167.1,369.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#26264B").s().p("AAPAYQAAgNgCgEQgCgEgIAAQgHAAgKAGIAAANIACArIgYAEIACgsIgChaIAYgEIgCAvIAAAVIAGgHIAKgIQAHgEAIAAQAIAAAGAGQAFAGAAAKIgBAUIACAsIgYAEg");
	this.shape_31.setTransform(158.3,366.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#26264B").s().p("AgaA6QgKgGgDgEIgCgDIAOgTQAPAPAPAAQAIAAAEgDQAFgEAAgFQAAgGgGgFQgFgEgJgDIgPgGQgIgEgGgGQgFgIAAgKQAAgQANgMQAOgMARAAQAJAAAIACQAHABAIAEIgMAbQgGgEgIgEQgJgEgHABQgFgBgEAEQgEADAAAFQAAAGAGADQAFAEAHAEIARAHQAIADAGAHQAGAGAAALQAAAQgOANQgNANgRAAQgOAAgKgGg");
	this.shape_32.setTransform(149,367.6);

	this.b14 = new lib.an_Button({'id': 'b14', 'label':'Recieve', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b14.setTransform(187.5,395.9,1.562,2.045);

	this.b13 = new lib.an_Button({'id': 'b13', 'label':'Send', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b13.setTransform(24.5,395.9,1.562,2.045);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#26264B").ss(1,1,1).p("AwTAAMAgnAAA");
	this.shape_33.setTransform(182.5,245.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.b13},{t:this.b14},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this._CASH},{t:this.instance},{t:this.BTNclose}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_WALLET, new cjs.Rectangle(13.2,95.2,343.1,346.8), null);


(lib.MC_USERNAME = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Bitmap25();
	this.instance.parent = this;
	this.instance.setTransform(84,87);

	this.txtpass2 = new lib.an_TextInput({'id': 'txtpass2', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtpass2.setTransform(25.5,376,3.2,2.044,0,0,0,0.3,0);

	this.txtuser2 = new lib.an_TextInput({'id': 'txtuser2', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtuser2.setTransform(25.5,321.2,3.2,2.045,0,0,0,0.3,0.1);

	this._full_name = new cjs.Text("Username", "18px 'Acme'", "#26264B");
	this._full_name.name = "_full_name";
	this._full_name.textAlign = "center";
	this._full_name.lineHeight = 25;
	this._full_name.lineWidth = 188;
	this._full_name.parent = this;
	this._full_name.setTransform(180.4,278.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26264B").s().p("AgNA8QgEgEAAgFQAAgHAFgDQAEgFAGAAQAEAAADADQADADAAAHQAAAGgEAEQgEAEgFAAQgFABgDgEgAgPAGQAXgOAAgOQAAgUgngBIAJgUQAaADAOAJQAOAJAAAPQAAAUghAZg");
	this.shape.setTransform(148.7,609.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("AADAVQACgVAAgUIAUgBQgEAXgIAUgAgYAVQACgVAAgUIAUgBQgEAXgIAUg");
	this.shape_1.setTransform(142.6,606.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_2.setTransform(135.2,611.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("AApAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIgBANIACArIgXAEIACguQAAgMgCgEQgCgEgIAAQgIAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAHgEQAIgEAHAAQAHAAAFAEQAFAFACAJQAPgSAPAAQAJAAAGAGQAGAGAAALIgCAUIACArIgYAEg");
	this.shape_3.setTransform(124,611.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJABIgJABIgNAFIgFgNQAIgGALgEQALgFAGAAQAcAAAAAbIAAAjQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADADAEABQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_4.setTransform(112.5,611.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_5.setTransform(103.6,611.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("AgWACQgBgXgDgTIAVgFIADAaQAPgZAOAAIgCAcQgKAAgGACQgGACgFAFIAAAHIACApIgYAFg");
	this.shape_6.setTransform(96.1,611.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_7.setTransform(88.5,611.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AgSArQgIgDgDgEIgDgEIAOgQIAEADIAHAGQAGAEAFAAQAJAAAAgJQAAgDgEgCIgJgFIgLgGQgHgDgDgFQgFgFAAgHQAAgMALgJQALgJAMAAQAHAAAGABQAHACAFADIgMAWIgDgDIgIgEQgFgDgFAAQgDAAgCACQgCACAAADQAAAEAGADIANAGQAHADAGAFQAHAHAAAJQAAAMgLAKQgKAJgNAAQgJAAgHgEg");
	this.shape_8.setTransform(80.8,611.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("AghA1QgLgKAAgOIgChXIAagFIgDBOQAAANAGAHQAGAHALAAQANAAAFgHQAGgHAAgQIgDhGIAagFIgCBOQAAAWgPANQgOANgVAAQgRAAgLgKg");
	this.shape_9.setTransform(71.6,609.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#26264B").s().p("AADAVQACgVAAgUIAUgBQgEAXgIAUgAgYAVQACgVAAgUIAUgBQgEAXgIAUg");
	this.shape_10.setTransform(62.9,606.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#26264B").s().p("AgKAxQgFgGAAgLIABgUIAAgcIgNAAIACgOIALAAIAAgUIAXgFIgDAZIAWAAIgCAOIgVAAIAAAlQAAAIABADQACADAFAAIALgCIABANQgMAKgJAAQgIAAgGgHg");
	this.shape_11.setTransform(51,610.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#26264B").s().p("AgbAkQgKgLAAgRQAAgWAMgQQANgQAPAAQARAAAJAKQAJALAAASQAAAXgLAPQgNAQgRAAQgPAAgJgLgAgRAAQAAAMAFAIQAFAHAIAAQAIAAAFgHQAEgGAAgNQAAgZgRAAQgSAAAAAYg");
	this.shape_12.setTransform(43.3,611.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#26264B").s().p("AgbgXIAAAmIADAvIgXAAIACgsIgChOIAWgBIA0BXIgChVIAXgCIgDBMIACArIgVAFg");
	this.shape_13.setTransform(32.8,609.5);

	this.b17 = new lib.an_Button({'id': 'b17', 'label':'Log out', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b17.setTransform(247,584.2,0.976,2.045);

	this.b16 = new lib.an_Button({'id': 'b16', 'label':'Done', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b16.setTransform(25.7,485.8,3.2,2.045);

	this.b15 = new lib.an_Button({'id': 'b15', 'label':'Change Password', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b15.setTransform(25.7,431.4,3.2,2.045);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#26264B").ss(1,1,1).p("A9UAAMA6pAAA");
	this.shape_14.setTransform(197.7,572.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.b15},{t:this.b16},{t:this.b17},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this._full_name},{t:this.txtuser2},{t:this.txtpass2},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_USERNAME, new cjs.Rectangle(9.1,87,377.3,543.2), null);


(lib.MC_SIGNUP = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.b6 = new lib.an_Button({'id': 'b6', 'label':'Sign in', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b6.setTransform(248.3,590.5,0.947,1.827,0,0,0,0,0.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26264B").s().p("AhOgmIABAAQgCg3gHgsIAxgKIAFArQAAgDAIgIQAIgKAOgKQAOgLAOAAQAaAAASAXQARAYAAAnQAAA2gcAhQgcAhgpAAIgTgBIACBOIg4ALgAgghQIAAAjIACA5QAWAEALABQAnAAAAg3QAAg2ggAAQgVAAgVAMg");
	this.shape.setTransform(232.2,135.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("AAcA+QgkAqgbAAQgWAAgOgPQgNgOAAgYIACgvIgEhiIA3gKIgGB8QAAARAHAIQAGAJAOAAQANAAAYgOIAAgZIgEhrIA3gKIgFBmIABAhQAAAbANAaIgqATQgOgbgDgQg");
	this.shape_1.setTransform(211.4,130.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AAkAAQAAgcgEgJQgFgKgTAAQgQAAgXAPIAAAeIAEBfIg3AKIAFhjQgBg2gIgsIAygKIAEAuQAGgIAIgIQAIgIAQgKQAQgKATAAQATAAAMAOQANAPAAAZIgDAuIAFBhIg3AKg");
	this.shape_2.setTransform(183.4,130.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("AhNCJQgWgOAAgWQgBgkBDgVQgPgIAAgNQgBgFAFgGQAEgFAHgGIAAgCQgdAAgSgQQgRgRAAgbQAAgkAdgbQAegaAmAAQAKAAAUAEQAUAEATAAIAfAAIgEAkIgfgEQAFAMAAAPQAAAQgIAPQgHAOgKAJQgKAKgKAGIgRAMQgHAEgBAEQAAAIARAGQAsARAPANQAPAMAAASQABAfgfATQgfAUgpAAQgqAAgYgOgAgmBKQgLAHAAAKQAAAKALAHQAMAGATAAQAUAAANgIQAMgHAAgMQAAgHgIgGQgIgHgUgIQgdAIgLAHgAgzhJQAAAlApAAQAhAAABgiQgBgUgIgKQgKgJgSAAQgmAAAAAkg");
	this.shape_3.setTransform(163,135.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("AgYA1IgEhiIA1gKIgEBlIAEBgIg2AKgAgXhdQgIgJAAgNQAAgOAKgLQALgLANAAQAOAAAHAIQAIAHAAAPQAAAQgLAKQgKALgNAAQgOAAgHgJg");
	this.shape_4.setTransform(147.3,126);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("Ag9CDQgVgNgJgKIgDgEIAfgsQAjAjAiAAQASAAALgJQALgIAAgOQAAgNgNgJQgNgJgTgHQgRgHgTgJQgTgJgNgPQgNgRAAgXQAAglAfgbQAfgbApAAQATAAASAEQASAEARAIIgcA8QgLgJgUgIQgUgHgPAAQgOAAgJAHQgJAHAAAMQAAAMANAJQANAJASAHIAlAQQATAIANAQQANAQAAAXQAAAngfAdQgfAdgoAAQgeAAgYgPg");
	this.shape_5.setTransform(131.4,127.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("AgNA8QgEgEAAgFQAAgHAFgDQAEgFAGAAQAEAAADADQADADAAAHQAAAGgEAEQgEAEgFAAQgFABgDgEgAgPAGQAXgOAAgOQAAgUgngBIAJgUQAaADAOAJQAOAJAAAPQAAAUghAZg");
	this.shape_6.setTransform(143.3,609.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AgKAxQgFgGgBgLIACgUIAAgcIgNAAIACgOIALAAIAAgUIAWgFIgCAZIAWAAIgCAOIgUAAIgBAlQAAAIABADQACADAFAAIALgCIABANQgMAKgJAAQgIAAgGgHg");
	this.shape_7.setTransform(137.1,610.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_8.setTransform(129,611.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("AAMAbQgPASgMAAQgJAAgGgGQgGgGAAgKIABgWIgCgqIAYgEIgDA2QAAAIADADQADAEAGAAQAFgBALgGIAAgKIgCguIAYgFIgCAsIAAAPQAAALAGAMIgSAIQgGgLgCgIg");
	this.shape_9.setTransform(119.9,611.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#26264B").s().p("AgbAkQgKgLAAgRQAAgWAMgQQANgQAPAAQARAAAJAKQAJALAAASQAAAXgLAPQgNAQgRAAQgPAAgJgLgAgRAAQAAAMAFAIQAFAHAIAAQAIAAAFgHQAEgGAAgNQAAgZgRAAQgSAAAAAYg");
	this.shape_10.setTransform(110.8,611.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#26264B").s().p("AgXAkQgIgLAAgRQAAgVANgRQANgQAQAAIAKABIALAFIgMAWQgHgFgIAAQgHAAgEAGQgFAFAAAMQAAALAFAHQAEAHAHAAQAFAAAEgDIAHgFIABgDIAJAJIgBADIgEAGIgGAHIgJAGQgHACgFAAQgNAAgJgLg");
	this.shape_11.setTransform(103.2,611.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#26264B").s().p("AgWAkQgJgLAAgRQAAgVANgRQANgQAQAAIAKABIALAFIgMAWQgHgFgIAAQgHAAgEAGQgFAFAAAMQABALAEAHQAEAHAHAAQAFAAAEgDIAHgFIACgDIAJAJIgCADIgEAGIgGAHIgJAGQgGACgGAAQgNAAgIgLg");
	this.shape_12.setTransform(96.2,611.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJABIgJABIgNAFIgFgNQAIgGALgEQALgFAGAAQAcAAAAAbIAAAjQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADADAEABQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_13.setTransform(88.3,611.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_14.setTransform(76.6,611.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJABIgJABIgNAFIgFgNQAIgGALgEQALgFAGAAQAcAAAAAbIAAAjQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADADAEABQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_15.setTransform(67.7,611.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_16.setTransform(56.7,611.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#26264B").s().p("AgaACIgNgrIAZgEIAIAnIAGAdIACAAIAGgXIAJgtIAXACIgOAsIgLArIgeACg");
	this.shape_17.setTransform(48.6,611.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJABIgJABIgNAFIgFgNQAIgGALgEQALgFAGAAQAcAAAAAbIAAAjQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADADAEABQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_18.setTransform(40.1,611.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#26264B").s().p("AAPAYQAAgNgCgEQgCgEgIAAQgHAAgKAGIAAANIACArIgYAEIACgsIgChaIAYgEIgCAvIAAAVIAGgHIAKgIQAHgEAIAAQAIAAAGAGQAFAGAAAKIgBAUIACAsIgYAEg");
	this.shape_19.setTransform(31.3,608.8);

	this.txtcpass1 = new lib.an_TextInput({'id': 'txtcpass1', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtcpass1.setTransform(25.5,393.3,3.2,2.044,0,0,0,0.3,0);

	this.txtpass1 = new lib.an_TextInput({'id': 'txtpass1', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtpass1.setTransform(25.5,337.4,3.2,2.044,0,0,0,0.3,0);

	this.txtphone1 = new lib.an_TextInput({'id': 'txtphone1', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtphone1.setTransform(25.5,282.6,3.2,2.045,0,0,0,0.3,0.1);

	this.b5 = new lib.an_Button({'id': 'b5', 'label':'Sign up', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b5.setTransform(24.6,447.9,3.2,2.045);

	this.txtemail1 = new lib.an_TextInput({'id': 'txtemail1', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtemail1.setTransform(25.5,229.4,3.2,2.044,0,0,0,0.3,0);

	this.txtuser1 = new lib.an_TextInput({'id': 'txtuser1', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtuser1.setTransform(25.5,174.6,3.2,2.045,0,0,0,0.3,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.txtuser1},{t:this.txtemail1},{t:this.b5},{t:this.txtphone1},{t:this.txtpass1},{t:this.txtcpass1},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.b6}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_SIGNUP, new cjs.Rectangle(23,100,323.2,531.4), null);


(lib.MC_SIGNIN = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26264B").s().p("AgNA8QgEgEAAgFQAAgHAFgDQAEgFAGAAQAEAAADADQADADAAAHQAAAGgEAEQgEAEgFAAQgFABgDgEgAgPAGQAXgOAAgOQAAgUgngBIAJgUQAaADAOAJQAOAJAAAPQAAAUghAZg");
	this.shape.setTransform(186,609.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("AgKAxQgFgGgBgLIABgUIAAgcIgMAAIACgOIAKAAIAAgUIAXgFIgBAZIAVAAIgCAOIgUAAIgBAlQAAAIACADQABADAFAAIALgCIABANQgMAKgJAAQgIAAgGgHg");
	this.shape_1.setTransform(179.7,610.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_2.setTransform(171.7,611.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("AAMAbQgPASgMAAQgJAAgGgGQgGgGAAgKIABgWIgCgqIAYgEIgDA2QAAAIADADQADAEAGAAQAFgBALgGIAAgKIgCguIAYgFIgCAsIAAAPQAAALAGAMIgSAIQgGgLgCgIg");
	this.shape_3.setTransform(162.6,611.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("AgbAkQgKgLAAgRQAAgWAMgQQANgQAPAAQARAAAJAKQAJALAAASQAAAXgLAPQgNAQgRAAQgPAAgJgLgAgRAAQAAAMAFAIQAFAHAIAAQAIAAAFgHQAEgGAAgNQAAgZgRAAQgSAAAAAYg");
	this.shape_4.setTransform(153.4,611.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("AgWAkQgJgLAAgRQAAgVANgRQANgQAQAAIAKABIALAFIgMAWQgHgFgIAAQgHAAgEAGQgFAFAAAMQABALAEAHQAEAHAHAAQAFAAAEgDIAHgFIACgDIAIAJIgBADIgEAGIgGAHIgJAGQgGACgGAAQgNAAgIgLg");
	this.shape_5.setTransform(145.8,611.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("AgWAkQgJgLAAgRQAAgVANgRQANgQAQAAIAKABIALAFIgNAWQgGgFgIAAQgHAAgFAGQgEAFAAAMQABALAEAHQAEAHAIAAQAEAAAEgDIAHgFIACgDIAJAJIgCADIgEAGIgHAHIgJAGQgFACgGAAQgNAAgIgLg");
	this.shape_6.setTransform(138.9,611.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJABIgJABIgNAFIgFgNQAIgGALgEQALgFAGAAQAcAAAAAbIAAAjQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADADAEABQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_7.setTransform(130.9,611.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_8.setTransform(119.3,611.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJABIgJABIgNAFIgFgNQAIgGALgEQALgFAGAAQAcAAAAAbIAAAjQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADADAEABQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_9.setTransform(110.3,611.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_10.setTransform(99.3,611.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#26264B").s().p("AgaACIgNgrIAZgEIAIAnIAGAdIACAAIAGgXIAJgtIAXACIgOAsIgLArIgeACg");
	this.shape_11.setTransform(91.2,611.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#26264B").s().p("AAMAfQgKAPgOAAQgLAAgHgHQgGgGAAgMQAAgMAKgHQAKgIAPAAIAMAAIAAgDQAAgJgEgDQgEgEgJABIgJABIgNAFIgFgNQAIgGALgEQALgFAGAAQAcAAAAAbIAAAjQAAAKAGANIgTAIQgEgJgCgHgAgPAQQAAAFADADQADADAEABQAGAAAKgIIAAgMQgKgCgFAAQgLAAAAAKg");
	this.shape_12.setTransform(82.8,611.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#26264B").s().p("AAPAYQAAgNgCgEQgCgEgIAAQgHAAgKAGIAAANIACArIgYAEIACgsIgChaIAYgEIgCAvIAAAVIAGgHIAKgIQAHgEAIAAQAIAAAGAGQAFAGAAAKIgBAUIACAsIgYAEg");
	this.shape_13.setTransform(74,608.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#26264B").s().p("AgKAxQgFgGAAgLIABgUIAAgcIgNAAIACgOIALAAIAAgUIAWgFIgCAZIAWAAIgCAOIgUAAIgBAlQAAAIABADQACADAFAAIALgCIABANQgMAKgJAAQgIAAgGgHg");
	this.shape_14.setTransform(63.4,610.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#26264B").s().p("AgKAVQACgVAAgUIATgBQgEAXgHAUg");
	this.shape_15.setTransform(58.5,606.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_16.setTransform(51.9,611.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#26264B").s().p("AgbAkQgKgLAAgRQAAgWAMgQQANgQAPAAQARAAAJAKQAJALAAASQAAAXgLAPQgNAQgRAAQgPAAgJgLgAgRAAQAAAMAFAIQAFAHAIAAQAIAAAFgHQAEgGAAgNQAAgZgRAAQgSAAAAAYg");
	this.shape_17.setTransform(42.8,611.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#26264B").s().p("AgxA9IACgsIgChPIAwAAQAXAAAOAOQAOAPAAAaQAAAggRATQgRATgcAAIglgCgAgYgqIgCA4IABAeIAVABQANAAAIgMQAIgMAAgVQAAgWgIgKQgIgLgQAAIgRABg");
	this.shape_18.setTransform(33,609.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#26264B").s().p("AghA8QgKgGAAgJQAAgRAdgIQgHgEAAgGQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBIAFgEIAAgBQgNAAgHgHQgIgHAAgMQAAgQANgLQANgMAQAAIANACQAJACAIAAIAOAAIgCAPIgNgCQACAGAAAHQAAAGgEAGQgDAHgEAEIgJAHIgHAFQgEACAAABQAAADAIADQATAIAGAFQAHAGAAAIQAAANgNAIQgOAJgRAAQgTAAgKgGgAgQAgQgFAEAAAEQAAAFAFACQAFADAJAAQAIAAAFgDQAGgEAAgFQAAgDgEgDQgDgDgJgDQgMADgFADgAgVggQAAARARAAQAOAAAAgPQAAgJgEgEQgEgFgHAAQgQAAAAAQg");
	this.shape_19.setTransform(230.6,375.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_20.setTransform(221,373.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#26264B").s().p("AgKAXIgCgqIAXgFIgCAsIACAqIgXAFgAgKgoQgDgEAAgGQAAgGAFgFQAEgFAFAAQAHAAADADQADAEAAAHQAAAGgFAFQgEAFgFAAQgGAAgEgEg");
	this.shape_21.setTransform(214.3,371);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#26264B").s().p("AgSArQgIgDgDgEIgDgEIAOgQIAEADIAIAGQAFAEAFAAQAJAAAAgJQAAgDgEgCIgJgFIgMgGQgFgDgEgFQgEgFgBgHQAAgMALgJQAKgJANAAQAHAAAGABQAHACAFADIgMAWIgDgDIgHgEQgGgDgFAAQgDAAgCACQgCACAAADQAAAEAGADIANAGQAHADAGAFQAHAHAAAJQAAAMgLAKQgKAJgNAAQgJAAgHgEg");
	this.shape_22.setTransform(208.3,373.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#26264B").s().p("AAMAcQgPASgMAAQgJAAgGgHQgGgGAAgLIABgUIgCgqIAYgFIgDA2QAAAHADAEQADAEAGgBQAFAAALgFIAAgMIgCguIAYgEIgCAsIAAAOQAAAMAGAMIgSAIQgGgMgCgGg");
	this.shape_23.setTransform(200.4,373.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_24.setTransform(188.2,373.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#26264B").s().p("AgKAXIgCgqIAXgFIgCAsIACAqIgXAFgAgKgoQgDgEAAgGQAAgGAFgFQAEgFAFAAQAHAAADADQADAEAAAHQAAAGgFAFQgEAFgFAAQgGAAgEgEg");
	this.shape_25.setTransform(181.4,371);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_26.setTransform(171.6,373.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#26264B").s().p("AghA8QgKgGAAgJQAAgRAdgIQgHgEAAgGQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBIAFgEIAAgBQgNAAgHgHQgIgHAAgMQAAgQANgLQANgMAQAAIANACQAJACAIAAIAOAAIgCAPIgNgCQACAGAAAHQAAAGgEAGQgDAHgEAEIgJAHIgHAFQgEACAAABQAAADAIADQATAIAGAFQAHAGAAAIQAAANgNAIQgOAJgRAAQgTAAgKgGgAgQAgQgFAEAAAEQAAAFAFACQAFADAJAAQAIAAAFgDQAGgEAAgFQAAgDgEgDQgDgDgJgDQgMADgFADgAgVggQAAARARAAQAOAAAAgPQAAgJgEgEQgEgFgHAAQgQAAAAAQg");
	this.shape_27.setTransform(162.6,375.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#26264B").s().p("AgKAXIgCgqIAXgFIgCAsIACAqIgXAFgAgKgoQgDgEAAgGQAAgGAFgFQAEgFAFAAQAHAAADADQADAEAAAHQAAAGgFAFQgEAFgFAAQgGAAgEgEg");
	this.shape_28.setTransform(155.7,371);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#26264B").s().p("AgSArQgHgDgDgEIgEgEIAPgQIACADIAIAGQAGAEAEAAQAKAAAAgJQAAgDgEgCIgJgFIgMgGQgFgDgFgFQgDgFAAgHQAAgMAKgJQAKgJAOAAQAGAAAGABQAHACAFADIgMAWIgDgDIgHgEQgGgDgEAAQgDAAgDACQgCACAAADQAAAEAGADIAMAGQAIADAHAFQAFAHAAAJQAAAMgKAKQgKAJgMAAQgJAAgIgEg");
	this.shape_29.setTransform(149.7,373.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#26264B").s().p("AgWACQgBgXgDgTIAVgFIADAaQAPgZAOAAIgCAcQgKAAgGACQgGACgFAFIAAAHIACApIgYAFg");
	this.shape_30.setTransform(140.7,373.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#26264B").s().p("AgbAkQgKgLAAgRQAAgWAMgQQANgQAPAAQARAAAJAKQAJALAAASQAAAXgLAPQgNAQgRAAQgPAAgJgLgAgRAAQAAAMAFAIQAFAHAIAAQAIAAAFgHQAEgGAAgNQAAgZgRAAQgSAAAAAYg");
	this.shape_31.setTransform(132.8,373.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#26264B").s().p("AAlAAQgBgcgFgJQgEgKgSAAQgSAAgWAPIAAAeIAFBfIg4AKIAFhjQgCg2gHgsIAygKIAEAuQAGgIAIgIQAIgIAPgKQARgKATAAQATAAANAOQAMAPAAAZIgCAuIAEBhIg4AKg");
	this.shape_32.setTransform(227.4,130.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#26264B").s().p("AgcCOIAEhjIgEiuIA5gKIgFCxIAEBqg");
	this.shape_33.setTransform(210.6,127);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#26264B").s().p("AAkAAQAAgcgEgJQgFgKgTAAQgQAAgXAPIAAAeIAEBfIg2AKIAEhjQgCg2gHgsIAygKIAEAuQAGgIAIgIQAHgIARgKQARgKASAAQASAAANAOQANAPAAAZIgDAuIAFBhIg3AKg");
	this.shape_34.setTransform(187.3,130.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#26264B").s().p("AhNCJQgWgOAAgWQAAgkBCgVQgQgIAAgNQAAgFAFgGQAEgFAHgGIAAgCQgdAAgRgQQgSgRAAgbQAAgkAdgbQAdgaAoAAQAKAAATAEQAUAEATAAIAfAAIgEAkIgfgEQAEAMABAPQgBAQgGAPQgIAOgKAJQgKAKgKAGIgRAMQgHAEAAAEQAAAIAQAGQArARAQANQAPAMAAASQAAAfgeATQgfAUgpAAQgqAAgYgOgAgmBKQgLAHAAAKQAAAKALAHQAMAGAUAAQASAAAOgIQAMgHAAgMQAAgHgIgGQgHgHgVgIQgcAIgMAHgAgzhJQABAlAoAAQAhAAAAgiQAAgUgJgKQgJgJgSAAQgmAAAAAkg");
	this.shape_35.setTransform(166.8,135.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#26264B").s().p("AgYA1IgEhiIA1gKIgEBlIAEBgIg2AKgAgXhdQgIgJAAgNQAAgOAKgLQALgLAOAAQANAAAHAIQAIAHAAAPQAAAQgKAKQgLALgNAAQgOAAgHgJg");
	this.shape_36.setTransform(151.1,126);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#26264B").s().p("Ag9CDQgVgNgJgKIgDgEIAfgsQAjAjAiAAQASAAALgJQALgIAAgOQAAgNgNgJQgNgJgTgHQgRgHgTgJQgTgJgNgPQgNgRAAgXQAAglAfgbQAfgbApAAQATAAASAEQASAEARAIIgcA8QgLgJgUgIQgUgHgPAAQgOAAgJAHQgJAHAAAMQAAAMANAJQANAJASAHIAlAQQATAIANAQQANAQAAAXQAAAngfAdQgfAdgoAAQgeAAgYgPg");
	this.shape_37.setTransform(135.3,127.1);

	this.b4 = new lib.an_Button({'id': 'b4', 'label':'Sign up', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b4.setTransform(248.3,590.5,0.947,1.827,0,0,0,0,0.1);

	this.b3 = new lib.an_Button({'id': 'b3', 'label':'Google', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b3.setTransform(187.6,399,1.562,2.045);

	this.b2 = new lib.an_Button({'id': 'b2', 'label':'Facebook', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b2.setTransform(24.6,399,1.562,2.045);

	this.b1 = new lib.an_Button({'id': 'b1', 'label':'Sign in', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b1.setTransform(24.6,286.3,3.2,2.045);

	this.txtpass = new lib.an_TextInput({'id': 'txtpass', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtpass.setTransform(25.5,229.4,3.2,2.044,0,0,0,0.3,0);

	this.txtuser = new lib.an_TextInput({'id': 'txtuser', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtuser.setTransform(25.5,174.6,3.2,2.045,0,0,0,0.3,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.txtuser},{t:this.txtpass},{t:this.b1},{t:this.b2},{t:this.b3},{t:this.b4},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_SIGNIN, new cjs.Rectangle(23,100,323.2,531.4), null);


(lib.MC_GAME = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Bitmap26();
	this.instance.parent = this;
	this.instance.setTransform(16,202);

	this.b9 = new lib.an_Button({'id': 'b9', 'label':'Lead Board', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b9.setTransform(215.8,77.6,1.289,1.585,0,0,0,0.1,0.1);

	this.b8 = new lib.an_Button({'id': 'b8', 'label':'Invite', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b8.setTransform(16.2,77.6,1.289,1.585,0,0,0,0.1,0.1);

	this.b10 = new lib.an_Button({'id': 'b10', 'label':'Play', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b10.setTransform(24.6,557.4,3.2,2.045);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.b10},{t:this.b8},{t:this.b9},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_GAME, new cjs.Rectangle(15.4,76.6,334.7,526.8), null);


(lib.MC_ENTEROTP = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26264B").s().p("AhXCNIAFhjIgFi3IBYAAQAoAAAXAXQAYAWAAAnQgBAngdAbQgeAbgpABIgXAAIAEBogAgkgCIAcAAQAUAAALgOQALgOAAgYQAAgtguAAIgVAAg");
	this.shape.setTransform(254.9,127.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("AgdCNIAFhjIgCiNIhJAFIAFgvIDBAAIgDAvIhIgFIgCCGIAFBqg");
	this.shape_1.setTransform(232.8,127.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AhaBsQgggmAAg4QAAhFAmgtQAmgtA6AAQAxAAAfAlQAfAlAAA5QAABHglAsQglAtg8AAQgvAAgggmgAgwhIQgSAYAAAuQAAAtATAeQATAdAeAAQAgAAARgZQARgZAAgsQAAhnhEAAQgfAAgRAXg");
	this.shape_2.setTransform(209.3,127.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("Ag0AFQgCg2gHgsIAxgKQAEAfABAdQAkg6AiAAIgFBAQgYAAgOAEQgOAEgNANIAAAPIAFBfIg3AKg");
	this.shape_3.setTransform(183,130.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("Ag6BTQgVgaAAgnQAAgxAdgmQAdgmAmAAQAeAAARARQAQARAAAeQAAASgGAXIgIAIIhmAJQAHAuAkAAQAMAAANgGQANgGAHgGIAIgGIATAXQgFAHgIAJQgJAJgJAGQgIAGgPAGQgOAFgPAAQghAAgVgZgAgWgyQgNAPgCAWIBFgJIABgPQAAgcgaAAQgRAAgMAPg");
	this.shape_4.setTransform(165.6,130.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("AgYBwQgNgPAAgYIACguIAAhAIgcAAIAFghIAXAAIAAgtIA2gLIgEA4IAxAAIgEAhIgvAAIgDBVQAAATAFAGQADAHAKAAQAJAAARgFIACAdQgbAXgUAAQgUAAgNgPg");
	this.shape_5.setTransform(149.6,128.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("AAkAAQAAgcgEgJQgFgKgTAAQgQAAgXAPIAAAeIAEBfIg3AKIAFhjQgBg2gIgsIAygKIAEAuQAGgIAIgIQAHgIARgKQAQgKATAAQATAAAMAOQANAPAAAZIgDAuIAFBhIg3AKg");
	this.shape_6.setTransform(131.2,130.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AhMCNIAEhjIgEi3ICYAAIgEAtIhbAAIgDBIIBRAAIgEArIhPAAIAAAQIADA/IBiAAIgEArg");
	this.shape_7.setTransform(111.5,127.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AglA+IACgsIgChPIAmAAQARAAAKAKQAKAJAAASQAAAQgMAMQgNAMgSAAIgKAAIACAugAgPgBIAMAAQAIAAAFgFQAFgHAAgKQAAgUgUAAIgJAAg");
	this.shape_8.setTransform(220.4,309.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("AgMA+IACgsIgBg9IggADIACgVIBVAAIgCAVIgggDIAAA6IACAvg");
	this.shape_9.setTransform(210.7,309.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#26264B").s().p("AgnAwQgOgRAAgZQAAgdAQgUQARgUAaAAQAUAAAPAQQANAQAAAaQAAAegRAUQgPATgaAAQgVAAgOgQgAgVgfQgHAKAAAUQgBAUAJANQAIANANAAQAOAAAHgLQAIgLgBgTQAAgtgdAAQgNAAgIAKg");
	this.shape_10.setTransform(200.4,309.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#26264B").s().p("AAOAzQgOASgKAAQgMAAgIgLQgJgKAAgRQAAgWANgOQAMgQASAAIAKABIAAgDIgCgqIAYgFIgCBcIAAAQQAAALAGAOIgUAIQgGgLAAgJgAgOAFQgEAGgBALQABAMAEAGQAEAHAIAAQAHAAAJgHIAAgnIgQgBQgHAAgFAFg");
	this.shape_11.setTransform(187.1,308.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#26264B").s().p("AAQAAQAAgMgCgEQgCgEgIAAQgHAAgKAGIAAANIACAqIgYAEIACgsQgBgXgDgTIAVgEIACAUIAGgHQAEgEAGgEQAIgEAIAAQAIAAAGAGQAFAGAAALIgBAUIACArIgYAEg");
	this.shape_12.setTransform(178,310.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_13.setTransform(169.2,310.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#26264B").s().p("AgSArQgIgDgDgEIgDgEIAOgQIAEADIAIAGQAGAEAEAAQAJAAAAgJQAAgDgEgCIgJgFIgMgGQgFgDgEgFQgEgFAAgHQgBgMALgJQAKgJANAAQAHAAAGABQAGACAGADIgMAWIgDgDIgHgEQgGgDgEAAQgDAAgDACQgCACAAADQAAAEAGADIANAGQAHADAGAFQAHAHgBAJQAAAMgKAKQgKAJgNAAQgIAAgIgEg");
	this.shape_14.setTransform(161.6,310.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#26264B").s().p("AgZAkQgJgLAAgRQAAgVANgRQAMgQARAAQANAAAHAHQAHAHAAAOQAAAIgCAKIgEACIgsAFQADAUAPAAQAFAAAGgDIAJgFIADgDIAJAKIgGAHIgIAHQgEADgGACQgGACgGAAQgPAAgJgLgAgJgWQgGAHgBAKIAegEIABgHQAAgMgMAAQgHAAgFAGg");
	this.shape_15.setTransform(154.1,310.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#26264B").s().p("AgGANIgEAAIgKAAIAAABIACAvIgYAAIACgsIgChPIAnAAQAQAAAKAJQAKAKAAAPQABALgGALQgGAIgKAGIAfAsIgWALgAgUgEIANAAQAHAAAFgFQAGgHAAgKQAAgSgVAAIgJAAg");
	this.shape_16.setTransform(145.5,309.4);

	this.b7 = new lib.an_Button({'id': 'b7', 'label':'Procced', 'disabled':false, 'visible':true, 'class':'ui-button'});

	this.b7.setTransform(24.6,228,3.2,2.045);

	this.txtotp = new lib.an_TextInput({'id': 'txtotp', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.txtotp.setTransform(25.5,175.4,3.2,2.045,0,0,0,0.3,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.txtotp},{t:this.b7},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_ENTEROTP, new cjs.Rectangle(23,100,323.2,223.1), null);


(lib.MC_CONTESTS = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this._Ctime1 = new cjs.Text("Contest name", "13px 'Acme'", "#FFFFFF");
	this._Ctime1.name = "_Ctime1";
	this._Ctime1.textAlign = "center";
	this._Ctime1.lineHeight = 18;
	this._Ctime1.lineWidth = 154;
	this._Ctime1.parent = this;
	this._Ctime1.setTransform(184.3,296.5);

	this._Cname1 = new cjs.Text("Contest name", "16px 'Acme'", "#FFFFFF");
	this._Cname1.name = "_Cname1";
	this._Cname1.textAlign = "center";
	this._Cname1.lineHeight = 22;
	this._Cname1.lineWidth = 154;
	this._Cname1.parent = this;
	this._Cname1.setTransform(184.3,277.1);

	this.instance = new lib.Bitmap22();
	this.instance.parent = this;
	this.instance.setTransform(153.6,180);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(1,1,1).p("AMyAAI5iAA");
	this.shape.setTransform(184.7,273.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("Aq4OEQh5gBAAh4IAAlbIAAy7QAAh4B5ABIVyAAQBzAAAEBwIAATCI5iAAIZiAAIAAFjQgEBwhzABgAMxGwg");
	this.shape_1.setTransform(184.7,230.3);

	this.BTNclose = new lib.transparent();
	this.BTNclose.name = "BTNclose";
	this.BTNclose.parent = this;
	this.BTNclose.setTransform(318.7,95.3,0.861,0.954,0,0,0,0.4,0.1);

	this.pop1x = new lib.pop0();
	this.pop1x.name = "pop1x";
	this.pop1x.parent = this;
	this.pop1x.setTransform(16.5,573.2,0.865,0.865,0,0,0,0.1,0.1);

	this.pop1x_1 = new lib.pop0();
	this.pop1x_1.name = "pop1x_1";
	this.pop1x_1.parent = this;
	this.pop1x_1.setTransform(16.5,484.1,0.865,0.865,0,0,0,0.1,0.1);

	this.pop1x_2 = new lib.pop0();
	this.pop1x_2.name = "pop1x_2";
	this.pop1x_2.parent = this;
	this.pop1x_2.setTransform(16.5,413.2,0.865,0.865,0,0,0,0.1,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AgbBCQgJgDgFgCIgEgDIAJgYQAGAFAKAEQAKAEAIAAQAIAAAGgFQAHgGAAgIQAAgTgYgEIgcgEIAAhFIAjADIAqgDIgEAdIg1gFIAAAZIAWAEQARADAKAKQAKAJAAAOQAAAUgPAOQgQAOgWAAQgKAAgKgDg");
	this.shape_2.setTransform(352.7,424.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("AgaBCQgKgDgEgCIgFgDIAIgZIAGADIAMAIQAIADAJAAQAJAAAGgEQAHgFAAgJQAAgKgIgFQgIgGgNAAIgSAAIAEgTIAMAAQALgBAGgFQAGgFAAgIQAAgIgFgFQgGgEgGAAQgIAAgIADQgIACgKAFIAFgZQARgGARgBQARABALAIQALAJAAAOQAAAKgFAIQgGAIgKAEQAPAFAHAIQAGAHAAANQAAATgPAMQgOANgWAAQgKAAgLgEg");
	this.shape_3.setTransform(342.3,424.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("AABBDIgxACIAEgWIAsgyQANgPAAgLQAAgMgOAAQgJAAgLAEQgKADgHAEIgFAEIAFgeQAJgFALgDQAMgEAIAAQASAAAKAJQALAKAAAPQAAAUgVAVIgkAmIBBgEIgEAcg");
	this.shape_4.setTransform(332.1,424.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("AgbBCQgJgDgFgCIgEgDIAJgYQAGAFAKAEQAKAEAIAAQAIAAAGgFQAHgGAAgIQAAgTgYgEIgcgEIAAhFIAjADIAqgDIgEAdIg1gFIAAAZIAWAEQARADAKAKQAKAJAAAOQAAAUgPAOQgQAOgWAAQgKAAgKgDg");
	this.shape_5.setTransform(321.5,424.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("AAFBEIACgjIg6AAIACgSIAxhOIAigDIgCBQIAUgBIgCAVIgRgBIABAjgAgaAOIAhAAIgBg1g");
	this.shape_6.setTransform(310.5,424.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AgaBCQgKgDgEgCIgFgDIAIgZIAGADIAMAIQAIADAJAAQAJAAAGgEQAHgFAAgJQAAgKgIgFQgIgGgNAAIgSAAIAEgTIAMAAQALgBAGgFQAGgFAAgIQAAgIgFgFQgGgEgGAAQgIAAgIADQgIACgKAFIAFgZQARgGARgBQARABALAIQALAJAAAOQAAAKgFAIQgGAIgKAEQAPAFAHAIQAGAHAAANQAAATgPAMQgOANgWAAQgKAAgLgEg");
	this.shape_7.setTransform(299.9,424.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AABBDIgxACIAEgWIAsgyQAMgPAAgLQAAgMgNAAQgJAAgLAEQgKADgGAEIgGAEIAGgeQAHgFAMgDQALgEAIAAQASAAALAJQALAKAAAPQAAAUgVAVIgkAmIBAgEIgDAcg");
	this.shape_8.setTransform(289.7,424.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("AgbBCQgJgDgFgCIgEgDIAJgYQAGAFAKAEQAKAEAIAAQAIAAAGgFQAHgGAAgIQAAgTgYgEIgcgEIAAhFIAjADIAqgDIgEAdIg1gFIAAAZIAWAEQARADAKAKQAKAJAAAOQAAAUgPAOQgQAOgWAAQgKAAgKgDg");
	this.shape_9.setTransform(352.7,493.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#26264B").s().p("AgaBDQgKgEgEgCIgFgDIAIgZIAGADIAMAHQAIAEAJAAQAJAAAGgEQAHgFAAgJQAAgJgIgGQgIgGgNAAIgSAAIAEgTIAMAAQALAAAGgGQAGgFAAgIQAAgIgFgFQgGgEgGAAQgIAAgIACQgIADgKAFIAFgZQARgHARAAQARABALAIQALAJAAAOQAAAKgFAIQgGAIgKAEQAPAFAHAIQAGAHAAANQAAASgPANQgOANgWAAQgKAAgLgDg");
	this.shape_10.setTransform(342.3,493.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#26264B").s().p("AABBDIgxACIAEgWIAsgyQANgPAAgLQAAgMgOAAQgJAAgLAEQgKADgHAEIgFAEIAFgeQAJgFALgDQAMgEAIAAQASAAAKAJQALAKAAAPQAAAUgVAVIgkAmIBBgEIgEAcg");
	this.shape_11.setTransform(332.1,493.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#26264B").s().p("AgbBCQgJgDgFgCIgEgDIAJgYQAGAFAKAEQAKAEAIAAQAIAAAGgFQAHgGAAgIQAAgTgYgEIgcgEIAAhFIAjADIAqgDIgEAdIg1gFIAAAZIAWAEQARADAKAKQAKAJAAAOQAAAUgPAOQgQAOgWAAQgKAAgKgDg");
	this.shape_12.setTransform(321.5,493.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#26264B").s().p("AAFBDIACgiIg6AAIACgSIAxhNIAigFIgCBRIAUgBIgCAVIgRgBIABAigAgaANIAhAAIgBg1g");
	this.shape_13.setTransform(310.5,493.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#26264B").s().p("AgaBDQgKgEgEgCIgFgDIAIgZIAGADIAMAHQAIAEAJAAQAJAAAGgEQAHgFAAgJQAAgJgIgGQgIgGgNAAIgSAAIAEgTIAMAAQALAAAGgGQAGgFAAgIQAAgIgFgFQgGgEgGAAQgIAAgIACQgIADgKAFIAFgZQARgHARAAQARABALAIQALAJAAAOQAAAKgFAIQgGAIgKAEQAPAFAHAIQAGAHAAANQAAASgPANQgOANgWAAQgKAAgLgDg");
	this.shape_14.setTransform(299.9,493.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#26264B").s().p("AABBDIgxACIAEgWIAsgyQAMgPAAgLQAAgMgNAAQgJAAgLAEQgKADgGAEIgGAEIAGgeQAHgFAMgDQALgEAIAAQASAAALAJQALAKAAAPQAAAUgVAVIgkAmIBAgEIgDAcg");
	this.shape_15.setTransform(289.7,493.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#26264B").s().p("AgbBCQgJgDgFgCIgEgDIAJgYQAGAFAKAEQAKAEAIAAQAIAAAGgFQAHgGAAgIQAAgTgYgEIgcgEIAAhFIAjADIAqgDIgEAdIg1gFIAAAZIAWAEQARADAKAKQAKAJAAAOQAAAUgPAOQgQAOgWAAQgKAAgKgDg");
	this.shape_16.setTransform(352.7,584.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#26264B").s().p("AgaBDQgKgDgEgEIgFgCIAIgZIAGADIAMAHQAIAEAJABQAJgBAGgFQAHgEAAgJQAAgKgIgFQgIgGgNAAIgSAAIAEgUIAMAAQALABAGgGQAGgFAAgIQAAgIgFgEQgGgGgGAAQgIAAgIADQgIADgKAFIAFgZQARgHARABQARAAALAJQALAIAAANQAAALgFAIQgGAIgKAEQAPAFAHAHQAGAJAAAMQAAASgPANQgOAMgWAAQgKAAgLgCg");
	this.shape_17.setTransform(342.3,584);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#26264B").s().p("AABBDIgxACIAEgWIAsgyQANgPAAgLQAAgMgOAAQgJAAgLAEQgKADgHAEIgFAEIAFgeQAJgFALgDQAMgEAIAAQASAAAKAJQALAKAAAPQAAAUgVAVIgkAmIBBgEIgEAcg");
	this.shape_18.setTransform(332.1,583.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#26264B").s().p("AgbBCQgJgDgFgCIgEgDIAJgYQAGAFAKAEQAKAEAIAAQAIAAAGgFQAHgGAAgIQAAgTgYgEIgcgEIAAhFIAjADIAqgDIgEAdIg1gFIAAAZIAWAEQARADAKAKQAKAJAAAOQAAAUgPAOQgQAOgWAAQgKAAgKgDg");
	this.shape_19.setTransform(321.5,584.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#26264B").s().p("AAFBDIACghIg6AAIACgSIAxhOIAigFIgCBRIAUgBIgCAVIgRAAIABAhgAgaANIAhAAIgBg1g");
	this.shape_20.setTransform(310.5,584);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#26264B").s().p("AgaBDQgKgDgEgEIgFgCIAIgZIAGADIAMAHQAIAEAJABQAJgBAGgFQAHgEAAgJQAAgKgIgFQgIgGgNAAIgSAAIAEgUIAMAAQALABAGgGQAGgFAAgIQAAgIgFgEQgGgGgGAAQgIAAgIADQgIADgKAFIAFgZQARgHARABQARAAALAJQALAIAAANQAAALgFAIQgGAIgKAEQAPAFAHAHQAGAJAAAMQAAASgPANQgOAMgWAAQgKAAgLgCg");
	this.shape_21.setTransform(299.9,584);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#26264B").s().p("AABBDIgxACIAEgWIAsgyQAMgPAAgLQAAgMgNAAQgJAAgLAEQgKADgGAEIgGAEIAGgeQAHgFAMgDQALgEAIAAQASAAALAJQALAKAAAPQAAAUgVAVIgkAmIBAgEIgDAcg");
	this.shape_22.setTransform(289.7,583.9);

	this.text = new cjs.Text("2342356", "16px 'Acme'", "#26264B");
	this.text.lineHeight = 22;
	this.text.lineWidth = 193;
	this.text.parent = this;
	this.text.setTransform(84.6,601.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAGAAAIgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgFADgHACQgIADgGAAQgSAAgKgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_23.setTransform(178.6,585.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#26264B").s().p("AAwAAQAAgOgCgEQgDgGgJAAQgJAAgLAHIgBARIACAxIgbAFIACg2QAAgOgCgEQgDgGgJAAQgJAAgMAIIAAAQIACAwIgcAFIADgzQgBgbgEgWIAagGIACAYIAHgIQAEgFAIgEQAJgGAJAAQAIAAAGAGQAGAGABAJQASgVASAAQALAAAGAIQAHAHAAANIgBAYIACAxIgcAFg");
	this.shape_24.setTransform(165.5,585.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_25.setTransform(152.1,585.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#26264B").s().p("AATAAQgBgOgCgEQgCgGgKAAQgIAAgMAIIAAAQIACAwIgcAFIADgzQgBgbgDgWIAZgGIACAYIAHgIQAEgFAHgEQAJgGAKAAQAJAAAHAIQAGAHABANIgCAYIADAxIgdAFg");
	this.shape_26.setTransform(141.7,585.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#26264B").s().p("AgaADQgBgcgEgWIAagGIACAgQASgeARAAIgCAgQgMAAgIACQgHADgGAGIAAAIIACAxIgcAEg");
	this.shape_27.setTransform(129.7,585.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#26264B").s().p("AgeAqQgKgNAAgUQAAgZAPgTQAPgTATAAQAPAAAIAIQAJAJAAAPQAAAKgDALIgFAEIgzAFQAEAXARAAQAHAAAHgDQAGgDAEgDIADgDIAKALQgCAEgFAFIgIAIQgEADgIACQgIADgHAAQgRAAgLgNgAgLgZQgHAHAAAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_28.setTransform(120.8,585.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#26264B").s().p("AggBHIAVgxIgPgpIgWgzIAcgFIAOAsIAGAWIADAAIAEgQIAPgzIAaAEIgkBhIgRAzg");
	this.shape_29.setTransform(111.3,587.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_30.setTransform(101.4,585.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#26264B").s().p("AgLAeIgDhpIAcgFIgDBrIADAxIgcAFg");
	this.shape_31.setTransform(93.9,582.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#26264B").s().p("AgsBIIADgyIgDheIAtAAQAUAAAMAMQAMAMAAATQAAAUgPAOQgQAOgUAAIgMAAIACA1gAgSgBIAPAAQAJABAGgIQAGgGAAgOQAAgWgYAAIgKAAg");
	this.shape_32.setTransform(86.7,583.5);

	this.text_1 = new cjs.Text("2342356", "16px 'Acme'", "#26264B");
	this.text_1.lineHeight = 22;
	this.text_1.lineWidth = 290;
	this.text_1.parent = this;
	this.text_1.setTransform(84.6,510.5);

	this.text_2 = new cjs.Text("2342356", "16px 'Acme'", "#26264B");
	this.text_2.lineHeight = 22;
	this.text_2.lineWidth = 290;
	this.text_2.parent = this;
	this.text_2.setTransform(84.6,438.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAGAAAIgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgFADgHACQgIADgGAAQgSAAgKgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_33.setTransform(178.6,494.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#26264B").s().p("AAwAAQAAgOgCgEQgDgGgJAAQgJAAgLAIIgBAQIACAxIgbAFIACg2QAAgOgCgEQgDgGgJAAQgJABgMAHIAAAQIACAwIgcAFIADgzQgBgbgEgXIAagFIACAYIAHgJQAEgDAIgGQAJgFAJAAQAIAAAGAGQAGAFABAKQASgVASAAQALAAAGAIQAHAHAAANIgBAYIACAxIgcAFg");
	this.shape_34.setTransform(165.5,494.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_35.setTransform(152.1,494.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#26264B").s().p("AATAAQgBgOgCgEQgCgGgKAAQgIABgMAHIAAAQIACAwIgcAFIADgzQgBgbgDgXIAZgFIACAYIAHgJQAEgDAHgGQAJgFAKAAQAJAAAHAIQAGAHABANIgCAYIADAxIgdAFg");
	this.shape_36.setTransform(141.7,494.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#26264B").s().p("AgaACQgBgbgEgWIAagGIACAgQASgfARAAIgCAhQgMAAgIACQgHADgGAGIAAAIIACAxIgcAEg");
	this.shape_37.setTransform(129.7,494.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#26264B").s().p("AgeAqQgKgNAAgUQAAgZAPgTQAPgTATAAQAPAAAIAIQAJAJAAAPQAAAKgDALIgFAEIgzAFQAEAXARAAQAHAAAHgDQAGgDAEgDIADgDIAKALQgCAEgFAFIgIAIQgEADgIACQgIADgHAAQgRAAgLgNgAgLgZQgHAHAAAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_38.setTransform(120.8,494.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#26264B").s().p("AggBHIAVgxIgPgpIgWgzIAcgFIAOAsIAGAWIADAAIAEgPIAPg0IAaAEIgkBgIgRA0g");
	this.shape_39.setTransform(111.3,497);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_40.setTransform(101.4,494.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#26264B").s().p("AgLAeIgDhpIAcgFIgDBrIADAxIgcAFg");
	this.shape_41.setTransform(93.9,491.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#26264B").s().p("AgsBIIADgzIgDhdIAtAAQAUABAMALQAMALAAAVQAAAUgPANQgQAOgUAAIgMAAIACA1gAgSAAIAPAAQAJAAAGgIQAGgGAAgNQAAgXgYAAIgKAAg");
	this.shape_42.setTransform(86.7,492.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAGAAAIgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgFADgHACQgIADgGAAQgSAAgKgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_43.setTransform(178.6,426.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#26264B").s().p("AAwAAQAAgNgCgGQgDgEgJAAQgJgBgLAIIgBAPIACAyIgbAFIACg2QAAgNgCgGQgDgEgJAAQgJAAgMAHIAAAPIACAxIgcAFIADgzQgBgbgEgXIAagFIACAYIAHgJQAEgDAIgGQAJgFAJAAQAIAAAGAGQAGAFABALQASgWASAAQALAAAGAHQAHAIAAANIgBAXIACAyIgcAFg");
	this.shape_44.setTransform(165.5,426.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_45.setTransform(152.1,426.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#26264B").s().p("AATAAQgBgNgCgGQgCgEgKAAQgIAAgMAHIAAAPIACAxIgcAFIADgzQgBgbgDgXIAZgFIACAYIAHgJQAEgDAHgGQAJgFAKAAQAJAAAHAHQAGAIABANIgCAXIADAyIgdAFg");
	this.shape_46.setTransform(141.7,426.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#26264B").s().p("AgaACQgBgagEgXIAagGIACAfQASgeARAAIgCAiQgMAAgIACQgHACgGAGIAAAIIACAwIgcAGg");
	this.shape_47.setTransform(129.7,426.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#26264B").s().p("AgeAqQgKgNAAgUQAAgZAPgTQAPgTATAAQAPAAAIAIQAJAJAAAPQAAAKgDALIgFAEIgzAFQAEAXARAAQAHAAAHgDQAGgDAEgDIADgDIAKALQgCAEgFAFIgIAIQgEADgIACQgIADgHAAQgRAAgLgNgAgLgZQgHAHAAAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_48.setTransform(120.8,426.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#26264B").s().p("AggBHIAVgxIgPgpIgWgzIAcgFIAOAtIAGAVIADAAIAEgPIAPgzIAaACIgkBhIgRA0g");
	this.shape_49.setTransform(111.3,428.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_50.setTransform(101.4,426.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#26264B").s().p("AgLAeIgDhpIAcgFIgDBrIADAxIgcAFg");
	this.shape_51.setTransform(93.9,423.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#26264B").s().p("AgsBJIADg0IgDhcIAtAAQAUAAAMALQAMALAAAVQAAATgPANQgQAPgUAAIgMAAIACA2gAgSAAIAPAAQAJgBAGgGQAGgIAAgMQAAgXgYAAIgKAAg");
	this.shape_52.setTransform(86.7,424.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#26264B").s().p("AARA7QgQAVgLAAQgQAAgJgMQgKgNAAgTQAAgZAPgSQAOgSAVAAIAMACIAAgEIgDgxIAdgGIgDBrIAAATQAAANAGAQIgWAKQgHgNAAgLgAgQAGQgFAGAAANQAAAOAEAIQAGAHAJAAQAIAAALgHIAAgtQgNgDgFAAQgKAAgFAHg");
	this.shape_53.setTransform(101.9,368.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#26264B").s().p("AgaACQgBgagEgXIAagFIACAeQASgeARABIgCAhQgMAAgIACQgHACgGAHIAAAHIACAwIgcAGg");
	this.shape_54.setTransform(93.4,371.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_55.setTransform(84.3,371.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#26264B").s().p("AggAqQgMgNAAgUQAAgaAPgTQAOgSASAAQAUAAALAMQALANAAAVQAAAagOATQgOASgVAAQgRAAgLgNgAgUAAQAAAPAGAIQAGAIAJAAQAKAAAFgHQAFgIAAgPQAAgdgVAAQgUAAAAAcg");
	this.shape_56.setTransform(74.4,371.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#26264B").s().p("AgvBHIACgzIgChdIAuAAQATAAALAJQAMAKAAAPQAAAZgUAJQAbAJAAAZQAAATgQANQgQANgZAAQgNAAgZgDgAgVARIABAiIATABQAKAAAGgGQAGgFAAgLQAAgLgGgFQgGgFgNAAIgRAAgAgUgKIANAAQAKAAAFgGQAGgGAAgLQAAgSgXAAIgKAAg");
	this.shape_57.setTransform(63.6,369.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#26264B").s().p("AAQA7QgPAVgLAAQgQAAgJgMQgKgNAAgTQAAgZAPgSQAOgSAVAAIALACIAAgEIgCgxIAcgGIgCBrIAAATQAAANAGAQIgWAKQgHgNgBgLgAgQAGQgFAGAAANQAAAOAEAIQAGAHAJAAQAIAAAKgHIAAgtQgMgDgGAAQgJAAgFAHg");
	this.shape_58.setTransform(48.4,368.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_59.setTransform(38.3,371.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAHAAAHgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgFADgHACQgIADgGAAQgSAAgKgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_60.setTransform(28.8,371.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#26264B").s().p("AgjBJIADgzIgDhYIAegGIgDBaIABAiIAqgDIgBAYg");
	this.shape_61.setTransform(20.4,369.2);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#26264B").s().p("AAAAUIghA3IgXgMIApg/Igsg3IAagSIAhA1IAgg2IAWANIgmA8IAsA7IgaARg");
	this.shape_62.setTransform(339.8,112.3);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#26264B").s().p("AgVAzQgJgFgEgEIgDgFIAQgTIADAEIAKAHQAHAEAFAAQALAAAAgJQAAgEgFgDIgKgGIgOgGQgHgEgFgFQgEgHAAgIQAAgOAMgLQAMgKAPAAIAQABIANAGIgNAaIgFgEIgIgFQgGgDgGAAQgDAAgDADQgDACAAAEQAAAEAIAEIAPAHQAIADAHAGQAHAIAAAKQABAPgMALQgNALgOAAQgLAAgIgEg");
	this.shape_63.setTransform(84.3,114.3);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#26264B").s().p("AgMA5QgGgIAAgLIABgZIAAggIgPAAIACgQIANAAIAAgYIAbgFIgDAdIAaAAIgDAQIgYAAIgBArQAAALACADQACADAFAAQAFAAAJgCIABAPQgOALgKAAQgKAAgHgIg");
	this.shape_64.setTransform(76.7,113.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#26264B").s().p("AgWAzQgIgFgEgEIgEgFIARgTIAEAEIAJAHQAHAEAFAAQALAAAAgJQAAgEgFgDIgKgGIgOgGQgHgEgEgFQgGgHAAgIQAAgOANgLQANgKAPAAIAOABIAOAGIgOAaIgDgEIgJgFQgHgDgEAAQgEAAgDADQgDACAAAEQAAAEAHAEIAPAHQAJADAHAGQAIAIAAAKQgBAPgMALQgLALgQAAQgJAAgKgEg");
	this.shape_65.setTransform(68.6,114.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAGAAAIgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgEADgIACQgIADgGAAQgSAAgKgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_66.setTransform(59.9,114.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#26264B").s().p("AgMA5QgGgIAAgLIABgZIAAggIgPAAIACgQIANAAIAAgYIAbgFIgDAdIAaAAIgDAQIgYAAIgBArQAAALACADQACADAFAAQAFAAAJgCIABAPQgOALgKAAQgKAAgHgIg");
	this.shape_67.setTransform(51.7,113.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#26264B").s().p("AASAAQAAgOgCgEQgCgGgKAAQgIAAgMAIIAAAQIACAwIgcAFIADgzQgBgbgEgWIAagGIACAYIAHgIQAEgFAHgEQAJgGAKAAQAJAAAHAIQAGAHABANIgCAYIADAxIgdAFg");
	this.shape_68.setTransform(42.3,114.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#26264B").s().p("AggAqQgMgNAAgUQAAgaAPgTQAOgSASAAQAUAAALAMQALANAAAVQAAAagOATQgOASgVAAQgRAAgLgNgAgUAAQAAAPAGAIQAGAIAJAAQAKAAAFgHQAFgIAAgPQAAgdgVAAQgUAAAAAcg");
	this.shape_69.setTransform(31.8,114.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#26264B").s().p("AglA3QgQgTAAgdQAAgiATgXQATgYAbAAQALAAAJACQAJACAEADIAEACIgOAdQgFgEgJgEQgIgDgIAAQgNAAgJAMQgJAMAAAWQAAAVAKAOQAJAOAOAAQAKAAAIgGQAJgGADgLIARAKQgHATgOAKQgOALgSAAQgXAAgPgUg");
	this.shape_70.setTransform(21.2,112.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#26264B").ss(1,1,1).p("AbtsjMg3ZAAAAbNMkMg2qAAA");
	this.shape_71.setTransform(182.1,475.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.text_2},{t:this.text_1},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.text},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.pop1x_2},{t:this.pop1x_1},{t:this.pop1x},{t:this.BTNclose},{t:this.shape_1},{t:this.shape},{t:this.instance},{t:this._Cname1},{t:this._Ctime1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_CONTESTS, new cjs.Rectangle(3.8,95.2,372.8,533.1), null);


(lib.MC_ADDRESS = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.BTNplay = new lib.transparent();
	this.BTNplay.name = "BTNplay";
	this.BTNplay.parent = this;
	this.BTNplay.setTransform(204.1,582.1,3.553,1.318,0,0,0,0.4,0.1);

	this.timeline.addTween(cjs.Tween.get(this.BTNplay).wait(1));

	// Layer_1
	this.instance = new lib.Bitmap18();
	this.instance.parent = this;
	this.instance.setTransform(94,64,0.719,0.719);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26264B").s().p("AgtAvIAygvIg1gtIAUgWIBNBDIhMBEg");
	this.shape.setTransform(335.5,598.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("AglgHIBIgNIADAcIhIANg");
	this.shape_1.setTransform(324.6,598.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AgOBFQgIgKAAgPIABgcIAAgmIgRAAIADgVIAOAAIAAgbIAhgGIgDAhIAeAAIgCAVIgeAAIgBAzQABAMACAEQACAEAGAAQAGAAAKgDIABARQgQAOgNABQgLgBgIgIg");
	this.shape_2.setTransform(312.3,599.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("AggADQgBgggEgbIAegGQADASAAASQAWgjAUAAIgDAmQgOABgJACQgIACgIAJIAAAJIADA5IghAHg");
	this.shape_3.setTransform(303.8,600.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("AAQArQgNAVgVAAQgOAAgJgJQgKgKAAgPQAAgQAOgMQAPgKAVAAIAQAAIAAgFQAAgMgGgEQgEgFgNAAQgGAAgHACQgJADgJAFIgIgUQALgHAPgHQAQgGAJAAQAnAAAAAnIAAAwQAAAOAJARIgbALQgFgMgEgKgAgVAXQAAAHADAEQAFAFAGAAQAJAAANgLIAAgQQgOgDgGAAQgRAAABAOg");
	this.shape_4.setTransform(292.9,600.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("AgOBFQgIgKAAgPIACgcIAAgmIgSAAIADgVIAPAAIAAgbIAggGIgDAhIAeAAIgDAVIgcAAIgBAzQgBAMADAEQACAEAGAAQAGAAAKgDIABARQgQAOgMABQgMgBgIgIg");
	this.shape_5.setTransform(282.8,599.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("AglBQQgNgIgFgGIgCgCIATgcQAWAWAUgBQALAAAHgEQAGgGAAgIQAAgIgIgFQgIgGgMgFIgVgJQgLgGgIgIQgIgLAAgOQAAgWASgQQAUgRAYAAQAMAAALACQALADAKAFIgRAkQgHgFgMgFQgMgFgKAAQgHAAgGAFQgGAEAAAHQAAAIAIAGQAIAFAKAFIAYAJQALAFAIAJQAIAKAAAOQAAAXgTASQgSASgYAAQgUAAgOgJg");
	this.shape_6.setTransform(271.7,598.3);

	this._full_name = new cjs.Text("Username", "18px 'Acme'", "#26264B");
	this._full_name.name = "_full_name";
	this._full_name.textAlign = "center";
	this._full_name.lineHeight = 25;
	this._full_name.lineWidth = 188;
	this._full_name.parent = this;
	this._full_name.setTransform(182,226.5);

	this.text = new cjs.Text("sdafagkag'kjasg'kasjdg", "16px 'Acme'", "#26264B");
	this.text.lineHeight = 22;
	this.text.lineWidth = 290;
	this.text.parent = this;
	this.text.setTransform(35.9,488.2);

	this.text_1 = new cjs.Text("your wallet address", "16px 'Acme'", "#26264B");
	this.text_1.lineHeight = 22;
	this.text_1.lineWidth = 290;
	this.text_1.parent = this;
	this.text_1.setTransform(35.9,434.6);

	this._mob_no = new cjs.Text("456-456-4567", "16px 'Acme'", "#26264B");
	this._mob_no.name = "_mob_no";
	this._mob_no.lineHeight = 22;
	this._mob_no.lineWidth = 290;
	this._mob_no.parent = this;
	this._mob_no.setTransform(35.9,378.6);

	this._email_id = new cjs.Text("tassaduq009@Gmail.com", "16px 'Acme'", "#26264B");
	this._email_id.name = "_email_id";
	this._email_id.lineHeight = 22;
	this._email_id.lineWidth = 290;
	this._email_id.parent = this;
	this._email_id.setTransform(35.9,329);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AghBHIAWgxIgQgpIgUgzIAcgFIANAsIAGAWIACAAIAFgPIAQg0IAaAEIgmBhIgRAzg");
	this.shape_7.setTransform(109.1,480.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAGAAAIgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgEADgIACQgIADgGAAQgSAAgKgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_8.setTransform(99.5,478.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("AgYACIgBAPIACA3IgcAAIACgzIgChZIAdgFIgCBEIA1hEIAVAPIgwA4IAyA7IgaARg");
	this.shape_9.setTransform(89.7,476.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#26264B").s().p("AASAcQAAgPgDgFQgCgFgJAAQgJAAgMAIIAAAPIADAyIgdAFIADgzIgDhqIAdgFIgDA3IAAAYIAIgIQAEgEAHgFQAIgFAKAAQAJAAAHAHQAGAIAAAMIgBAXIADAzIgdAFg");
	this.shape_10.setTransform(74,475.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#26264B").s().p("AgWAzQgIgFgEgEIgDgFIAQgTIAEAEIAJAHQAGAEAGAAQALAAAAgJQAAgEgFgDIgKgGIgOgGQgHgEgEgFQgFgHgBgIQAAgOANgLQANgKAPAAIAPABIANAGIgNAaIgFgEIgIgFQgHgDgEAAQgEAAgDADQgDACAAAEQAAAEAIAEIAOAHQAJADAHAGQAHAIABAKQgBAPgLALQgMALgQAAQgJAAgKgEg");
	this.shape_11.setTransform(64.2,478.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_12.setTransform(55.2,478.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#26264B").s().p("AAZBJIAEgzIgBgLIg4AAIAAAIIACA2IgdAAIADgzIgDhZIAegFIgCA9IA2AAIgCg4IAfgFIgEBbIADA2g");
	this.shape_13.setTransform(43.1,476.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#26264B").s().p("AgVAzQgJgFgEgEIgDgFIAQgTIADAEIAKAHQAHAEAFAAQALAAAAgJQAAgEgFgDIgKgGIgOgGQgHgEgFgFQgEgHAAgIQAAgOAMgLQAMgKAPAAIAQABIANAGIgNAaIgFgEIgIgFQgGgDgGAAQgDAAgDADQgDACAAAEQAAAEAIAEIAPAHQAIADAHAGQAHAIAAAKQABAPgMALQgNALgOAAQgLAAgIgEg");
	this.shape_14.setTransform(159.5,424.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#26264B").s().p("AgWAzQgIgFgEgEIgEgFIARgTIADAEIAKAHQAGAEAGAAQALAAAAgJQAAgEgFgDIgLgGIgNgGQgHgEgEgFQgGgHABgIQgBgOANgLQAMgKAQAAIAOABIAPAGIgPAaIgDgEIgJgFQgGgDgFAAQgFAAgCADQgDACAAAEQAAAEAHAEIAQAHQAIADAHAGQAIAIAAAKQAAAPgNALQgMALgPAAQgKAAgJgEg");
	this.shape_15.setTransform(151.2,424.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#26264B").s().p("AgeAqQgKgNAAgUQAAgZAPgTQAPgTATAAQAPAAAIAIQAJAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAHAAAHgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgFADgHACQgIADgGAAQgSAAgLgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_16.setTransform(142.5,424.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#26264B").s().p("AgaACQgBgbgEgWIAagGIACAgQASgfARAAIgCAiQgMgBgIACQgHADgGAGIAAAIIACAxIgcAEg");
	this.shape_17.setTransform(134.5,424.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#26264B").s().p("AAQA7QgPAVgMAAQgPAAgJgMQgKgNAAgSQAAgaAPgSQAOgSAVAAIALACIAAgEIgCgyIAcgEIgCBrIAAASQAAAMAGARIgWAKQgHgNgBgLgAgQAFQgFAHAAAOQAAANAEAIQAGAHAJAAQAIAAAKgHIAAgtQgMgCgFAAQgKAAgFAFg");
	this.shape_18.setTransform(124.9,421.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#26264B").s().p("AARA7QgQAVgLAAQgQAAgJgMQgKgNAAgSQAAgaAPgSQAOgSAVAAIAMACIAAgEIgDgyIAdgEIgDBrIAAASQAAAMAGARIgWAKQgHgNAAgLgAgQAFQgFAHAAAOQAAANAEAIQAGAHAJAAQAIAAALgHIAAgtQgNgCgFAAQgKAAgFAFg");
	this.shape_19.setTransform(114.4,421.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#26264B").s().p("AAYAhIgyAAIgLAnIgbgBIATg0IAchZIAlgCIAbBYIASA1IgfAEgAgTAKIAkAAIgQg3IgCAAg");
	this.shape_20.setTransform(102.5,422.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#26264B").s().p("AgMA6QgGgJAAgLIABgYIAAghIgPAAIACgRIANAAIAAgWIAbgGIgDAcIAaAAIgDARIgYAAIgBArQAAAKACAEQACADAFAAQAFAAAJgCIABAOQgOAMgKAAQgKAAgHgHg");
	this.shape_21.setTransform(88.9,422.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgFAEIgzAFQAEAXASAAQAGAAAGgDQAHgDAEgDIAEgDIAJALQgCAEgFAFIgIAIQgFADgHACQgHADgIAAQgQAAgLgNgAgLgZQgGAHgCAMIAjgFIABgIQAAgOgNAAQgJAAgGAIg");
	this.shape_22.setTransform(80.4,424.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#26264B").s().p("AgLAeIgChpIAcgFIgEBrIADAxIgbAFg");
	this.shape_23.setTransform(73.1,421.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#26264B").s().p("AgLAeIgChpIAcgFIgDBrIACAxIgbAFg");
	this.shape_24.setTransform(67.8,421.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_25.setTransform(60.1,424);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#26264B").s().p("AAGARIgGgaIgGAXIgOA5IgmACIgMg4IgXhVIAcgEIARBQIAJAmIACAAIAMgmIAPg4IgEgUIAagEIARBQIAJAmIACAAIAKgkIAUhSIAXADIgWBTIgOA5IgmACg");
	this.shape_26.setTransform(45.6,422.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#26264B").s().p("AgdAqQgLgNAAgUQAAgZAPgTQAPgTATAAQAPAAAJAIQAIAJAAAPQAAAKgDALIgEAEIg0AFQAEAXARAAQAGAAAIgDQAGgDAEgDIADgDIAKALQgCAEgEAFIgJAIQgEADgIACQgIADgGAAQgSAAgKgNgAgLgZQgGAHgBAMIAjgFIAAgIQAAgOgOAAQgIAAgGAIg");
	this.shape_27.setTransform(83.3,370.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#26264B").s().p("AATAAQgBgOgCgEQgCgGgKAAQgIABgLAHIAAAQIABAwIgcAFIADgzQgBgbgDgXIAZgFIACAYIAHgIQAEgFAHgEQAJgGAKAAQAJAAAHAIQAHAHAAANIgCAYIADAxIgdAFg");
	this.shape_28.setTransform(73.1,370.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#26264B").s().p("AggAqQgMgNAAgUQAAgaAPgTQAOgSASAAQAUAAALAMQALANAAAVQAAAagOATQgOASgVAAQgRAAgLgNgAgUAAQAAAPAGAIQAGAIAJAAQAKAAAFgHQAFgIAAgPQAAgdgVAAQgUAAAAAcg");
	this.shape_29.setTransform(62.6,370.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#26264B").s().p("AASAcQAAgPgDgFQgCgFgJAAQgJAAgMAIIAAAPIADAyIgdAFIADgzIgDhqIAdgFIgDA3IAAAYIAIgIQAEgEAHgFQAIgFAKAAQAJAAAHAHQAGAIAAAMIgBAXIADAzIgdAFg");
	this.shape_30.setTransform(52.1,367.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#26264B").s().p("AgsBIIADgyIgDheIAtAAQAUAAAMAMQAMALAAAVQAAATgPAOQgQAOgUAAIgMAAIACA1gAgSgBIAPAAQAJABAGgIQAGgGAAgNQAAgXgYAAIgKAAg");
	this.shape_31.setTransform(42,368.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#26264B").s().p("AgLAeIgDhpIAcgFIgCBrIACAxIgcAFg");
	this.shape_32.setTransform(80.7,319);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#26264B").s().p("AgMAbIgCgxIAbgGIgCA0IACAxIgbAFgAgMgvQgDgFAAgGQgBgIAGgFQAGgGAGAAQAHAAAEAEQADAEAAAHQABAJgGAFQgFAGgHgBQgHABgEgFg");
	this.shape_33.setTransform(75.6,319.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#26264B").s().p("AAOAkQgMASgRAAQgMAAgIgIQgIgIAAgNQAAgOAMgJQANgJARAAIANAAIAAgEQAAgKgEgEQgEgEgLAAQgEAAgHACQgHACgIAFIgGgRQAJgGANgFQANgFAHAAQAhAAAAAgIAAAoQAAALAIAPIgXAJQgFgKgCgIgAgSATQAAAGADAEQAEADAFAAQAIAAAKgIIAAgOQgLgDgGAAQgNAAAAAMg");
	this.shape_34.setTransform(67.9,321.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#26264B").s().p("AAwAAQAAgOgCgEQgDgFgJgBQgJABgLAGIgBARIACAxIgbAFIACg2QAAgOgCgEQgDgFgJgBQgJAAgMAIIAAAQIACAwIgcAFIADgzQgBgbgEgWIAagGIACAYIAHgIQAEgFAIgEQAJgGAJAAQAIAAAGAGQAGAGABAKQASgWASAAQALAAAGAIQAHAHAAANIgBAYIACAxIgcAFg");
	this.shape_35.setTransform(54.6,321.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#26264B").s().p("AgnBIIADgyIgDheIBOAAIgCAXIgvAAIgBAmIApAAIgCAUIgoAAIAAAJIABAgIAzAAIgDAWg");
	this.shape_36.setTransform(41.5,319.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#26264B").ss(1,1,1).p("A3JAAMAuSAAA");
	this.shape_37.setTransform(186,272.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this._email_id},{t:this._mob_no},{t:this.text_1},{t:this.text},{t:this._full_name},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_ADDRESS, new cjs.Rectangle(33.9,64,325,570.9), null);


(lib.dirt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(59,41,6,0.506)").s().p("Ag/BrIgHABQgwAAgigWQgjgVAAgeQAAgPALgPQgUgRAAgWQAAgeAigVQAigWAxAAIAXACQAdgNAjAAQAhAAAcALIACAAQAvAAAhAUQAiAUADAdIAAAEQAAAPgLAPQAUARAAAWIAAAEQgCAdgiAUQgiAUgvAAIgSgBQgdAMgiAAQggAAgegMg");
	this.shape.setTransform(77.9,28.6,0.373,0.373);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(1.3,0,0,4).p("AAIh2QAhAAAcALIACAAQAvAAAhAUQAiAUADAeIAAADQAAAPgLAPQAUARAAAWIAAADQgCAegiAUQgiAUgvAAIgSgBQgeALghAAQghAAgdgKIgHAAQgwAAgigVQgjgWAAgeQAAgPALgPQgUgRAAgWQAAgeAigVQAigWAxAAIAXACQAdgNAjAAg");
	this.shape_1.setTransform(76.8,27.7,0.373,0.373);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#734B02").s().p("Ag/BrIgHAAQgwABgigWQgjgVAAgeQAAgPALgPQgUgRAAgWQAAgeAigVQAigVAxAAIAXABQAdgNAjAAQAhAAAcAMIACAAQAvgBAhAVQAiATADAdIAAAEQAAAPgLAPQAUARAAAWIAAAEQgCAdgiATQgiAVgvgBIgSAAQgeAMghAAQghAAgdgMg");
	this.shape_2.setTransform(76.8,27.7,0.373,0.373);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(59,41,6,0.506)").s().p("AgoBGIgFAAQgfAAgWgOQgXgOAAgTQAAgKAHgKQgNgKAAgPQAAgTAXgOQAWgOAfAAIAPABQAUgIAVAAQAWAAASAHIABAAQAfAAAVANQAWANACATIAAACQAAAKgHAKQANAKAAAPIAAACQgCATgWANQgVANgfAAIgMgBQgRAIgXAAQgWAAgSgHg");
	this.shape_3.setTransform(31,20.5,0.373,0.373);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(0.8,0,0,4).p("AAFhMQAWAAASAHIABAAQAeAAAWANQAWANACATIAAACQAAAKgHAJQANALAAAPIAAACQgCATgVANQgWANgeAAIgNgBQgRAIgXAAQgWAAgSgHIgEAAQggAAgWgOQgXgOAAgTQAAgKAHgKQgNgLAAgOQAAgTAXgOQAWgOAfAAIAPABQARgIAYAAg");
	this.shape_4.setTransform(30.3,20,0.373,0.373);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#734B02").s().p("AgoBGIgEAAQggAAgWgOQgWgOgBgTQABgKAGgKQgNgLAAgOQAAgTAXgOQAWgOAfAAIAPABQARgIAYAAQAWAAASAHIABAAQAfAAAWANQAVANACATIAAACQAAAKgGAJQAMALAAAPIAAACQgCATgWANQgVANgeAAIgNgBQgRAIgXAAQgWAAgSgHg");
	this.shape_5.setTransform(30.3,20,0.373,0.373);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(59,41,6,0.506)").s().p("AgoBGIgFAAQgfAAgWgOQgXgOABgTQAAgKAGgKQgNgKAAgPQAAgTAXgOQAWgOAfAAIAPABQATgIAWAAQAWAAASAHIABAAQAfAAAVANQAWANACATIAAACQAAAKgHAKQANAKAAAPIAAACQgBATgWANQgWANgfAAIgLgBQgSAIgXAAQgWAAgSgHg");
	this.shape_6.setTransform(102.4,31.8,0.373,0.373);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(0.8,0,0,4).p("AAFhMQAWAAASAHIABAAQAeAAAXANQAWANABATIAAACQAAALgHAIQANALAAAPIAAACQgCATgWANQgVANgfAAIgLgBQgSAIgXAAQgWAAgSgHIgFAAQgfAAgWgOQgWgOAAgTQAAgLAGgJQgNgLAAgOQAAgTAWgOQAWgOAgAAIAPABQASgIAXAAg");
	this.shape_7.setTransform(101.7,31.2,0.373,0.373);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#734B02").s().p("AgoBGIgFAAQgfAAgXgOQgVgOAAgTQAAgLAGgJQgNgLAAgOQAAgTAXgOQAWgOAfAAIAPABQASgIAXAAQAWAAASAHIABAAQAfAAAVANQAXANABATIAAACQAAALgHAIQANALAAAPIAAACQgCATgVANQgXANgeAAIgLgBQgTAIgWAAQgWAAgSgHg");
	this.shape_8.setTransform(101.7,31.2,0.373,0.373);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(59,41,6,0.506)").s().p("AgoBvIgFAAQgfAAgWgWQgWgWAAgfQAAgQAHgOQgNgSAAgXQAAgfAWgWQAWgWAfAAIAPABQAUgNAVAAQAVAAATAMIABAAQAeAAAWAVQAVAUACAeIAAAEQAAAQgHAOQANASAAAXIAAAEQgCAegVAVQgWAUgeAAIgMAAQgTAMgVAAQgXAAgRgMg");
	this.shape_9.setTransform(41,37.3,0.373,0.373);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(1.1,0,0,4).p("AAFh6QAVAAATAMIABAAQAeAAAWAVQAVAUACAeIAAAEQAAAQgHAOQANASAAAXIAAAEQgCAegVAVQgWAUgeAAIgMAAQgTAMgVAAQgXAAgRgMIgFAAQgfAAgWgWQgWgWAAgfQAAgQAHgOQgNgSAAgXQAAgfAWgWQAWgWAfAAQAIAAAHABQAUgNAVAAg");
	this.shape_10.setTransform(40.3,36.4,0.373,0.373);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#734B02").s().p("AgoBvIgFAAQgfAAgWgWQgWgWAAgfQAAgQAHgOQgNgSAAgXQAAgfAWgWQAWgWAfAAIAPABQAUgNAVAAQAVAAATAMIABAAQAeAAAWAVQAVAUACAeIAAAEQAAAQgHAOQANASAAAXIAAAEQgCAegVAVQgWAUgeAAIgMAAQgTAMgVAAQgXAAgRgMg");
	this.shape_11.setTransform(40.3,36.4,0.373,0.373);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3B2906").s().p("AgoBvIgFAAQgfABgVgXQgXgWAAgfQABgRAGgNQgNgTAAgWQAAgfAWgXQAWgWAgABQAHAAAHABQATgNAWAAQAVAAASAMIACAAQAegBAVAVQAWAVACAeIAAAEQAAAPgHAPQANATAAAWIAAAEQgCAegVAVQgWAUgeAAIgMgBQgSANgWAAQgWAAgSgMg");
	this.shape_12.setTransform(12.3,35.4,0.373,0.373);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(1.1,0,0,4).p("AAFh6QAVAAASAMIABAAQAfAAAWAVQAVAUACAeIAAAEQAAAPgHAQQANASAAAWIAAAEQgCAegVAVQgWAVgeAAIgMgBQgTAMgVAAQgXAAgRgMIgFABQgfAAgWgWQgWgXAAgfQAAgRAHgNQgNgTAAgWQAAgfAWgWQAWgWAfAAQAIAAAHABQATgNAWAAg");
	this.shape_13.setTransform(11.5,34.5,0.373,0.373);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#734B02").s().p("AgoBvIgFABQgeAAgXgXQgVgWgBgfQABgRAGgNQgNgTAAgWQAAgfAWgXQAWgVAggBQAHABAHABQATgMAWAAQAWgBARALIABAAQAfABAVAUQAXAVABAeIAAAEQAAAPgHAQQANARAAAXIAAAEQgBAegWAVQgWAVgeAAIgMgBQgTAMgVgBQgWAAgSgLg");
	this.shape_14.setTransform(11.5,34.5,0.373,0.373);

	this.instance = new lib.Shape_10();
	this.instance.parent = this;
	this.instance.setTransform(114.3,23.9,0.373,0.373,0,0,0,13.6,13);
	this.instance.alpha = 0.801;

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(1.1,0,0,4).p("AAGiBQAWAAAUAMIABAAQAgAAAXAWQAXAWABAgIAAAEQAAARgHAPQAOATAAAYIAAAFQgCAfgXAWQgXAWggAAIgMgBQgVANgWAAQgXAAgTgMIgFAAQghAAgXgXQgYgYAAghQAAgRAHgPQgNgTAAgYQAAghAXgYQAXgXAiAAQAJAAAGACQAUgOAYAAg");
	this.shape_15.setTransform(112.9,22.3,0.373,0.373);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#734B02").s().p("AgrB2IgEAAQgiAAgWgXQgYgYAAghQAAgRAHgPQgOgTAAgYQABghAXgYQAXgXAhAAQAKAAAGACQAUgOAYAAQAVAAAVAMIABAAQAfAAAYAWQAWAWACAgIAAAEQAAARgHAPQANATAAAYIAAAFQgBAfgXAWQgXAWggAAIgNgBQgUANgWAAQgXAAgUgMg");
	this.shape_16.setTransform(112.9,22.3,0.373,0.373);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#3B2906").s().p("Ag+CsIgHAAQgwAAgigiQgigiAAgwQAAgZAKgXQgUgcAAgjQAAgwAigiQAigiAwAAQANAAAKACQAegTAiAAQAhAAAcARIABAAQAvAAAhAgQAiAgACAuIAAAGQAAAZgKAWQAUAcAAAkIAAAGQgCAugiAgQgiAgguAAIgSgCQgdATgiAAQghAAgcgRg");
	this.shape_17.setTransform(132.6,33.5,0.373,0.373);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(1.6,0,0,4).p("AAIi8QAhAAAcARIACAAQAuAAAiAgQAhAgADAuIAAAGQAAAYgLAXQAUAcAAAkIAAAGQgCAugiAgQghAggvAAIgSgCQgdATghAAQgiAAgcgRIgHAAQgwAAgigiQgigiAAgwQAAgYAKgYQgUgcAAgjQAAgwAjgiQAigiAwAAQANAAAJACQAdgTAjAAg");
	this.shape_18.setTransform(131.5,32.2,0.373,0.373);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#734B02").s().p("Ag+CsIgHAAQgwAAgigiQgigiAAgwQAAgYAKgYQgUgcAAgjQAAgwAjgiQAigiAwAAQANAAAJACQAdgTAjAAQAhAAAcARIACAAQAuAAAiAgQAhAgADAuIAAAGQAAAYgLAXQAUAcAAAkIAAAGQgCAugiAgQghAggvAAIgSgCQgdATghAAQgiAAgcgRg");
	this.shape_19.setTransform(131.5,32.2,0.373,0.373);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#3B2906").s().p("AgoBvIgEAAQggAAgWgVQgWgXAAgfQAAgPAGgQQgMgRAAgXQAAgfAWgWQAWgXAfAAQAJAAAGACQATgNAWAAQAVABATAKIABAAQAdAAAXAWQAVAUABAeIAAAEQAAARgGANQANATAAAWIAAAEQgBAegXAVQgVAVgfgBIgLgBQgTAMgVAAQgXABgRgMg");
	this.shape_20.setTransform(91.7,48,0.373,0.373);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(1.1,0,0,4).p("AAFh6QAWAAARALIACAAQAeAAAWAVQAVAVACAeIAAAEQAAARgHANQANATAAAWIAAAEQgBAegXAVQgVAVgeAAIgMgCQgTANgVAAQgWAAgSgMIgFABQgfAAgWgXQgWgWAAgfQAAgPAHgQQgNgRAAgXQAAgfAWgXQAWgWAfAAQAJAAAGACQATgNAWAAg");
	this.shape_21.setTransform(91,47.1,0.373,0.373);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#734B02").s().p("AgoBvIgEABQggAAgWgXQgWgWAAgfQAAgPAHgPQgNgSAAgXQAAgfAWgXQAWgVAgAAQAHgBAHACQATgMAWAAQAVAAATALIABAAQAeAAAVAUQAWAVABAeIAAAEQAAARgGAOQANASAAAWIAAAEQgBAegXAVQgVAUgfABIgLgCQgTAMgVABQgXAAgRgMg");
	this.shape_22.setTransform(91,47.1,0.373,0.373);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#3B2906").s().p("AgoBvIgFABQgeAAgXgWQgWgXAAgfQAAgPAHgPQgNgSAAgXQAAgfAWgWQAWgWAgAAQAHAAAHABQAUgNAVAAQAVAAASAMIABAAQAfAAAVAVQAWAUACAeIAAAEQgBARgGAOQANASAAAWIAAAEQgCAegVAVQgXAVgeAAIgLgBQgTAMgVAAQgWAAgSgMg");
	this.shape_23.setTransform(60.7,48.2,0.373,0.373);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(1.1,0,0,4).p("AAFh6QAVAAATAMIABAAQAeAAAWAVQAVAUACAeIAAAEQAAARgHAOQANASAAAWIAAAEQgBAegWAVQgWAVgeAAIgMgBQgTAMgVAAQgWAAgSgMIgEABQgfAAgXgWQgWgXAAgfQAAgPAHgPQgNgSAAgXQAAgfAWgWQAWgWAgAAQAIAAAGABQAUgNAVAAg");
	this.shape_24.setTransform(60,47.3,0.373,0.373);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#734B02").s().p("AgoBvIgFABQgfAAgWgWQgWgXAAgfQAAgPAHgPQgNgSAAgXQAAgfAWgWQAWgWAgAAQAHAAAHABQAUgNAVAAQAVAAASAMIABAAQAfAAAVAVQAWAUACAeIAAAEQAAARgHAOQANASAAAWIAAAEQgCAegVAVQgWAVgeAAIgMgBQgTAMgVAAQgWAAgSgMg");
	this.shape_25.setTransform(60,47.3,0.373,0.373);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.lf(["#563B09","#291D04"],[0,1],-101.4,0,101.5,0).s().p("Av2KtQBTgRA0g8QBNAbBbgBQCBABBig1QBhg0AShOQASACAYAAQB3AABTg/QBUg+AAhZQAAgjgPghIAMgOQBcgBBBg+QBCg+AAhYQAAgtgUgpQgSgogigeIAAgBQAAhMgahNQAMghAAgkQAAhRg6g+Qg4g8hVgJQgrgogogdQAWgFATgBQApABAlARQAugiA7AAQBFAAAxArQAwArAAA9IgBAIQBbAKBGAlQBGAmAfA0QAUgLAXABQAdAAAZARQAZAQAOAdIAIAAQBEAAAxArQAwArAAA8QAAAUgFAQIAYgCQBFAAAxAsQAwAtAAA+QAAASgGAWQAeAVARAeQASAgAAAjQAAAogUAgQADAOAAAPQAAA/gxAsQgwAshFAAQggAAgegLQgVAlgnAXQgoAWgvAAQgvAAgogVQgMAhgaAVQgbAWghgBQgkAAgcgZQgPAFgSAAIgNgBQgMAkgbAVQgbAXgiAAQgqAAgfghQgoAVguAAQgNAAgQgDQgMAigaATQgaAVggAAQgdAAgZgSQgZgRgOgdQgSAGgXAAQgNgBgPgCIgMAKQgTAqgpAZQgqAbgzAAQgkgBgigPIgXACQgsA2hRAfQhVAhhjAAQiEAAhhg1gAHdiaQgPgQgJgUIAAADQAAAQgEAOIAcADIAAAAg");
	this.shape_26.setTransform(100.8,27.6,0.373,0.373);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#593D09","#7F5200"],[0,1],-185.5,0,185.6,0).s().p("AhYLPQhCgSgwghQgRAHgTAAQghAAgbgWQgbgUgMgjIgDAAQgfAjgrAAQgoAAgegeQgegegGgtQgTgNgPgSQgPAYgWAOQgYAOgaAAQgdAAgZgRQgZgSgOgcQhogFhTgnQhRgoghg8QgPAWgWAMQgWANgZABQgpgBgegfQghANggAAQgnAAglgRQgMACgLAAQgkAAgdgaQgcgZgKgoQgpAcgxAAQgiAAgggOQgcgFgWgUQgWgVgLgfIgCgCQgXAOgbgBQgsAAgggkQgfglAAg0QAAg0AfgkQAggkAsAAQAXAAAUALQAYgyAqgcQAqgeAyAAQAyAAAqAeQAWg6AtgkQAuglA3AAQAlABAiAQQAagkAjgTQAlgVApABQA3gBAtAkQAsAhAWA5QAVg7AtgmQAvglA4AAQAyAAArAdQAqAdAYAyQAqgIArAAIAcABQAZgjAkgUQAlgVApABQAdAAAZAJQAGhSA0g4QA0g4BHgBQAzAAAsAhQAugpA8AAQA9AAAyAtQAsAIAkAfQAkAeATAuQAVgKAXgJQAXgfAngRQATgMAWgGQgGgTAAgSQAAg3AqgqQgIgWAAgXQAAg+AwgrQAxgrBFAAQApAAAlASQAvgiA7AAQBFAAAwArQAxArAAA9IgBAIQBbAKBGAlQBFAmAfA0QAVgLAXABQAdgBAZASQAZAQAOAdIAIAAQBEAAAxArQAwArAAA9QAAAQgFATIAYgCQBEAAAxAsQAxAsAAA/QAAAWgGASQAeAVARAeQASAgAAAjQAAAmgUAiQADAOAAAPQAAA+gxAtQgwAshFAAQggAAgegLQgVAlgnAXQgoAWgwAAQguAAgogVQgMAigbAUQgbAWgggBQgjAAgdgZQgQAFgRAAIgNgBQgMAjgbAWQgbAXgiAAQgqAAgfghQgoAVguAAIgdgCQgNAhgZAUQgbAUggAAQgdAAgZgSQgZgQgOgdQgWAFgTAAIgcgDIgMAKQgTAqgpAZQgqAagzABQglgBghgPIgYACQgrA2hSAgQhUAghkAAQhKAAhEgTgAPMiKQgFgKgCgKQgVAAgQgCIgBADQAbAKASAJIAAAAgAUmiaQgPgQgKgUIAAADQAAARgDANIAcADIAAAAg");
	this.shape_27.setTransform(69.2,27.5,0.373,0.373);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.instance},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AqyJkIAAzHIVlAAIAATHg");
	mask.setTransform(70.9,-39.6);

	// Layer_2
	this.mole = new lib.mcmole();
	this.mole.name = "mole";
	this.mole.parent = this;
	this.mole.setTransform(68.9,90.5,0.722,0.723,0,0,0,45.4,71.5);
	new cjs.ButtonHelper(this.mole, 0, 1, 1);

	var maskedShapeInstanceList = [this.mole];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.mole).wait(1));

}).prototype = getMCSymbolPrototype(lib.dirt, new cjs.Rectangle(0,0,140,55.1), null);


(lib.dashboard = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.BTNusername = new lib.transparent();
	this.BTNusername.name = "BTNusername";
	this.BTNusername.parent = this;
	this.BTNusername.setTransform(82.8,7.2,2.964,0.797,0,0,0,0.6,0.2);

	this.BTNwallet = new lib.transparent();
	this.BTNwallet.name = "BTNwallet";
	this.BTNwallet.parent = this;
	this.BTNwallet.setTransform(274.8,39.2,1.901,0.797,0,0,0,0.5,0.2);

	this.BTNcontest = new lib.transparent();
	this.BTNcontest.name = "BTNcontest";
	this.BTNcontest.parent = this;
	this.BTNcontest.setTransform(274.8,3.5,1.901,0.797,0,0,0,0.5,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.BTNcontest},{t:this.BTNwallet},{t:this.BTNusername}]}).wait(1));

	// Layer_3
	this._CASH = new cjs.Text("300", "21px 'Acme'", "#040202");
	this._CASH.name = "_CASH";
	this._CASH.lineHeight = 29;
	this._CASH.lineWidth = 44;
	this._CASH.parent = this;
	this._CASH.setTransform(315.7,36.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.988)").s().p("AgoA0QgNgRABgcQAAghAQgVQAQgVAZgBQAWAAANASQANARABAcQgBAigPAVQgQAUgbABQgVAAgOgSgAgSgjQgGALAAAYQAAAYAGAMQAHAMAMAAQAOAAAEgKQAGgKAAgaQAAgagGgKQgFgKgNAAQgNgBgGAKg");
	this.shape.setTransform(341.9,25.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.988)").s().p("AgoA0QgMgRAAgcQAAghAQgVQAQgVAZgBQAXAAAMASQANARAAAcQAAAigPAVQgQAUgbABQgVAAgOgSgAgSgjQgGALAAAYQAAAYAGAMQAGAMANAAQANAAAGgKQAFgKAAgaQAAgagFgKQgGgKgNAAQgNgBgGAKg");
	this.shape_1.setTransform(329.9,25.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.988)").s().p("AAABEIACgzIgCgzIgcAAIAAgOIA5gTIgDBRIADA2g");
	this.shape_2.setTransform(318.8,25.5);

	this._full_name = new cjs.Text("Username", "21px 'Acme'");
	this._full_name.name = "_full_name";
	this._full_name.lineHeight = 29;
	this._full_name.lineWidth = 91;
	this._full_name.alpha = 0.98823529;
	this._full_name.parent = this;
	this._full_name.setTransform(114.3,12.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this._full_name},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this._CASH}]}).wait(1));

	// Layer_4
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.988)").s().p("AZwGAIAAr/IEMAAIAAL/gA97GAIAAr/MA3rAAAIAAL/gA7+FPMAvUAAAIAAo8ItHAAIAFAFQAjAjAAAyIAABLQAAAygjAiQgjAjgyAAIvPAAQgxAAgkgjQgjgiAAgyIAAhLQAAgyAjgjIAFgFIweAAg");
	this.shape_3.setTransform(189.9,37);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer_1
	this.instance = new lib.Bitmap11();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.dashboard, new cjs.Rectangle(-1.6,-1.4,383.2,76.8), null);


(lib.MC_GAMEOVER = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		const holes = [this.hole1, this.hole2, this.hole3, this.hole4, this.hole5, this.hole6, this.hole7, this.hole8];
		holes.forEach(hole => hole.mole.addEventListener('click', bonk));
		
		function bonk(e) {
			console.log(e);
			_tx._TFOUTPUT.text = (Number(_tx._TFOUTPUT.text) + 1) + "";
		}
		let lastHole;
		timeUp = false;
		function randomTime(min, max) {
			return Math.round(Math.random() * (max - min) + min);
		}
		function randomHole(holes) {
			const idx = Math.floor(Math.random() * holes.length);
			const hole = holes[idx];
			if (hole === lastHole) {
				return randomHole(holes);
			}
			lastHole = hole;
			return hole;
		}
		peep();
		function peep() {
			const time = randomTime(500, 1000);
			const hole = randomHole(holes);
			console.log(time, hole.name);
		
			createjs.Tween.get(hole.mole).to({
				y: hole.mole.y - 90
			}, 250, createjs.Ease.easeOutCirc).call(function () {
				setTimeout(asd, time);
				function asd() {
					createjs.Tween.get(hole.mole).to({
						y: hole.mole.y + 90
					}, 250, createjs.Ease.easeOutCirc).call(function () {
						setTimeout(asd, time);
						function asd() {
							if (!timeUp)
								peep();
							else gameEnd();
						}
					});
				}
			});
		}
		function gameEnd() {
			console.log("GAME END");
			createjs.Tween.get(_tx._minigameover).to({
				x: 180
			}, 250, createjs.Ease.easeOutCirc).call(function () {});
		}
		setTimeout(function () {
			timeUp = true;
		}, 20000);
		_tx = this;
		_tx._minigameover.x = 500;
		_tx._TFOUTPUT.text = "0";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// Layer_4
	this._TFOUTPUT = new cjs.Text("", "18px 'Acme'", "#FFFFFF");
	this._TFOUTPUT.name = "_TFOUTPUT";
	this._TFOUTPUT.textAlign = "center";
	this._TFOUTPUT.lineHeight = 25;
	this._TFOUTPUT.lineWidth = 93;
	this._TFOUTPUT.parent = this;
	this._TFOUTPUT.setTransform(111,42.1);

	this.instance = new lib.element_small_weapon2x();
	this.instance.parent = this;
	this.instance.setTransform(7,23);

	this.instance_1 = new lib.element_radio_off2x();
	this.instance_1.parent = this;
	this.instance_1.setTransform(11,28);

	this.instance_2 = new lib.status_bar_money2x();
	this.instance_2.parent = this;
	this.instance_2.setTransform(11,27,0.64,0.64);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance},{t:this._TFOUTPUT}]}).wait(2));

	// mole.svg
	this.hole8 = new lib.dirt();
	this.hole8.name = "hole8";
	this.hole8.parent = this;
	this.hole8.setTransform(87.1,209.7,0.813,0.813,0,0,0,70,27.6);

	this._minigameover = new lib.miniGameover();
	this._minigameover.name = "_minigameover";
	this._minigameover.parent = this;
	this._minigameover.setTransform(509.4,404.6,1,1,0,0,0,111.9,129.9);

	this.hole8_1 = new lib.dirt();
	this.hole8_1.name = "hole8_1";
	this.hole8_1.parent = this;
	this.hole8_1.setTransform(274.9,219.7,0.813,0.813,0,0,0,70,27.6);

	this.hole7 = new lib.dirt();
	this.hole7.name = "hole7";
	this.hole7.parent = this;
	this.hole7.setTransform(155,289.5,0.845,0.845,0,0,0,70,27.6);

	this.hole6 = new lib.dirt();
	this.hole6.name = "hole6";
	this.hole6.parent = this;
	this.hole6.setTransform(286.1,329.2,0.859,0.859,0,0,0,70,27.6);

	this.hole5 = new lib.dirt();
	this.hole5.name = "hole5";
	this.hole5.parent = this;
	this.hole5.setTransform(92.7,380.6,0.845,0.845,0,0,0,70,27.6);

	this.hole4 = new lib.dirt();
	this.hole4.name = "hole4";
	this.hole4.parent = this;
	this.hole4.setTransform(225.9,454.7,0.859,0.859,0,0,0,70,27.5);

	this.hole3 = new lib.dirt();
	this.hole3.name = "hole3";
	this.hole3.parent = this;
	this.hole3.setTransform(70,473.6,0.901,0.901,0,0,0,70,27.6);

	this.hole2 = new lib.dirt();
	this.hole2.name = "hole2";
	this.hole2.parent = this;
	this.hole2.setTransform(282.7,596.4,0.817,0.817,0,0,0,70,27.6);

	this.hole1 = new lib.dirt();
	this.hole1.name = "hole1";
	this.hole1.parent = this;
	this.hole1.setTransform(142.8,573.5,0.831,0.831,0,0,0,70,27.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.hole1},{t:this.hole2},{t:this.hole3},{t:this.hole4},{t:this.hole5},{t:this.hole6},{t:this.hole7},{t:this.hole8_1},{t:this._minigameover},{t:this.hole8}]}).wait(2));

	// Layer_3
	this.instance_3 = new lib.KakaoTalk_20180517_170256231();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-17,-44,0.498,0.498);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8FAE6B").s().p("A/wKfIAA09MA/hAAAIAAU9g");
	this.shape.setTransform(179.9,580.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_3}]}).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.4,-44,644.7,727.9);


// stage content:
(lib.index1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		_t = this;
		var timertext = 1;
		setTimeout(myFunction, 1000);
		function myFunction() {
			timertext--;
			if (timertext >= 0) {
				setTimeout(myFunction, 1000);
			} else {
				//_t.play();
			}
		}
		_t.BTNwelcome.addEventListener("click", function (e) {
			_t.gotoAndStop(1);
		});
		//this.stop();
	}
	this.frame_1 = function() {
		this.stop();
		_t = this;
		function leadboardList() {
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/leadboardList.php',
				type: 'GET',
				crossDomain: true,
				success: function (response) {
					console.log(response);
					var result = JSON.parse(response);
				}
			});
		}
		function contestList() {
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/contestList.php',
				type: 'GET',
				crossDomain: true,
				success: function (response) {
					console.log(response);
					var result = JSON.parse(response);
					console.log(result.contest[0]);
					var result2 = JSON.parse(result.contest);
		
					_t.mc_contests._Cname1.text = result2.contest_name;
					_t.mc_contests._Ctime1.text = result["contest"][1];
				}
			});
		}
		function walletList() {
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/walletList.php',
				type: 'GET',
				crossDomain: true,
				success: function (response) {
					console.log(response);
					var result = JSON.parse(response);
				}
			});
		}
		function forgotPassword() {
			//http://localhost/UAT/platinum_api/forgetPassword.php
			//Parameter-{email}
		}
		function reset() {
			createjs.Tween.get(_t.mc_contests).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_gameover).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_username).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_enterotp).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_wallet).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_signup).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_address).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_game).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
			createjs.Tween.get(_t.mc_signin).to({
				x: 1000,
				y: _t.originY
			}, 100, createjs.Ease.easeOutCirc).call(function () {});
		}
		setTimeout(myFunction, 150);
		function myFunction() {
			document.getElementById("txtuser1").placeholder = "Username";
			document.getElementById("txtemail1").placeholder = "Email";
			document.getElementById("txtphone1").placeholder = "Phone number";
			document.getElementById("txtpass1").placeholder = "Password";
			document.getElementById("txtcpass1").placeholder = "Confirm Password";
			document.getElementById("txtpass1").type = 'password';
			document.getElementById("txtcpass1").type = 'password';
			document.getElementById("txtuser2").placeholder = "Username";
			document.getElementById("txtpass2").placeholder = "Password";
			document.getElementById("txtpass2").type = 'password';
			document.getElementById("txtuser").placeholder = "Username";
			document.getElementById("txtpass").placeholder = "Password";
			document.getElementById("txtpass").type = 'password';
			document.getElementById("txtotp").placeholder = "Enter OTP code";
			_t.originX = _t.mc_signin.x;
			_t.originy = _t.mc_signin.y;
		
			document.getElementById("b17").addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_dashboard).to({
						x: 1000,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
				setTimeout(function () {
					createjs.Tween.get(_t.mc_signin).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			document.getElementById("b16").addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_game).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			_t.mc_dashboard.BTNusername.addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_username).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			_t.mc_wallet.BTNclose.addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_game).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			_t.mc_dashboard.BTNwallet.addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_wallet).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			_t.mc_dashboard.BTNcontest.addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_contests).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			_t.mc_contests.BTNclose.addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_game).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			document.getElementById("b12").addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_dashboard).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
					createjs.Tween.get(_t.mc_game).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			
			document.getElementById("b11").addEventListener("click", function (e) {
				_t.mc_gameover.play();
			});
			document.getElementById("b10").addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_dashboard).to({
						x: 1000,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
					createjs.Tween.get(_t.mc_gameover).to({
						x: _t.originX - 32,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {_t.mc_gameover.play();});
					
				}, 120);
			});
			document.getElementById("b9").addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_contests).to({
						x: _t.originX,
						y: _t.originY
					}, 500, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			_t.mc_address.BTNplay.addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_dashboard).to({
						x: _t.originX,
						y: _t.originY
					}, 100, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
				setTimeout(function () {
					createjs.Tween.get(_t.mc_game).to({
						x: _t.originX,
						y: _t.originY
					}, 100, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			document.getElementById("b6").addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_signin).to({
						x: _t.originX,
						y: _t.originY
					}, 100, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			document.getElementById("b4").addEventListener("click", function (e) {
				reset();
				setTimeout(function () {
					createjs.Tween.get(_t.mc_signup).to({
						x: _t.originX,
						y: _t.originY
					}, 100, createjs.Ease.easeOutCirc).call(function () {});
				}, 120);
			});
			document.getElementById("b1").addEventListener("click", function (e) {
				login();
			});
			document.getElementById("b5").addEventListener("click", function (e) {
				signup();
			});
			document.getElementById("b15").addEventListener("click", function (e) {
				changepassword();
			});
			document.getElementById("b7").addEventListener("click", function (e) {
				verifyOTP();
			});
		}
		function changepassword() {
			var txtuser = document.getElementById("txtuser2").value.trim();
			var txtpass = document.getElementById("txtpass2").value.trim();
			var data = {
				email: txtuser,
				newpassword: txtpass
			};
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/changePassword.php',
				type: 'POST',
				data: data,
				crossDomain: true,
				success: function (response) {
					console.log(response);
					var result = JSON.parse(response);
					//"Email does not exist!"
					alert(result.message);
				}
			});
		}
		function updateuserdetails() {
			/*Parameter-
		{
		full_name
		user_image
		mob_no
		gender
		email
		country
		state
		city
		pincode
		dob
		user_id
		}*/
			var txtuser = document.getElementById("txtuser2").value.trim();
			var txtpass = document.getElementById("txtpass2").value.trim();
			var data = {
				full_name: txtuser,
				user_image: txtuser,
				mob_no: txtuser,
				gender: txtuser,
				email: txtpass,
				country: txtuser,
				state: txtuser,
				city: txtuser,
				pincode: txtuser,
				dob: txtuser,
				user_id: txtuser
			};
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/userUpdate.php',
				type: 'POST',
				data: data,
				crossDomain: true,
				success: function (response) {
					console.log(response);
					if (response == "User Update Successfully") {
						alert("User Update Successfully!");
					} else {
						alert("User Update Failed!");
					}
				}
			});
		}
		function login() {
			var txtuser = document.getElementById("txtuser").value.trim();
			var txtpass = document.getElementById("txtpass").value.trim();
			var data = {
				username: txtuser,
				password: txtpass
			};
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/userLogin.php',
				type: 'POST',
				data: data,
				crossDomain: true,
				success: function (response) {
					console.log(response);
					var result = JSON.parse(response);
					_t.mc_address._email_id.text = result.email_id;
					_t.mc_address._mob_no.text = result.mob_no;
					_t.mc_address._full_name.text = result.full_name;
					_t.mc_username._full_name.text = result.full_name;
					_t.mc_dashboard._full_name.text = result.full_name;
					document.getElementById("txtuser2").value = result.email_id;
					if (!isNaN(result.user_id)) {
						console.log(result);
						walletList();
						contestList();
						leadboardList();
						reset();
						setTimeout(function () {
							createjs.Tween.get(_t.mc_enterotp).to({
								x: _t.originX,
								y: _t.originY
							}, 100, createjs.Ease.easeOutCirc).call(function () {});
						}, 140);
					} else {
						alert("Login Failed!");
					}
				}
			});
		}
		function signup() {
			/*	
			full_name
			password
			mob_no
			email
			*/
			var txtuser = document.getElementById("txtuser1").value.trim();
			var txtpass = document.getElementById("txtpass1").value.trim();
			var txtcpass = document.getElementById("txtcpass1").value.trim();
			var txtphone = document.getElementById("txtphone1").value.trim();
			var mob_no = document.getElementById("txtuser").value.trim();
			var email = document.getElementById("txtemail1").value.trim();
			if (txtcpass != txtpass) {
				alert("Password missmatch!");
				return;
			}
			if (txtpass == "") {
				alert("Password cannot be empty!");
				return;
			}
			if (email.indexOf(".") == -1 || email.indexOf("@") == -1) {
				alert("Invalid Email!");
				return;
			}
			function telephoneCheck(str) {
				var isphone = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/.test(str);
				return isphone;
			}
			if (!telephoneCheck(txtphone)) {
				alert("Invalid Phone number!");
				return;
			}
			var data = {
				full_name: txtuser,
				password: txtpass,
				mob_no: txtphone,
				email: email
			};
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/userRegistration.php',
				type: 'POST',
				data: data,
				crossDomain: true,
				success: function (response) {
					console.log(response);
					/*
						{"status":1,"message":"Mobile Number or Email is Already Registered","response":"1"}
						{"status":1,"message":"success","response":"1"}*/
					var result = JSON.parse(response);
					if (result.message == "success") {
						reset();
					} else {
						alert("Registration Failed! " + result.message + "!");
					}
				}
			});
		}
		function verifyOTP() {
			/*http://localhost/UAT/platinum_api/varifyOtp.php
		Parameter-
		{
			otp
			email
		}*/
			var txtotp = document.getElementById("txtotp").value.trim();
			var data = {
				otp: txtotp,
				email: "0"
			};
			$.ajax({
				url: 'http://localhost/UAT/platinum_api/varifyOtp.php',
				type: 'POST',
				data: data,
				crossDomain: true,
				success: function (response) {
					console.log(response);
					var result = JSON.parse(response);
					if (result.message == "Invalid OTP") {
						reset();
						setTimeout(function () {
							createjs.Tween.get(_t.mc_address).to({
								x: _t.originX,
								y: _t.originY
							}, 100, createjs.Ease.easeOutCirc).call(function () {});
						}, 120);
					} else {
						alert("Login Failed!");
					}
				}
			});
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// Layer_2
	this.instance = new lib.an_CSS({'id': '', 'href':'assets/textfields.css'});

	this.instance.setTransform(2218.6,-206,1,1,0,0,0,50,11);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2));

	// Layer_3
	this.mc_dashboard = new lib.dashboard();
	this.mc_dashboard.name = "mc_dashboard";
	this.mc_dashboard.parent = this;
	this.mc_dashboard.setTransform(1649.2,66,1,1,0,0,0,192,66);

	this.timeline.addTween(cjs.Tween.get(this.mc_dashboard).wait(2));

	// Layer_1
	this.BTNwelcome = new lib.transparent();
	this.BTNwelcome.name = "BTNwelcome";
	this.BTNwelcome.parent = this;
	this.BTNwelcome.setTransform(204.1,582.1,3.553,1.318,0,0,0,0.4,0.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26264B").s().p("Ag1AFQgBg2gHgsIAygKQADAfABAdQAkg6AiAAIgFBAQgYAAgOAEQgOAEgNANIAAAPIAFBfIg3AKg");
	this.shape.setTransform(275.4,448.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26264B").s().p("Ag6BTQgVgaAAgnQAAgxAdgmQAdgmAmAAQAeAAARARQAQARAAAeQAAASgGAXIgIAIIhmAJQAHAuAkAAQAMAAANgGQANgGAHgGIAIgGIATAXQgFAHgIAJQgJAJgJAGQgIAGgPAGQgOAFgPAAQghAAgVgZgAgWgyQgNAPgCAWIBFgJIABgPQAAgcgaAAQgRAAgMAPg");
	this.shape_1.setTransform(258,449);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#26264B").s().p("AgXBwQgOgPAAgYIACguIAAhAIgcAAIAFghIAXAAIAAgtIA2gLIgEA4IAxAAIgEAhIgvAAIgDBVQABATAEAHQADAGAKAAQAKAAAQgFIADAdQgcAXgUAAQgUAAgMgPg");
	this.shape_2.setTransform(241.9,446.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#26264B").s().p("AAkAAQAAgcgEgJQgFgKgTAAQgQAAgXAPIAAAeIAEBfIg3AKIAFhjQgBg2gIgsIAygKIAEAuQAGgIAIgIQAHgIAQgKQARgKATAAQASAAANAOQANAPAAAZIgCAuIAEBhIg3AKg");
	this.shape_3.setTransform(223.6,449);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#26264B").s().p("AAcA+QgkAqgbAAQgWAAgOgPQgNgOAAgYIACgvIgEhiIA3gKIgGB8QAAARAHAIQAGAJAOAAQANAAAYgOIAAgZIgEhrIA3gKIgFBmIABAhQAAAbANAaIgqATQgOgbgDgQg");
	this.shape_4.setTransform(202.9,448.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#26264B").s().p("AAzCOIAEhjIgBgWIhtAAIAAAPIAFBqIg6AAIAFhjIgFiuIA7gKIgFB3IBrAAIgChtIA5gKIgFCxIAEBqg");
	this.shape_5.setTransform(177.9,445.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#26264B").s().p("Ag6BTQgVgaAAgnQAAgxAdgmQAdgmAmAAQAeAAARARQAQARAAAeQAAASgGAXIgIAIIhmAJQAHAuAkAAQAMAAANgGQANgGAHgGIAIgGIATAXQgFAHgIAJQgJAJgJAGQgIAGgPAGQgOAFgPAAQghAAgVgZgAgWgyQgNAPgCAWIBFgJIABgPQAAgcgaAAQgRAAgMAPg");
	this.shape_6.setTransform(148.2,449);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#26264B").s().p("AgWA7IgFjOIA3gLIgFDSIAEBhIg2AKg");
	this.shape_7.setTransform(134,443.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#26264B").s().p("AhABSQgWgZAAgnQAAg0AcgkQAcglAlAAQAmAAAVAZQAVAYAAApQAAA1gbAkQgbAkgoAAQgiAAgXgagAgoAAQAAAdAMAQQAMAQASAAQATAAAKgPQAKgOAAgdQAAg6goAAQgpAAAAA3g");
	this.shape_8.setTransform(118.9,449);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#26264B").s().p("ABJCPIAChrIgFhrIgyCHIgxACIguiJIgDBrIACBrIgyAAIAFhkIADivIBBgKIAzCdIAEAAIA8ibIA5ACIABCvIAIBqg");
	this.shape_9.setTransform(93.8,445);

	this.instance_1 = new lib.AnimateCC();
	this.instance_1.parent = this;
	this.instance_1.setTransform(180.7,260.9);

	this.instance_2 = new lib.Bitmap14();
	this.instance_2.parent = this;
	this.instance_2.setTransform(158,566);

	this.mc_username = new lib.MC_USERNAME();
	this.mc_username.name = "mc_username";
	this.mc_username.parent = this;
	this.mc_username.setTransform(2253.4,312.6,1,1,0,0,0,190.5,312.6);

	this.mc_wallet = new lib.MC_WALLET();
	this.mc_wallet.name = "mc_wallet";
	this.mc_wallet.parent = this;
	this.mc_wallet.setTransform(1581.7,218.5,1,1,0,0,0,172.8,218.5);

	this.mc_contests = new lib.MC_CONTESTS();
	this.mc_contests.name = "mc_contests";
	this.mc_contests.parent = this;
	this.mc_contests.setTransform(2887.5,293.9,1,1,0,0,0,185.8,311.6);

	this.mc_gameover = new lib.MC_GAMEOVER();
	this.mc_gameover.name = "mc_gameover";
	this.mc_gameover.parent = this;
	this.mc_gameover.setTransform(549.4,264.8,1,1,0,0,0,143.5,264.8);

	this.mc_game = new lib.MC_GAME();
	this.mc_game.name = "mc_game";
	this.mc_game.parent = this;
	this.mc_game.setTransform(540.5,299.2,1,1,0,0,0,172.5,299.2);

	this.mc_address = new lib.MC_ADDRESS();
	this.mc_address.name = "mc_address";
	this.mc_address.parent = this;
	this.mc_address.setTransform(890.7,306.7,1,1,0,0,0,172.7,306.7);

	this.mc_enterotp = new lib.MC_ENTEROTP();
	this.mc_enterotp.name = "mc_enterotp";
	this.mc_enterotp.parent = this;
	this.mc_enterotp.setTransform(1960.4,315.5,1,1,0,0,0,170.6,159);

	this.mc_signup = new lib.MC_SIGNUP();
	this.mc_signup.name = "mc_signup";
	this.mc_signup.parent = this;
	this.mc_signup.setTransform(1218.1,313.2,1,1,0,0,0,170.6,313.2);

	this.mc_signin = new lib.MC_SIGNIN();
	this.mc_signin.name = "mc_signin";
	this.mc_signin.parent = this;
	this.mc_signin.setTransform(169.6,310.6,1,1,0,0,0,172.8,315.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#26264B").ss(1,1,1).p("A8JAAMA4TAAA");
	this.shape_10.setTransform(179.7,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.BTNwelcome}]}).to({state:[{t:this.shape_10},{t:this.mc_signin},{t:this.mc_signup},{t:this.mc_enterotp},{t:this.mc_address},{t:this.mc_game},{t:this.mc_gameover},{t:this.mc_contests},{t:this.mc_wallet},{t:this.mc_username}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(207.9,100.5,2240.7,853.5);
// library properties:
lib.properties = {
	id: 'BDB8DD5CD7232943A2058EB62E43F25C',
	width: 359,
	height: 636,
	fps: 24,
	color: "#F5F5F5",
	opacity: 1.00,
	manifest: [
		{src:"images/index_atlas_.png?1531018444219", id:"index_atlas_"},
		{src:"https://code.jquery.com/jquery-2.2.4.min.js?1531018444407", id:"lib/jquery-2.2.4.min.js"},
		{src:"components/sdk/anwidget.js?1531018444407", id:"sdk/anwidget.js"},
		{src:"components/ui/src/textinput.js?1531018444407", id:"an.TextInput"},
		{src:"components/ui/src/css.js?1531018444407", id:"an.CSS"},
		{src:"components/ui/src/button.js?1531018444407", id:"an.Button"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['BDB8DD5CD7232943A2058EB62E43F25C'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}

function _updateVisibility(evt) {
	if((this.getStage() == null || this._off || this._lastAddedFrame != this.parent.currentFrame) && this._element) {
		this._element.detach();
		stage.removeEventListener('drawstart', this._updateVisibilityCbk);
		this._updateVisibilityCbk = false;
	}
}
function _handleDrawEnd(evt) {
	var props = this.getConcatenatedDisplayProps(this._props), mat = props.matrix;
	var tx1 = mat.decompose(); var sx = tx1.scaleX; var sy = tx1.scaleY;
	var dp = window.devicePixelRatio || 1; var w = this.nominalBounds.width * sx; var h = this.nominalBounds.height * sy;
	mat.tx/=dp;mat.ty/=dp; mat.a/=(dp*sx);mat.b/=(dp*sx);mat.c/=(dp*sy);mat.d/=(dp*sy);
	this._element.setProperty('transform-origin', this.regX + 'px ' + this.regY + 'px');
	var x = (mat.tx + this.regX*mat.a + this.regY*mat.c - this.regX);
	var y = (mat.ty + this.regX*mat.b + this.regY*mat.d - this.regY);
	var tx = 'matrix(' + mat.a + ',' + mat.b + ',' + mat.c + ',' + mat.d + ',' + x + ',' + y + ')';
	this._element.setProperty('transform', tx);
	this._element.setProperty('width', w);
	this._element.setProperty('height', h);
	this._element.update();
}

function _tick(evt) {
	var stage = this.getStage();
	stage&&stage.on('drawend', this._handleDrawEnd, this, true);
	if(!this._updateVisibilityCbk) {
		this._updateVisibilityCbk = stage.on('drawstart', this._updateVisibility, this, false);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;